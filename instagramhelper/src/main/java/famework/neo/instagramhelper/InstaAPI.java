package famework.neo.instagramhelper;

import famework.neo.instagramhelper.models.AccessTokenModel;
import famework.neo.instagramhelper.models.RecentMedia;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by webwerks on 23/05/17.
 */

public interface InstaAPI {

        String BASE_URL="https://api.instagram.com/";

       String AUTH_URL = "oauth/authorize/";
       String TOKEN_URL = "oauth/access_token/";
       String API_URL = "v1/";


    @GET(AUTH_URL)
    Call<Object> authorize(@Query("client_id")String clientId,
                   @Query("redirect_uri")String redirect,
                   @Query("response_type")String type);
    @GET(TOKEN_URL)
    Call<Object> token(@Query("client_id")String clientId,
               @Query("client_secret")String clientSecret,
               @Query("redirect_uri")String redirect
               );
    @FormUrlEncoded
    @POST(TOKEN_URL)
    Call<AccessTokenModel> obtainToken(@Field("client_id") String clientId,
                                       @Field("client_secret")String clientSecret,
                                       @Field("grant_type")String grantType,
                                       @Field("redirect_uri")String redirectUri,
                                       @Field("code")String code
                     );

    @GET(API_URL+"users/self/media/recent/")
    Call<RecentMedia> getRecentMedia(@Query("access_token")String accessToken);




}
