package famework.neo.instagramhelper.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ganesh.K on 13/7/17.
 */

public class AccessTokenModel {


    @SerializedName("access_token")
    @Expose

    private  String accessToken;

    private User user;


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


}
