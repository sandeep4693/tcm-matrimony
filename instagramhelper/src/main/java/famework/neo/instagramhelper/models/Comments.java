package famework.neo.instagramhelper.models;

/**
 * Created by webwerks on 26/05/17.
 */

public class Comments
{
    private String count;

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [count = "+count+"]";
    }
}
