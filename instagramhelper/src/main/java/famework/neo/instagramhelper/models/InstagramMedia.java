package famework.neo.instagramhelper.models;

/**
 * Created by webwerks on 26/05/17.
 */

public class InstagramMedia
{
    private String[] tags;

    private Object location;

    private String link;

    private Caption caption;

    private String user_has_liked;

    private String type;

    private String id;

    private Likes likes;

    private Images images;

    private String[] users_in_photo;

    private String created_time;

    private User user;

    private Object attribution;

    private String filter;

    private Comments comments;

    public String[] getTags ()
    {
        return tags;
    }

    public void setTags (String[] tags)
    {
        this.tags = tags;
    }

    public Object getLocation ()
{
    return location;
}

    public void setLocation (Object location)
    {
        this.location = location;
    }

    public String getLink ()
    {
        return link;
    }

    public void setLink (String link)
    {
        this.link = link;
    }

    public Caption getCaption ()
    {
        return caption;
    }

    public void setCaption (Caption caption)
    {
        this.caption = caption;
    }

    public String getUser_has_liked ()
    {
        return user_has_liked;
    }

    public void setUser_has_liked (String user_has_liked)
    {
        this.user_has_liked = user_has_liked;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public Likes getLikes ()
    {
        return likes;
    }

    public void setLikes (Likes likes)
    {
        this.likes = likes;
    }

    public Images getImages ()
    {
        return images;
    }

    public void setImages (Images images)
    {
        this.images = images;
    }

    public String[] getUsers_in_photo ()
    {
        return users_in_photo;
    }

    public void setUsers_in_photo (String[] users_in_photo)
    {
        this.users_in_photo = users_in_photo;
    }

    public String getCreated_time ()
    {
        return created_time;
    }

    public void setCreated_time (String created_time)
    {
        this.created_time = created_time;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    public Object getAttribution ()
{
    return attribution;
}

    public void setAttribution (Object attribution)
    {
        this.attribution = attribution;
    }

    public String getFilter ()
    {
        return filter;
    }

    public void setFilter (String filter)
    {
        this.filter = filter;
    }

    public Comments getComments ()
    {
        return comments;
    }

    public void setComments (Comments comments)
    {
        this.comments = comments;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tags = "+tags+", location = "+location+", link = "+link+", caption = "+caption+", user_has_liked = "+user_has_liked+", type = "+type+", id = "+id+", likes = "+likes+", images = "+images+", users_in_photo = "+users_in_photo+", created_time = "+created_time+", user = "+user+", attribution = "+attribution+", filter = "+filter+", comments = "+comments+"]";
    }
}

