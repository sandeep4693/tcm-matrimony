package famework.neo.instagramhelper;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import famework.neo.instagramhelper.models.AccessTokenModel;
import famework.neo.instagramhelper.models.InstagramMedia;
import famework.neo.instagramhelper.models.RecentMedia;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by webwerks on 24/05/17.
 */

public class InstagramHelper implements InstagramDialog.OAuthDialogListener {

    private static InstagramHelper instance;
    private InstagramDialog mDialog;
    private InstagramSession mSession;
    private String mAccessToken;
    String clientId, clientSecret, callbackUrl;
    Context context;
    boolean initialized = false;
    public static final String TAG = "InstagramHelper";
    private InstaAPI api;
    private Client listener;

    //initialize dependencies.
    private InstagramHelper(Context context) {

        this.context = context;
        mSession = new InstagramSession(context);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        api = new Retrofit.Builder().baseUrl(InstaAPI.BASE_URL)
                .client(new OkHttpClient.Builder().addInterceptor(interceptor).build())
                .addConverterFactory(GsonConverterFactory.create()).build()
                .create(InstaAPI.class);
    }

    public static synchronized InstagramHelper getInstance(Context context) {
        if (instance == null) {
            instance = new InstagramHelper(context);
        } else if (!instance.context.equals(context)) {
            instance = null;
            instance = new InstagramHelper(context);
        }
        return instance;
    }

    public InstagramHelper setListener(Client listener) {
        this.listener = listener;
        return this;
    }

    public InstagramHelper initialize(String clientId, String clientSecret, String callbackUrl) {

        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.callbackUrl = callbackUrl;

        mAccessToken = mSession.getAccessToken();

        mDialog = new InstagramDialog(context, InstaAPI.BASE_URL + InstaAPI.AUTH_URL + "?client_id="
                + clientId + "&redirect_uri=" + callbackUrl + "&response_type=code", this);


        initialized = true;
        return this;
    }

    public boolean hasAccessToken() {
        return (mSession.getAccessToken() != null);
    }

    public void authorize() {
        mDialog.show();
    }


    private void getAccessToken(String authCode) {

        api.obtainToken(clientId, clientSecret, "authorization_code", callbackUrl, authCode).enqueue(new Callback<AccessTokenModel>() {
            @Override
            public void onResponse(Call<AccessTokenModel> call, Response<AccessTokenModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.e(TAG, "getAccessToken Response=" + response.body());
                        String accessToken = response.body().getAccessToken();
                        mSession.storeAccessToken(accessToken);
                        listener.onSuccess();
                    }
                } else {
                    listener.onFail("Loging failed");
                }

            }

            @Override
            public void onFailure(Call<AccessTokenModel> call, Throwable t) {

            }
        });
    }


    public void getImages() {

        api.getRecentMedia(mSession.getAccessToken()).enqueue(new Callback<RecentMedia>() {
            @Override
            public void onResponse(Call<RecentMedia> call, retrofit2.Response<RecentMedia> response) {
                Log.d(TAG, response + "");
                listener.onSuccess();
                List<InstagramMedia> recentMedia = response.body().getData();
                listener.onInstaImages(recentMedia);
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                listener.onFail("" + t.toString());
                Log.e(TAG, t.toString());
            }


        });

    }

    @Override
    public void onComplete(String code) {
        getAccessToken(code);
    }

    @Override
    public void onError(String error) {
        Log.d(TAG, "Auth ERROR- " + error);
    }

    public interface Client {
        void onSuccess();

        void onFail(String error);

        void onInstaImages(List<InstagramMedia> recentMedia);
    }


}
