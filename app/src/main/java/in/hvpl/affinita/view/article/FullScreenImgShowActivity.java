package in.hvpl.affinita.view.article;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.ActivityFullScreenImgShowBinding;
import in.hvpl.affinita.model.ArticleModel;


public class FullScreenImgShowActivity extends BaseActivity<ActivityFullScreenImgShowBinding> {
    private static final String TAG = FullScreenImgShowActivity.class.getSimpleName();
    private boolean flag;
    private String url;
    private ArticleModel articleModel;

    public static void FullScreencall(Activity activity) {
        View decorView = activity.getWindow().getDecorView();

        int uiOptions = 0;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        } else {

        }
        decorView.setSystemUiVisibility(uiOptions);

    }

    @Override
    public int getContentViewResource() {
        return R.layout.activity_full_screen_img_show;
    }

    @Override
    public void initUI() {
        mBinding.setIsBoy(App.getInstance().isBoy());
        articleModel = (ArticleModel) getIntent().getSerializableExtra(Constants.IntentExtras.ARTICLE_MODEL);
        url = articleModel.getArticleImgUrl();
        ((ProgressBar) findViewById(R.id.progressbar)).getIndeterminateDrawable().setColorFilter(0xFFFF0000, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public String getScreenName() {
        return TAG;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    protected void onResume() {
        super.onResume();
        FullScreencall(this);
        if (flag) {
            Glide.with(FullScreenImgShowActivity.this).load((getIntent().getStringExtra("image"))).centerCrop().placeholder(R.drawable.shape_background_article).diskCacheStrategy(DiskCacheStrategy.ALL).into(((ImageView) findViewById(R.id.dialog_img)));
            ((ProgressBar) findViewById(R.id.progressbar)).setVisibility(View.GONE);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (flag) {
                    onBackPressed();
                } else {
                    flag = true;
                    Intent i;
                    i = new Intent(FullScreenImgShowActivity.this, ArticleDetailActivity.class);
                    i.putExtra(Constants.IntentExtras.ARTICLE_MODEL, articleModel);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(i);
                }
            }
        }, 500);
    }

    @Override
    public void onBackPressed() {
        if (flag) {
            super.onBackPressed();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            } else {

            }
        } else {

        }
    }

}
