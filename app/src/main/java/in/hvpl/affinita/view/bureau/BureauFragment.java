package in.hvpl.affinita.view.bureau;

import android.app.ProgressDialog;
import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.HashMap;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.FragmentBureauBinding;

import static in.hvpl.affinita.common.utils.SharedPreferencesHelper.ADVISORY_URL;

public class BureauFragment extends BaseFragment<FragmentBureauBinding> {

    private static final String TAG = BureauFragment.class.getSimpleName();
    ProgressDialog pd;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_bureau;
    }

    @Override
    public void initUI() {

        setToolbarTitle(getString(R.string.activity_title_bureau));
        setToolBarVisibility(true);
        if (Tools.isConnected(getContext())) {
            pd = new ProgressDialog(getContext());
            pd.setMessage(getActivity().getString(R.string.dialog_msg_please_wait));
            pd.show();
            String url = SharedPreferencesHelper.getBureauUrl(getActivity(), ADVISORY_URL)/*Constants.BUREAU_URL*/;
            WebView webView = mBinding.webViewBureau;
            final WebSettings settings = webView.getSettings();

            settings.setJavaScriptEnabled(true);
            settings.setDisplayZoomControls(false);
            settings.setAppCacheEnabled(true);
            settings.setLoadsImagesAutomatically(true);
            settings.setBuiltInZoomControls(false);
            settings.setPluginState(WebSettings.PluginState.ON);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

            webView.setWebViewClient(new BureauWebViewClients());

            HashMap<String, String> headerMap = new HashMap<>();
            //put all headers in this header map
            headerMap.put("Content-Type", "application/json");
            headerMap.put("api-version", "v1");

            webView.loadUrl(url, headerMap);

        } else {
            Tools.showNoInternetMsg(getContext());
        }
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.BUREAU_DETAILS;
    }

    @Override
    public String getClassName() {
        return TAG;
    }


    public class BureauWebViewClients extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            if (!pd.isShowing()) {
                pd.show();
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            System.out.println("on finish");
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }
}
