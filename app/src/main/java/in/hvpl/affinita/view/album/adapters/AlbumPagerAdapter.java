package in.hvpl.affinita.view.album.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import in.hvpl.affinita.view.album.fragments.FBGalleryFragment;
import in.hvpl.affinita.view.album.fragments.GalleryFragment;
import in.hvpl.affinita.view.album.fragments.InstagramGalleryFragment;

/**
 * Created by Ganesh.K on 10/7/17.
 */

public class AlbumPagerAdapter extends FragmentPagerAdapter {

    public AlbumPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new GalleryFragment();

            case 1:
                return new FBGalleryFragment();
            case 2:
                return  new InstagramGalleryFragment();

                default:
                    return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
