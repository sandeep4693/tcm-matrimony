package in.hvpl.affinita.view.album;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.GradientDrawable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.FacebookHelper;
import in.hvpl.affinita.common.widget.EqualSpacingItemDecoration;
import in.hvpl.affinita.databinding.ActivityAlbumBinding;
import in.hvpl.affinita.view.album.adapters.AlbumCreateAdapter;
import in.hvpl.affinita.view.album.adapters.AlbumPagerAdapter;
import in.hvpl.affinita.viewmodel.AlbumCreateViewModel;

public class AlbumCreateActivity extends BaseActivity<ActivityAlbumBinding> {

    private static final String TAG = AlbumCreateActivity.class.getSimpleName();

    private AlbumCreateViewModel viewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_album;
    }

    @Override
    public void initUI() {
        setToolBar(mBinding.toolBar);
        setToolbarTitle(getString(R.string.activity_title_album));
        viewModel = ViewModelProviders.of(this).get(AlbumCreateViewModel.class);
        mBinding.setViewModel(viewModel);
        bindSelectedImageList();
        setViewPagerWithTab();
        scrollUpdateImage();

    }

    private void scrollUpdateImage() {
        viewModel.getCurrentAddedImage().observe(this, position -> {
            mBinding.rvAlbum.smoothScrollToPosition(position);
            mBinding.rvAlbum.getAdapter().notifyDataSetChanged();
        });
    }

    private void bindSelectedImageList() {
        /**
         * this code is for carousel effect to selected photo list
         *
         */
//        CarouselLayoutManager layoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL,true);
//        layoutManager.setPostLayoutListener(new CarouselZoomPostLayoutListener());
//        mBinding.rvAlbum.addOnScrollListener(new CenterScrollListener());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mBinding.rvAlbum.setLayoutManager(layoutManager);
        mBinding.rvAlbum.setHasFixedSize(true);
        mBinding.rvAlbum.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
        mBinding.rvAlbum.setAdapter(new AlbumCreateAdapter(viewModel.selectedAlbumList, viewModel));

    }

    private void setViewPagerWithTab() {
        LinearLayout linearLayout = (LinearLayout) mBinding.tabs.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(ContextCompat.getColor(this, R.color.divider_color));
        drawable.setSize(2, 2);
        /*linearLayout.setDividerPadding(10);*/
        linearLayout.setDividerDrawable(drawable);
        mBinding.viewPager.setAdapter(new AlbumPagerAdapter(getSupportFragmentManager()));
        mBinding.tabs.setupWithViewPager(mBinding.viewPager);
        mBinding.tabs.getTabAt(0).setIcon(R.drawable.gallery);
        mBinding.tabs.getTabAt(1).setIcon(R.drawable.fb);
        mBinding.tabs.getTabAt(2).setIcon(R.drawable.insta);
        mBinding.viewPager.setOffscreenPageLimit(1);


        int tabIconColor = ContextCompat.getColor(AlbumCreateActivity.this, App.getInstance().isBoy() ? R.color.boy_big_btn_bg : R.color.girl_big_btn_bg);
        mBinding.tabs.getTabAt(0).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        int tabIconColor1 = ContextCompat.getColor(AlbumCreateActivity.this, R.color.edit_bg);
        mBinding.tabs.getTabAt(2).getIcon().setColorFilter(tabIconColor1, PorterDuff.Mode.SRC_IN);

        mBinding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(AlbumCreateActivity.this, App.getInstance().isBoy() ? R.color.boy_big_btn_bg : R.color.girl_big_btn_bg);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int tabIconColor = ContextCompat.getColor(AlbumCreateActivity.this, R.color.edit_bg);
                tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.CREATE_ALBUM;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FacebookHelper.getInstance().getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }

    /*    @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.my_album_menu, menu);
            return true;
        }*/
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_album_menu, menu);
        MenuItem settingsMenuItem = menu.findItem(R.id.action_done);
        SpannableString s = new SpannableString(settingsMenuItem.getTitle());
        s.setSpan(new ForegroundColorSpan(Color.RED), 0, s.length(), 0);
        settingsMenuItem.setTitle(s);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        if (id == R.id.action_done) {
        viewModel.uploadAlbumPhoto(this);
//        }

        return super.onOptionsItemSelected(item);
    }

}
