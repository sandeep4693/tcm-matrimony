package in.hvpl.affinita.view.editprofile.fragment;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.FragmentEditProfileMenuBinding;
import in.hvpl.affinita.model.ImageIconWithTitleModel;
import in.hvpl.affinita.view.editprofile.EditProfileAdapter;
import in.hvpl.affinita.view.widget.ItemClickSupport;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;
import neo.architecture.lifecycle.BaseLifecycleActivity;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class EditProfileMenuFragment extends BaseFragment<FragmentEditProfileMenuBinding> {

    private static final String TAG = EditProfileMenuFragment.class.getSimpleName();
    private List<ImageIconWithTitleModel> imageIconWithTitleModels;
    private EditProfileAdapter editProfileAdapter;
    private EditProfileViewModel viewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_edit_profile_menu;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_profile_info));
        viewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setViewModel(viewModel);
        bindList();
    }

    @Override
    public void onResume() {
        super.onResume();
        updatePendingTopicText();
    }

    private void updatePendingTopicText() {
        Activity context = getActivity();
        viewModel.isPendingTopicShow.set(false);
        if (!SharedPreferencesHelper.isBasicInfoUpdated(context)) {
            viewModel.isPendingTopicShow.set(true);
            viewModel.pendingTopic.set(getString(R.string.basic_information));
        } else if (!SharedPreferencesHelper.isAboutMeUpdated(context)) {
            viewModel.isPendingTopicShow.set(true);
            viewModel.pendingTopic.set(getString(R.string.about_me));
        } else if (!SharedPreferencesHelper.isEducationUpdated(context)) {
            viewModel.isPendingTopicShow.set(true);
            viewModel.pendingTopic.set(getString(R.string.education));
        } else if (!SharedPreferencesHelper.isCareerUpdated(context)) {
            viewModel.isPendingTopicShow.set(true);
            viewModel.pendingTopic.set(getString(R.string.career));
        } else if (!SharedPreferencesHelper.isBackgroundUpdated(context)) {
            viewModel.isPendingTopicShow.set(true);
            viewModel.pendingTopic.set(getString(R.string.background));
        } else if (!SharedPreferencesHelper.isFamilyDetailsUpdated(context)) {
            viewModel.isPendingTopicShow.set(true);
            viewModel.pendingTopic.set(getString(R.string.family_detail));
        } else if (!SharedPreferencesHelper.isPartnerPreferenceUpdated(context)) {
            viewModel.isPendingTopicShow.set(true);
            viewModel.pendingTopic.set(getString(R.string.partner_preferences));
        }

    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    private void bindList() {
        prepareProfileCategories();
        editProfileAdapter = new EditProfileAdapter(imageIconWithTitleModels);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mBinding.editProfileRv.getContext(),
                linearLayoutManager.getOrientation());
        mBinding.editProfileRv.addItemDecoration(dividerItemDecoration);
        mBinding.editProfileRv.setLayoutManager(linearLayoutManager);
        mBinding.editProfileRv.setAdapter(editProfileAdapter);

        ItemClickSupport.addTo(mBinding.editProfileRv).setOnItemClickListener((recyclerView, position, v) -> {
            String name = imageIconWithTitleModels.get(position).getTitle();
            BaseLifecycleActivity activity = (BaseLifecycleActivity) v.getContext();
            viewModel.onCategoryClick(activity, name, true);
        });

    }

    private void prepareProfileCategories() {
        imageIconWithTitleModels = new ArrayList<>();
        imageIconWithTitleModels.add(new ImageIconWithTitleModel(App.getInstance().isBoy() ? Constants.Icon.IC_MALE : Constants.Icon.IC_FEMALE, getString(R.string.basic_information)));
        imageIconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_ABOUT_ME, getString(R.string.about_me)));
        imageIconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_EDUCATION, getString(R.string.education)));
        imageIconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_CAREER, getString(R.string.career)));
        imageIconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_BACKGROUND, getString(R.string.background)));
        imageIconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_ASTRO_DETAILS, getString(R.string.astro_details)));
        imageIconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_FAMILY, getString(R.string.family_detail)));
        imageIconWithTitleModels.add(new ImageIconWithTitleModel(App.getInstance().isBoy() ? Constants.Icon.IC_FEMALE : Constants.Icon.IC_MALE, getString(R.string.partner_preferences)));
    }

}
