package in.hvpl.affinita.view.viewprofile.adapters;

import android.content.Intent;
import android.databinding.ObservableArrayList;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.RowPhotoBinding;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.view.viewprofile.ViewPhotoSlideActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;

/**
 * Created by Ganesh.K on 19/7/17.
 */

public class PhotoAdapter extends PagerAdapter {
    private boolean isClickEnable;
    private ObservableArrayList<AlbumInformationModel> albumInformationModels;
    public PhotoAdapter(ObservableArrayList<AlbumInformationModel> albumInformationModels) {
        this.albumInformationModels = albumInformationModels;
    }

    @Override
    public int getCount() {
        return albumInformationModels.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RowPhotoBinding binding = RowPhotoBinding.inflate(LayoutInflater.from(container.getContext()), container, true);
        binding.setModel(albumInformationModels.get(position));

            binding.viewPhotoLayout.setOnClickListener(v -> {
                ArrayList<AlbumInformationModel> tableData = albumInformationModels;

                BaseLifecycleActivity activity = (BaseLifecycleActivity) v.getContext();
                Intent intent = new Intent(activity, ViewPhotoSlideActivity.class);

                intent.putParcelableArrayListExtra(Constants.IntentExtras.PHOTO_LIST, tableData);
                activity.startActivity(intent);
            });
        return binding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}

