package in.hvpl.affinita.view.article;

import android.content.Context;
import android.databinding.ObservableArrayList;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.yayandroid.parallaxrecyclerview.ParallaxViewHolder;

import in.hvpl.affinita.R;
import in.hvpl.affinita.databinding.RowArticleBinding;
import in.hvpl.affinita.model.ArticleModel;
import in.hvpl.affinita.view.OnLoadMoreListener;
import in.hvpl.affinita.viewmodel.ArticleListViewModel;

/**
 * Created by webwerks1 on 11/7/17.
 */

public class ArticleRecyclerAdapter extends RecyclerView.Adapter<ArticleRecyclerAdapter.ViewHolder> {

    private final ArticleListViewModel viewModel;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private ObservableArrayList<ArticleModel> articleModels;
    private OnLoadMoreListener onLoadMoreListener;
    private LayoutInflater inflater;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private Context context;
    /*
    private int[] imageIds = new int[]{R.mipmap.test_image_1,
            R.mipmap.test_image_2, R.mipmap.test_image_3,
            R.mipmap.test_image_4, R.mipmap.test_image_5};
    */


    public ArticleRecyclerAdapter(Context context, ObservableArrayList<ArticleModel> articleModels, ArticleListViewModel viewModel, RecyclerView recyclerView) {
        this.viewModel = viewModel;
        this.articleModels = articleModels;
        this.inflater = LayoutInflater.from(context);
        this.context = context;


        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }
   /* @Override
    public int getItemViewType(int position) {
        return articleModels.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }*/

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        return new ViewHolder(RowArticleBinding.inflate(inflater, viewGroup, false));
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.rowArticleBinding.invalidateAll();
        viewHolder.rowArticleBinding.setModel(articleModels.get(position));
        viewHolder.rowArticleBinding.setViewModel(viewModel);
        if (viewHolder.rowArticleBinding.hasPendingBindings()) {
            viewHolder.rowArticleBinding.executePendingBindings();
        }

        // # CAUTION:
        // Important to call this method
        viewHolder.getBackgroundImage().reuse();
    }

    @Override
    public int getItemCount() {
        return articleModels.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public boolean isLoadingData() {
        return isLoading;
    }

    /**
     * # CAUTION:
     * ViewHolder must extend from ParallaxViewHolder
     */
    public static class ViewHolder extends ParallaxViewHolder {
        RowArticleBinding rowArticleBinding;

        public ViewHolder(RowArticleBinding rowArticleBinding) {
            super(rowArticleBinding.getRoot());
            this.rowArticleBinding = rowArticleBinding;
        }

        @Override
        public int getParallaxImageId() {
            return R.id.backgroundImage;
        }
    }
}