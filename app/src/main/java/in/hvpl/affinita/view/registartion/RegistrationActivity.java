package in.hvpl.affinita.view.registartion;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.view.WindowManager;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.ActivityRegistrationBinding;
import in.hvpl.affinita.viewmodel.RegistrationViewModel;
import neo.architecture.lifecycle.FragmentsManagerHelper;
import pub.devrel.easypermissions.EasyPermissions;

public class RegistrationActivity extends BaseActivity<ActivityRegistrationBinding> {
    private static final String TAG = RegistrationActivity.class.getSimpleName();
    private RegistrationViewModel registrationViewModel;

    @Override
    public int getContentViewResource() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_registration;
    }

    @Override
    public void initUI() {
        registrationViewModel = ViewModelProviders.of(this).get(RegistrationViewModel.class);
        mBinding.setViewModel(registrationViewModel);
        moveToMobileRegistrationScreen();

        String[] perms = {Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS};
        if (!EasyPermissions.hasPermissions(this, perms)) {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_otp),
                    Constants.PermissionRequestCode.RC_SMS, perms);
        }

    }

    private void moveToMobileRegistrationScreen() {
        fragmentTransaction(FragmentsManagerHelper.ADD_FRAGMENT, new MobileRegistrationFragment(),
                R.id.container_registration, false);
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.REGISTRATION_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }


}
