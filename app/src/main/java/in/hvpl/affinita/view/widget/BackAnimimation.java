package in.hvpl.affinita.view.widget;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class BackAnimimation extends Animation {
    int targetHeight;
    int originalHeight;
    int extraHeight;
    View view;
    boolean down;
    int deviceheight;
    boolean flag=false;
    public BackAnimimation(View view ,int targetHeight, boolean down,int deviceheight) {
        this.view = view;
        this.targetHeight = targetHeight;
        this.down = down;
        this.deviceheight=deviceheight;
        originalHeight = view.getHeight();
        extraHeight = this.targetHeight - originalHeight;
        flag=false;
    }

    @Override
    public void applyTransformation(float interpolatedTime, Transformation t) {
        int newHeight;
       newHeight = (int) (targetHeight - extraHeight * (1- interpolatedTime));

        view.getLayoutParams().height = newHeight;
        view.requestLayout();



    }

    @Override
    public void initialize(int width, int height, int parentWidth,
                           int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);

    }

}
