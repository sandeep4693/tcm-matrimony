package in.hvpl.affinita.view.home;

import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.ActivityHomeBinding;
import in.hvpl.affinita.model.UserDetailModel;
import in.hvpl.affinita.view.myprofile.MyProfileFragment;
import in.hvpl.affinita.viewmodel.HomeViewModel;
import neo.architecture.lifecycle.FragmentsManagerHelper;

public class HomeActivity extends BaseActivity<ActivityHomeBinding> {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private HomeViewModel homeViewModel;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            UserDetailModel userDetailModel = App.getInstance().getUserDetailModel().get();
            int status = intent.getIntExtra(Constants.IntentExtras.STATUS, userDetailModel.getStatus());
            if (status == Constants.AppStatus.HOLD) {
                userDetailModel.setAppStatus(status);
            } else {
                userDetailModel.setStatus(status);
            }
            App.getInstance().setUserDetailModel(userDetailModel);
            bottomMenuSelectorLogic();
        }
    };

    @Override
    public int getContentViewResource() {
        return R.layout.activity_home;
    }

    @Override
    public void initUI() {
        setToolBar(mBinding.toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        mBinding.setViewModel(homeViewModel);
        fragmentTransaction(FragmentsManagerHelper.REPLACE_FRAGMENT, new MyProfileFragment(), R.id.container, false);

        isDataLoaded().observe(this, isDataLoad -> {
            if (isDataLoad) {
                bottomMenuSelectorLogic();
                setProposalUnreadCount(App.getInstance().getUserDetailModel().get().getProposalUnreadCount());
                setConnectionUnreadCount(App.getInstance().getUserDetailModel().get().getConnectionUnreadCount());
            }
        });

        isUnreadCountRefresh().observe(this, isRefresh -> {
            if (isRefresh) {
                homeViewModel.connectionUnreadCount.set(SharedPreferencesHelper.getConnectedUnreadCount(this));
                homeViewModel.proposalUnreadCount.set(SharedPreferencesHelper.getProposalUnreadCount(this));
            }
        });
    }

    private void bottomMenuSelectorLogic() {
        homeViewModel.status.set(App.getInstance().getUserDetailModel().get().getStatus());
        homeViewModel.isSelectable.set(App.getInstance().getUserDetailModel().get().getStatus() == Constants.AppStatus.ACTIVE
                && App.getInstance().getUserDetailModel().get().getAppStatus() != Constants.AppStatus.HOLD);
    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SEND_FOR_APPROVAL:
                    homeViewModel.onBottomMenuClick(mBinding.rvInsight);
                    break;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter(Constants.ActionIntent.ACTIVATION_STATUS)
        );
    }


    @Override
    protected void onDestroy() {
        SharedPreferencesHelper.setReceiverId(this, 0);
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

}
