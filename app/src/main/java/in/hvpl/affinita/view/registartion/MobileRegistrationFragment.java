package in.hvpl.affinita.view.registartion;

import android.arch.lifecycle.ViewModelProviders;

import com.google.firebase.iid.FirebaseInstanceId;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.FragmentMobileRegistrationBinding;
import in.hvpl.affinita.viewmodel.RegistrationViewModel;
import neo.architecture.lifecycle.BaseLifecycleFragment;
import neo.architecture.lifecycle.FragmentsManagerHelper;


public class MobileRegistrationFragment extends BaseLifecycleFragment<FragmentMobileRegistrationBinding> {
    private static final String TAG = MobileRegistrationFragment.class.getSimpleName();

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_mobile_registration;
    }

    @Override
    public void initUI() {
        FirebaseInstanceId.getInstance().getToken();
        RegistrationViewModel viewModel = ViewModelProviders.of(getActivity()).get(RegistrationViewModel.class);
        mBinding.setViewModel(viewModel);
        subscribeToModel(viewModel);
    }

    /**
     * @param viewModel
     */
    private void subscribeToModel(RegistrationViewModel viewModel) {
        viewModel.getRegiData().observe(this, registrationResponse -> {
            if (registrationResponse != null) {
                if (registrationResponse.isSuccess()) {
                    fragmentTransaction(FragmentsManagerHelper.REPLACE_FRAGMENT,
                            new OtpValidationFragment(), R.id.container_registration, false);
                } else {
                    Tools.showToast(getContext(), registrationResponse.getMessage());
                }
            }
        });
    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }
}
