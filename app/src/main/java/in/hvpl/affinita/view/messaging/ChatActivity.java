package in.hvpl.affinita.view.messaging;

import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.ActivityChatBinding;
import in.hvpl.affinita.model.ConnectionModel;
import in.hvpl.affinita.model.MessageModel;
import in.hvpl.affinita.view.widget.RecyclerViewPositionHelper;
import in.hvpl.affinita.viewmodel.ChatViewModel;


public class ChatActivity extends BaseActivity<ActivityChatBinding> {

    private static final String TAG = ChatActivity.class.getSimpleName();
    private ChatViewModel viewModel;
    RecyclerViewPositionHelper mRecyclerViewHelper;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_chat;
    }

    @Override
    public void initUI() {

        setUpViewModel();
        setToolbar();
        bindMessagesList();
        viewModel.addChatHeader();

        viewModel.isChatRefresh.observe(this, isDataFresh -> {
            mBinding.rvMsg.getAdapter().notifyDataSetChanged();

        });
        viewModel.isMessageSend.observe(this, isMessageSend -> {
            mBinding.edtMsg.setText("");
            mBinding.rvMsg.getAdapter().notifyItemInserted(0);
            mBinding.rvMsg.smoothScrollToPosition(0);
        });

        loadChatMessages("0");
        onLoadMore();

        mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(mBinding.rvMsg);
        int visibleItemCount = mBinding.rvMsg.getChildCount();
        int totalItemCount = mRecyclerViewHelper.getItemCount();
        int firstVisibleItem = mRecyclerViewHelper.findFirstVisibleItemPosition();
        int lastVisibleItem = mRecyclerViewHelper.findLastVisibleItemPosition();

    }


    private void bindMessagesList() {
        ChatAdapter adapter = new ChatAdapter(this, viewModel.messageModels);
        mBinding.rvMsg.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true));
        mBinding.rvMsg.setAdapter(adapter);
    }

    private void setUpViewModel() {
        viewModel = ViewModelProviders.of(this).get(ChatViewModel.class);
        mBinding.setViewModel(viewModel);
        mBinding.setMsg("");
        viewModel.connectionModel = (ConnectionModel) getIntent().getSerializableExtra(Constants.IntentExtras.CONNECTION_DATA);
    }

    private void setToolbar() {
        String url = viewModel.connectionModel.getClientInfo().getPhoto();
        mBinding.backArrow.setImageDrawable(App.getInstance().isBoy() ? this.getResources().getDrawable(R.drawable.ic_boy_back) : this.getResources().getDrawable(R.drawable.ic_girl_back));

        mBinding.backArrow.setOnClickListener(v -> {
            onBackPressed();
        });

        Glide.with(ChatActivity.this)
                .load(url)
                .asBitmap()
                .centerCrop()
                .placeholder(App.getInstance().isBoy() ? ChatActivity.this.getResources().getDrawable(R.drawable.girl_profile) : ChatActivity.this.getResources().getDrawable(R.drawable.boy_profile))
                .into(new BitmapImageViewTarget(mBinding.image) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(ChatActivity.this.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        mBinding.image.setImageDrawable(circularBitmapDrawable);
                    }
                });
        mBinding.title.setText(viewModel.connectionModel.getClientInfo().getName());

    }

    @Override
    public String getScreenName() {
        return viewModel.connectionModel.getClientInfo().getName()
                + "_" +
                viewModel.connectionModel.getClientInfo().getReceiverId()
                + "_MESSAGE_" +
                Constants.SCREEN_NAME;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    private void loadChatMessages(String messageId) {
        Tools.printError(TAG, "messageId" + messageId);
        viewModel.getChatMessageList(this, messageId);
    }

    private void onLoadMore() {

        mBinding.mySwipeRefreshLayout.setOnRefreshListener(() -> {
            try {
                int size = viewModel.messageModels.size();
                if (!viewModel.connectionModel.getCommonHobbies().isEmpty()) {
                    size--;
                }
                if (size > 0) {
                    size--;

                    loadChatMessages(String.valueOf(viewModel.messageModels.get(size).getId()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                mBinding.mySwipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            MessageModel messageModel = (MessageModel) intent.getSerializableExtra(Constants.IntentExtras.MESSAGE);
            viewModel.readMessage(ChatActivity.this, String.valueOf(messageModel.getId()));
            viewModel.messageModels.add(0, messageModel);
            mBinding.rvMsg.getAdapter().notifyItemInserted(0);
            if (mBinding.rvMsg.getVerticalScrollbarPosition() == 0) {
                mBinding.rvMsg.smoothScrollToPosition(0);
            }

        }
    };

    @Override
    protected void onStart() {
        SharedPreferencesHelper.setReceiverId(this, viewModel.connectionModel.getClientInfo().getReceiverId());
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter(Constants.ActionIntent.NEW_MESSAGE)
        );
    }


    @Override
    protected void onDestroy() {
        SharedPreferencesHelper.setReceiverId(this, 0);
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }


}
