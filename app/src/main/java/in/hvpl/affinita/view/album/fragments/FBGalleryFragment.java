package in.hvpl.affinita.view.album.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.FacebookHelper;
import in.hvpl.affinita.common.widget.GridSpacingItemDecoration;
import in.hvpl.affinita.databinding.FragmentFbgalleryBinding;
import in.hvpl.affinita.view.album.adapters.GalleryAdapter;
import in.hvpl.affinita.viewmodel.AlbumCreateViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class FBGalleryFragment extends BaseFragment<FragmentFbgalleryBinding> {

    private static final String TAG = FBGalleryFragment.class.getSimpleName();

    private AlbumCreateViewModel viewModel;
    // create boolean for fetching data
    private boolean _hasLoadedOnce = false;


    @Override
    public int getContentViewResource() {
        return R.layout.fragment_fbgallery;
    }

    @Override
    public void initUI() {
        viewModel = ViewModelProviders.of(getActivity()).get(AlbumCreateViewModel.class);
        mBinding.setViewModel(viewModel);
        bindImageList();

    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    private void bindImageList() {
        int columns = 3;
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), columns);
        mBinding.rvGallery.setLayoutManager(layoutManager);
        mBinding.rvGallery.setHasFixedSize(true);
        layoutManager.setSpanCount(columns);
        GridSpacingItemDecoration itemOffsetDecoration = new GridSpacingItemDecoration(columns, getResources().getDimensionPixelSize(R.dimen.item_padding), false);
        mBinding.rvGallery.addItemDecoration(itemOffsetDecoration);
        mBinding.rvGallery.setAdapter(new GalleryAdapter(viewModel.facebookAlbumList, viewModel));

    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if (this.isVisible()) {
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                getFbPhoto();
                _hasLoadedOnce = true;
            }
        }
    }

    private void getFbPhoto() {

        if(FacebookHelper.getInstance().getAccessToken()!=null)
            viewModel.getFacebookImages(getBaseLifeCycleActivity());

        viewModel.getFacebookPhotoResponse().observe(this, facebookPhotoResponse -> {
            if (facebookPhotoResponse != null) {
                if(facebookPhotoResponse.getFacebookPhotos()!=null)
                viewModel.facebookAlbumList.addAll(facebookPhotoResponse.getFacebookPhotos());
                mBinding.rvGallery.getAdapter().notifyDataSetChanged();
            }
        });
    }
}
