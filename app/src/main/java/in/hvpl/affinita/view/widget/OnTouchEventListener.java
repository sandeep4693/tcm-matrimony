package in.hvpl.affinita.view.widget;

import android.view.MotionEvent;

public interface OnTouchEventListener {
    void onTouchEvent(MotionEvent ev);
}
