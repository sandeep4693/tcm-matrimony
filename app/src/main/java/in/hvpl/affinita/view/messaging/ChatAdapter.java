package in.hvpl.affinita.view.messaging;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.hvpl.affinita.App;
import in.hvpl.affinita.databinding.HeaderChatBinding;
import in.hvpl.affinita.databinding.RowChatBinding;
import in.hvpl.affinita.model.MessageModel;

/**
 * Created by Ganesh.K on 7/8/17.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<MessageModel> messageModels;
    private LayoutInflater layoutInflater;
    private boolean isBoy;
    private Context context;


    public ChatAdapter(Context context, ArrayList<MessageModel> messageModels) {
        this.messageModels = messageModels;
        layoutInflater = LayoutInflater.from(context);
        isBoy = App.getInstance().isBoy();
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        switch (viewType) {
            case MessageModel.HEADER:
                return new HeaderViewHolder(HeaderChatBinding.inflate(layoutInflater, viewGroup, false));

            case MessageModel.DATA:

                return new ChatViewHolder(RowChatBinding.inflate(layoutInflater, viewGroup, false));
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,int position) {
        MessageModel messageModel=messageModels.get(position);
        if(messageModel!=null){
            switch (messageModel.getmType()){
                case MessageModel.HEADER:
                    HeaderViewHolder headerChatBinding=(HeaderViewHolder) holder;
                    headerChatBinding.binding.setModel(messageModels.get(position));
                    headerChatBinding.binding.setIsBoy(isBoy);

                    break;

                    case MessageModel.DATA:
                        ChatViewHolder chatViewHolder=(ChatViewHolder)holder;
                        chatViewHolder.binding.setModel(messageModels.get(position));
                        chatViewHolder.binding.setIsBoy(isBoy);
                        if(position==(getItemCount()-1)){

                            // here goes some code
                            //  callback.sendMessage(Message);
                        }
                        break;

            }
        }


    }

    @Override
    public int getItemCount() {
        return messageModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return messageModels.get(position).getmType();
    }

  public static   class ChatViewHolder extends RecyclerView.ViewHolder {
        RowChatBinding binding;

        public ChatViewHolder(RowChatBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder{
        HeaderChatBinding binding;

        public HeaderViewHolder(HeaderChatBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }
    }
}
