package in.hvpl.affinita.view.forceupdate;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import in.hvpl.affinita.BuildConfig;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.model.response.CheckForceUpdateResponse;

/**
 * Created by Ganesh.K on 22/9/17.
 */

public class ForceUpdateService extends IntentService {

    public ForceUpdateService() {
        super(ForceUpdateService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        NetworkHelper.create(this)
                .setShowOnFailureDialog(false)
                .setShowProgressLoading(false)
                .callWebService(ApiClient.getClient().checkForceUpdate("A", BuildConfig.VERSION_NAME, Constants.BUREAU_ID),
                        new NetworkHelper.NetworkCallBack<CheckForceUpdateResponse>() {
                            @Override
                            public void onSyncData(CheckForceUpdateResponse data) {

                                if (data.isSuccess()) {

                                    Intent intent = new Intent(ForceUpdateService.this, ForceUpdateActivity.class);
                                    intent.putExtra(Constants.IntentExtras.FORCE_UPDATE, data.getCheckForceUpdateData().getForceUpdate());
                                    intent.putExtra(Constants.IntentExtras.FORCE_UPDATE_MSG, data.getMessage());
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onFailed(CheckForceUpdateResponse error) {

                            }

                            @Override
                            public void noInternetConnection() {

                            }
                        });
    }
}
