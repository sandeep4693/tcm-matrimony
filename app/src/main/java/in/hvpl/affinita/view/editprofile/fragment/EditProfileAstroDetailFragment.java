package in.hvpl.affinita.view.editprofile.fragment;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.Fragment;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.FragmentEditProfileAstroDetailBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.request.AstroInfoRequestModel;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileAstroDetailFragment extends BaseFragment<FragmentEditProfileAstroDetailBinding> {

    private static final String TAG = EditProfileAstroDetailFragment.class.getSimpleName();

    private EditProfileViewModel editProfileViewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_edit_profile_astro_detail;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_edit_astro_details));
        editProfileViewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setFragment(this);
        mBinding.setViewModel(editProfileViewModel);
        mBinding.setModel(new AstroInfoRequestModel(SharedPreferencesHelper.getClientId(getContext()),editProfileViewModel.userDetailModel.get().getOtherInformationModel()));
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_ASTRO_DETAILS_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            SelectionModel selectionModel = data.getParcelableExtra(Constants.IntentExtras.SELECTION_MODEL);
            String editedDetail = data.getStringExtra(Constants.IntentExtras.EDITED_DETAILS);
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SELECT_MANGLIK:
                    mBinding.getModel().setManglikId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setManglik(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_NAKSHATRA:
                    mBinding.getModel().setNakshatraId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setNakshatra(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_RASHI:
                    mBinding.getModel().setRashiId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setRashi(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_GOTHRA:
                    mBinding.getModel().setGothraId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setGothra(selectionModel.getTitle());
                    break;
            }
        }
    }
}
