package in.hvpl.affinita.view.editprofile.fragment;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.Fragment;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.widget.CustomDatePicker;
import in.hvpl.affinita.common.widget.CustomNumberPicker;
import in.hvpl.affinita.databinding.FragmentEditProfileBasicInfoBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.request.BasicInfoRequestModel;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */

public class EditProfileBasicInfoFragment extends BaseFragment<FragmentEditProfileBasicInfoBinding> implements CustomDatePicker.OnDateSelectionListener,CustomNumberPicker.OnNumberSelectionListener{


    private static final String TAG = EditProfileBasicInfoFragment.class.getSimpleName();
    private EditProfileViewModel editProfileViewModel;

    public EditProfileBasicInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_edit_profile_basic_info;

    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_edit_basic_info));
        editProfileViewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setFragment(this);
        mBinding.setViewModel(editProfileViewModel);
        mBinding.setOnDateSelection(this);
        mBinding.setOnNumberSelection(this);
        mBinding.setModel(new BasicInfoRequestModel(SharedPreferencesHelper.getClientId(getContext()),editProfileViewModel.userDetailModel.get().getBasicInformationModel(),
                editProfileViewModel.userDetailModel.get().getContactInformationModel(),
                editProfileViewModel.userDetailModel.get().getOtherInformationModel()));
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_BASIC_INFORMATION_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            SelectionModel selectionModel = data.getParcelableExtra(Constants.IntentExtras.SELECTION_MODEL);
            String editedDetail = data.getStringExtra(Constants.IntentExtras.EDITED_DETAILS);
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SELECT_DIET:
                    mBinding.getModel().setDietId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setDietTitle(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_SMOKING:
                    mBinding.getModel().setSmokingId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setSmoking(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_DRINKING:
                    mBinding.getModel().setDrinkingId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setDrinking(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_MARITAL_STATUS:
                    mBinding.getModel().setMartialStatusId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setMartialStatus(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_DISABILITY:
                    mBinding.getModel().setDisabilityId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setDisability(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_COUNTRY:
                    mBinding.getModel().setCountryId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCountry(selectionModel.getTitle());

                    mBinding.getModel().setStateId("" + 0);
                    mBinding.getModel().setState(getString(R.string.activity_title_select_state));

                    mBinding.getModel().setCityId("" + 0);
                    mBinding.getModel().setCity(getString(R.string.activity_title_select_city));

                    break;

                case Constants.ActivityResultRequestCode.SELECT_STATE:
                    mBinding.getModel().setStateId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setState(selectionModel.getTitle());

                    mBinding.getModel().setCityId("" + 0);
                    mBinding.getModel().setCity(getString(R.string.activity_title_select_city));

                    break;

                case Constants.ActivityResultRequestCode.SELECT_CITY:
                    mBinding.getModel().setCityId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCity(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_ADDRESS:
                    mBinding.getModel().setAddress(editedDetail);
                    break;

                case Constants.ActivityResultRequestCode.SELECT_PINCODE:
                    mBinding.getModel().setPinCode(editedDetail);
                    break;
            }
        }
    }


    @Override
    public void onDateSelect(String date, int requestCode) {
        mBinding.getModel().setDob(date);
    }

    @Override
    public void onNumberSelection(String value1, String value2, int requestCode) {
        mBinding.getModel().setFeet(value1);
        mBinding.getModel().setInches(value2);
    }
}
