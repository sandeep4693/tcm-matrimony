package in.hvpl.affinita.view.viewprofile;

import android.content.Intent;
import android.databinding.ObservableArrayList;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.ActivityViewPhotoSlideBinding;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.view.viewprofile.adapters.ViewPhotoAdapter;
import in.hvpl.affinita.viewmodel.ViewPhotoSlideViewModel;

public class ViewPhotoSlideActivity extends BaseActivity<ActivityViewPhotoSlideBinding> {

    private static final String TAG = ViewPhotoSlideActivity.class.getSimpleName();

    private ViewPhotoSlideViewModel viewModel;
    private ObservableArrayList<AlbumInformationModel> albumInformationModels = new ObservableArrayList<>();

    @Override
    public int getContentViewResource() {
        return R.layout.activity_view_photo_slide;
    }

    @Override
    public void initUI() {
        this.setFinishOnTouchOutside(false);
        Intent intent = this.getIntent();
        ArrayList<AlbumInformationModel> arrayList = intent.getParcelableArrayListExtra(Constants.IntentExtras.PHOTO_LIST);
//        albumInformationModels = getIntent().getSerializableExtra(Constants.IntentExtras.PHOTO_LIST);
//        ObservableArrayList<AlbumInformationModel> albumInformationModels = (ObservableArrayList<AlbumInformationModel>)bundle.getSerializable(Constants.IntentExtras.PHOTO_LIST);

        albumInformationModels.addAll(arrayList);
        setAlbumViewPager();

    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    private void setAlbumViewPager() {
        mBinding.viewPagerPhoto.setAdapter(new ViewPhotoAdapter(albumInformationModels));
        mBinding.viewPagerCount.setText(1 + " of " + albumInformationModels.size());

        mBinding.viewPagerPhoto.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mBinding.viewPagerCount.setText(position + 1 + " of " + albumInformationModels.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    protected void onStart() {
        overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out);
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
