package in.hvpl.affinita.view.article;

import android.content.Context;
import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import in.hvpl.affinita.databinding.RowArticleCategoryListBinding;
import in.hvpl.affinita.model.ArticleCategoryModel;
import in.hvpl.affinita.viewmodel.ArticleListViewModel;

/**
 * Created by webwerks1 on 11/7/17.
 */

public class ArticleCategoryAdapter extends RecyclerView.Adapter<ArticleCategoryAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    private ObservableArrayList<ArticleCategoryModel> articleCategoryModels;
    private ArticleListViewModel articleListViewModel;

    /*
    private int[] imageIds = new int[]{R.mipmap.test_image_1,
            R.mipmap.test_image_2, R.mipmap.test_image_3,
            R.mipmap.test_image_4, R.mipmap.test_image_5};
    */


    public ArticleCategoryAdapter(Context context,ObservableArrayList<ArticleCategoryModel> articleCategoryModels,ArticleListViewModel articleListViewModel) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.articleCategoryModels=articleCategoryModels;
        this.articleListViewModel=articleListViewModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        return new ViewHolder(RowArticleCategoryListBinding.inflate(inflater, viewGroup, false));
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.binding.setModel(articleCategoryModels.get(position));
        viewHolder.binding.setPosition(position);
        viewHolder.binding.setViewModel(articleListViewModel);
    }

    @Override
    public int getItemCount() {
        return articleCategoryModels.size();
    }

    /**
     * # CAUTION:
     * ViewHolder must extend from ParallaxViewHolder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        RowArticleCategoryListBinding binding;


        public ViewHolder(RowArticleCategoryListBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

    }


}