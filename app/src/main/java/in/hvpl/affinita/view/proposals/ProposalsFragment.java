package in.hvpl.affinita.view.proposals;


import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.ObservableArrayList;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.widget.EqualSpacingItemDecoration;
import in.hvpl.affinita.databinding.FragmentProposalsBinding;
import in.hvpl.affinita.model.ProposalModel;
import in.hvpl.affinita.viewmodel.ProposalViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProposalsFragment extends BaseFragment<FragmentProposalsBinding> {

    private static final String TAG = ProposalsFragment.class.getSimpleName();

    private ProposalViewModel viewModel;
    private BroadcastReceiver mProposalReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getProposalList(true);

        }
    };

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_proposals;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_proposals));
        setToolBarVisibility(true);
        viewModel = ViewModelProviders.of(this).get(ProposalViewModel.class);
        mBinding.setViewModel(viewModel);
        bindSelectedImageList(mBinding.latestMatchRv, viewModel.matchesModels);
        bindSelectedImageList(mBinding.likeYouRv, viewModel.whoLikeYouModels);
        bindSelectedImageList(mBinding.youLikeRv, viewModel.youLikeModels);
        bindSelectedImageList(mBinding.recentlyViewedRv, viewModel.recentViewModels);
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver((mProposalReceiver),
                new IntentFilter(Constants.ActionIntent.PROPOSAL)
        );
    }

    @Override
    public void onResume() {
        getProposalList(false);
        super.onResume();
    }

    private void getProposalList(boolean isLoadInBackground) {
        viewModel.getProposalsList(getContext(), isLoadInBackground).observe(this, isDataLoad -> {
            //getBaseActivity().stopLoading();
            if (isDataLoad) {
                mBinding.latestMatchRv.getAdapter().notifyDataSetChanged();
                mBinding.likeYouRv.getAdapter().notifyDataSetChanged();
                mBinding.youLikeRv.getAdapter().notifyDataSetChanged();
                mBinding.recentlyViewedRv.getAdapter().notifyDataSetChanged();
            }
        });
    }

    private void bindSelectedImageList(RecyclerView recyclerView, ObservableArrayList<ProposalModel> proposalModelObservableArrayList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
        recyclerView.setAdapter(new ProposalAdapter(proposalModelObservableArrayList, viewModel));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mProposalReceiver);
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.PROPOSALS_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }
}
