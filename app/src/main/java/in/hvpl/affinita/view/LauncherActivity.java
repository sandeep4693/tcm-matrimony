package in.hvpl.affinita.view;

import android.content.Intent;
import android.os.Handler;
import android.view.WindowManager;

import in.hvpl.affinita.BuildConfig;
import in.hvpl.affinita.R;
import in.hvpl.affinita.databinding.ActivityLauncherBinding;
import neo.architecture.lifecycle.BaseLifecycleActivity;

public class LauncherActivity extends BaseLifecycleActivity<ActivityLauncherBinding> {


    private static final String TAG = LauncherActivity.class.getSimpleName();
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1500;

    @Override
    public int getContentViewResource() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_launcher;
    }

    @Override
    public void initUI() {
        /*
         * Showing splash screen with a timer. This will be useful when you
         * want to show case your app logo / company
         *
         */

        mBinding.setAppVersion(BuildConfig.VERSION_NAME);
        new Handler().postDelayed(() -> {
            // This method will be executed once the timer is over
            // Start your app main activity
            startActivityWithAnimation(new Intent(LauncherActivity.this, SplashScreenActivity.class));
            finish();
        }, SPLASH_TIME_OUT);
    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }
}