package in.hvpl.affinita.view.editprofile;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;

import java.util.ArrayList;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.ActivityEditProfileSelectionBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.viewmodel.EditProfileSelectionViewModel;

public class EditProfileSelectionActivity extends BaseActivity<ActivityEditProfileSelectionBinding> {

    private static final String TAG = EditProfileSelectionActivity.class.getSimpleName();

    private ArrayList<SelectionModel> selectionModels;
    private EditProfileSelectionAdapter adapter;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_edit_profile_selection;
    }

    @Override
    public void initUI() {
        setToolBar(mBinding.toolBar);
        setToolbarTitle(getIntent().getStringExtra(Constants.IntentExtras.TITLE));
        EditProfileSelectionViewModel viewModel = ViewModelProviders.of(this).get(EditProfileSelectionViewModel.class);
        selectionModels = this.getIntent().getParcelableArrayListExtra(Constants.IntentExtras.MASTER_LIST);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mBinding.editProfileSelectionRv.setLayoutManager(linearLayoutManager);
        adapter = new EditProfileSelectionAdapter(selectionModels, viewModel);
        mBinding.editProfileSelectionRv.setAdapter(adapter);

        mBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

}
