package in.hvpl.affinita.view.album.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.widget.GridSpacingItemDecoration;
import in.hvpl.affinita.databinding.FragmentGalleryBinding;
import in.hvpl.affinita.view.album.adapters.GalleryAdapter;
import in.hvpl.affinita.viewmodel.AlbumCreateViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends BaseFragment<FragmentGalleryBinding> {

    private static final String TAG = GalleryFragment.class.getSimpleName();
    // create boolean for fetching data
    private boolean _hasLoadedOnce = false;
    private AlbumCreateViewModel viewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_gallery;
    }

    @Override
    public void initUI() {

        viewModel = ViewModelProviders.of(getActivity()).get(AlbumCreateViewModel.class);
        mBinding.setViewModel(viewModel);
        bindImageList();

        if(!_hasLoadedOnce){
            getGalleryData();
        }

    }


    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    private void bindImageList() {
        int columns = 3;
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), columns);
        mBinding.rvGallery.setLayoutManager(layoutManager);
        mBinding.rvGallery.setHasFixedSize(true);
        layoutManager.setSpanCount(columns);
        GridSpacingItemDecoration itemOffsetDecoration = new GridSpacingItemDecoration(columns, getResources().getDimensionPixelSize(R.dimen.item_padding), false);
        mBinding.rvGallery.addItemDecoration(itemOffsetDecoration);
        mBinding.rvGallery.setAdapter(new GalleryAdapter(viewModel.galleryAlbumList, viewModel));

    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if (this.isVisible()) {
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                getGalleryData();
                _hasLoadedOnce = true;
            }
        }
    }

    private void getGalleryData() {
        _hasLoadedOnce=true;
        viewModel.getGalleryData(getBaseLifeCycleActivity()).observe(this, isLoad -> mBinding.rvGallery.getAdapter().notifyDataSetChanged());
    }
}
