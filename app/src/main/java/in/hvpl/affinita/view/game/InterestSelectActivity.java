package in.hvpl.affinita.view.game;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.igalata.bubblepicker.BubblePickerListener;
import com.igalata.bubblepicker.adapter.BubblePickerAdapter;
import com.igalata.bubblepicker.model.BubbleGradient;
import com.igalata.bubblepicker.model.PickerItem;
import com.igalata.bubblepicker.rendering.BubblePicker;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.ActivityInterestSelectBinding;
import in.hvpl.affinita.model.Options;
import in.hvpl.affinita.viewmodel.InterestSelectViewModel;

public class InterestSelectActivity extends BaseActivity<ActivityInterestSelectBinding> {

    private static final String TAG = InterestSelectActivity.class.getSimpleName();

    private InterestSelectViewModel viewModel;
    Animation RightSwipe;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_interest_select;
    }

    @Override
    public void initUI() {
        setToolBar(mBinding.toolBar);
        setToolbarTitle(getString(R.string.activity_title_select_hobbies));
        RightSwipe = AnimationUtils.loadAnimation(this, R.anim.left_slide);
        viewModel = ViewModelProviders.of(this).get(InterestSelectViewModel.class);
        viewModel.imgUrl.set(getIntent().getStringExtra(Constants.IntentExtras.IMAGE_PATH));
        mBinding.setViewModel(viewModel);
        subscribe();

    }

    private void subscribe() {
        viewModel.getInterestQuestions(this).observe(this, interestResponseModel -> {

            if (interestResponseModel.isSuccess()) {
                viewModel.questionaryList = interestResponseModel.getData().getQuestionaryList();
                initPicker(0);
            }
        });

        viewModel.getIsNextQuestion().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                initPicker(integer);
            }
        });
    }


    private void initPicker(int pos) {

        mBinding.frameLayout.removeAllViews();
        mBinding.txtQuestion.setText(viewModel.questionaryList.get(pos).getQuestion());

        final TypedArray colors = getResources().obtainTypedArray(R.array.colors);
        ArrayList<Options> titles = viewModel.questionaryList.get(pos).getOptions();

        BubblePicker picker = new BubblePicker(this);
        picker.setAdapter(new BubblePickerAdapter() {
            @Override
            public int getTotalCount() {
                return titles.size();
            }

            @NotNull
            @Override
            public PickerItem getItem(int position) {
                PickerItem item = new PickerItem();
                item.setTitle(titles.get(position).getOption());
                item.setGradient(new BubbleGradient(colors.getColor((position * 2) % 8, 0),
                        colors.getColor((position * 2) % 8 + 1, 0), BubbleGradient.VERTICAL));
                //item.setTypeface(mediumTypeface);
                item.setTextColor(ContextCompat.getColor(InterestSelectActivity.this, android.R.color.white));

                return item;
            }
        });
        picker.setAnimation(RightSwipe);
        picker.setCenterImmediately(true);
        picker.setMaxSelectedCount(1);
        picker.setBubbleSize(150);
        picker.setListener(new BubblePickerListener() {
            @Override
            public void onBubbleSelected(@NotNull PickerItem item) {
                Tools.printError(TAG, "onBubbleSelected=" + item.getTitle());
                viewModel.answerList.add(item.getTitle());
                viewModel.isOptionSelected = true;
            }

            @Override
            public void onBubbleDeselected(@NotNull PickerItem item) {
                viewModel.answerList.remove(pos);
                viewModel.isOptionSelected = false;
            }
        });
        mBinding.frameLayout.addView(picker);


        mBinding.llmain.setAnimation(RightSwipe);
        mBinding.llmain.startAnimation(RightSwipe);
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.PLAY_GAME_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }
}
