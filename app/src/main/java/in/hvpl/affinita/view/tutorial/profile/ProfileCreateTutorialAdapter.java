package in.hvpl.affinita.view.tutorial.profile;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import in.hvpl.affinita.R;
import in.hvpl.affinita.databinding.ItemProfileCreateTutorialPagerBinding;


/**
 * Created by webwerks on 15/6/17.
 */

public class ProfileCreateTutorialAdapter extends PagerAdapter {

    private Integer[] imgArr = {
            R.drawable.screen1,
            R.drawable.screen2,
            R.drawable.screen3,
            R.drawable.screen4,
            R.drawable.screen5,
    };

    @Override
    public int getCount() {
        return imgArr.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ItemProfileCreateTutorialPagerBinding binding = ItemProfileCreateTutorialPagerBinding.inflate(LayoutInflater.from(container.getContext()), container, true);
        binding.imgTutorialItem.setImageResource(imgArr[position]);
        return binding.getRoot();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}