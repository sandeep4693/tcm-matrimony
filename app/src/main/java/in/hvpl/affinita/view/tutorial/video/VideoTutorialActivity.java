package in.hvpl.affinita.view.tutorial.video;

import android.net.Uri;
import android.widget.MediaController;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.ActivityVideoTutorialBinding;
import neo.architecture.lifecycle.BaseLifecycleActivity;

public class VideoTutorialActivity extends BaseLifecycleActivity<ActivityVideoTutorialBinding> {

    private static final String TAG = VideoTutorialActivity.class.getSimpleName();

    @Override
    public int getContentViewResource() {
        return R.layout.activity_video_tutorial;
    }

    @Override
    public void initUI() {

        if (SharedPreferencesHelper.isVideoPlay(this))
            mBinding.videoView.setMediaController(new MediaController(this));
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.DEMO_VIDEO_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Uri UriPath = Uri.parse
                ("android.resource://" + getPackageName() + "/" + R.raw.video);

        mBinding.videoView.setVideoURI(UriPath);
        mBinding.videoView.requestFocus();
        mBinding.videoView.start();
        mBinding.videoView.setOnCompletionListener(mediaPlayer -> {
            setResult(RESULT_OK);
            onBackPressed();
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        mBinding.videoView.pause();

    }
}
