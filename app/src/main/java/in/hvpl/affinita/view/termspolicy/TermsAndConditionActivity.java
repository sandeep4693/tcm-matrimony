package in.hvpl.affinita.view.termspolicy;

import android.app.ProgressDialog;
import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.HashMap;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.ActivityTermsAndConditionBinding;

public class TermsAndConditionActivity extends BaseActivity<ActivityTermsAndConditionBinding> {

    private static final String TAG = TermsAndConditionActivity.class.getSimpleName();

    ProgressDialog pd;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_terms_and_condition;
    }

    @Override
    public void initUI() {

        if (Tools.isConnected(this)) {
            pd = new ProgressDialog(this);
            pd.setMessage("Please wait Loading...");
            pd.show();
            String url = Constants.BUREAU_TERMS_AND_CONDITION_URL;
            WebView webView = mBinding.webViewTerms;
            final WebSettings settings = webView.getSettings();

            settings.setJavaScriptEnabled(true);
            settings.setDisplayZoomControls(false);
            settings.setAppCacheEnabled(true);
            settings.setLoadsImagesAutomatically(true);
            settings.setBuiltInZoomControls(false);
            settings.setPluginState(WebSettings.PluginState.ON);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

            webView.setWebViewClient(new BureauWebViewClients());

            HashMap<String, String> headerMap = new HashMap<>();
            //put all headers in this header map
            headerMap.put("Content-Type", "application/json");
            headerMap.put("api-version", "v1");

            webView.loadUrl(url, headerMap);

        } else {
            Tools.showNoInternetMsg(this);
        }

    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    public class BureauWebViewClients extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            if (!pd.isShowing()) {
                pd.show();
            }

            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            System.out.println("on finish");
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }
}
