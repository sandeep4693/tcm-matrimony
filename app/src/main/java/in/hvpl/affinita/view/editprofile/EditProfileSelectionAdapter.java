package in.hvpl.affinita.view.editprofile;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.List;

import in.hvpl.affinita.databinding.RowEditProfileSelectionBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.viewmodel.EditProfileSelectionViewModel;

/**
 * Created by webwerks1 on 18/7/17.
 */

public class EditProfileSelectionAdapter extends RecyclerView.Adapter<EditProfileSelectionAdapter.EditProfileViewHolder> implements Filterable {

    private List<SelectionModel> selectionModels;
    private List<SelectionModel> mFilteredList;
    private EditProfileSelectionViewModel viewModel;

    public EditProfileSelectionAdapter(List<SelectionModel> categoryList,EditProfileSelectionViewModel viewModel){
        this.selectionModels = categoryList;
        this.viewModel=viewModel;
        this.mFilteredList=selectionModels;
    }
    @Override
    public EditProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new EditProfileViewHolder(RowEditProfileSelectionBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
    }

    @Override
    public void onBindViewHolder(EditProfileViewHolder holder, int position) {
        SelectionModel selectionModel = mFilteredList.get(position);
        holder.rowEditProfileSelectionBinding.setModel(selectionModel);
        holder.rowEditProfileSelectionBinding.setViewModel(viewModel);
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString().toLowerCase();

                if (charString.isEmpty()) {
                    mFilteredList = selectionModels;
                } else {
                    ArrayList<SelectionModel> filteredList = new ArrayList<>();
                    for (SelectionModel selectionModel : mFilteredList) {
                        if (selectionModel.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(selectionModel);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<SelectionModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class EditProfileViewHolder extends RecyclerView.ViewHolder{
        public RowEditProfileSelectionBinding rowEditProfileSelectionBinding;

        public EditProfileViewHolder(RowEditProfileSelectionBinding rowEditProfileSelectionBinding) {
            super(rowEditProfileSelectionBinding.getRoot());
            this.rowEditProfileSelectionBinding = rowEditProfileSelectionBinding ;
        }
    }
}
