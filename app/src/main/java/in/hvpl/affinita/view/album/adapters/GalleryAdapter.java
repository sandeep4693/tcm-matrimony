package in.hvpl.affinita.view.album.adapters;

import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import in.hvpl.affinita.databinding.RowGalleryBinding;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.viewmodel.AlbumCreateViewModel;

/**
 * Created by Ganesh.K on 10/7/17.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {

    private ObservableArrayList<AlbumInformationModel> albumInformationModels;
    private AlbumCreateViewModel albumCreateViewModel;

    public GalleryAdapter(ObservableArrayList<AlbumInformationModel> albumInformationModels, AlbumCreateViewModel albumCreateViewModel) {
        this.albumInformationModels = albumInformationModels;
        this.albumCreateViewModel = albumCreateViewModel;
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        return new GalleryViewHolder(RowGalleryBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false));
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder galleryViewHolder, int position) {
        galleryViewHolder.rowGalleryBinding.setModel(albumInformationModels.get(position));
        galleryViewHolder.rowGalleryBinding.setViewModel(albumCreateViewModel);
    }

    @Override
    public int getItemCount() {
        return albumInformationModels.size();
    }

    class GalleryViewHolder extends RecyclerView.ViewHolder {

        RowGalleryBinding rowGalleryBinding;

        public GalleryViewHolder(RowGalleryBinding rowGalleryBinding) {
            super(rowGalleryBinding.getRoot());
            this.rowGalleryBinding = rowGalleryBinding;
        }
    }
}
