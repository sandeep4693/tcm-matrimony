package in.hvpl.affinita.view.editprofile.fragment;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.Fragment;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.FragmentEditProfileBackgroundBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.request.BackgroundRequestModel;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileBackgroundFragment extends BaseFragment<FragmentEditProfileBackgroundBinding> {
    private static final String TAG = EditProfileBackgroundFragment.class.getSimpleName();
    private EditProfileViewModel editProfileViewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_edit_profile_background;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_edit_background));
        editProfileViewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setViewModel(editProfileViewModel);
        mBinding.setFragment(this);
        mBinding.setModel(new BackgroundRequestModel(SharedPreferencesHelper.getClientId(getContext()),editProfileViewModel.userDetailModel.get().getBasicInformationModel(),editProfileViewModel.userDetailModel.get().getContactInformationModel(),editProfileViewModel.userDetailModel.get().getOtherInformationModel()));


    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_BACKGROUND_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            SelectionModel selectionModel = data.getParcelableExtra(Constants.IntentExtras.SELECTION_MODEL);
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SELECT_RELIGION:
                    mBinding.getModel().setReligionId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setReligion(selectionModel.getTitle());

                    mBinding.getModel().setCasteId(""+0);
                    mBinding.getModel().setCaste(getString(R.string.activity_title_select_cast));

                    mBinding.getModel().setSubCasteId(""+0);
                    mBinding.getModel().setSubCaste(getString(R.string.dont_know));
                    break;

                case Constants.ActivityResultRequestCode.SELECT_CASTE:
                    mBinding.getModel().setCasteId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCaste(selectionModel.getTitle());

                    mBinding.getModel().setSubCasteId(""+0);
                    mBinding.getModel().setSubCaste(getString(R.string.dont_know));
                    break;
                case Constants.ActivityResultRequestCode.SELECT_SUB_CASTE:
                    mBinding.getModel().setSubCasteId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setSubCaste(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_MOTHER_TONGUE:
                    mBinding.getModel().setMotherTongueId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setMotherTongue(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_RESIDENT:
                    mBinding.getModel().setResidentId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setResident(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_COUNTRY:
                    mBinding.getModel().setCountryId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCountry(selectionModel.getTitle());

                    mBinding.getModel().setStateId(""+0);
                    mBinding.getModel().setState(getString(R.string.dont_know));

                    mBinding.getModel().setCityId(""+0);
                    mBinding.getModel().setCity(getString(R.string.dont_know));
                    break;

                case Constants.ActivityResultRequestCode.SELECT_STATE:
                    mBinding.getModel().setStateId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setState(selectionModel.getTitle());

                    mBinding.getModel().setCityId(""+0);
                    mBinding.getModel().setCity(getString(R.string.dont_know));

                    break;

                case Constants.ActivityResultRequestCode.SELECT_OUTLOOK:
                    mBinding.getModel().setOutlookId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setOutlook(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_CITY:
                    mBinding.getModel().setCityId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCity(selectionModel.getTitle());
                    break;
            }
        }
    }
}
