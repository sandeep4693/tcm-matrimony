package in.hvpl.affinita.view.block;

import android.content.Intent;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.ActivityBlockBinding;
import in.hvpl.affinita.view.registartion.RegistrationActivity;

public class BlockActivity extends BaseActivity<ActivityBlockBinding> {
    private static final String TAG = BlockActivity.class.getSimpleName();

    @Override
    public int getContentViewResource() {
        return R.layout.activity_block;
    }

    @Override
    public void initUI() {
        SharedPreferencesHelper.blockUser(this);
        mBinding.txtYes.setOnClickListener(view -> {
            Intent intent = new Intent(this, RegistrationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
            finish();
        });
    }

    @Override
    public String getScreenName() {
        return TAG;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onBackPressed() {
    }
}
