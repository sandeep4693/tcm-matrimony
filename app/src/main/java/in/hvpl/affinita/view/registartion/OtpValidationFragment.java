package in.hvpl.affinita.view.registartion;


import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.FragmentOtpValidationBinding;
import in.hvpl.affinita.model.UserDetailModel;
import in.hvpl.affinita.view.home.HomeActivity;
import in.hvpl.affinita.viewmodel.RegistrationViewModel;


public class OtpValidationFragment extends BaseFragment<FragmentOtpValidationBinding> {

    private static final String TAG = OtpValidationFragment.class.getSimpleName();
    private Handler handler = new Handler();
    private RegistrationViewModel viewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_otp_validation;
    }

    @Override
    public void initUI() {
        viewModel = ViewModelProviders.of(getActivity()).get(RegistrationViewModel.class);
        viewModel.isOtpScreen.set(true);
        viewModel.registrationModel.setFcmToken(SharedPreferencesHelper.getFcmToken(getActivity()));
        mBinding.setViewModel(viewModel);
        startTimerForResendOtp();
        subscribeToModel(viewModel);


        //remove this code after auto detect sms
        //setOTP(viewModel);

    }

    Runnable runnable = () -> {
        viewModel.isResendVisible.set(true);
    };


    private void subscribeToModel(RegistrationViewModel viewModel) {
        viewModel.getUserDetailsResponseModel().observe(this, userDetailsResponseModel -> {
            if (userDetailsResponseModel != null) {
                if (userDetailsResponseModel.isSuccess()) {
                    //Tools.showToast(getActivity(), "Hi " + userDetailsResponseModel.getUserDetailModel().getBasicInformationModel().getFirstName() + " Welcome");
                    Tools.showToast(getActivity(), getActivity().getString(R.string.success_msg_otp));
                    SharedPreferencesHelper.setIsLogin(getActivity());
                    App.getInstance().setUserDetailModel(userDetailsResponseModel.getUserDetailModel());
                    setFirebaseUserProperty(userDetailsResponseModel.getUserDetailModel());
                    startActivityWithAnimation(new Intent(getActivity(), HomeActivity.class));
                    getActivity().finish();

                } else {
                    Tools.showToast(getContext(), userDetailsResponseModel.getMessage());
                }
            }
        });

        viewModel.getRegiData().observe(this, registrationResponse -> {
            if (registrationResponse != null) {
                if (registrationResponse.isSuccess()) {
                    viewModel.isResendVisible.set(false);
                    startTimerForResendOtp();
                } else {
                    Tools.showToast(getContext(), registrationResponse.getMessage());
                }
            }
        });
    }

    private void setFirebaseUserProperty(UserDetailModel userDetailModel) {
        getmFirebaseAnalytics().setUserId(SharedPreferencesHelper.getClientId(getActivity()));
        getmFirebaseAnalytics().setUserProperty(Constants.AnalyticsEventKey.USER_NAME,
                userDetailModel.getBasicInformationModel().getFirstName()
                        + " " +
                        userDetailModel.getBasicInformationModel().getLastName());
    }


    private void startTimerForResendOtp() {
        handler.postDelayed(runnable, 20000);
    }

    private void setOTP(String opt) {
        String[] arrOTP = opt.split("");
        mBinding.edtOtp1.setText(arrOTP[1]);
       mBinding.edtOtp2.setText(arrOTP[2]);
       mBinding.edtOtp3.setText(arrOTP[3]);
       mBinding.edtOtp4.setText(arrOTP[4]);
    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onResume() {
        /**
         * register broadcast reciver for sms
         */
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        /**
         * unregister broadcast reciver for sms
         */
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }

    /**
     * broadcast reciver for sms
     */
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                final String senderNum = intent.getStringExtra("number");
                if (senderNum.contains("VRIOTP")) {
                    setOTP(message);
                    viewModel.onDoneClick(mBinding.txtTitleOtp);
                }
            }
        }
    };
}
