package in.hvpl.affinita.view.profileplay;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.ActivityProfilePlayBinding;
import in.hvpl.affinita.view.viewprofile.ViewProfileActivity;

public class ProfilePlayActivity extends BaseActivity<ActivityProfilePlayBinding> implements MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener {
    private static final String TAG = ProfilePlayActivity.class.getSimpleName();
    private boolean isFirstTime = true, isPlayingAudio = false;
    //recorded Audio Path
    private String AUDIO_FILE_PATH;

    private MediaPlayer mediaPlayer, demoMediaPlayer;
    // Handler to update UI timer, progress bar etc,.
    private Handler mHandler = new Handler();
    //	private SongsManager songManager;
    private ProfilePlayUtilities utils;
    private ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
    private final static int MAX_VOLUME = 100;
    private int comeFrom;
    private Uri imageUri;
    Handler handler1 = new Handler();
    Handler handler2 = new Handler();
    Runnable runnable1, runnable2;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_profile_play;
    }

    @Override
    public void initUI() {
        mBinding.setIsBoy(App.getInstance().isBoy());
        AUDIO_FILE_PATH = getIntent().getStringExtra(Constants.IntentExtras.AUDIO_FILE_PATH);
        comeFrom = getIntent().getIntExtra(Constants.IntentExtras.COME_FROM, 0);
        String imgUrl = getIntent().getStringExtra(Constants.IntentExtras.IMAGE_PATH);
        File file = new File(imgUrl);
        imageUri = Uri.fromFile(file);
        Tools.printError(TAG, "imageUri=" + imageUri.getEncodedPath());
        setCircularBlurImage(mBinding.imgProfilePicBlur, imageUri, App.getInstance().isBoy() ? ProfilePlayActivity.this.getResources().getDrawable(R.drawable.girl_profile) : ProfilePlayActivity.this.getResources().getDrawable(R.drawable.boy_profile), 0);
        setCircularBlurImage(mBinding.imgProfilePic, imageUri, App.getInstance().isBoy() ? ProfilePlayActivity.this.getResources().getDrawable(R.drawable.girl_profile) : ProfilePlayActivity.this.getResources().getDrawable(R.drawable.boy_profile), 1);



        utils = new ProfilePlayUtilities();




        blink();

        mBinding.llProfilePlay.setOnClickListener(view -> {
            if (demoMediaPlayer != null) {
                if (demoMediaPlayer.isPlaying()) {
                    pause();
                } else {
                    play();
                }
            } else {
                playBackgroundMusic();
                playSong();
                mBinding.imgPlay.setVisibility(View.GONE);
            }
        });
    }


    public void playSong() {
        // Mediaplayer
        mediaPlayer = new MediaPlayer();
        // Listeners
        mBinding.songProgressBar.setOnSeekBarChangeListener(this); // Important
        mediaPlayer.setOnCompletionListener(this); // Important
        // Play song
        try {

            runnable1 = () -> {
                File file = new File(AUDIO_FILE_PATH);
                if (file != null) {
                    try {
                        isPlayingAudio = true;
//                            isAudioPlaying.setValue(true);
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(AUDIO_FILE_PATH);
                        mediaPlayer.prepare();
                        if (mBinding.imgPlay.getVisibility()!=View.VISIBLE){
                            mediaPlayer.start();
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Tools.showToast(ProfilePlayActivity.this, "File Not Found");
                }
            };
            handler1.postDelayed(runnable1, 3000);

            // set Progress bar values
            mBinding.songProgressBar.setProgress(0);
            mBinding.songProgressBar.setMax(100);

            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void playBackgroundMusic() {
        demoMediaPlayer = new MediaPlayer();
        try {
            if (demoMediaPlayer != null) {
                AssetFileDescriptor afd = getAssets().openFd("audio_demo.mp3");
                demoMediaPlayer.reset();
                demoMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());

                demoMediaPlayer.prepare();
                demoMediaPlayer.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void play() {

        if (demoMediaPlayer != null) {
            demoMediaPlayer.start();
            mBinding.imgPlay.setVisibility(View.GONE);
        }

        if (mediaPlayer != null) {
            mediaPlayer.start();
            mBinding.imgPlay.setVisibility(View.GONE);

        }


    }

    private void pause() {
        if (demoMediaPlayer != null) {
            demoMediaPlayer.pause();
            mBinding.imgPlay.setVisibility(View.VISIBLE);
        }

        if (mediaPlayer != null) {
            mediaPlayer.pause();
            mBinding.imgPlay.setVisibility(View.VISIBLE);
        }


    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {

            if (demoMediaPlayer != null) {
                if (isPlayingAudio) {
                    final float volume = (float) (1 - (Math.log(MAX_VOLUME - 30) / Math.log(MAX_VOLUME)));
                    demoMediaPlayer.setVolume(volume, volume);
                } else {
                    final float volume = (float) (1 - (Math.log(MAX_VOLUME - 50) / Math.log(MAX_VOLUME)));
                    demoMediaPlayer.setVolume(volume, volume);
                }
            }

            long totalDuration = mediaPlayer.getDuration();
            long currentDuration = mediaPlayer.getCurrentPosition();
            if (totalDuration >= 0 && currentDuration >= 0 && totalDuration <= 3200000) {
                // Displaying Total Duration time
                mBinding.songTotalDurationLabel.setText(utils.milliSecondsToTimer(totalDuration));
                // Displaying time completed playing
                mBinding.songCurrentDurationLabel.setText(utils.milliSecondsToTimer(currentDuration));

                // Updating progress bar
                int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));
                if (totalDuration - currentDuration > 0 && totalDuration - currentDuration < 5500) {
                    // setCircularBlurImage(mBinding.imgProfilePic,imageUri,App.getInstance().isBoy() ? ProfilePlayActivity.this.getResources().getDrawable(R.drawable.boy_profile) : ProfilePlayActivity.this.getResources().getDrawable(R.drawable.girl_profile),1);
                    mBinding.imgProfilePicBlur.setVisibility(View.INVISIBLE);
                }
                //Log.d("Progress", ""+progress);
                mBinding.songProgressBar.setProgress(progress);
            } else {
                mBinding.songTotalDurationLabel.setText(utils.milliSecondsToTimer(0));
                // Displaying time completed playing
                mBinding.songCurrentDurationLabel.setText(utils.milliSecondsToTimer(0));
            }

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    @Override
    public String getScreenName() {

        if (comeFrom == Constants.ActivityComeFrom.COME_FROM_PROPOSAL || comeFrom == Constants.ActivityComeFrom.COME_FROM_AUDIO_PLAY) {
            return App.getInstance().getUserProfileDetailModel().getBasicInformationModel().getFirstName()
                    + "_" +
                    App.getInstance().getUserProfileDetailModel().getBasicInformationModel().getClientId()
                    + "_VIDEO_PLAY_" +
                    Constants.SCREEN_NAME;
        } else {
            return App.getInstance().getUserDetailModel().get().getBasicInformationModel().getFirstName()
                    + "_" +
                    App.getInstance().getUserDetailModel().get().getBasicInformationModel().getClientId()
                    + "_VIDEO_PLAY_" +
                    Constants.SCREEN_NAME;
        }


    }

    @Override
    public String getClassName() {
        return TAG;
    }

    /**
     * On Song Playing completed
     * if repeat is ON play same song again
     * if shuffle is ON play random song
     */
    @Override
    public void onCompletion(MediaPlayer mp) {
        if (isFirstTime) {
            isFirstTime = false;
        } else {
            isPlayingAudio = false;
            runnable2 = () -> {
                demoMediaPlayer.stop();
                demoMediaPlayer.release();
                if (comeFrom == Constants.ActivityComeFrom.COME_FROM_PROPOSAL) {
                    Intent intent = new Intent(ProfilePlayActivity.this, ViewProfileActivity.class);
                    intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_AUDIO_PLAY);
                    intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE, getIntent().getStringExtra(Constants.IntentExtras.PROPOSAL_TYPE));
                    intent.putExtra(Constants.IntentExtras.PROPOSAL_MATCH_ID, getIntent().getStringExtra(Constants.IntentExtras.PROPOSAL_MATCH_ID));
                    ProfilePlayActivity.this.startActivityWithAnimation(intent);
                    onBackPressed();
                } else {
                    onBackPressed();
                }

//                    btnPlay.setImageDrawable(getDrawable(R.drawable.btn_play));
            };
            handler2.postDelayed(runnable2, 3000);

        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    /**
     * When user starts moving the progress handler
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
//        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
//        mHandler.removeCallbacks(mUpdateTimeTask);
//        int totalDuration = mediaPlayer.getDuration();
//        int demoTotalDuration = demoMediaPlayer.getDuration();
//        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
//        int demoCurruntPosition = utils.progressToTimer(seekBar.getProgress(),demoTotalDuration);
//        // forward or backward to certain seconds
//        mediaPlayer.seekTo(currentPosition);
//        demoMediaPlayer.seekTo(demoCurruntPosition);
//        // update timer progress again
//        updateProgressBar();
    }

    @Override
    public void onDestroy() {
        mHandler.removeCallbacks(mUpdateTimeTask);
        handler1.removeCallbacks(runnable1);
        handler2.removeCallbacks(runnable2);
        if (mediaPlayer != null || demoMediaPlayer != null) {
            mediaPlayer.release();
            demoMediaPlayer.release();
        }
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        pause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        play();
        super.onResume();
    }

    @Override
    public void onBackPressed() {

        if (mediaPlayer != null || demoMediaPlayer != null) {
            mediaPlayer.release();
            demoMediaPlayer.release();
        }
        mHandler.removeCallbacks(mUpdateTimeTask);
        handler1.removeCallbacks(runnable1);
        handler2.removeCallbacks(runnable2);
        mediaPlayer = null;
        demoMediaPlayer = null;
        super.onBackPressed();

    }

    public void setCircularBlurImage(ImageView image, Uri url, Drawable placeholder, int isView) {
        Tools.printError("isView ::::::", isView + "");
        Bitmap myBitmap = BitmapFactory.decodeFile(url.getPath());

        if (isView == 0) {

            Bitmap blurredBitmap = BlurBuilder.blur(this, myBitmap);

            image.setImageBitmap(blurredBitmap);

            /*Glide.with(image.getContext())
                    .load(myBitmap.getNinePatchChunk())
                    .asBitmap()
                    .centerCrop()
                    .transform(new BlurTransformation(image.getContext()))
                    .placeholder(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(new BitmapImageViewTarget(image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(image.getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            image.setImageDrawable(circularBitmapDrawable);
                        }
                    });*/
        } else {


            image.setImageBitmap(myBitmap);

           /* Glide.with(image.getContext())
                    .load(url)
                    .asBitmap()
                    .centerCrop()
                    .placeholder(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(new BitmapImageViewTarget(image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(image.getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            image.setImageDrawable(circularBitmapDrawable);
                        }
                    });*/
        }
    }

    private void blink() {
        final Handler handler = new Handler();
        new Thread(() -> {
            int timeToBlink = 500;    //in milissegunds
            try {
                Thread.sleep(timeToBlink);
            } catch (Exception e) {
            }
            handler.post(() -> {
                if (mBinding.txtBinkMsg.getVisibility() == View.VISIBLE) {
                    mBinding.txtBinkMsg.setVisibility(View.INVISIBLE);
                } else {
                    mBinding.txtBinkMsg.setVisibility(View.VISIBLE);
                }
                blink();
            });
        }).start();
    }

    public static class BlurBuilder {
        private static final float BITMAP_SCALE = 0.4f;
        private static final float BLUR_RADIUS = 17.5f;

        public static Bitmap blur(Context context, Bitmap image) {
            int width = Math.round(image.getWidth() * BITMAP_SCALE);
            int height = Math.round(image.getHeight() * BITMAP_SCALE);

            Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
            Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

            RenderScript rs = RenderScript.create(context);
            ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
            Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
            theIntrinsic.setRadius(BLUR_RADIUS);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);

            return outputBitmap;
        }
    }

}
