package in.hvpl.affinita.view.connection;

import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import in.hvpl.affinita.databinding.RowConnectionBinding;
import in.hvpl.affinita.model.ConnectionModel;
import in.hvpl.affinita.viewmodel.ConnectionViewModel;

/**
 * Created by Ganesh.K on 3/8/17.
 */

public class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.ConnectionHolder> {

    private ConnectionViewModel viewModel;
    private ObservableArrayList<ConnectionModel> connectionModels;

    public ConnectionAdapter(ConnectionViewModel viewModel) {
        this.viewModel = viewModel;
        this.connectionModels=viewModel.connectionModels;
    }

    @Override
    public ConnectionHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ConnectionHolder(RowConnectionBinding.inflate(LayoutInflater.from(viewGroup.getContext()),viewGroup,false));
    }

    @Override
    public void onBindViewHolder(ConnectionHolder connectionHolder, int position) {
        connectionHolder.binding.setViewModel(viewModel);
        connectionHolder.binding.setModel(connectionModels.get(position));
    }

    @Override
    public int getItemCount() {
        return connectionModels.size();
    }


    class ConnectionHolder extends RecyclerView.ViewHolder {

        RowConnectionBinding binding;

        public ConnectionHolder(RowConnectionBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
