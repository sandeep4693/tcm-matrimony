package in.hvpl.affinita.view.viewprofile.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.hvpl.affinita.databinding.RowBasicInfoSliderBinding;
import in.hvpl.affinita.model.ImageIconWithTitleModel;

/**
 * Created by Ganesh.K on 20/7/17.
 */

public class BasicInfoSliderAdapter extends RecyclerView.Adapter<BasicInfoSliderAdapter.BasicInfoSliderViewHolder> {

    private ArrayList<ImageIconWithTitleModel> iconWithTitleModels;

    public BasicInfoSliderAdapter(ArrayList<ImageIconWithTitleModel> iconWithTitleModels) {
        this.iconWithTitleModels = iconWithTitleModels;
    }

    @Override
    public BasicInfoSliderViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new BasicInfoSliderViewHolder(RowBasicInfoSliderBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false));
    }

    @Override
    public void onBindViewHolder(BasicInfoSliderViewHolder basicInfoSliderViewHolder, int position) {
        basicInfoSliderViewHolder.binding.setModel(iconWithTitleModels.get(position));
    }

    @Override
    public int getItemCount() {
        return iconWithTitleModels.size();
    }

    class BasicInfoSliderViewHolder extends RecyclerView.ViewHolder {

        RowBasicInfoSliderBinding binding;

        public BasicInfoSliderViewHolder(RowBasicInfoSliderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
