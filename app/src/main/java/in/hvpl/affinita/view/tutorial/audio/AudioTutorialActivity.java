package in.hvpl.affinita.view.tutorial.audio;

import android.media.MediaPlayer;
import android.os.Handler;
import android.view.View;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.ActivityAudioTutorialBinding;
import neo.architecture.lifecycle.BaseLifecycleActivity;

import static in.hvpl.affinita.common.utils.Constants.IntentExtras.IMAGE_PATH;

public class AudioTutorialActivity extends BaseLifecycleActivity<ActivityAudioTutorialBinding> {

    private static final String TAG = AudioTutorialActivity.class.getSimpleName();
    private MediaPlayer mediaPlayer;
    private Handler seekHandler = new Handler();

    @Override
    public int getContentViewResource() {
        return R.layout.activity_audio_tutorial;
    }

    @Override
    public void initUI() {

        mBinding.setIsBoy(App.getInstance().isBoy());
        mBinding.setImgPath(getIntent().getStringExtra(IMAGE_PATH));

        mediaPlayer = MediaPlayer.create(this, R.raw.audio);
        mBinding.seekBar.setMax(mediaPlayer.getDuration());
        seekUpdation();

        mediaPlayer.setOnCompletionListener(mediaPlayer -> {
            setResult(RESULT_OK);
            onBackPressed();
        });

        mBinding.rlAudioPlay.setOnClickListener(view -> {
            if (SharedPreferencesHelper.isAudioPlay(this)) {
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        stopPlaying();
                    } else {
                        startPlaying();
                    }
                }
            }
        });
    }

    private void stopPlaying() {
        if (mediaPlayer != null)
            mediaPlayer.pause();

        mBinding.txtPlay.setVisibility(View.VISIBLE);
        mBinding.txtMicroPhone.setVisibility(View.GONE);
    }

    private void startPlaying() {
        if (mediaPlayer != null)
            mediaPlayer.start();

        mBinding.txtPlay.setVisibility(View.GONE);
        mBinding.txtMicroPhone.setVisibility(View.VISIBLE);
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.DEMO_AUDIO_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    public void seekUpdation() {

        mBinding.seekBar.setProgress(mediaPlayer.getCurrentPosition());
        seekHandler.postDelayed(run, 1000);
    }

    Runnable run = () -> seekUpdation();

    @Override
    protected void onResume() {
        super.onResume();
        startPlaying();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopPlaying();
    }
}
