package in.hvpl.affinita.view.editprofile;

import android.text.InputType;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.ActivityEditProfileEnterDetailBinding;
import in.hvpl.affinita.viewmodel.EditProfileSelectionViewModel;

public class EditProfileEnterDetailActivity extends BaseActivity<ActivityEditProfileEnterDetailBinding> {
    private static final String TAG = EditProfileEnterDetailActivity.class.getSimpleName();
    private String detail;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_edit_profile_enter_detail;
    }

    @Override
    public void initUI() {
        String title = getIntent().getStringExtra(Constants.IntentExtras.TITLE);
        setToolBar(mBinding.toolBar);
        setToolbarTitle(title);
        detail = this.getIntent().getStringExtra(Constants.IntentExtras.EDIT_DETAIL);
        if (title.equals(getResources().getString(R.string.acticity_title_enter_pin_code))) {
            mBinding.editDetailEt.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        mBinding.setEditedDetail(detail);
        mBinding.setIsBoy(App.getInstance().isBoy());
        mBinding.setSelectionModel(new EditProfileSelectionViewModel());

    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }
}
