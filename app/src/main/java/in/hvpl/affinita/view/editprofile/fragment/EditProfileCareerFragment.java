package in.hvpl.affinita.view.editprofile.fragment;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.Fragment;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.FragmentEditProfileCareerBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.request.CareerRequestModel;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileCareerFragment extends BaseFragment<FragmentEditProfileCareerBinding> {
    private static final String TAG = EditProfileCareerFragment.class.getSimpleName();
    private EditProfileViewModel editProfileViewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_edit_profile_career;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_edit_career));
        editProfileViewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setFragment(this);
        mBinding.setViewModel(editProfileViewModel);
        mBinding.setModel(new CareerRequestModel(SharedPreferencesHelper.getClientId(getContext()),editProfileViewModel.userDetailModel.get().getEducationInformationModel()));
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_CAREER_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            SelectionModel selectionModel = data.getParcelableExtra(Constants.IntentExtras.SELECTION_MODEL);
            String editedDetail = data.getStringExtra(Constants.IntentExtras.EDITED_DETAILS);
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SELECT_SALARY_RANGE:
                    mBinding.getModel().setSalaryRangeId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setSalaryRange(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_DISIGNATION:
                    mBinding.getModel().setDesignationId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setDesignationName(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_FIELD:
                    mBinding.getModel().setFieldId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setFieldName(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_CURRENCY:
                    mBinding.getModel().setCurrencyId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCurrencyTitle(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_COMPANY:
                    mBinding.getModel().setCompanyName(editedDetail);
                    break;
            }
        }
    }
}
