package in.hvpl.affinita.view.editprofile.fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.FragmentEditProfileAboutMeBinding;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileAboutMeFragment extends BaseFragment<FragmentEditProfileAboutMeBinding> {
    private static final String TAG = EditProfileAboutMeFragment.class.getSimpleName();
    private EditProfileViewModel editProfileViewModel;
    @Override
    public int getContentViewResource() {
        return R.layout.fragment_edit_profile_about_me;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_edit_about_me));
        editProfileViewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setAboutMe(editProfileViewModel.userDetailModel.get().getAboutMe());
        mBinding.setViewModel(editProfileViewModel);
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_ABOUT_ME_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }
}
