package in.hvpl.affinita.view;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.ActivitySplashScreenBinding;
import in.hvpl.affinita.view.forceupdate.ForceUpdateService;
import in.hvpl.affinita.view.home.HomeActivity;
import in.hvpl.affinita.view.tutorial.profile.ProfileCreateTutorialActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;

public class SplashScreenActivity extends BaseLifecycleActivity<ActivitySplashScreenBinding> {

    private static final String TAG = SplashScreenActivity.class.getSimpleName();
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 7500;
    private DisplayMetrics metrics;

    @Override
    public int getContentViewResource() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_splash_screen;
    }

    @Override
    protected void onResume() {
        super.onResume();
        playVideo();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mBinding.mViewSplashVideo.stopPlayback();
    }

    private void playVideo() {
        String path = null;

       /* if (metrics.densityDpi >= metrics.DENSITY_LOW && metrics.densityDpi < metrics.DENSITY_MEDIUM) {
            Log.v("VIDEO", "IN LOW");
            path = "android.resource://" + getPackageName() + "/" + R.raw.human_ventures_ldpi;

        } else if (metrics.densityDpi >= metrics.DENSITY_MEDIUM && metrics.densityDpi < metrics.DENSITY_HIGH) {
            Log.v("VIDEO", "IN MEDIUM");
            path = "android.resource://" + getPackageName() + "/" + R.raw.human_ventures_mdpi;

        } else if (metrics.densityDpi >= metrics.DENSITY_HIGH && metrics.densityDpi < metrics.DENSITY_XHIGH) {
            Log.v("VIDEO", "IN HIGH");
            path = "android.resource://" + getPackageName() + "/" + R.raw.human_ventures_hdpi;

        } else if (metrics.densityDpi >= metrics.DENSITY_XHIGH && metrics.densityDpi < metrics.DENSITY_XXHIGH) {
            Log.v("VIDEO", "IN X HIGH");
            path = "android.resource://" + getPackageName() + "/" + R.raw.human_ventures_xhdpi;

        } else if (metrics.densityDpi >= metrics.DENSITY_XXHIGH && metrics.densityDpi < metrics.DENSITY_XXXHIGH) {
            Log.v("VIDEO", "IN XX HIGH");
            path = "android.resource://" + getPackageName() + "/" + R.raw.human_ventures_xxhdpi;

        } else if (metrics.densityDpi >= metrics.DENSITY_XXXHIGH) {
            Log.v("VIDEO", "IN XXX HIGH");
            path = "android.resource://" + getPackageName() + "/" + R.raw.human_ventures_xxhdpi;
        } else {
            path = "android.resource://" + getPackageName() + "/" + R.raw.human_ventures_xxhdpi;
        }*/
        path = "android.resource://" + getPackageName() + "/" + R.raw.human_ventures_xxhdpi;

        mBinding.mViewSplashVideo.setOnPreparedListener(mp -> {
            AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            int maxVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_SYSTEM);
            mp.setVolume(maxVolume, maxVolume);
        });

        mBinding.mViewSplashVideo.setVideoURI(Uri.parse(path));
        mBinding.mViewSplashVideo.start();
    }

    @Override
    public void initUI() {

        /*
         * Showing splash screen with a timer. This will be useful when you
         * want to show case your app logo / company
         */

        metrics = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        mBinding.mViewSplashVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                startService(new Intent(SplashScreenActivity.this, ForceUpdateService.class));
                if (SharedPreferencesHelper.isLogin(SplashScreenActivity.this)) {
                    startActivityWithAnimation(new Intent(SplashScreenActivity.this, HomeActivity.class));
                } else {
                    startActivityWithAnimation(new Intent(SplashScreenActivity.this, ProfileCreateTutorialActivity.class));
                }

                finish();
            }
        });
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.SPLASH_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }
}
