package in.hvpl.affinita.view.album.adapters;

import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import in.hvpl.affinita.App;
import in.hvpl.affinita.databinding.RowAlbumCreateBinding;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.viewmodel.AlbumCreateViewModel;

/**
 * Created by webwerks on 7/7/17.
 */

public class AlbumCreateAdapter extends RecyclerView.Adapter<AlbumCreateAdapter.AlbumCreateViewHolder> {

    private ObservableArrayList<AlbumInformationModel> albumInformationModels;
    private boolean isBoy;
    private AlbumCreateViewModel viewModel;

    public AlbumCreateAdapter(ObservableArrayList<AlbumInformationModel> albumInformationModelArrayList, AlbumCreateViewModel viewModel) {
        this.albumInformationModels = albumInformationModelArrayList;
        isBoy = App.getInstance().isBoy();
        this.viewModel = viewModel;
    }

    @Override
    public AlbumCreateViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        RowAlbumCreateBinding rowAlbumBinding = RowAlbumCreateBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        return new AlbumCreateViewHolder(rowAlbumBinding);
    }

    @Override
    public void onBindViewHolder(AlbumCreateViewHolder albumCreateViewHolder, int position) {
        albumCreateViewHolder.rowAlbumCreateBinding.setIsBoy(isBoy);
        albumCreateViewHolder.rowAlbumCreateBinding.setAlbumModel(albumInformationModels.get(position));
        albumCreateViewHolder.rowAlbumCreateBinding.setPosition(position);
        albumCreateViewHolder.rowAlbumCreateBinding.setViewModel(viewModel);
    }

    @Override
    public int getItemCount() {
        return albumInformationModels.size();
    }

    public class AlbumCreateViewHolder extends RecyclerView.ViewHolder {

        public RowAlbumCreateBinding rowAlbumCreateBinding;

        public AlbumCreateViewHolder(RowAlbumCreateBinding rowAlbumCreateBinding) {
            super(rowAlbumCreateBinding.getRoot());
            this.rowAlbumCreateBinding = rowAlbumCreateBinding;
        }
    }
}
