package in.hvpl.affinita.view.myprofile;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.cocosw.bottomsheet.BottomSheet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.common.widget.EqualSpacingItemDecoration;
import in.hvpl.affinita.databinding.FragmentMyProfileBinding;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.view.album.AlbumCreateActivity;
import in.hvpl.affinita.view.block.BlockActivity;
import in.hvpl.affinita.view.tutorial.audio.AudioTutorialActivity;
import in.hvpl.affinita.view.tutorial.video.VideoTutorialActivity;
import in.hvpl.affinita.view.viewprofile.ViewProfileActivity;
import in.hvpl.affinita.viewmodel.MyProfileViewModel;
import in.hvpl.photo_crop.ImageCropActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static in.hvpl.affinita.common.utils.Constants.ActivityResultRequestCode.REQUEST_RECORD_AUDIO;
import static in.hvpl.affinita.common.utils.Constants.IntentExtras.IMAGE_PATH;
import static in.hvpl.affinita.common.utils.Constants.PermissionRequestCode.RC_CAMERA_AND_STORAGE;
import static in.hvpl.affinita.common.utils.Constants.PermissionRequestCode.RC_RECORD_AUDIO_AND_STORAGE;
import static in.hvpl.affinita.common.utils.Constants.PermissionRequestCode.RC_STORAGE;
import static in.hvpl.affinita.common.utils.Constants.PermissionRequestCode.RC_STORAGE_AUDIO;

public class MyProfileFragment extends BaseFragment<FragmentMyProfileBinding> implements EasyPermissions.PermissionCallbacks, MyProfileAlbumAdapter.OnAlbumClickListener {

    private static final String TAG = MyProfileFragment.class.getSimpleName();
    private static final String AUDIO_FILE_PATH = Constants.FOLDER_PATH + File.separator + "recorded_audio" + Constants.AUDIO_FILE_EXTENTION;
    private static MyProfileAlbumAdapter.OnAlbumClickListener onAlbumClickListener;
    private MyProfileViewModel viewModel;
    private TextView textView;

    @BindingAdapter({"bind:albumList"})
    public static void setAlbumPhoto(RecyclerView recyclerView, ObservableArrayList<AlbumInformationModel> albumInformationModelArrayList) {

        recyclerView.addItemDecoration(new EqualSpacingItemDecoration(4, EqualSpacingItemDecoration.HORIZONTAL));
        if (albumInformationModelArrayList == null)
            albumInformationModelArrayList = new ObservableArrayList<>();
        recyclerView.setAdapter(new MyProfileAlbumAdapter(albumInformationModelArrayList, onAlbumClickListener));

    }

    public static int getAlbumSize(ObservableArrayList<AlbumInformationModel> albumInformationModelArrayList) {
        int albumSize = 0;
        if (albumInformationModelArrayList == null)
            albumInformationModelArrayList = new ObservableArrayList<>();
        int size = albumInformationModelArrayList.size();
        for (int position = 0; position < size; position++) {
            if (!albumInformationModelArrayList.get(position).getAlbumPhotoPath().equals("")) {
                albumSize++;
                if (albumSize == 2) {
                    break;
                }
            }
        }
        return albumSize;
    }

    @BindingAdapter({"bind:albumList"})
    public static void setClick(RelativeLayout relativeLayout, ObservableArrayList<AlbumInformationModel> albumInformationModelArrayList) {
        if (getAlbumSize(albumInformationModelArrayList) >= 2) {
            relativeLayout.setClickable(true);
        } else {
            relativeLayout.setClickable(false);
        }
    }

    @BindingAdapter({"bind:albumList"})
    public static void setVisibilty(ImageView imageView, ObservableArrayList<AlbumInformationModel> albumInformationModelArrayList) {
        if (getAlbumSize(albumInformationModelArrayList) >= 2) {
            imageView.setVisibility(View.GONE);
        } else {
            imageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_my_profile;
    }

    @Override
    public void initUI() {
        onAlbumClickListener = this;
        setToolbarTitle(getString(R.string.activity_title_edit_profile));
        setToolBarVisibility(false);
        viewModel = ViewModelProviders.of(MyProfileFragment.this).get(MyProfileViewModel.class);
        mBinding.setViewModel(viewModel);
        mBinding.setMyProfileFragment(this);

        mBinding.txtTakeSelfie.setOnClickListener(view -> {
            Context context = view.getContext();
            String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (EasyPermissions.hasPermissions(context, perms)) {
                // Already have permission, do the thing
                profilePicClick();

            } else {
                // Do not have permissions, request them now
                String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                EasyPermissions.requestPermissions(this, getString(R.string.rationale_camera),
                        Constants.PermissionRequestCode.RC_CAMERA_AND_STORAGE, permission);
            }
        });

        getBaseActivity().isDataLoaded().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isDataLoad) {

                if (isDataLoad) {
                    /**
                     * manage in baseactivity
                     */
                    if (App.getInstance().getUserDetailModel().get().getStatus() == Constants.AppStatus.BLOCK) {
                        SharedPreferencesHelper.blockUser(getActivity());
                        Intent intent = new Intent(getActivity(), BlockActivity.class);
                        startActivity(intent);
                        getActivity().finish();

                    } else if (App.getInstance().getUserDetailModel().get().getStatus() != Constants.AppStatus.INACTIVE && App.getInstance().getUserDetailModel().get().getStatus() != Constants.AppStatus.ACTIVATION_LINK) {
                        viewModel.isPartnerPreferenceView.set(true);
                        SharedPreferencesHelper.setIsPartnerPreferenceView(getContext(), true);
                        SharedPreferencesHelper.setIsDataUpdated(getContext(), true);
                        SharedPreferencesHelper.setIsVideoPlay(getContext(), true);
                        SharedPreferencesHelper.setIsAudioPlay(getContext(), true);
                        SharedPreferencesHelper.setIsAlbumUpdate(getContext());
                        SharedPreferencesHelper.setIsGameUpdate(getContext());
                        SharedPreferencesHelper.setIsPersonalInfoUpdate(getContext());
                        SharedPreferencesHelper.setIsPartnerPreferenceUpdated(getActivity(), true);
                        SharedPreferencesHelper.setIsPartnerPreferenceUpdate(getContext());
                        SharedPreferencesHelper.setIsViewProfileUpdate(getContext());
                    }

                    Glide.with(getActivity()).load(viewModel.userDetailModel.get().getBasicInformationModel().getPhotoUrl())
                            .thumbnail(0.5f)
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(viewModel.isBoy() ? R.drawable.boy_profile : R.drawable.girl_profile)
                            .into(mBinding.imgProfilePic);

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                    mBinding.rvAlbum.setLayoutManager(linearLayoutManager);
                    // mBinding.rvAlbum.addItemDecoration(new EqualSpacingItemDecoration(16, EqualSpacingItemDecoration.HORIZONTAL));
                    subscribe();

                }
            }
        });
    }

    @Override
    public void onResume() {
        viewModel.isPartnerPreferenceView.set(SharedPreferencesHelper.isPartnerPreferenceView(getContext()));
        viewModel.isProfileInformationView.set(SharedPreferencesHelper.isDataUpdated(getContext()));
        super.onResume();
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_PROFILE;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    private void subscribe() {
        observePermission();
        observeOnClick();
    }

    private void observeOnClick() {
        viewModel.getOnClickCallBack().observe(this, id -> {
            switch (id) {
                case R.id.rvProfilePic:
                    profilePicClick();
                    viewModel.getOnClickCallBack().setValue(0);
                    break;

                case R.id.btnAudioEdit:
                    editAudioClick();
                    viewModel.getOnClickCallBack().setValue(0);
                    break;

                case R.id.view_profile_layout:
                    onViewProfileClick();
                    viewModel.getOnClickCallBack().setValue(0);
                    break;
            }

        });
    }

    private void observePermission() {
        viewModel.getPermissionCallBack().observe(this, requestCode -> {
            switch (requestCode) {
                case Constants.PermissionRequestCode.RC_CAMERA_AND_STORAGE:
                    String[] perms = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    EasyPermissions.requestPermissions(this, getString(R.string.rationale_camera),
                            Constants.PermissionRequestCode.RC_CAMERA_AND_STORAGE, perms);
                    viewModel.getPermissionCallBack().setValue(0);

                    break;

                case RC_RECORD_AUDIO_AND_STORAGE:
                    String[] perms2 = {Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    EasyPermissions.requestPermissions(this, getString(R.string.rationale_record),
                            Constants.PermissionRequestCode.RC_RECORD_AUDIO_AND_STORAGE, perms2);
                    viewModel.getPermissionCallBack().setValue(0);
                    break;

                case RC_STORAGE_AUDIO:
                    String[] perms3 = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    EasyPermissions.requestPermissions(this, getString(R.string.rationale_store_audio),
                            Constants.PermissionRequestCode.RC_STORAGE_AUDIO, perms3);
                    viewModel.getPermissionCallBack().setValue(0);
                    break;
            }


        });
    }

    @AfterPermissionGranted(RC_CAMERA_AND_STORAGE)
    private void profilePicClick() {
        if (SharedPreferencesHelper.isVideoPlay(getContext())) {
            openDialogForVideo();
        } else {
            startTutorialActivity(Constants.ActivityResultRequestCode.TUTORIAL_PROFILE_PIC, getString(R.string.msg_tutorial_demo_video));
        }
    }

    private void openDialogForVideo() {
        //https://android-arsenal.com/details/1/1044
        new BottomSheet.Builder(getActivity()).title("Select").sheet(R.menu.bottom_video_tutorial_menu).listener((dialog, which) -> {
            switch (which) {
                case R.id.takePhoto:
                    takeProfilePic();
                    break;
                case R.id.play_demo:
                    playTutorialVideo();
                    break;
            }
        }).show();

    }

    private void openDialogForAudio() {
        //https://android-arsenal.com/details/1/1044
        new BottomSheet.Builder(getActivity()).title("Select").sheet(R.menu.bottom_audio_tutorial_menu).listener((dialog, which) -> {
            switch (which) {
                case R.id.record_audio:
                    takeAudio();
                    break;
                case R.id.play_audio:
                    playTutorialAudio();
                    break;
            }
        }).show();

    }

    private void playTutorialVideo() {
        Intent intent = new Intent(getContext(), VideoTutorialActivity.class);
        startActivityForResultWithAnimation(intent, Constants.ActivityResultRequestCode.PLAY_VIDEO_TUTORIAL);
    }


    private void takeProfilePic() {
        getmFirebaseAnalytics().setCurrentScreen(getActivity(), Constants.AnalyticsScreenName.TAKE_SELFIE_SCREEN, getClassName());
        Intent intent = new Intent(getContext(), ImageCropActivity.class);
        intent.putExtra("ACTION", Constants.IntentExtras.ACTION_CAMERA);
        startActivityForResultWithAnimation(intent, Constants.ActivityResultRequestCode.CROP_IMAGE_REQUEST);

    }

    @AfterPermissionGranted(RC_RECORD_AUDIO_AND_STORAGE)
    private void editAudioClick() {
        if (SharedPreferencesHelper.isAudioPlay(getContext())) {
            openDialogForAudio();
        } else {
            startTutorialActivity(Constants.ActivityResultRequestCode.TUTORIAL_AUDIO_PLAY, getString(R.string.msg_tutorial_demo_audio));
        }
    }

    @AfterPermissionGranted(RC_STORAGE_AUDIO)
    private void onViewProfileClick() {

        if (SharedPreferencesHelper.getIsViewProfileUpdate(getActivity())) {

            if (!Constants.AUDIO_FILE.exists()) {
                Constants.AUDIO_FILE.mkdir();
            }
            File audioFile = new File(Tools.getAudioFile(getActivity()));
            File image = new File(Tools.getProfilePhoto(getActivity()));
            if (image.exists()) {
                if (audioFile.exists()) {
                    viewProfile();
                } else {
                    viewModel.downloadAudio(getActivity(), audioFile).observe(this, isFileLoad -> {
                        if (isFileLoad) {
                            viewProfile();
                        }
                    });
                }
            } else {
                viewModel.downloadImage(getActivity(), image).observe(this, isFileLoad -> {
                    if (isFileLoad) {
                        if (audioFile.exists()) {
                            viewProfile();
                        } else {
                            viewModel.downloadAudio(getActivity(), audioFile).observe(this, isLoad -> {
                                if (isLoad) {
                                    viewProfile();
                                }
                            });
                        }
                    }
                });
            }
        } else {
            startTutorialActivity(Constants.ActivityResultRequestCode.TUTORIAL_VIEW_PROFILE, getString(R.string.msg_tutorial_view_profile));
        }

    }

    private void viewProfile() {
        Intent intent = new Intent(getContext(), ViewProfileActivity.class);
        intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_VIEW_PROFILE);
        ((BaseLifecycleActivity) getContext())
                .startActivityForResultWithAnimation
                        (intent, Constants.ActivityResultRequestCode.SEND_FOR_APPROVAL);
    }

    private void takeAudio() {

        if (!Constants.AUDIO_FILE.exists()) {
            Constants.AUDIO_FILE.mkdir();
        }
        File audioFile = new File(Tools.getAudioFile(getActivity()));


        AndroidAudioRecorder androidAudioRecorder = AndroidAudioRecorder.with(this)
                // Required
                .setFilePath(AUDIO_FILE_PATH)
                .setColor(ContextCompat.getColor(getContext(), viewModel.isBoy() ? R.color.boy_btn_bg : R.color.girl_btn_bg))
                .setRequestCode(REQUEST_RECORD_AUDIO)
                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_44100)
                .setAutoStart(false)
                .setKeepDisplayOn(true)
                .setOldFilePath(audioFile.getAbsolutePath());

        if (viewModel.userDetailModel.get().getAudioFile().equals("")) {
            // Start recording
            androidAudioRecorder.recordFromFragment();
        } else {
            if (audioFile.exists()) {
                androidAudioRecorder.recordFromFragment();
            } else {
                viewModel.downloadAudio(getActivity(), audioFile).observe(this, isFileLoad -> {
                    if (isFileLoad) {
                        androidAudioRecorder.recordFromFragment();
                    } else {
                        androidAudioRecorder.recordFromFragment();
                    }
                });
            }


        }


        getmFirebaseAnalytics().setCurrentScreen(getActivity(), Constants.AnalyticsScreenName.CREATE_AUDIO_SCREEN, getClassName());

    }


    private void playTutorialAudio() {
        Intent intent = new Intent(getActivity(), AudioTutorialActivity.class);
        intent.putExtra(IMAGE_PATH, viewModel.userDetailModel.get().getBasicInformationModel().getPhotoUrl());
        startActivityForResultWithAnimation(intent, Constants.ActivityResultRequestCode.PLAY_AUDIO_TUTORIAL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.CROP_IMAGE_REQUEST:
                    String imagePath = data.getStringExtra(IMAGE_PATH);
                    uploadProfilePic(imagePath);
                    break;
                case Constants.ActivityResultRequestCode.PLAY_VIDEO_TUTORIAL:
                    SharedPreferencesHelper.setIsVideoPlay(getContext(), true);
                    takeProfilePic();
                    break;

                case Constants.ActivityResultRequestCode.PLAY_AUDIO_TUTORIAL:
                    SharedPreferencesHelper.setIsAudioPlay(getContext(), true);
                    takeAudio();
                    break;

                case REQUEST_RECORD_AUDIO:
                    uploadAudioFile();
                    break;

                case Constants.ActivityResultRequestCode.TUTORIAL_PROFILE_PIC:
                    playTutorialVideo();
                    break;

                case Constants.ActivityResultRequestCode.TUTORIAL_AUDIO_PLAY:
                    playTutorialAudio();
                    break;

                case Constants.ActivityResultRequestCode.TUTORIAL_ALBUM:
                    SharedPreferencesHelper.setIsAlbumUpdate(getContext());
                    startActivityWithAnimation(new Intent(getActivity(), AlbumCreateActivity.class));
                    break;

                case Constants.ActivityResultRequestCode.TUTORIAL_GAME:
                    SharedPreferencesHelper.setIsGameUpdate(getContext());
                    viewModel.startInterestSelectActivity(this);
                    break;

                case Constants.ActivityResultRequestCode.TUTORIAL_PERSONAL_INFO:
                    SharedPreferencesHelper.setIsPersonalInfoUpdate(getContext());
                    viewModel.startEditPersonalInfoActivity(this);
                    break;

                case Constants.ActivityResultRequestCode.TUTORIAL_PARTNER_PREFERENCE:
                    SharedPreferencesHelper.setIsPartnerPreferenceUpdate(getContext());
                    viewModel.onPartnerPrefernceClick(this);
                    break;

                case Constants.ActivityResultRequestCode.TUTORIAL_VIEW_PROFILE:
                    SharedPreferencesHelper.setIsViewProfileUpdate(getContext());
                    onViewProfileClick();
                    break;

                case Constants.ActivityResultRequestCode.GAME_PLAYED:
                    viewModel.startEditPersonalInfoActivity(this);
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);


    }

    private void startTutorialActivity(int resultCode, String msg) {
        viewModel.startTutorialActivity(this, resultCode, msg);
    }

    private void uploadAudioFile() {

        viewModel.uploadAudio(getActivity(), AUDIO_FILE_PATH,
                App.getInstance().getUserDetailModel().get().getBasicInformationModel().getClientId())
                .observe(this, uploadProfilePicResponseModel -> {
                    if (uploadProfilePicResponseModel == null) {
                        //Tools.showToast(getActivity(), getString(R.string.server_error));
                        Tools.showToast(getActivity(), getString(R.string.audio_upload_error));
                    } else {
                        if (uploadProfilePicResponseModel.isSuccess()) {
                            mBinding.imgRlRv.setVisibility(View.GONE);
                            //Tools.showToast(getActivity(), uploadProfilePicResponseModel.getMessage());
                            Tools.showToast(getActivity(), getString(R.string.audio_upload_success));
                            viewModel.userDetailModel.get().setAudioFile(uploadProfilePicResponseModel.getFilePath());
                            App.getInstance().setUserDetailModel(viewModel.userDetailModel.get());

                            try {
                                Tools.copyFile(new File(AUDIO_FILE_PATH), new File(Tools.getAudioFile(getActivity())));
                            } catch (IOException e) {
                                e.printStackTrace();
                                Tools.printError(TAG, "Enter in catch uploadAudioFile");
                            }
                        } else {
                            Tools.showToast(getActivity(), uploadProfilePicResponseModel.getMessage());
                        }
                    }
                });
    }

    private void uploadProfilePic(String imagePath) {

        viewModel.uploadProfilePic(getActivity(), imagePath,
                App.getInstance().getUserDetailModel().get().getBasicInformationModel().getClientId())
                .observe(this, uploadProfilePicResponseModel -> {
                    if (uploadProfilePicResponseModel == null) {
                        //Tools.showToast(getActivity(), getString(R.string.server_error));
                        Tools.showToast(getActivity(), getString(R.string.error_uploading_selfie));
                    } else {
                        if (uploadProfilePicResponseModel.isSuccess()) {
                            viewModel.userDetailModel.get().getBasicInformationModel().setPhotoUrl(uploadProfilePicResponseModel.getFilePath());
                            // mBinding.executePendingBindings();
                            App.getInstance().setUserDetailModel(viewModel.userDetailModel.get());
                            mBinding.imgEdit.setVisibility(View.GONE);
                            Tools.showToast(getActivity(), uploadProfilePicResponseModel.getMessage());
                            try {
                                Tools.copyFile(new File(imagePath), new File(Tools.getProfilePhoto(getActivity())));
                            } catch (IOException e) {
                                e.printStackTrace();
                                Tools.printError(TAG, "Enter in catch uploadAudioFile");
                            }
                        } else {
                            Tools.showToast(getActivity(), uploadProfilePicResponseModel.getMessage());
                        }
                    }
                });

    }


    @AfterPermissionGranted(RC_STORAGE)
    private void moveAlbumCreateActivty() {
        if (SharedPreferencesHelper.getIsAlbumUpdate(getContext())) {
            startActivityWithAnimation(new Intent(getActivity(), AlbumCreateActivity.class));
        } else {
            startTutorialActivity(Constants.ActivityResultRequestCode.TUTORIAL_ALBUM, getString(R.string.msg_tutorial_album));
        }

    }

    @Override
    public void onAlbumClick(int position) {

        if (!viewModel.userDetailModel.get().getAudioFile().isEmpty()) {
            String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
            if (EasyPermissions.hasPermissions(getContext(), perms)) {
                // Already have permission, do the thing
                moveAlbumCreateActivty();

            } else {
                // Do not have permissions, request them now
                EasyPermissions.requestPermissions(this, getString(R.string.rationale_album),
                        RC_STORAGE, perms);

            }
        }


    }
}
