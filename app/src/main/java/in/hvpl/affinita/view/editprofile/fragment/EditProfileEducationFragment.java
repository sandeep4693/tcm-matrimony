package in.hvpl.affinita.view.editprofile.fragment;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.Fragment;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.FragmentEditProfileEducationBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.request.EducationRequestModel;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileEducationFragment extends BaseFragment<FragmentEditProfileEducationBinding> {
    private static final String TAG = EditProfileEducationFragment.class.getSimpleName();
    private EditProfileViewModel editProfileViewModel;
    public EditProfileEducationFragment() {
        // Required empty public constructor
    }

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_edit_profile_education;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_edit_education));
        editProfileViewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setFragment(this);
        mBinding.setViewModel(editProfileViewModel);
        mBinding.setModel(new EducationRequestModel(SharedPreferencesHelper.getClientId(getContext()),editProfileViewModel.userDetailModel.get().getEducationInformationModel()));

    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_EDUCATION_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            SelectionModel selectionModel = data.getParcelableExtra(Constants.IntentExtras.SELECTION_MODEL);
            String editedDetail = data.getStringExtra(Constants.IntentExtras.EDITED_DETAILS);
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SELECT_HIGHEST_EDUCATION:
                    mBinding.getModel().setHighestEducationId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setHighestEducation(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_STREAM:
                    mBinding.getModel().setStream(editedDetail);
                    break;
                case Constants.ActivityResultRequestCode.SELECT_UNIVERSITY:
                    mBinding.getModel().setUniversity(editedDetail);
                    break;
            }
        }
    }
}
