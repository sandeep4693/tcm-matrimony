package in.hvpl.affinita.view.article;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.ActivityArticleListBinding;
import in.hvpl.affinita.model.request.GetArticleRequest;
import in.hvpl.affinita.view.OnLoadMoreListener;
import in.hvpl.affinita.viewmodel.ArticleListViewModel;

public class ArticleListFragment extends BaseFragment<ActivityArticleListBinding> {

    private static final String TAG = ArticleListFragment.class.getCanonicalName();
    Handler handler;
    Context context;
    private ArticleListViewModel viewModel;
    private ArticleRecyclerAdapter articleRecyclerAdapter;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_article_list;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_insight));
        setToolBarVisibility(true);

        viewModel = ViewModelProviders.of(this).get(ArticleListViewModel.class);

        bindArticleList();

        MenuCreateDynamic();

        subscribeData();

        handler = new Handler();

        viewModel.getArticleCategories(getContext());

        mBinding.setCategoryTitle(viewModel.categoryTitle);
    }

    private void LoadMore() {

        articleRecyclerAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                //add null , so the adapter will check view_type and show progress bar at bottom
//                    viewModel.articleCategoryModels.add(null);
//                    articleRecyclerAdapter.notifyItemInserted(viewModel.articleCategoryModels.size() - 1);
                try {
                    if (viewModel.currentPage <= viewModel.lastPage) {
                        Tools.printError(TAG, "viewModel.currentPage=" + viewModel.currentPage);
                        viewModel.getArticle(getContext(), new GetArticleRequest("" + viewModel.currentPage, viewModel.currentCategoryId));
                    } else {
                        Tools.showToast(getContext(), "Loading data completed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void subscribeData() {

        viewModel.getLiveDataArticle().observe(this, articleModels -> {
            closeMenu();
            articleRecyclerAdapter.setLoaded();
            if (articleModels != null) {
                mBinding.tvNoInternet.setVisibility(View.GONE);
                int lastCount = viewModel.articleModels.size();
                viewModel.articleModels.addAll(articleModels);
                articleRecyclerAdapter.notifyDataSetChanged();
                mBinding.menuDialogLaout.rvCategory.getAdapter().notifyDataSetChanged();
                if (lastCount == 0) {
                    mBinding.myrecyclerview.smoothScrollToPosition(1);
//                    mBinding.myrecyclerview.smoothScrollToPosition(0);
                } else {
                    mBinding.myrecyclerview.smoothScrollToPosition(lastCount);
                }

            } else {
                mBinding.tvNoInternet.setVisibility(View.VISIBLE);
            }


        });

        viewModel.getLiveDataCategory().observe(this, articleCategoryModels -> {
            if (articleCategoryModels != null) {
                viewModel.articleCategoryModels.addAll(articleCategoryModels);
                mBinding.menuDialogLaout.rvCategory.getAdapter().notifyDataSetChanged();
            }
        });
    }

    private void bindArticleList() {
        mBinding.myrecyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.myrecyclerview.setHasFixedSize(true);
        articleRecyclerAdapter = new ArticleRecyclerAdapter(getContext(), viewModel.articleModels, viewModel, mBinding.myrecyclerview);
        mBinding.myrecyclerview.setAdapter(articleRecyclerAdapter);
        LoadMore();
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.INSIGHTS_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    private void MenuCreateDynamic() {

        RecyclerView recyclerView = mBinding.menuDialogLaout.rvCategory;
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new ArticleCategoryAdapter(getActivity(), viewModel.articleCategoryModels, viewModel));


        mBinding.articleLatestStories.setOnClickListener(v -> {

            mBinding.articleLatestStories.setEnabled(false);
            if (mBinding.dialogLayout.getVisibility() == View.VISIBLE) {
                closeMenu();
                mBinding.articleLatestStories.setEnabled(true);
            } else {
                AppBarLayout appBarLayout = mBinding.appbar;
                appBarLayout.setExpanded(true, true);
                Animation slide_down = AnimationUtils.loadAnimation(getContext().getApplicationContext(),
                        R.anim.slide_in_down);

                mBinding.dialogLayout.startAnimation(slide_down);
                mBinding.dialogLayout.setVisibility(View.VISIBLE);
                mBinding.articleLatestStories.setEnabled(true);
            }
        });

        mBinding.activityCustomBehaviorHeader.setOnClickListener(v -> {

            // when click on latest story to open menu properly..

            AppBarLayout appBarLayout = mBinding.appbar;
            appBarLayout.setExpanded(true, true);
            if (mBinding.dialogLayout.getVisibility() == View.VISIBLE) {
                closeMenu();
            } else {
                Animation slide_down = AnimationUtils.loadAnimation(getContext().getApplicationContext(),
                        R.anim.slide_in_down);

                mBinding.dialogLayout.startAnimation(slide_down);
                mBinding.dialogLayout.setVisibility(View.VISIBLE);
            }
//
        });

        mBinding.menuDialogLaout.menuClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeMenu();
            }
        });
    }


    public void hideSoftKeyboard() {
        Tools.hideKeyboard(getActivity());
    }

    /*  In menu click on Close ---------Menu close with animation */
    public void closeMenu() {

        if (mBinding.dialogLayout.getVisibility() == View.VISIBLE) {
            mBinding.menuDialogLaout.menuClose.setEnabled(false);

            hideSoftKeyboard();
            Animation slide_up = AnimationUtils.loadAnimation(getActivity().getApplicationContext(),
                    R.anim.slide_out_up);
            slide_up.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                    mBinding.dialogLayout.setVisibility(View.GONE);
                    mBinding.menuDialogLaout.linearMenu.setVisibility(View.VISIBLE);
                    mBinding.menuDialogLaout.menuClose.setEnabled(true);


                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mBinding.dialogLayout.startAnimation(slide_up);
        }

    }


}
