package in.hvpl.affinita.view;

import android.os.Handler;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.ActivityTutorialBinding;

public class TutorialActivity extends BaseActivity<ActivityTutorialBinding> {

    private static final String TAG = TutorialActivity.class.getSimpleName();
    private static int SPLASH_TIME_OUT = 3000;
    private Handler handler;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_tutorial;
    }

    @Override
    public void initUI() {
        mBinding.txtMsg.setText(getIntent().getStringExtra(Constants.IntentExtras.TUTORIAL_MESSAGE));
        mBinding.setIsBoy(App.getInstance().isBoy());

        /*
         * Showing splash screen with a timer. This will be useful when you
         * want to show case your app logo / company
         */
        handler = new Handler();
        handler.postDelayed(() -> {
            // This method will be executed once the timer is over
            // Start your app main activity
            setResult(RESULT_OK);
            onBackPressed();
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onPause() {
        handler.removeCallbacksAndMessages(null);
        super.onPause();
    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }
}
