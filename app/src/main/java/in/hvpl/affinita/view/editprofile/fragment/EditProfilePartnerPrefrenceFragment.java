package in.hvpl.affinita.view.editprofile.fragment;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.List;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.common.widget.CustomNumberPicker;
import in.hvpl.affinita.databinding.FragmentEditProfilePartnerPrefrenceBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.request.PartnerPrefrenceRequestModel;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfilePartnerPrefrenceFragment extends BaseFragment<FragmentEditProfilePartnerPrefrenceBinding> implements CustomNumberPicker.OnNumberSelectionListener {
    private static final String TAG = EditProfilePartnerPrefrenceFragment.class.getSimpleName();
    private EditProfileViewModel editProfileViewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_edit_profile_partner_prefrence;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_edit_partner_preference));
        editProfileViewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setViewModel(editProfileViewModel);
        mBinding.setFragment(this);
        mBinding.setOnNumberSelection(this);
        mBinding.setModel(new PartnerPrefrenceRequestModel(SharedPreferencesHelper.getClientId(getContext()), editProfileViewModel.userDetailModel.get().getPartnerPreferenceDetailsModel()));


        if (editProfileViewModel.userDetailModel.get().getPartnerPreferenceDetailsModel().getAgeMin().equals("")) {
            int min = 0, max = 0;
            int age = Tools.getAge(App.getInstance().getUserDetailModel().get().getBasicInformationModel().getDob());
            if (App.getInstance().isBoy()) {
                min = age - 5;
                max = age - 1;
            } else {
                min = age + 1;
                max = age + 5;
            }

            mBinding.getModel().setFromAge(String.valueOf(min));
            mBinding.getModel().setToAge(String.valueOf(max));

        }


        if (!SharedPreferencesHelper.isPartnerPreferenceUpdated(getContext())) {
            List<SelectionModel> list = editProfileViewModel.getMasterData().getMaritalStatusModels();
            int maritalStatusId = editProfileViewModel.userDetailModel.get().getBasicInformationModel().getMaritalStatusModel().getId();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                if (list.get(i).getId() == maritalStatusId) {
                    mBinding.getModel().setMartialStatus(list.get(i).getTitle());
                    mBinding.getModel().setMartialStatusId("" + list.get(i).getId());
                    break;
                }
            }
        }

    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_PARTNER_PREFERENCE_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            SelectionModel selectionModel = data.getParcelableExtra(Constants.IntentExtras.SELECTION_MODEL);
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SELECT_DIET:
                    mBinding.getModel().setDietId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setDietTitle(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_SMOKING:
                    mBinding.getModel().setSmokingId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setSmoking(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_DRINKING:
                    mBinding.getModel().setDrinkingId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setDrinking(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_MARITAL_STATUS:
                    mBinding.getModel().setMartialStatusId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setMartialStatus(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_COUNTRY:
                    mBinding.getModel().setCountryId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCountry(selectionModel.getTitle());

                    mBinding.getModel().setStateId("" + 0);
                    mBinding.getModel().setState(getString(R.string.doesnt_matter));

                    mBinding.getModel().setCityId("" + 0);
                    mBinding.getModel().setCity(getString(R.string.doesnt_matter));

                  /*  if (selectionModel.getId() == 0) {
                        mBinding.getModel().setStateId("" + 0);
                        mBinding.getModel().setState(getString(R.string.doesnt_matter));

                        mBinding.getModel().setCityId("" + 0);
                        mBinding.getModel().setCity(getString(R.string.doesnt_matter));
                    } else {
                        mBinding.getModel().setStateId("" + 0);
                        mBinding.getModel().setState(getString(R.string.activity_title_select_state));

                        mBinding.getModel().setCityId("" + 0);
                        mBinding.getModel().setCity(getString(R.string.activity_title_select_city));
                    }*/


                    break;

                case Constants.ActivityResultRequestCode.SELECT_STATE:
                    mBinding.getModel().setStateId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setState(selectionModel.getTitle());

                    mBinding.getModel().setCityId("" + 0);
                    mBinding.getModel().setCity(getString(R.string.doesnt_matter));

                   /* if (selectionModel.getId() == 0) {
                        mBinding.getModel().setCityId("" + 0);
                        mBinding.getModel().setCity(getString(R.string.doesnt_matter));
                    } else {
                        mBinding.getModel().setCityId("" + 0);
                        mBinding.getModel().setCity(getString(R.string.activity_title_select_city));
                    }*/


                    break;

                case Constants.ActivityResultRequestCode.SELECT_CITY:
                    mBinding.getModel().setCityId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCity(selectionModel.getTitle());
                    break;

                case Constants.ActivityResultRequestCode.SELECT_RELIGION:
                    mBinding.getModel().setReligionId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setReligion(selectionModel.getTitle());

                    mBinding.getModel().setCasteId("" + 0);
                    mBinding.getModel().setCaste(getString(R.string.activity_title_select_cast));
                    break;

                case Constants.ActivityResultRequestCode.SELECT_CASTE:
                    mBinding.getModel().setCasteId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setCaste(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_MANGLIK:
                    mBinding.getModel().setManglikId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setManglik(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_MOTHER_TONGUE:
                    mBinding.getModel().setMotherTongueId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setMotherTongue(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_HIGHEST_EDUCATION:
                    mBinding.getModel().setEducationId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setEducation(selectionModel.getTitle());
                    break;
            }
        }
    }

    @Override
    public void onNumberSelection(String value1, String value2, int requestCode) {

        switch (requestCode) {
            case Constants.ActivityResultRequestCode.SELECT_AGE:
                mBinding.getModel().setFromAge(value1);
                mBinding.getModel().setToAge(value2);
                break;

            case Constants.ActivityResultRequestCode.SELECT_MIN_HEIGHT:
                mBinding.getModel().setFromFeet(value1);
                mBinding.getModel().setFromInches(value2);
                editProfileViewModel.openNumberPickerDialog(mBinding.view, mBinding.getModel().getToFeet(), mBinding.getModel().getToInches(), Constants.ActivityResultRequestCode.SELECT_MAX_HEIGHT, this, Integer.parseInt(mBinding.getModel().getHeightMin()));

                break;
            case Constants.ActivityResultRequestCode.SELECT_MAX_HEIGHT:
                mBinding.getModel().setToFeet(value1);
                mBinding.getModel().setToInches(value2);
                break;
        }

    }
}
