package in.hvpl.affinita.view.article;

import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Build;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.ActivityArticleViewBinding;
import in.hvpl.affinita.model.ArticleModel;
import in.hvpl.affinita.view.widget.ParallaxScrollView;

public class ArticleDetailActivity extends BaseActivity<ActivityArticleViewBinding> implements ParallaxScrollView.ScrollViewListener {
    private static final String TAG = ArticleDetailActivity.class.getCanonicalName();
    ArticleModel articleModel;
    String imageUrl;

    @Override
    public int getContentViewResource() {
        return R.layout.activity_article_view;
    }

    @Override
    public void initUI() {
        articleModel = (ArticleModel) getIntent().getSerializableExtra(Constants.IntentExtras.ARTICLE_MODEL);
        imageUrl = articleModel.getArticleImgUrl();
        mBinding.linCustomDialog.setVisibility(View.VISIBLE);
        mBinding.dialogCustomLayout.progressbar.getIndeterminateDrawable().setColorFilter(0xFFFF0000, PorterDuff.Mode.SRC_IN);
        mBinding.setModel(articleModel);

        Glide.with(this).load(imageUrl)
                .centerCrop().placeholder(R.drawable.shape_background_article)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mBinding.dialogCustomLayout.dialogImg);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            Slide slide = new Slide(Gravity.BOTTOM);
            slide.addTarget(R.id.dialog_img);

            getWindow().setEnterTransition(slide);
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, (int) getResources().getDimension(R.dimen.dimen_10dp), 0, 0);
            mBinding.backImg.setLayoutParams(lp);
//            mBinding..setPaddingRelative(0, (int) getResources().getDimension(R.dimen.margin10), 0, 0);
        }


        Glide.with(ArticleDetailActivity.this)
                .load(imageUrl).centerCrop().placeholder(R.drawable.shape_background_article)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mBinding.imageView1);

        mBinding.scrollView1.setImageViewToParallax(mBinding.imageView1, mBinding.imageView2);
        mBinding.scrollView1.setScrollViewListener(this);


       /* WebView MyWebView = mBinding.articleActivityWebView;
        MyWebView.getSettings().setJavaScriptEnabled(true);
        MyWebView.getSettings().setDomStorageEnabled(true);
        MyWebView.loadUrl("https://android-developers.googleblog.com/");
        MyWebView.getSettings().setDefaultTextEncodingName("utf-8");
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            MyWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        }
        MyWebView.setWebViewClient(new WebViewClient(){

            public void onPageFinished(WebView view, String url) {
                mBinding.linCustomDialog.setVisibility(View.GONE);
            }

        });*/

        mBinding.articleActivityWebView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                mBinding.linCustomDialog.setVisibility(View.GONE);
                mBinding.articleActivityWebView.setVisibility(View.VISIBLE);
                mBinding.scrollView1.automaticalScroll();
            }

        });

        WebSettings settings = mBinding.articleActivityWebView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");

        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        /*MyWebView.setWebViewClient(new MyBrowser());*/

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            settings.setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        }

        mBinding.articleActivityWebView.loadData(articleModel.getArticleDesc(), "text/html", "UTF-8");


    }

    @Override
    public String getScreenName() {
        return articleModel.getArticleTitle().toUpperCase() + Constants.SCREEN_NAME;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onScrollChanged(ParallaxScrollView scrollView, int x, int y, int oldx, int oldy) {

        if (y >= oldy) {
           /* if (findViewById(R.id.toolbar_view).getVisibility() == View.VISIBLE) {
                findViewById(R.id.toolbar_view).setVisibility(View.GONE);
            }*/
        } else {
            Rect scrollBounds = new Rect();
            mBinding.scrollView1.getHitRect(scrollBounds);
            if (mBinding.imageView1.getLocalVisibleRect(scrollBounds)) {
                //  findViewById(R.id.toolbar_view).setVisibility(View.GONE);
            } else {
                //   findViewById(R.id.toolbar_view).setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public void onBackPressed() {
        if (mBinding.linCustomDialog.getVisibility() == View.VISIBLE) {
        } else {
            super.onBackPressed();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            } else {

            }
        }
    }

}
