package in.hvpl.affinita.view.editprofile;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import in.hvpl.affinita.App;
import in.hvpl.affinita.databinding.RowEditProfileMenuBinding;
import in.hvpl.affinita.model.ImageIconWithTitleModel;

/**
 * Created by webwerks1 on 18/7/17.
 */

public class EditProfileAdapter extends RecyclerView.Adapter<EditProfileAdapter.EditProfileViewHolder> {

    private List<ImageIconWithTitleModel> categoryList;
    private boolean isBoy;

    public EditProfileAdapter(List<ImageIconWithTitleModel> categoryList){
        this.categoryList = categoryList;
        this.isBoy= App.getInstance().isBoy();
    }
    @Override
    public EditProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new EditProfileViewHolder(RowEditProfileMenuBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false));
    }

    @Override
    public void onBindViewHolder(EditProfileViewHolder holder, int position) {
        ImageIconWithTitleModel imageIconWithTitleModel = categoryList.get(position);
        holder.rowEditProfileBinding.setIsBoy(isBoy);
        holder.rowEditProfileBinding.setModel(imageIconWithTitleModel);

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public class EditProfileViewHolder extends RecyclerView.ViewHolder{
        public RowEditProfileMenuBinding rowEditProfileBinding;

        public EditProfileViewHolder(RowEditProfileMenuBinding rowEditProfileBinding) {
            super(rowEditProfileBinding.getRoot());
            this.rowEditProfileBinding = rowEditProfileBinding ;
        }
    }
}
