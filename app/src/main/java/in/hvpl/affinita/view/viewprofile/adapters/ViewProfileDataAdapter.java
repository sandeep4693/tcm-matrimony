package in.hvpl.affinita.view.viewprofile.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.hvpl.affinita.databinding.RowViewProfileBinding;
import in.hvpl.affinita.model.ViewProfileModel;
import in.hvpl.affinita.viewmodel.ViewProfileViewModel;

/**
 * Created by Ganesh.K on 21/7/17.
 */

public class ViewProfileDataAdapter extends RecyclerView.Adapter<ViewProfileDataAdapter.ViewProfileViewHolder> {

    private ArrayList<ViewProfileModel> viewProfileModels;
    private ViewProfileViewModel viewProfileViewModel;
    private boolean isBoy;

    public ViewProfileDataAdapter(ArrayList<ViewProfileModel> viewProfileModels, ViewProfileViewModel viewProfileViewModel, boolean isBoy) {
        this.viewProfileModels = viewProfileModels;
        this.viewProfileViewModel = viewProfileViewModel;
        this.isBoy = isBoy;
    }

    @Override
    public ViewProfileViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewProfileViewHolder(RowViewProfileBinding.inflate(LayoutInflater.from(viewGroup.getContext()), viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewProfileViewHolder viewProfileViewHolder, int position) {
        viewProfileViewHolder.rowViewProfileBinding.setIsBoy(isBoy);
        viewProfileViewHolder.rowViewProfileBinding.setModel(viewProfileModels.get(position));
        viewProfileViewHolder.rowViewProfileBinding.setViewModel(viewProfileViewModel);
    }

    @Override
    public int getItemCount() {
        return viewProfileModels.size();
    }

    public class ViewProfileViewHolder extends RecyclerView.ViewHolder {

        RowViewProfileBinding rowViewProfileBinding;

        public ViewProfileViewHolder(RowViewProfileBinding rowViewProfileBinding) {
            super(rowViewProfileBinding.getRoot());
            this.rowViewProfileBinding = rowViewProfileBinding;
        }
    }
}
