package in.hvpl.affinita.view.tutorial.profile;

import android.arch.lifecycle.ViewModelProviders;
import android.view.WindowManager;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.ActivityProfileCreateTutorialBinding;
import in.hvpl.affinita.viewmodel.ProfileCreateTutorialViewModel;
import neo.architecture.lifecycle.BaseLifecycleActivity;


public class ProfileCreateTutorialActivity extends BaseLifecycleActivity<ActivityProfileCreateTutorialBinding> {
    private static final String TAG = ProfileCreateTutorialActivity.class.getSimpleName();
    private ProfileCreateTutorialViewModel viewModel;

    @Override
    public int getContentViewResource() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return R.layout.activity_profile_create_tutorial;
    }

    @Override
    public void initUI() {
        viewModel = ViewModelProviders.of(this).get(ProfileCreateTutorialViewModel.class);
        mBinding.setViewModel(viewModel);
        setPhotoViewPager();
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.TUTORIAL_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    public void setPhotoViewPager() {
        mBinding.viewPagerPhoto.setAdapter(new ProfileCreateTutorialAdapter());
        Tools.setUpViewPagerIndicator(mBinding.viewPagerPhoto, mBinding.viewPagerCountDots, viewModel.position,viewModel.msg,true);

    }
}
