package in.hvpl.affinita.view.editprofile;

import android.arch.lifecycle.ViewModelProviders;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.ActivityEditProfileBinding;
import in.hvpl.affinita.view.editprofile.fragment.EditProfileMenuFragment;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

import static neo.architecture.lifecycle.FragmentsManagerHelper.REPLACE_FRAGMENT;

public class EditProfileActivity extends BaseActivity<ActivityEditProfileBinding> {

    private static final String TAG = EditProfileActivity.class.getSimpleName();
    private EditProfileViewModel editProfileViewModel;


    @Override
    public int getContentViewResource() {
        return R.layout.activity_edit_profile;
    }

    @Override
    public void initUI() {
        editProfileViewModel = ViewModelProviders.of(this).get(EditProfileViewModel.class);
        editProfileViewModel.setMasterData(getIntent().getParcelableExtra(Constants.IntentExtras.MASTER_DATA));
        setToolBar(mBinding.toolBar);
        String title = getIntent().getStringExtra(Constants.IntentExtras.TITLE);
        if(title != null){
            editProfileViewModel.onCategoryClick(EditProfileActivity.this,title,false);
        }else{
            setToolbarTitle(getString(R.string.activity_title_profile_info));
            fragmentTransaction(REPLACE_FRAGMENT,new EditProfileMenuFragment(),R.id.container_edit_profile,false);
        }

    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.PERSONAL_PROFILE_INFO;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

}
