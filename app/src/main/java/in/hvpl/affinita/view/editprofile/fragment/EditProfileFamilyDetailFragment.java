package in.hvpl.affinita.view.editprofile.fragment;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.app.Fragment;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.FragmentFamilyDetailBinding;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.request.FamilyInfoRequestModel;
import in.hvpl.affinita.viewmodel.EditProfileViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFamilyDetailFragment extends BaseFragment<FragmentFamilyDetailBinding> {
    private static final String TAG = EditProfileFamilyDetailFragment.class.getSimpleName();

    private EditProfileViewModel editProfileViewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_family_detail;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_edit_family_details));
        editProfileViewModel = ViewModelProviders.of(getActivity()).get(EditProfileViewModel.class);
        mBinding.setFragment(this);
        mBinding.setViewModel(editProfileViewModel);
        mBinding.setModel(new FamilyInfoRequestModel(SharedPreferencesHelper.getClientId(getContext()), editProfileViewModel.userDetailModel.get().getParentInformationModels()));
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.EDIT_FAMILY_DETAILS_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            SelectionModel selectionModel = data.getParcelableExtra(Constants.IntentExtras.SELECTION_MODEL);
            String editedDetail = data.getStringExtra(Constants.IntentExtras.EDITED_DETAILS);
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SELECT_OCCUPATION_TYPE_FATHER:
                    mBinding.getModel().setOccupationTypeFatherId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setOccupationTypeFather(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_FIELD:
                    mBinding.getModel().setFieldId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setField(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_DISIGNATION:
                    mBinding.getModel().setDesignationId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setDesignation(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_HIGHEST_EDUCATION:
                    mBinding.getModel().setEducationId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setEducation(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_OCCUPATION_TYPE_MOTHER:
                    mBinding.getModel().setOccupationTypeMotherId(String.valueOf(selectionModel.getId()));
                    mBinding.getModel().setOccupationTypeMother(selectionModel.getTitle());
                    break;
                case Constants.ActivityResultRequestCode.SELECT_STREAM:
                    mBinding.getModel().setStream(editedDetail);
                    break;

                case Constants.ActivityResultRequestCode.FATHER_NAME:
                    mBinding.getModel().setFatherName(editedDetail);
                    break;
            }
        }
    }
}
