package in.hvpl.affinita.view.forceupdate;

import android.content.Intent;
import android.view.View;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.databinding.ActivityForceUpdateBinding;
import in.hvpl.affinita.view.registartion.RegistrationActivity;

public class ForceUpdateActivity extends BaseActivity<ActivityForceUpdateBinding> {

    private static final String TAG = ForceUpdateActivity.class.getSimpleName();

    @Override
    public int getContentViewResource() {

        return R.layout.activity_force_update;
    }

    @Override
    public void initUI() {
        this.setFinishOnTouchOutside(false);
        mBinding.setActivity(this);
        mBinding.txtMsg.setText(getIntent().getStringExtra(Constants.IntentExtras.FORCE_UPDATE_MSG));
        if (getIntent().getIntExtra(Constants.IntentExtras.FORCE_UPDATE, 0) == 1) {
            mBinding.txtLater.setVisibility(View.GONE);
        }
    }


    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    public void onLaterClick() {
        finish();
    }

    public void onUpdateClick() {

        if (getIntent().getIntExtra(Constants.IntentExtras.FORCE_UPDATE, 0) == 1) {
            SharedPreferencesHelper.blockUser(this);
            Intent intent = new Intent(this, RegistrationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
    }
}
