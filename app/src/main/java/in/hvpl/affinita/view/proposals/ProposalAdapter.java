package in.hvpl.affinita.view.proposals;

import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import in.hvpl.affinita.databinding.RowProposalBinding;
import in.hvpl.affinita.model.ProposalModel;
import in.hvpl.affinita.viewmodel.ProposalViewModel;

/**
 * Created by webwerks1 on 18/7/17.
 */

public class ProposalAdapter extends RecyclerView.Adapter<ProposalAdapter.ProposalViewHolder> {

    private ObservableArrayList<ProposalModel> proposalModels;
    private ProposalViewModel viewModel;

    public ProposalAdapter(ObservableArrayList<ProposalModel> proposalModelObservableArrayList,ProposalViewModel proposalViewModel){
        this.proposalModels = proposalModelObservableArrayList;
        this.viewModel = proposalViewModel;
    }


    @Override
    public ProposalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RowProposalBinding rowProposalBinding = RowProposalBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ProposalViewHolder(rowProposalBinding);
    }

    @Override
    public void onBindViewHolder(ProposalViewHolder holder, int position) {
        holder.rowProposalBinding.setModel(proposalModels.get(position));
        holder.rowProposalBinding.setViewModel(viewModel);
    }

    @Override
    public int getItemCount() {
        return proposalModels.size();
    }

    public class ProposalViewHolder extends RecyclerView.ViewHolder {
        public RowProposalBinding rowProposalBinding;

        public ProposalViewHolder(RowProposalBinding rowProposalBinding) {
            super(rowProposalBinding.getRoot());
            this.rowProposalBinding =rowProposalBinding;
        }
    }
}
