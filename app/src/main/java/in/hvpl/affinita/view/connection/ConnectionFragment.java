package in.hvpl.affinita.view.connection;


import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.databinding.FragmentConnectionBinding;
import in.hvpl.affinita.viewmodel.ConnectionViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConnectionFragment extends BaseFragment<FragmentConnectionBinding> {

    private static final String TAG = ConnectionFragment.class.getSimpleName();
    private ConnectionViewModel viewModel;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_connection;
    }

    @Override
    public void initUI() {
        setToolbarTitle(getString(R.string.activity_title_connection));
        setToolBarVisibility(true);
        viewModel = ViewModelProviders.of(this).get(ConnectionViewModel.class);
        mBinding.setViewModel(viewModel);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        mBinding.rvConnection.setLayoutManager(manager);
        mBinding.rvConnection.setAdapter(new ConnectionAdapter(viewModel));

        viewModel.isConnectionListRefresh.observe(this, isDataLoad -> {
            mBinding.rvConnection.getAdapter().notifyDataSetChanged();
        });
    }


    @Override
    public void onResume() {
        viewModel.getConnectionList(getContext(), false);
        super.onResume();
    }

    @Override
    public String getScreenName() {
        return Constants.AnalyticsScreenName.CONNECTION_SCREEN;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver((mMessageReceiver),
                new IntentFilter(Constants.ActionIntent.NEW_MESSAGE)
        );

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver((mAcceptRequestReceiver),
                new IntentFilter(Constants.ActionIntent.ACCEPT_REQUEST)
        );
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mAcceptRequestReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            viewModel.getConnectionList(getContext(), true);
        }
    };

    private BroadcastReceiver mAcceptRequestReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            viewModel.getConnectionList(getContext(), true);
        }
    };
}
