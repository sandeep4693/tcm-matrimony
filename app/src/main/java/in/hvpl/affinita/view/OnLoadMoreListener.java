package in.hvpl.affinita.view;

/**
 * Created by webwerks1 on 13/7/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
