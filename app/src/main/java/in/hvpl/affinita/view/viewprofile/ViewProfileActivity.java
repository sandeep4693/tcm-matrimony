package in.hvpl.affinita.view.viewprofile;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.ActivityViewProfileBinding;
import in.hvpl.affinita.interfaces.DialogClickListener;
import in.hvpl.affinita.view.viewprofile.adapters.BasicInfoSliderAdapter;
import in.hvpl.affinita.view.viewprofile.adapters.PhotoAdapter;
import in.hvpl.affinita.view.viewprofile.adapters.ViewProfileDataAdapter;
import in.hvpl.affinita.viewmodel.ViewProfileViewModel;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public class ViewProfileActivity extends BaseActivity<ActivityViewProfileBinding> implements EasyPermissions.PermissionCallbacks {

    private static final String TAG = ViewProfileActivity.class.getSimpleName();
    private String screenName = "";
    private ViewProfileViewModel viewModel;
    private Menu menu;
    private int activityComeFrom = 0;
    private boolean isShowConnectPopup = false;


    @Override
    public int getContentViewResource() {
        return R.layout.activity_view_profile;
    }

    @Override
    public String getScreenName() {
        return screenName;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void initUI() {
        isShowConnectPopup = false;
        setToolBar(mBinding.toolBar);
        setToolbarTitle(getString(R.string.activity_title_view_profile));
        viewModel = ViewModelProviders.of(this).get(ViewProfileViewModel.class);
        viewModel.activityComeFrom = getIntent().getIntExtra(Constants.IntentExtras.COME_FROM, 0);
        mBinding.setTitle(getString(R.string.basic_information));
        mBinding.setViewModel(viewModel);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        mBinding.flAlbum.getLayoutParams().width = width;
        mBinding.flAlbum.getLayoutParams().height = width;

        screenName = TAG;

        switch (viewModel.activityComeFrom) {
            case Constants.ActivityComeFrom.COME_FROM_AUDIO_PLAY:
            case Constants.ActivityComeFrom.COME_FROM_PROPOSAL:
                viewModel.setUserDetailModel(App.getInstance().getUserProfileDetailModel());
                viewModel.setAudioFilePath(Tools.getProfileAudioFile());
                viewModel.setImagePath(Tools.getProposalPhoto());
                setToolbarTitle(viewModel.userDetailModel.getBasicInformationModel().getFirstName());
                setAlbumViewPager();
                setBasicInfoSlider();
                setViewProfileData();
                screenName = viewModel.userDetailModel.getBasicInformationModel().getFirstName()
                        + "_" +
                        viewModel.userDetailModel.getBasicInformationModel().getClientId()
                        + Constants.SCREEN_NAME;

                setCurrentScreenName();
                break;

            case Constants.ActivityComeFrom.COME_FROM_PARTNER_PREFERENCE:
                setToolbarTitle(getString(R.string.activity_title_partner_preference));
                mBinding.flAlbum.setVisibility(View.GONE);
                viewModel.setUserDetailModel(App.getInstance().getUserDetailModel().get());
                setViewProfileData();
                screenName = Constants.AnalyticsScreenName.PARTNER_PREFERNCE_SCREEN;
                setCurrentScreenName();
                break;

            case Constants.ActivityComeFrom.COME_FROM_VIEW_PROFILE:
                viewModel.setUserDetailModel(App.getInstance().getUserDetailModel().get());
                viewModel.setAudioFilePath(Tools.getAudioFile(this));
                viewModel.setImagePath(Tools.getProfilePhoto(this));
                setToolbarTitle(viewModel.userDetailModel.getBasicInformationModel().getFirstName());
                setAlbumViewPager();
                setBasicInfoSlider();
                setViewProfileData();
                int status = viewModel.userDetailModel.getStatus();
                if (status == Constants.AppStatus.INACTIVE
                        || status == Constants.AppStatus.CANT_APPROVE
                        || status == Constants.AppStatus.UPDATE_REQUEST
                        || status == Constants.AppStatus.ACTIVATION_LINK) {
                    viewModel.isApprove.set(false);
                }

                screenName = viewModel.userDetailModel.getBasicInformationModel().getFirstName()
                        + "_" +
                        viewModel.userDetailModel.getBasicInformationModel().getClientId()
                        + Constants.SCREEN_NAME;
                setCurrentScreenName();
                break;

        }


    }


    private void setAlbumViewPager() {
        mBinding.viewPagerPhoto.setAdapter(new PhotoAdapter(viewModel.createAlbumList()));
        if (viewModel.createAlbumList().size() > 0)
            Tools.setUpViewPagerIndicator(mBinding.viewPagerPhoto, mBinding.viewPagerCountDots, null);
    }

    private void setBasicInfoSlider() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
//        linearLayoutManager.scrollToPositionWithOffset(mBinding.rvSliderInfo.get, 0);

        mBinding.rvSliderInfo.setLayoutManager(linearLayoutManager);
        mBinding.rvSliderInfo.setAdapter(new BasicInfoSliderAdapter(viewModel.createBasicSliderList()));
    }

    private void setViewProfileData() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        mBinding.scrollView.setNestedScrollingEnabled(false);
        mBinding.rvViewProfile.setLayoutManager(linearLayoutManager);
        mBinding.rvViewProfile.setAdapter(new ViewProfileDataAdapter(viewModel.getViewProfileList(this), viewModel, App.getInstance().isBoy()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.VIEW_PROFILE_SLIDER:
                    viewModel.setUserDetailModel(App.getInstance().getUserDetailModel().get());
                    setBasicInfoSlider();
                    break;
                case Constants.ActivityResultRequestCode.VIEW_PROFILR_LIST:
                    viewModel.setUserDetailModel(App.getInstance().getUserDetailModel().get());
                    setViewProfileData();
                    break;

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_profile_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_connect) {
            onConnect(Constants.ConnectOperation.CONNECT);
        } else if (id == R.id.action_request) {
            CharSequence options[] = getResources().getStringArray(R.array.connect_option);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Select");
            builder.setItems(options, (dialog, which) -> {
                // the user clicked on colors[which]
                switch (which) {
                    case 0:
                        onConnect(Constants.ConnectOperation.ACCEPT_REQUEST);
                        break;

                    case 1:
                        onConnect(Constants.ConnectOperation.LATER);
                        break;

                    case 2:
                        Tools.showAlertDialog(this,
                                "",
                                getString(R.string.alert_msg_not_interested),
                                "Yes", "No", 0, new DialogClickListener() {
                                    @Override
                                    public void onPositiveClick(int requestCode) {
                                        onConnect(Constants.ConnectOperation.NOT_INTERESTED);
                                    }

                                    @Override
                                    public void onNegativeClick(int requestCode) {

                                    }
                                });
                        break;

                }
            });
            builder.show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void onConnect(int operation) {

        viewModel.onConnect(this, SharedPreferencesHelper.getClientId(this), getIntent().getStringExtra(Constants.IntentExtras.PROPOSAL_MATCH_ID), operation)
                .observe(this, operationId -> {
                    MenuItem btnConnect = menu.findItem(R.id.action_connect);
                    MenuItem btnRequest = menu.findItem(R.id.action_request);
                    switch (operationId) {
                        case Constants.ConnectOperation.CONNECT:
                            getmFirebaseAnalytics().logEvent(Constants.AnalyticsEventKey.YOU_LIKE, null);
                            btnConnect.setVisible(false);
                            btnRequest.setVisible(false);
                            isShowConnectPopup = false;
                            //Tools.showToast(this, getString(R.string.view_profile_connect_success_response));
                            break;
                        case Constants.ConnectOperation.ACCEPT_REQUEST:
                            getmFirebaseAnalytics().logEvent(Constants.AnalyticsEventKey.MUTUAL_CONNECTION, null);
                            btnConnect.setVisible(false);
                            btnRequest.setVisible(false);
                            isShowConnectPopup = false;
                            break;

                        case Constants.ConnectOperation.LATER:

                            break;

                        case Constants.ConnectOperation.NOT_INTERESTED:
                            onBackPressed();
                            break;
                    }
                });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        updateMenuTitle();

        return super.onPrepareOptionsMenu(menu);
    }

    private void updateMenuTitle() {
        if (menu != null) {
            MenuItem btnConnect = menu.findItem(R.id.action_connect);
            MenuItem btnRequest = menu.findItem(R.id.action_request);


            SpannableString s = new SpannableString(btnConnect.getTitle());
            s.setSpan(new ForegroundColorSpan(App.getInstance().isBoy() ? ContextCompat.getColor(this, R.color.boy_bottom_menu_select) : ContextCompat.getColor(this, R.color.girl_bottom_menu_select)), 0, s.length(), 0);
            btnConnect.setTitle(s);
            int comeFrom=getIntent().getIntExtra(Constants.IntentExtras.COME_FROM, 0);
            if (comeFrom == Constants.ActivityComeFrom.COME_FROM_AUDIO_PLAY ||
                    comeFrom==Constants.ActivityComeFrom.COME_FROM_PROPOSAL) {
                String proposalType = getIntent().getStringExtra(Constants.IntentExtras.PROPOSAL_TYPE);
                switch (proposalType) {
                    case Constants.ProposalType.LATEST_MATCH:
                        btnConnect.setVisible(true);
                        btnRequest.setVisible(false);
                        isShowConnectPopup = true;
                        break;
                    case Constants.ProposalType.WHO_YOU_LIKE:
                        btnConnect.setVisible(false);
                        btnRequest.setVisible(true);
                        break;
                    case Constants.ProposalType.RECENT_VIEW:
                        btnConnect.setVisible(true);
                        btnRequest.setVisible(false);
                        break;

                    default:
                        btnConnect.setVisible(false);
                        btnRequest.setVisible(false);
                        break;
                }
            } else {
                btnConnect.setVisible(false);
                btnRequest.setVisible(false);
            }
        }
    }

    @Override
    public void onBackPressed() {

        /*if (menu != null) {
            MenuItem btnConnect = menu.findItem(R.id.action_connect);

            if (getIntent().getIntExtra(Constants.IntentExtras.COME_FROM, 0) == Constants.ActivityComeFrom.COME_FROM_AUDIO_PLAY) {
                String proposalType = getIntent().getStringExtra(Constants.IntentExtras.PROPOSAL_TYPE);
                switch (proposalType) {
                    case Constants.ProposalType.LATEST_MATCH:
                        //btnConnect.setVisible(true);
                        if(btnConnect.isVisible()){

                        }

                        break;
                }
            }
        }*/
        if (isShowConnectPopup) {
            Tools.showToast(this, this.getString(R.string.dialog_msg_profile_viewed_without_connect));
        }
        super.onBackPressed();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }
}

/**
 * {notificationData={"notification_type":"action","icon":"myicon","notificationId":1504788331,"body":"We have added Akila to your proposals","title":"Notification"}}
 */