package in.hvpl.affinita.view.myprofile;

import android.databinding.ObservableArrayList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import in.hvpl.affinita.App;
import in.hvpl.affinita.databinding.RowAlbumBinding;
import in.hvpl.affinita.model.AlbumInformationModel;

/**
 * Created by webwerks on 26/6/17.
 */

public class MyProfileAlbumAdapter extends RecyclerView.Adapter<MyProfileAlbumAdapter.AlbumViewHolder> {

    private ObservableArrayList<AlbumInformationModel> albumInformationModels;
    private boolean isBoy;
    private OnAlbumClickListener onAlbumClickListener;


    public MyProfileAlbumAdapter(ObservableArrayList<AlbumInformationModel> albumInformationModels, OnAlbumClickListener onAlbumClickListener) {
        if (albumInformationModels == null)
            albumInformationModels = new ObservableArrayList<>();
        this.albumInformationModels = albumInformationModels;
        isBoy = App.getInstance().isBoy();
        this.onAlbumClickListener = onAlbumClickListener;
    }

    @Override
    public AlbumViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        RowAlbumBinding rowAlbumBinding = RowAlbumBinding.inflate(LayoutInflater.from(viewGroup.getContext()));
       /* int height = viewGroup.getMeasuredHeight() - 50;
        rowAlbumBinding.imgAlbum.setMinimumHeight(height);
        rowAlbumBinding.imgAlbum.setMinimumWidth(height);*/

        return new AlbumViewHolder(rowAlbumBinding);
    }

    @Override
    public void onBindViewHolder(AlbumViewHolder albumViewHolder, int position) {
        albumViewHolder.rowAlbumBinding.setAlbumModel(albumInformationModels.get(position));
        albumViewHolder.rowAlbumBinding.setIsBoy(isBoy);

        albumViewHolder.rowAlbumBinding.rvImgAlbum.setOnClickListener(view -> {
            onAlbumClickListener.onAlbumClick(position);
        });
    }

    @Override
    public int getItemCount() {
        return albumInformationModels.size();
    }

    public interface OnAlbumClickListener {
        void onAlbumClick(int position);
    }

    public static class AlbumViewHolder extends RecyclerView.ViewHolder {

        public RowAlbumBinding rowAlbumBinding;

        public AlbumViewHolder(RowAlbumBinding rowAlbumBinding) {
            super(rowAlbumBinding.getRoot());
            this.rowAlbumBinding = rowAlbumBinding;

        }
    }

}
