package in.hvpl.affinita.view.album.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;

import famework.neo.instagramhelper.InstagramHelper;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.widget.GridSpacingItemDecoration;
import in.hvpl.affinita.databinding.FragmentInstagramGalleryBinding;
import in.hvpl.affinita.view.album.adapters.GalleryAdapter;
import in.hvpl.affinita.viewmodel.AlbumCreateViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstagramGalleryFragment extends BaseFragment<FragmentInstagramGalleryBinding> {

    private static final String TAG = InstagramGalleryFragment.class.getSimpleName();

    private AlbumCreateViewModel viewModel;
    // create boolean for fetching data
    private boolean _hasLoadedOnce = false;

    @Override
    public int getContentViewResource() {
        return R.layout.fragment_instagram_gallery;
    }

    @Override
    public void initUI() {
        viewModel = ViewModelProviders.of(getActivity()).get(AlbumCreateViewModel.class);
        mBinding.setViewModel(viewModel);
        viewModel.hasInstaAccessToken.set(InstagramHelper.getInstance(getContext()).hasAccessToken());
        bindImageList();

        /*if(!_hasLoadedOnce){
            getInstaGallery();
        }*/
    }

    @Override
    public String getScreenName() {
        return null;
    }

    @Override
    public String getClassName() {
        return TAG;
    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);

        if (this.isVisible()) {
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                getInstaGallery();
                _hasLoadedOnce = true;
            }
        }
    }

    private void bindImageList() {
        int columns = 3;
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), columns);
        mBinding.rvGallery.setLayoutManager(layoutManager);
        mBinding.rvGallery.setHasFixedSize(true);
        layoutManager.setSpanCount(columns);
        GridSpacingItemDecoration itemOffsetDecoration = new GridSpacingItemDecoration(columns, getResources().getDimensionPixelSize(R.dimen.item_padding), false);
        mBinding.rvGallery.addItemDecoration(itemOffsetDecoration);
        mBinding.rvGallery.setAdapter(new GalleryAdapter(viewModel.instagramAlbumList,viewModel));

    }

    private void getInstaGallery() {

        if (InstagramHelper.getInstance(getContext()).hasAccessToken()){
            viewModel.getInstagramImages(getBaseLifeCycleActivity()).observe(this, isDataLoad -> {
                if (isDataLoad) {
                    mBinding.rvGallery.getAdapter().notifyDataSetChanged();
                }
            });
        }
    }


}
