package in.hvpl.affinita.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.util.Log;
import android.view.View;

import com.yayandroid.parallaxrecyclerview.ParallaxImageView;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.ArticleCategoryModel;
import in.hvpl.affinita.model.ArticleModel;
import in.hvpl.affinita.model.request.GetArticleRequest;
import in.hvpl.affinita.model.response.ArticleCategoryResponseModel;
import in.hvpl.affinita.model.response.ArticleListResponseModel;
import in.hvpl.affinita.view.article.FullScreenImgShowActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;

/**
 * Created by webwerks1 on 11/7/17.
 */

public class ArticleListViewModel extends BaseViewModel {

    public ObservableArrayList<ArticleModel> articleModels = new ObservableArrayList<>();
    public ObservableArrayList<ArticleCategoryModel> articleCategoryModels = new ObservableArrayList<>();

    public MutableLiveData<ObservableArrayList<ArticleModel>> liveDataArticle = new MutableLiveData<ObservableArrayList<ArticleModel>>();
    public MutableLiveData<ObservableArrayList<ArticleCategoryModel>> liveDataCategory = new MutableLiveData<ObservableArrayList<ArticleCategoryModel>>();

    public int lastPage = 1;
    public int currentPage = 1;
    public String currentCategoryId = "0";
    public ObservableField<String> categoryTitle = new ObservableField<>();

    public void onCategoryClick(View view, ArticleCategoryModel articleCategoryModel, int position) {
        currentPage = 1;
        articleModels.clear();
        for (int i = 0; i < articleCategoryModels.size(); i++) {
            articleCategoryModels.get(i).setSelect(i == position);
        }
        currentCategoryId = articleCategoryModel.getId();
        categoryTitle.set(articleCategoryModel.getTitle());
        getArticle(view.getContext(), new GetArticleRequest("" + currentPage, articleCategoryModel.getId()));

    }

    public void onArticleClick(View view, ArticleModel articleModel, ParallaxImageView parallaxImageView) {
        BaseLifecycleActivity baseLifecycleActivity = (BaseLifecycleActivity) view.getContext();
        Log.e("image url ::::::", articleModel.getArticleImgUrl());
        Intent intent = new Intent(baseLifecycleActivity, FullScreenImgShowActivity.class);
        intent.putExtra(Constants.IntentExtras.ARTICLE_MODEL, articleModel);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(baseLifecycleActivity, parallaxImageView, baseLifecycleActivity.getResources().getString(R.string.app_name));
            baseLifecycleActivity.startActivity(intent, options.toBundle());
        } else {
            baseLifecycleActivity.startActivityWithAnimation(intent);
        }
    }


    public MutableLiveData<ObservableArrayList<ArticleModel>> getLiveDataArticle() {
        return liveDataArticle;
    }


    public MutableLiveData<ObservableArrayList<ArticleCategoryModel>> getLiveDataCategory() {
        return liveDataCategory;
    }


    public void getArticleCategories(Context context) {

        ArticleCategoryModel articleCategoryModel = new ArticleCategoryModel();
        articleCategoryModel.setId("0");
        articleCategoryModel.setTitle(context.getString(R.string.article_category_title));
        articleCategoryModel.setSelect(true);
        articleCategoryModels.add(articleCategoryModel);

        categoryTitle.set(articleCategoryModel.getTitle());

        NetworkHelper.create(context)
                .setMessage(context.getString(R.string.dialog_msg_get_article_category))
                .setShowProgressLoading(false)
                .callWebService(ApiClient.getClient().getArticleCategories(), new NetworkHelper.NetworkCallBack<ArticleCategoryResponseModel>() {
                    @Override
                    public void onSyncData(ArticleCategoryResponseModel data) {

                        if (data.isSuccess()) {
                            liveDataCategory.setValue(data.getArticleCategoryModels());
                            getArticle(context, new GetArticleRequest("1", "0"));
                        } else {

                            Tools.showToast(context, data.getMessage());
                        }
                    }

                    @Override
                    public void onFailed(ArticleCategoryResponseModel error) {
                        liveDataArticle.setValue(null);
                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });


    }

    public void getArticle(Context context, GetArticleRequest articleRequest) {

        NetworkHelper.create(context)
                .setMessage(context.getString(R.string.dialog_msg_get_article))
                .callWebService(ApiClient.getClient().getArticleList(articleRequest), new NetworkHelper.NetworkCallBack<ArticleListResponseModel>() {
                    @Override
                    public void onSyncData(ArticleListResponseModel data) {

                        if (data.isSuccess()) {
                            currentPage++;
                            lastPage = data.getArticleData().getLastPage();
                            liveDataArticle.setValue(data.getArticleData().getArticleModels());

                        } else {
                            Tools.showToast(context, data.getMessage());
                        }
                    }

                    @Override
                    public void onFailed(ArticleListResponseModel error) {

                        liveDataArticle.setValue(null);
                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });


    }
}
