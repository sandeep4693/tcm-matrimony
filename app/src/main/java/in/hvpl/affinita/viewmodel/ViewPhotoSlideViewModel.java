package in.hvpl.affinita.viewmodel;

import android.databinding.ObservableArrayList;

import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.model.UserDetailModel;

/**
 * Created by webwerks1 on 17/8/17.
 */

public class ViewPhotoSlideViewModel extends BaseViewModel{
    public UserDetailModel userDetailModel;
    public void setUserDetailModel(UserDetailModel userDetailModel) {
        this.userDetailModel = userDetailModel;
    }
    public ObservableArrayList<AlbumInformationModel> createAlbumList() {
        ObservableArrayList<AlbumInformationModel> albumInformationModels = new ObservableArrayList<>();
        albumInformationModels.add(new AlbumInformationModel("0", userDetailModel.getBasicInformationModel().getPhotoUrl()));
        for (int i = 0; i < userDetailModel.getAlbumModels().size(); i++) {
            if (!userDetailModel.getAlbumModels().get(i).getAlbumPhotoPath().equals("")) {
                albumInformationModels.add(userDetailModel.getAlbumModels().get(i));
            }
        }
        return albumInformationModels;
    }
}
