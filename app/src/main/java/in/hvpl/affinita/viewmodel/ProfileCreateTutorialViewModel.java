package in.hvpl.affinita.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.content.Intent;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.view.View;

import in.hvpl.affinita.view.registartion.RegistrationActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;

/**
 * Created by webwerks on 15/6/17.
 */

public class ProfileCreateTutorialViewModel extends ViewModel {
    /**
     * ObservableInt position use for to hide and show  Create Profile Button
     */
    public final ObservableInt position = new ObservableInt();
    public final ObservableField<String> msg=new ObservableField<>();


    public void createProfileClick(View view) {
        BaseLifecycleActivity activity = ((BaseLifecycleActivity) view.getContext());
        activity.startActivityWithAnimation(new Intent(view.getContext(), RegistrationActivity.class));
        activity.finish();
    }
}
