package in.hvpl.affinita.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableArrayList;
import android.view.View;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.model.ConnectionModel;
import in.hvpl.affinita.model.response.ConnectionResponseModel;
import in.hvpl.affinita.view.messaging.ChatActivity;

/**
 * Created by Ganesh.K on 3/8/17.
 */

public class ConnectionViewModel extends BaseViewModel {

    public String url = "http://staging.php-dev.in:8844/HumanVentures/matrimonial/files/6cd/d60/ea0/045/eb7/a6e/c44/c54/d29/ed4/6cdd60ea0045eb7a6ec44c54d29ed402.jpg";
    public ObservableArrayList<ConnectionModel> connectionModels = new ObservableArrayList<>();
    public MutableLiveData<Boolean> isConnectionListRefresh = new MutableLiveData<>();

    public void getConnectionList(Context context,boolean isRunInbackground) {
        BaseActivity baseActivity = (BaseActivity) context;
        NetworkHelper.create(context)
                .setMessage(context.getString(R.string.dialog_msg_loading_connection))
                .setShowOnFailureDialog(!isRunInbackground)
                .setShowProgressLoading(!isRunInbackground)
                .callWebService(ApiClient.getClient().getConnectionList(SharedPreferencesHelper.getClientId(context)),
                        new NetworkHelper.NetworkCallBack<ConnectionResponseModel>() {
                            @Override
                            public void onSyncData(ConnectionResponseModel data) {
                                if (data.isSuccess()) {
                                    connectionModels.clear();
                                    connectionModels.addAll(data.getConnectionData().getConnectionModels());
                                    isConnectionListRefresh.setValue(true);
                                    baseActivity.setConnectionUnreadCount(data.getConnectionData().getUnreadMsgCount());
                                } /*else {
                                    Tools.showToast(context, data.getMessage());
                                }*/
                            }

                            @Override
                            public void onFailed(ConnectionResponseModel error) {
                            }

                            @Override
                            public void noInternetConnection() {

                            }
                        }

                );

    }

    public void onChatClick(View view, ConnectionModel connectionModel) {
        Intent intent = new Intent(view.getContext(), ChatActivity.class);
        intent.putExtra(Constants.IntentExtras.CONNECTION_DATA, connectionModel);
        ((BaseActivity) view.getContext()).startActivityWithAnimation(intent);
    }

}
//Message data payload: {notification_type=message, body=love, icon=myicon, title=New Message, notificationId=146}

//Message dat0a payload: {msg={"notificationId":8,"title":"Message Title 8","body":"Message body 8"}}