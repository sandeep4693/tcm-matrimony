package in.hvpl.affinita.viewmodel;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.database.Cursor;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import famework.neo.instagramhelper.InstagramHelper;
import famework.neo.instagramhelper.models.InstagramMedia;
import in.hvpl.affinita.App;
import in.hvpl.affinita.Instagram.ApplicationData;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.FacebookHelper;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.model.UserDetailModel;
import in.hvpl.affinita.model.base.ResponseModel;
import in.hvpl.affinita.model.response.AlbumUploadResponseModel;
import in.hvpl.affinita.model.response.FacebookPhotoResponse;
import neo.architecture.lifecycle.BaseLifecycleActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/**
 * Created by webwerks on 7/7/17.
 */

public class AlbumCreateViewModel extends BaseViewModel {

    private static final String TAG = AlbumCreateViewModel.class.getSimpleName();
    public ObservableArrayList<AlbumInformationModel> albumModels = App.getInstance().getUserDetailModel().get().getAlbumModels();
    public ObservableArrayList<AlbumInformationModel> selectedAlbumList = (ObservableArrayList<AlbumInformationModel>) albumModels.clone();
    public ObservableArrayList<AlbumInformationModel> galleryAlbumList = new ObservableArrayList<>();
    public ObservableArrayList<AlbumInformationModel> facebookAlbumList = new ObservableArrayList<>();
    public ObservableArrayList<AlbumInformationModel> instagramAlbumList = new ObservableArrayList<>();
    public MutableLiveData<Integer> currentAddedImage = new MutableLiveData<>();
    private MutableLiveData<FacebookPhotoResponse> facebookPhotoResponse = new MutableLiveData<>();

    public ObservableBoolean hasFbAccessToken = new ObservableBoolean(FacebookHelper.getInstance().getAccessToken() != null);
    public ObservableBoolean hasInstaAccessToken = new ObservableBoolean(false);

    public MutableLiveData<FacebookPhotoResponse> getFacebookPhotoResponse() {
        return facebookPhotoResponse;
    }

    private Handler handler;
    private Thread thread;
    private final String[] projection = new String[]{MediaStore.Images.Media._ID, MediaStore.Images.Media.DISPLAY_NAME, MediaStore.Images.Media.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME};


    public MutableLiveData<Integer> getCurrentAddedImage() {
        return currentAddedImage;
    }


    public void onImageSelect(View view, AlbumInformationModel albumInformationModel) {


        int destinationPosition = getEmptySelectPosition();
        if (destinationPosition == -1) {
            Tools.showToast(view.getContext(), "You can select your 9 best images that describe you");
        } else {
            if (albumInformationModel.getFromPhoto() != Constants.FROM_IMAGE.FROM_GALLERY) {
                if (!Constants.ALBUM_PHOTO.exists()) {
                    Constants.ALBUM_PHOTO.mkdir();
                }
                File file = new File(Constants.ALBUM_PHOTO.getAbsoluteFile() + File.separator + destinationPosition + Constants.PHOTO_FILE_EXTENTION);
                NetworkHelper.create(view.getContext())
                        .setMessage("Download Image")
                        .downloadFile(albumInformationModel.getAlbumPhotoPath(), file,
                                new NetworkHelper.NetworkCallBack<ResponseBody>() {
                                    @Override
                                    public void onSyncData(ResponseBody data) {
                                        albumInformationModel.setAlbumPhotoPath(file.getAbsolutePath());
                                        currentAddedImage.setValue(destinationPosition);
                                        selectedAlbumList.set(destinationPosition, albumInformationModel);
                                    }

                                    @Override
                                    public void onFailed(ResponseBody error) {

                                    }

                                    @Override
                                    public void noInternetConnection() {

                                    }
                                });
            } else {
                currentAddedImage.setValue(destinationPosition);
                selectedAlbumList.set(destinationPosition, albumInformationModel);

            }


        }
    }

    public void onRemoveImage(View view, int position) {

        AlbumInformationModel model = selectedAlbumList.get(position);

        switch (model.getFromPhoto()) {
            case Constants.FROM_IMAGE.FROM_EMPTY:
            case Constants.FROM_IMAGE.FROM_GALLERY:

                removeImage(position);
                break;

            case Constants.FROM_IMAGE.FROM_FB:
            case Constants.FROM_IMAGE.FROM_INSTA:
                File file = new File(model.getAlbumPhotoPath());
                file.delete();
                removeImage(position);
                break;

            case Constants.FROM_IMAGE.FROM_SERVER:
                deleteAlbumPhoto(view.getContext(), position, model);
                break;
        }

    }

    private void deleteAlbumPhoto(Context context, int position, AlbumInformationModel model) {
        NetworkHelper.create(context)
                .setMessage("Deleting...")
                .callWebService(ApiClient.getClient().removeImage(SharedPreferencesHelper.getClientId(context),
                        model.getAlbumPhotoId()),
                        new NetworkHelper.NetworkCallBack<ResponseModel>() {
                            @Override
                            public void onSyncData(ResponseModel data) {

                                if (data.isSuccess()) {
                                    removeImage(position);
                                    albumModels.remove(position);
                                    albumModels.add(new AlbumInformationModel("", "", Constants.FROM_IMAGE.FROM_EMPTY));
                                    App.getInstance().getUserDetailModel().get().setAlbumModels(albumModels);
                                }
                                Tools.showToast(context, "" + data.getMessage());
                            }

                            @Override
                            public void onFailed(ResponseModel error) {

                            }

                            @Override
                            public void noInternetConnection() {

                            }
                        });
    }

    private void removeImage(int position) {

        selectedAlbumList.remove(position);
        selectedAlbumList.add(new AlbumInformationModel("", "", Constants.FROM_IMAGE.FROM_EMPTY));
        currentAddedImage.setValue(position);
    }

    public void uploadAlbumPhoto(Context context) {
        List<MultipartBody.Part> albumsList = new ArrayList<>();
        for (int index = 0; index < selectedAlbumList.size(); index++) {
            int imageFrom = selectedAlbumList.get(index).getFromPhoto();
            switch (imageFrom) {
                case Constants.FROM_IMAGE.FROM_GALLERY:
                case Constants.FROM_IMAGE.FROM_FB:
                case Constants.FROM_IMAGE.FROM_INSTA:
                    Log.d(TAG, "requestUploadSurvey: survey image " + index + "  " + selectedAlbumList.get(index).getAlbumPhotoPath());
                    File file = new File(selectedAlbumList.get(index).getAlbumPhotoPath());
                    RequestBody imageBody = RequestBody.create(MediaType.parse("image/*"), file);
                    albumsList.add(MultipartBody.Part.createFormData("image[]", file.getName(), imageBody));
                    break;
            }
        }

        RequestBody clientIdBody = RequestBody.create(MediaType.parse("text/plain"), SharedPreferencesHelper.getClientId(context));

        if (albumsList.size() > 0) {
            NetworkHelper.create(context)
                    .setMessage(context.getString(R.string.dialog_msg_upload_album))
                    .callWebService(ApiClient.getClient().addImages(albumsList, clientIdBody),
                            new NetworkHelper.NetworkCallBack<AlbumUploadResponseModel>() {
                                @Override
                                public void onSyncData(AlbumUploadResponseModel data) {

                                    if (data.isSuccess()) {
                                        resetAlbumModel(context, data);
                                    }

                                    Tools.showToast(context, data.getMessage());
                                }

                                @Override
                                public void onFailed(AlbumUploadResponseModel error) {

                                }

                                @Override
                                public void noInternetConnection() {

                                }
                            });
        } else {

            if (isValidAlbum()) {
                ((BaseLifecycleActivity) context).onBackPressed();
            } else {
                Tools.showToast(context, context.getString(R.string.msg_error_album_done));
            }
        }
    }

    private boolean isValidAlbum() {

        int pos = 0;
        int size = albumModels.size();
        for (int position = 0; position < size; position++) {
            if (!albumModels.get(position).getAlbumPhotoPath().equals("")) {
                pos++;
                if (pos == 2)
                    return true;
            }
        }

        return false;
    }

    private void resetAlbumModel(Context context, AlbumUploadResponseModel data) {

        ObservableArrayList<AlbumInformationModel> list = data.getAlbumInformationModels();
        for (int i = 0; i < list.size(); i++) {
            albumModels.set(getEmptyAlbumPosition(), list.get(i));
        }
        UserDetailModel userDetailModel = App.getInstance().getUserDetailModel().get();
        userDetailModel.setAlbumModels(albumModels);
        App.getInstance().setUserDetailModel(userDetailModel);

        ((BaseLifecycleActivity) context).onBackPressed();
    }

    public int getEmptySelectPosition() {
        int emptyPos = -1;
        int size = selectedAlbumList.size();
        for (int position = 0; position < size; position++) {
            if (selectedAlbumList.get(position).getAlbumPhotoPath().equals("")) {
                emptyPos = position;
                break;
            }
        }

        return emptyPos;
    }

    public int getEmptyAlbumPosition() {
        int emptyPos = -1;
        int size = albumModels.size();
        for (int position = 0; position < size; position++) {
            if (albumModels.get(position).getAlbumPhotoPath().equals("")) {
                emptyPos = position;
                break;
            }
        }

        return emptyPos;
    }

    @SuppressLint("HandlerLeak")
    public MutableLiveData<Boolean> getGalleryData(BaseLifecycleActivity activity) {

        MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>();

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case Constants.FETCH_STARTED: {
                        activity.showProgressLoading("Loading Gallery Images");
                        break;
                    }
                    case Constants.FETCH_COMPLETED: {
                        activity.stopLoading();
                        Tools.printError(TAG, "" + galleryAlbumList.size());
                        mutableLiveData.setValue(true);


                        break;
                    }
                    case Constants.ERROR: {
                        activity.stopLoading();
                        mutableLiveData.setValue(false);

                    }
                    default: {
                        super.handleMessage(msg);
                    }
                }
            }
        };

        if (galleryAlbumList.size() == 0) {
            getGalleryImages(activity);
        }

        return mutableLiveData;

    }

    /**
     * Get gallery images
     */
    private void getGalleryImages(BaseLifecycleActivity activity) {
        galleryAlbumList.clear();
        abortLoading();
        ImageLoaderRunnable runnable = new ImageLoaderRunnable(activity);
        thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Stop loading gallery data task
     */
    public void abortLoading() {
        if (thread == null)
            return;
        if (thread.isAlive()) {
            thread.interrupt();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void removeHandler() {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }

    /**
     * Loading data task
     */
    private class ImageLoaderRunnable implements Runnable {

        private BaseLifecycleActivity activity;

        public ImageLoaderRunnable(BaseLifecycleActivity activity) {
            this.activity = activity;
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

            Message message;
            if (galleryAlbumList.size() == 0) {
                /*
                If the adapter is null, this is first time this activity's view is
                being shown, hence send FETCH_STARTED message to show progress bar
                while images are loaded from phone
                 */
                message = handler.obtainMessage();
                message.what = Constants.FETCH_STARTED;
                message.sendToTarget();
            }

            if (Thread.interrupted()) {
                return;
            }

            Cursor cursor = activity.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                    null, null, MediaStore.Images.Media.DATE_ADDED);

            if (cursor == null) {
                message = handler.obtainMessage();
                message.what = Constants.ERROR;
                message.sendToTarget();
                return;
            }

            File file;


            if (cursor.moveToLast()) {
                do {
                    if (Thread.interrupted()) {
                        return;
                    }
                    String id = String.valueOf(cursor.getLong(cursor.getColumnIndex(projection[0])));
                    String name = cursor.getString(cursor.getColumnIndex(projection[1]));
                    String path = cursor.getString(cursor.getColumnIndex(projection[2]));
                    String bucket = cursor.getString(cursor.getColumnIndex(projection[3]));

                    file = new File(path);
                    if (file.exists()) {
                        AlbumInformationModel image = new AlbumInformationModel("", path, Constants.FROM_IMAGE.FROM_GALLERY);
                        galleryAlbumList.add(image);
                    }

                } while (cursor.moveToPrevious());
            }
            cursor.close();

            if (handler != null) {
                message = handler.obtainMessage();
                message.what = Constants.FETCH_COMPLETED;
                message.sendToTarget();
            }

            Thread.interrupted();

        }
    }


    /**
     * Facebook Login
     */

    public void onFacebookLoginClick(View view) {

        FacebookHelper.getInstance().doLogin((Activity) view.getContext(), new FacebookHelper.FbCallBack() {
            @Override
            public void onSuccess() {
                hasFbAccessToken.set(true);
                getFacebookImages((BaseLifecycleActivity) view.getContext());
                Tools.printError(TAG, "onFacebookLoginClick onSuccess");
            }

            @Override
            public void onError() {
                hasFbAccessToken.set(false);
                Tools.printError(TAG, "onFacebookLoginClick onError");
            }
        });
    }


    public void getFacebookImages(BaseLifecycleActivity activity) {

        //hasFbAccessToken.set(true);

        if (facebookAlbumList.size() == 0)
            FacebookHelper.getInstance().getFacebookUserPhotos(activity, facebookPhotoResponse);


    }


    public void onInstagramLogin(View view) {

        InstagramHelper.getInstance(view.getContext())
                .initialize(ApplicationData.CLIENT_ID,
                        ApplicationData.CLIENT_SECRET, ApplicationData.CALLBACK_URL)
                .setListener(new InstagramHelper.Client() {
                    @Override
                    public void onSuccess() {
                        getInstagramImages((BaseLifecycleActivity) view.getContext());
                        hasInstaAccessToken.set(true);
                    }

                    @Override
                    public void onFail(String error) {
                        hasInstaAccessToken.set(false);
                    }

                    @Override
                    public void onInstaImages(List<InstagramMedia> recentMedia) {
                        hasInstaAccessToken.set(true);
                    }
                })
                .authorize();
    }

    public MutableLiveData<Boolean> getInstagramImages(BaseLifecycleActivity activity) {
        MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>();
        if (instagramAlbumList.size() == 0) {
            if (InstagramHelper.getInstance(activity).hasAccessToken()) {

                activity.showProgressLoading(activity.getString(R.string.dialog_msg_get_instagram_photo));

                InstagramHelper.getInstance(activity)
                        .setListener(new InstagramHelper.Client() {
                            @Override
                            public void onSuccess() {
                                activity.stopLoading();
                                hasInstaAccessToken.set(true);

                            }

                            @Override
                            public void onFail(String error) {
                                activity.stopLoading();
                                mutableLiveData.setValue(false);

                            }

                            @Override
                            public void onInstaImages(List<InstagramMedia> recentMedia) {
                                if (recentMedia != null) {
                                    for (int i = 0; i < recentMedia.size(); i++) {
                                        instagramAlbumList.add(new AlbumInformationModel("", recentMedia.get(i).getImages().getStandard_resolution().getUrl(), Constants.FROM_IMAGE.FROM_INSTA));
                                    }
                                    activity.stopLoading();
                                    mutableLiveData.setValue(true);
                                    hasInstaAccessToken.set(true);
                                }
                            }
                        }).getImages();
            }
        }

        return mutableLiveData;
    }


}
