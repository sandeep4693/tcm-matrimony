package in.hvpl.affinita.viewmodel;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.view.View;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.view.article.ArticleListFragment;
import in.hvpl.affinita.view.bureau.BureauFragment;
import in.hvpl.affinita.view.connection.ConnectionFragment;
import in.hvpl.affinita.view.myprofile.MyProfileFragment;
import in.hvpl.affinita.view.proposals.ProposalsFragment;
import neo.architecture.lifecycle.BaseLifecycleActivity;
import neo.architecture.lifecycle.FragmentsManagerHelper;

/**
 * Created by webwerks on 27/6/17.
 */

public class HomeViewModel extends BaseViewModel {

    public ObservableInt selectedMenuPos = new ObservableInt(0);
    public ObservableBoolean isSelectable = new ObservableBoolean(true);
    public ObservableInt proposalUnreadCount = new ObservableInt(0);
    public ObservableInt connectionUnreadCount = new ObservableInt(0);
    public ObservableInt status = new ObservableInt(0);


    public void onBottomMenuClick(View view) {

        BaseLifecycleActivity baseLifecycleActivity = (BaseLifecycleActivity) view.getContext();
        switch (view.getId()) {

            case R.id.rvMyProfile:
                if (status.get() != Constants.AppStatus.PENDING_FOR_APPROVAL) {
                    baseLifecycleActivity.fragmentTransaction(FragmentsManagerHelper.REPLACE_FRAGMENT, new MyProfileFragment(), R.id.container, false);
                    selectedMenuPos.set(0);
                }
                break;

            case R.id.rlBureau:
                baseLifecycleActivity.fragmentTransaction(FragmentsManagerHelper.REPLACE_FRAGMENT, new BureauFragment(), R.id.container, false);
                selectedMenuPos.set(1);
                break;

            case R.id.rvConnection:
                if (isSelectable.get()) {
                    baseLifecycleActivity.fragmentTransaction(FragmentsManagerHelper.REPLACE_FRAGMENT, new ConnectionFragment(), R.id.container, false);
                    selectedMenuPos.set(2);
                }
                break;
            case R.id.rvInsight:
                baseLifecycleActivity.fragmentTransaction(FragmentsManagerHelper.REPLACE_FRAGMENT, new ArticleListFragment(), R.id.container, false);
                selectedMenuPos.set(3);
                break;
            case R.id.rvProposals:
                if (isSelectable.get()) {
                    baseLifecycleActivity.fragmentTransaction(FragmentsManagerHelper.REPLACE_FRAGMENT, new ProposalsFragment(), R.id.container, false);
                    selectedMenuPos.set(4);
                }
                break;
        }
    }
}
