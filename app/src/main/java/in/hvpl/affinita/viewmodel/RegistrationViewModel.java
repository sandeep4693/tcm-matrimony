package in.hvpl.affinita.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.view.View;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.NetworkHelper.NetworkCallBack;
import in.hvpl.affinita.model.request.RegistrationModel;
import in.hvpl.affinita.model.response.RegistrationResponseModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.response.UserDetailsResponseModel;
import in.hvpl.affinita.view.termspolicy.TermsAndConditionActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;

/**
 * Created by webwerks on 15/6/17.
 */

public class RegistrationViewModel extends BaseViewModel {

    public String otp;// remove this line and his references  when you add auto detect sms functionality

    private MutableLiveData<RegistrationResponseModel> regiData = new MutableLiveData<>();
    private MutableLiveData<UserDetailsResponseModel> userDetailsResponseModel = new MutableLiveData<>();
    public ObservableBoolean isResendVisible=new ObservableBoolean(false);


    public RegistrationModel registrationModel = new RegistrationModel();
    public ObservableBoolean isOtpScreen=new ObservableBoolean();
    public boolean isAcceptTermsAndCondition=true;
    public String otp1;
    public String otp2;
    public String otp3;
    public String otp4;


    public MutableLiveData<RegistrationResponseModel> getRegiData() {
        return regiData;
    }

    public MutableLiveData<UserDetailsResponseModel> getUserDetailsResponseModel() {
        return userDetailsResponseModel;
    }

    public void onTermsAndConditionClick(View view){
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        activity.startActivity(new Intent(activity, TermsAndConditionActivity.class));
    }

    public void onDoneClick(View view) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        Tools.hideKeyboard(activity);
        if (isOtpScreen.get()) {
            clickFromOtp(activity);
        } else {
            if(isAcceptTermsAndCondition)
            clickFromMobile(activity);
            else {
                Tools.showToast(activity, activity.getString(R.string.error_accept_terms_and_condition));
            }
        }

    }

    public void onResendOtp(View view){
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        clickFromMobile(activity);
    }

    private void clickFromMobile(BaseLifecycleActivity activity) {

        if (registrationModel.getMobileNo().length() == 10) {
            NetworkHelper.create(activity)
                    .setMessage(activity.getString(R.string.dialog_msg_mobile_registration))
                    .callWebService(ApiClient.getClient().getOtp(registrationModel),
                            new NetworkCallBack<RegistrationResponseModel>() {
                                @Override
                                public void onSyncData(RegistrationResponseModel data) {
                                    regiData.setValue(data);
                                }

                                @Override
                                public void onFailed(RegistrationResponseModel error) {

                                }

                                @Override
                                public void noInternetConnection() {

                                }
                            });
        } else {
            Tools.showToast(activity, activity.getString(R.string.error_enter_valid_mobile_no));
        }
    }

    private void clickFromOtp(BaseLifecycleActivity activity) {
        String otp = otp1 + otp2 + otp3 + otp4;
        if (otp.length() == 4) {
            registrationModel.setOtp(otp);
            NetworkHelper.create(activity)
                    .setMessage(activity.getString(R.string.dialog_msg_verify_otp))
                    .callWebService(ApiClient.getClient().verifyOtp(registrationModel),
                            new NetworkCallBack<UserDetailsResponseModel>() {
                                @Override
                                public void onSyncData(UserDetailsResponseModel data) {
                                    userDetailsResponseModel.setValue(data);
                                }

                                @Override
                                public void onFailed(UserDetailsResponseModel error) {

                                }

                                @Override
                                public void noInternetConnection() {

                                }
                            });

        } else {
            Tools.showToast(activity, activity.getString(R.string.error_enter_valid_otp));
        }
    }
}
