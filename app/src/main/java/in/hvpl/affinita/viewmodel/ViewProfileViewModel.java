package in.hvpl.affinita.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.view.View;

import java.io.File;
import java.util.ArrayList;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.interfaces.DialogClickListener;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.model.ImageIconWithTitleModel;
import in.hvpl.affinita.model.ParentInformationModel;
import in.hvpl.affinita.model.UserDetailModel;
import in.hvpl.affinita.model.ViewProfileModel;
import in.hvpl.affinita.model.base.ResponseModel;
import in.hvpl.affinita.model.response.ApproveResponseModel;
import in.hvpl.affinita.model.response.MasterResponseModel;
import in.hvpl.affinita.view.editprofile.EditProfileActivity;
import in.hvpl.affinita.view.profileplay.ProfilePlayActivity;
import in.hvpl.affinita.view.termspolicy.TermsAndConditionActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;
import okhttp3.ResponseBody;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Ganesh.K on 19/7/17.
 */

public class ViewProfileViewModel extends BaseViewModel {

    public UserDetailModel userDetailModel;
    private static final String COLON = " : ";
    private static final String NEW_LINE = "\n";
    public int partnerPrefPos = 0;
    private String audioFilePath;
    private String imagePath;
    public int activityComeFrom = 0;


    public ObservableBoolean isEditable = new ObservableBoolean();
    public ObservableBoolean isApprove = new ObservableBoolean(true);
    public ObservableBoolean isTermAndConditionVisible = new ObservableBoolean();


    public void setAudioFilePath(String audioFilePath) {
        this.audioFilePath = audioFilePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setUserDetailModel(UserDetailModel userDetailModel) {
        this.userDetailModel = userDetailModel;
    }

    public void onTermsAndConditionClick(View view) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        activity.startActivity(new Intent(activity, TermsAndConditionActivity.class));
    }

    public ArrayList<ImageIconWithTitleModel> createBasicSliderList() {
        ArrayList<ImageIconWithTitleModel> iconWithTitleModels = new ArrayList<>();

        iconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_BIRTHDAY, String.valueOf(Tools.getAge(userDetailModel.getBasicInformationModel().getDob())) + "yrs"));
        iconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_RULER, userDetailModel.getBasicInformationModel().getHeightModel().getFeet() + "'" + userDetailModel.getBasicInformationModel().getHeightModel().getInches() + "\""));
        iconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_LOCATION, userDetailModel.getContactInformationModel().getCityModel().getTitle()));
        iconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_DIET, userDetailModel.getOtherInformationModel().getDietTypeModel().getTitle()));
        iconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_SMOKING, userDetailModel.getOtherInformationModel().getSmokeModel().getTitle()));
        iconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_DRINK, userDetailModel.getOtherInformationModel().getDrinkModel().getTitle()));
        iconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_MARITAL_STATUS, userDetailModel.getBasicInformationModel().getMaritalStatusModel().getTitle()));
        iconWithTitleModels.add(new ImageIconWithTitleModel(Constants.Icon.IC_DISABILITY, userDetailModel.getBasicInformationModel().getDisabilityModel().getTitle()));
        return iconWithTitleModels;
    }

    public ObservableArrayList<AlbumInformationModel> createAlbumList() {
        ObservableArrayList<AlbumInformationModel> albumInformationModels = new ObservableArrayList<>();
        albumInformationModels.add(new AlbumInformationModel("0", userDetailModel.getBasicInformationModel().getPhotoUrl()));
        for (int i = 0; i < userDetailModel.getAlbumModels().size(); i++) {
            if (!userDetailModel.getAlbumModels().get(i).getAlbumPhotoPath().equals("")) {
                albumInformationModels.add(userDetailModel.getAlbumModels().get(i));
            }
        }
        return albumInformationModels;
    }

    public ArrayList<ViewProfileModel> getViewProfileList(Context context) {
        ArrayList<ViewProfileModel> iconWithTitleModels = new ArrayList<>();

        String desc;

        boolean isEditable = activityComeFrom != Constants.ActivityComeFrom.COME_FROM_AUDIO_PLAY && activityComeFrom != Constants.ActivityComeFrom.COME_FROM_PROPOSAL;

        this.isEditable.set(isEditable);
        isTermAndConditionVisible.set(activityComeFrom == Constants.ActivityComeFrom.COME_FROM_VIEW_PROFILE);

        if (activityComeFrom != Constants.ActivityComeFrom.COME_FROM_PARTNER_PREFERENCE) {

            desc = userDetailModel.getAboutMe();
            if (!desc.equals("")) {
                iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_ABOUT_ME, context.getString(R.string.about_me), desc, true, false, isEditable));
            }

            desc = getEducationData(context);
            if (!desc.equals("")) {
                iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_EDUCATION, context.getString(R.string.education), desc, true, false, isEditable));
            }

            desc = getCareerData(context);
            if (!desc.equals("")) {
                iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_CAREER, context.getString(R.string.career), desc, true, false, isEditable));
            }

            desc = getBackgroundData(context);
            if (!desc.equals("")) {
                iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_BACKGROUND, context.getString(R.string.background), desc, true, false, isEditable));
            }

            desc = getAstroDetails(context);
            if (!desc.equals("")) {
                iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_ASTRO_DETAILS, context.getString(R.string.astro_details), desc, true, false, isEditable));
            }

            desc = userDetailModel.getOtherInformationModel().getHobbies();
            if (!desc.equals("")) {
                iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_HOBBIES, context.getString(R.string.hobbies), desc, true, false, false));
            }

            if (userDetailModel.getShowFamilyDetails().toLowerCase().equals("yes")) {
                desc = getFatherDetails(context);
                if (!desc.equals("")) {
                    iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_FAMILY, context.getString(R.string.family_detail), desc, true, false, isEditable));
                }

                desc = getMotherDetails(context);
                if (!desc.equals("")) {
                    iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_FAMILY, context.getString(R.string.family_detail), desc, false, false, false));

                }
            }


        }

        iconWithTitleModels.add(new ViewProfileModel(App.getInstance().isBoy() ? Constants.Icon.IC_FEMALE : Constants.Icon.IC_MALE, context.getString(R.string.partner_preferences), "", true, false, isEditable));
        partnerPrefPos = iconWithTitleModels.size();

        desc = getPartnerAge(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_BIRTHDAY, context.getString(R.string.age), desc, false, true, false));

        }

        desc = getPartnerHight(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_RULER, context.getString(R.string.height), desc, false, true, false));
        }


        desc = getPartnerLocation(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_LOCATION, context.getString(R.string.location), desc, false, true, false));
        }

        desc = getPartnerMaritalStatus(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_MARITAL_STATUS, context.getString(R.string.marital_status), desc, false, true, false));
        }


        desc = getPartnerSmoking(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_SMOKING, context.getString(R.string.smoking), desc, false, true, false));
        }

        desc = getPartnerDrinking(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_DRINK, context.getString(R.string.drinking), desc, false, true, false));
        }


        desc = getPartnerDiet(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_DIET, context.getString(R.string.diet), desc, false, true, false));
        }

        desc = getPartnerBackground(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_BACKGROUND, context.getString(R.string.background), desc, false, true, false));
        }

        desc = getPartnerEducation(context);
        if (!desc.equals("")) {
            iconWithTitleModels.add(new ViewProfileModel(Constants.Icon.IC_EDUCATION, context.getString(R.string.education), desc, false, true, false));
        }

        return iconWithTitleModels;
    }


    private String getEducationData(Context context) {

        return getValidString(context.getString(R.string.highest_education)
                + COLON +
                userDetailModel.getEducationInformationModel().getEducationModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.stream)
                + COLON +
                userDetailModel.getEducationInformationModel().getGraduationStream()
                + NEW_LINE +
                context.getString(R.string.university)
                + COLON +
                userDetailModel.getEducationInformationModel().getUniversity());
    }

    private String getCareerData(Context context) {

        return getValidString(context.getString(R.string.place_of_work)
                + COLON +
                userDetailModel.getEducationInformationModel().getCompanyName()
                + NEW_LINE +
                context.getString(R.string.industry)
                + COLON +
                userDetailModel.getEducationInformationModel().getOccupationFieldModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.position)
                + COLON +
                userDetailModel.getEducationInformationModel().getDesignationModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.annual_income)
                + COLON +
                userDetailModel.getEducationInformationModel().getSalaryRangeModel().getTitle()
                + " " +
                userDetailModel.getEducationInformationModel().getCurrencyModel().getTitle());
    }

    private String getBackgroundData(Context context) {

        return getValidString(context.getString(R.string.religion)
                + COLON +
                userDetailModel.getOtherInformationModel().getReligionModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.mother_tongue)
                + COLON +
                userDetailModel.getOtherInformationModel().getMotherTongueModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.caste)
                + COLON +
                userDetailModel.getOtherInformationModel().getCasteModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.sub_caste)
                + COLON +
                userDetailModel.getOtherInformationModel().getSubcasteModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.origin)
                + COLON +
                userDetailModel.getContactInformationModel().getNativeModel().getNativeCity().getTitle()
                + "," +
                userDetailModel.getContactInformationModel().getNativeModel().getNativeState().getTitle()
                + "," +
                userDetailModel.getContactInformationModel().getNativeModel().getNativeCountry().getTitle()
                + NEW_LINE +
                context.getString(R.string.resident)
                + COLON +
                userDetailModel.getContactInformationModel().getResidenceStatusModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.outlook)
                + COLON +
                userDetailModel.getOtherInformationModel().getOutlookModel().getTitle());
    }

    private String getAstroDetails(Context context) {

        return getValidString(getValidString(context.getString(R.string.gothra)
                + COLON +
                userDetailModel.getOtherInformationModel().getGothraModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.rashi)
                + COLON +
                userDetailModel.getOtherInformationModel().getRashiModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.nakshatra)
                + COLON +
                userDetailModel.getOtherInformationModel().getNakshatraModel().getTitle()
                + NEW_LINE +
                context.getString(R.string.manglik)
                + COLON +
                userDetailModel.getOtherInformationModel().getManglikModel().getTitle()));
    }

    private String getFatherDetails(Context context) {

        String fatherDetail = "";

        if (userDetailModel.getParentInformationModels().size() > 0) {
            ParentInformationModel parentInformationModel = userDetailModel.getParentInformationModels().get(0);

            fatherDetail =
                    context.getString(R.string.occupation_type)
                            + COLON +
                            parentInformationModel.getOccupationTypeModel().getTitle()
                            + NEW_LINE +
                            context.getString(R.string.field)
                            + COLON +
                            parentInformationModel.getOccupationFieldModel().getTitle()
                            + NEW_LINE +
                            context.getString(R.string.designation)
                            + COLON +
                            parentInformationModel.getDesignationModel().getTitle()
                            + NEW_LINE +
                            context.getString(R.string.education)
                            + COLON +
                            parentInformationModel.getEducationModel().getTitle()
                            + NEW_LINE +
                            context.getString(R.string.stream)
                            + COLON +
                            parentInformationModel.getStream();

            fatherDetail = parentInformationModel.getRelation()
                    + NEW_LINE + getValidString(fatherDetail);

        }

        return fatherDetail;
    }

    private String getMotherDetails(Context context) {

        String motherInfo = "";

        if (userDetailModel.getParentInformationModels().size() > 1) {
            ParentInformationModel parentInformationModel = userDetailModel.getParentInformationModels().get(1);

            if (parentInformationModel.getOccupationTypeModel().getTitle().equals("")) {
                motherInfo = parentInformationModel.getRelation()
                        + NEW_LINE +
                        context.getString(R.string.occupation_type)
                        + COLON +
                        context.getString(R.string.house_wife);
            } else {
                motherInfo = context.getString(R.string.occupation_type)
                        + COLON +
                        parentInformationModel.getOccupationTypeModel().getTitle();

                motherInfo = parentInformationModel.getRelation()
                        + NEW_LINE + getValidString(motherInfo);
            }


        }

        return motherInfo;
    }


    private String getPartnerAge(Context context) {

        return context.getString(R.string.age)
                + NEW_LINE +
                userDetailModel.getPartnerPreferenceDetailsModel().getAgeMin()
                + " - " +
                userDetailModel.getPartnerPreferenceDetailsModel().getAgeMax();
    }

    private String getPartnerHight(Context context) {
        return context.getString(R.string.height)
                + NEW_LINE +
                userDetailModel.getPartnerPreferenceDetailsModel().getHeightMinModel().getFeet()
                + "'" +
                userDetailModel.getPartnerPreferenceDetailsModel().getHeightMinModel().getInches()
                + "\""
                + " to " +
                userDetailModel.getPartnerPreferenceDetailsModel().getHeightMaxModel().getFeet()
                + "'" +
                userDetailModel.getPartnerPreferenceDetailsModel().getHeightMaxModel().getInches()
                + "\"";
    }

    private String getPartnerLocation(Context context) {

        return getValidString(context.getString(R.string.location)
                + COLON +
                getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getCityModel().getTitle(),context))
                .replaceAll(COLON, NEW_LINE);
    }


    private String getPartnerMaritalStatus(Context context) {

        return getValidString(context.getString(R.string.marital_status)
                + COLON +
                getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getMaritalStatusModel().getTitle(),context))
                .replaceAll(COLON, NEW_LINE);

    }

    private String getPartnerSmoking(Context context) {

        return getValidString(context.getString(R.string.smoking)
                + COLON +
                getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getSmokeModel().getTitle(),context))
                .replaceAll(COLON, NEW_LINE);
    }

    private String getPartnerDrinking(Context context) {

        return getValidString(context.getString(R.string.drinking)
                + COLON +
                getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getDrinkModel().getTitle(),context))
                .replaceAll(COLON, NEW_LINE);
    }

    private String getPartnerDiet(Context context) {

        return getValidString(context.getString(R.string.diet)
                + COLON +
                getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getDietTypeModel().getTitle(),context))
                .replaceAll(COLON, NEW_LINE);
    }

    private String getPartnerBackground(Context context) {

        String string = getValidString(
                context.getString(R.string.religion)
                        + COLON +
                        getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getReligionModel().getTitle(),context)
                        + NEW_LINE +
                        context.getString(R.string.caste)
                        + COLON +
                        getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getCasteModel().getTitle(),context)
                        + NEW_LINE +
                        context.getString(R.string.manglik)
                        + COLON +
                        getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getManglikCheckModel().getTitle(),context)
                        + NEW_LINE +
                        context.getString(R.string.mother_tongue)
                        + COLON +
                        getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getMotherTongueModel().getTitle(),context));

        if (!string.equals("")) {
            return context.getString(R.string.background)
                    + NEW_LINE + string;
        }

        return string;

    }

    private String getPartnerEducation(Context context) {

        return getValidString(context.getString(R.string.education)
                + COLON +
                getAnyPartnerPref(userDetailModel.getPartnerPreferenceDetailsModel().getEducationModel().getTitle(),context))
                .replaceAll(COLON, NEW_LINE);
    }


    private String getAnyPartnerPref(String title,Context context) {
        if(title.equals("")){
            title=context.getString(R.string.doesnt_matter);
        }

        return title;
    }

    private String getValidString(String string) {
        String str = "";

        String[] arr = string.split(NEW_LINE);

        for (int i = 0; i < arr.length; i++) {
            if (isNotEmptyData(arr[i])) {
                if (str.equals("")) {
                    str = str + arr[i];
                } else {
                    str = str + NEW_LINE + arr[i];
                }

            }
        }

        return str;
    }

    private boolean isNotEmptyData(String string) {
        String[] arr = string.split(COLON);
        if (arr.length > 1) {
            String str = arr[1].trim();
            if (str.contains("null") || str.isEmpty() || str == null) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    public void onEditClick(View view, int requestCode, String title) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();


        NetworkHelper.create(activity).callWebService(ApiClient.getClient().getMasterData(), new NetworkHelper.NetworkCallBack<MasterResponseModel>() {
            @Override
            public void onSyncData(MasterResponseModel data) {
                if (data.isSuccess()) {
                    Intent intent = new Intent(activity, EditProfileActivity.class);
                    intent.putExtra(Constants.IntentExtras.MASTER_DATA, data.getMasterData());
                    intent.putExtra(Constants.IntentExtras.TITLE, title);
                    activity.startActivityForResultWithAnimation(intent, requestCode);
                } else {
                    Tools.showToast(activity, data.getMessage());
                }

            }

            @Override
            public void onFailed(MasterResponseModel error) {
                Tools.printError("Master Error", error.toString());
            }

            @Override
            public void noInternetConnection() {

            }
        });

    }

    public void onApproveButtonClick(View view) {
        BaseActivity activity = (BaseActivity) view.getContext();
        NetworkHelper.create(activity).callWebService(ApiClient.getClient().sendForApproval(userDetailModel.getBasicInformationModel().getClientId()), new NetworkHelper.NetworkCallBack<ApproveResponseModel>() {
            @Override
            public void onSyncData(ApproveResponseModel data) {
                if (data.isSuccess()) {

                    activity.getmFirebaseAnalytics().logEvent(Constants.AnalyticsEventKey.SEND_FOR_APPROVAL, null);

                    userDetailModel.setStatus(data.getData().getStatus());
                    isApprove.set(true);

                    Tools.showAlertDialog(activity, "Thank you", data.getMessage(),
                            "OK", "", 1,
                            new DialogClickListener() {
                                @Override
                                public void onPositiveClick(int requestCode) {
                                    activity.setResult(Activity.RESULT_OK);
                                    activity.onBackPressed();
                                }

                                @Override
                                public void onNegativeClick(int requestCode) {

                                }
                            });

                } else {
                    Tools.showToast(activity, data.getMessage());
                }

            }

            @Override
            public void onFailed(ApproveResponseModel error) {

            }

            @Override
            public void noInternetConnection() {

            }
        });
    }

    public void playAudio(View view) {

        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();

        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

        if (EasyPermissions.hasPermissions(activity, perms)) {
            // Already have permission, do the thing
            switch (activityComeFrom) {
                case Constants.ActivityComeFrom.COME_FROM_VIEW_PROFILE:
                case Constants.ActivityComeFrom.COME_FROM_AUDIO_PLAY:
                    Intent intent = new Intent(activity, ProfilePlayActivity.class);
                    intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_VIEW_PROFILE);
                    intent.putExtra(Constants.IntentExtras.IMAGE_PATH, imagePath);
                    intent.putExtra(Constants.IntentExtras.AUDIO_FILE_PATH, audioFilePath);
                    activity.startActivityWithAnimation(intent);
                    break;

                case Constants.ActivityComeFrom.COME_FROM_PROPOSAL:
                    downloadAudio(activity);
                    break;

            }
        } else {
            EasyPermissions.requestPermissions(activity, activity.getString(R.string.rationale_store_audio),
                    Constants.PermissionRequestCode.RC_STORAGE_AUDIO, perms);

        }


    }


    public MutableLiveData<Integer> onConnect(BaseActivity activity, String clientId, String id, int operation) {

        MutableLiveData<Integer> mutableLiveData = new MutableLiveData<>();

        NetworkHelper.create(activity)
                .setMessage("Connecting")
                .callWebService(ApiClient.getClient().connectRequest(clientId, id, String.valueOf(operation)),
                        new NetworkHelper.NetworkCallBack<ResponseModel>() {
                            @Override
                            public void onSyncData(ResponseModel data) {
                                if (data.isSuccess()) {
                                    mutableLiveData.setValue(operation);
                                }
                                Tools.showToast(activity, data.getMessage());
                            }

                            @Override
                            public void onFailed(ResponseModel error) {

                            }

                            @Override
                            public void noInternetConnection() {

                            }
                        });

        return mutableLiveData;
    }


    public void downloadAudio(BaseLifecycleActivity activity) {
        File audioFile = new File(Tools.getProfileAudioFile());
        File imageFile = new File(Tools.getProposalPhoto());
        if (imageFile.exists()) {
            imageFile.delete();
        }
        if (audioFile.exists()) {
            audioFile.delete();
        }

        NetworkHelper.create(activity)
                .setMessage("Download Audio File")
                .downloadFile(App.getInstance().getUserProfileDetailModel().getAudioFile(), audioFile, new NetworkHelper.NetworkCallBack<ResponseBody>() {
                    @Override
                    public void onSyncData(ResponseBody data) {
                        playAudioActivity(activity, imageFile);
                    }

                    @Override
                    public void onFailed(ResponseBody error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });


    }

    private void playAudioActivity(BaseLifecycleActivity activity, File imageFile) {
        String image = App.getInstance().getUserProfileDetailModel().getBasicInformationModel().getPhotoUrl();
        NetworkHelper.create(activity)
                .setMessage("Download Image File")
                .downloadFile(image, imageFile, new NetworkHelper.NetworkCallBack<ResponseBody>() {
                    @Override
                    public void onSyncData(ResponseBody data) {
                        activityComeFrom = Constants.ActivityComeFrom.COME_FROM_AUDIO_PLAY;
                        Intent intent = new Intent(activity, ProfilePlayActivity.class);
                        intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_VIEW_PROFILE);
                        intent.putExtra(Constants.IntentExtras.IMAGE_PATH, Tools.getProposalPhoto());
                        intent.putExtra(Constants.IntentExtras.AUDIO_FILE_PATH, Tools.getProfileAudioFile());
                        activity.startActivityWithAnimation(intent);
                    }

                    @Override
                    public void onFailed(ResponseBody error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });
    }
}
