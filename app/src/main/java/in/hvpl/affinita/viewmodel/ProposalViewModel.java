package in.hvpl.affinita.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableArrayList;
import android.view.View;

import java.io.File;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.ProposalModel;
import in.hvpl.affinita.model.request.GetClientDetailsModel;
import in.hvpl.affinita.model.response.ProposalResponseModel;
import in.hvpl.affinita.model.response.UserDetailsResponseModel;
import in.hvpl.affinita.view.profileplay.ProfilePlayActivity;
import in.hvpl.affinita.view.viewprofile.ViewProfileActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;
import okhttp3.ResponseBody;

/**
 * Created by webwerks1 on 18/7/17.
 */

public class ProposalViewModel extends BaseViewModel {

    private static final String TAG = ProposalViewModel.class.getSimpleName();
    public ObservableArrayList<ProposalModel> matchesModels = new ObservableArrayList<>();
    public ObservableArrayList<ProposalModel> whoLikeYouModels = new ObservableArrayList<>();
    public ObservableArrayList<ProposalModel> youLikeModels = new ObservableArrayList<>();
    public ObservableArrayList<ProposalModel> recentViewModels = new ObservableArrayList<>();


    public MutableLiveData<Boolean> getProposalsList(Context context, boolean isLoadInBackground) {
        MutableLiveData<Boolean> isDataLoad = new MutableLiveData<>();

        NetworkHelper networkHelper = NetworkHelper.create(context);
        if (isLoadInBackground) {
            networkHelper.setShowProgressLoading(false);
        }
        networkHelper.setMessage(context.getString(R.string.dialog_msg_proposal_load));
        networkHelper.callWebService(ApiClient.getClient().getProposalData(SharedPreferencesHelper.getClientId(context)),
                new NetworkHelper.NetworkCallBack<ProposalResponseModel>() {
                    @Override
                    public void onSyncData(ProposalResponseModel data) {
                        if (data.isSuccess()) {

                            matchesModels.clear();
                            whoLikeYouModels.clear();
                            youLikeModels.clear();
                            recentViewModels.clear();

                            matchesModels.addAll(data.getProposalData().getMatcheModels());
                            whoLikeYouModels.addAll(data.getProposalData().getWhoLikeYouModels());
                            youLikeModels.addAll(data.getProposalData().getYouLikeModels());
                            recentViewModels.addAll(data.getProposalData().getRecentViewModels());

                            ((BaseActivity) context).setProposalUnreadCount(data.getProposalData().getProposalCount());

                            isDataLoad.setValue(true);
                        } else {
                            isDataLoad.setValue(false);
                        }
                    }

                    @Override
                    public void onFailed(ProposalResponseModel error) {
                        isDataLoad.setValue(false);
                    }

                    @Override
                    public void noInternetConnection() {

                    }
                }
        );

        return isDataLoad;
    }

    public void onProfileClick(View view, ProposalModel proposalModel) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();


        NetworkHelper.create(activity)
                .callWebService(ApiClient.getClient().getClientDetails(new GetClientDetailsModel(proposalModel.getProfileId(), proposalModel.getMatchId())),
                        new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                            @Override
                            public void onSyncData(UserDetailsResponseModel data) {
                                if (data.isSuccess()) {
                                    App.getInstance().setUserProfileDetailModel(data.getUserDetailModel());
                                    if (proposalModel.isView() == 1) {
                                        Intent intent = new Intent(activity, ViewProfileActivity.class);
                                        intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_PROPOSAL);
                                        intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE, proposalModel.getType());
                                        intent.putExtra(Constants.IntentExtras.IMAGE_PATH, Tools.getProposalPhoto());
                                        intent.putExtra(Constants.IntentExtras.PROPOSAL_MATCH_ID, proposalModel.getMatchId());
                                        activity.startActivityWithAnimation(intent);
                                    } else {
                                        downloadAudio(activity, proposalModel.getType(), proposalModel.getImageUrl(), proposalModel.getMatchId());
                                    }
                                } else {
                                    Tools.showToast(activity, data.getMessage());
                                }
                            }

                            @Override
                            public void onFailed(UserDetailsResponseModel error) {

                            }

                            @Override
                            public void noInternetConnection() {

                            }

                        });
    }


    public void downloadAudio(BaseLifecycleActivity activity, String type, String image, String matchId) {
        File audioFile = new File(Tools.getProfileAudioFile());
        File imageFile = new File(Tools.getProposalPhoto());
        if (imageFile.exists()) {
            imageFile.delete();
        }
        if (audioFile.exists()) {
            audioFile.delete();
        }
        if (App.getInstance().getUserProfileDetailModel().getAudioFile().equals("")) {
            Intent intent = new Intent(activity, ViewProfileActivity.class);
            intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_PROPOSAL);
            intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE, type);
            intent.putExtra(Constants.IntentExtras.IMAGE_PATH, Tools.getProposalPhoto());
            intent.putExtra(Constants.IntentExtras.PROPOSAL_MATCH_ID, matchId);
            activity.startActivityWithAnimation(intent);
        } else {

            NetworkHelper.create(activity)
                    .setMessage("Download Audio File")
                    .downloadFile(App.getInstance().getUserProfileDetailModel().getAudioFile(), audioFile, new NetworkHelper.NetworkCallBack<ResponseBody>() {
                        @Override
                        public void onSyncData(ResponseBody data) {
                            playAudioActivity(activity, image, imageFile, type, matchId);
                        }

                        @Override
                        public void onFailed(ResponseBody error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });

        }
    }

    private void playAudioActivity(BaseLifecycleActivity activity, String image, File imageFile, String type, String matchId) {
        NetworkHelper.create(activity)
                .setMessage("Download Image File")
                .downloadFile(image, imageFile, new NetworkHelper.NetworkCallBack<ResponseBody>() {
                    @Override
                    public void onSyncData(ResponseBody data) {
                        Intent intent = new Intent(activity, ProfilePlayActivity.class);
                        intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_PROPOSAL);
                        intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE, type);
                        intent.putExtra(Constants.IntentExtras.IMAGE_PATH, Tools.getProposalPhoto());
                        intent.putExtra(Constants.IntentExtras.PROPOSAL_MATCH_ID, matchId);
                        intent.putExtra(Constants.IntentExtras.AUDIO_FILE_PATH, Tools.getProfileAudioFile());
                        activity.startActivityWithAnimation(intent);
                    }

                    @Override
                    public void onFailed(ResponseBody error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });
    }

}
