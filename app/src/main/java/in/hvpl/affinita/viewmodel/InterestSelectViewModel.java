package in.hvpl.affinita.viewmodel;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.view.View;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.InterestQuestionary;
import in.hvpl.affinita.model.UserDetailModel;
import in.hvpl.affinita.model.response.HobbiesResponseModel;
import in.hvpl.affinita.model.response.InterestResponseModel;
import in.hvpl.affinita.view.game.InterestSelectActivity;

/**
 * Created by Ganesh.K on 31/7/17.
 */

public class InterestSelectViewModel extends BaseViewModel {

    private static final String TAG = InterestSelectViewModel.class.getSimpleName();

    public ObservableField<String> imgUrl = new ObservableField<>("");

    private MutableLiveData<Integer> nextQuestion = new MutableLiveData<>();

    public boolean isOptionSelected = false;

    public ObservableArrayList<String> answerList = new ObservableArrayList<>();

    public ObservableArrayList<InterestQuestionary> questionaryList = new ObservableArrayList<>();


    public MutableLiveData<Integer> getIsNextQuestion() {
        return nextQuestion;
    }

    public MutableLiveData<InterestResponseModel> getInterestQuestions(Activity activity) {

        MutableLiveData<InterestResponseModel> responseModelMutableLiveData = new MutableLiveData<>();

        NetworkHelper.create(activity).callWebService(ApiClient.getClient().getInterestQuestions(),
                new NetworkHelper.NetworkCallBack<InterestResponseModel>() {

                    @Override
                    public void onSyncData(InterestResponseModel data) {
                        responseModelMutableLiveData.setValue(data);
                    }

                    @Override
                    public void onFailed(InterestResponseModel error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });
        return responseModelMutableLiveData;
    }

    public void onNextClicked(View view) {
        InterestSelectActivity activity = (InterestSelectActivity) view.getContext();

        if (isOptionSelected) {
            if (answerList.size() != questionaryList.size()) {
                nextQuestion.setValue(answerList.size());
                isOptionSelected = !isOptionSelected;
            } else {
                updateHobbies(activity);
            }
        } else {
            Tools.showTestToast(activity, "Please select any option!");
        }

    }

    private void updateHobbies(InterestSelectActivity activity) {

        String hobbies = answerList.toString().replaceAll("\\[", "").replaceAll("]", "");
        String clientId = SharedPreferencesHelper.getClientId(activity);
        NetworkHelper.create(activity)
                .setMessage(activity.getString(R.string.dialog_msg_update_hobbies))
                .callWebService(ApiClient.getClient().updateHobbies(clientId, hobbies), new NetworkHelper.NetworkCallBack<HobbiesResponseModel>() {
                    @Override
                    public void onSyncData(HobbiesResponseModel data) {
                        if (data.isSuccess()) {

                            UserDetailModel userDetailModel = App.getInstance().getUserDetailModel().get();
                            userDetailModel.getOtherInformationModel()
                                    .setHobbies(data.getHobbiesResponseData().getHobbies());
                            App.getInstance().setUserDetailModel(userDetailModel);

                            activity.setResult(Activity.RESULT_OK);
                            activity.onBackPressed();
                        }

                        Tools.showToast(activity, data.getMessage());
                    }

                    @Override
                    public void onFailed(HobbiesResponseModel error) {
                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });
    }
}
