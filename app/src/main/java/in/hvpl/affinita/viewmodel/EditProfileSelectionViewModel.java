package in.hvpl.affinita.viewmodel;

import android.content.Intent;
import android.view.View;

import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.SelectionModel;
import neo.architecture.lifecycle.BaseLifecycleActivity;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Ganesh.K on 25/7/17.
 */

public class EditProfileSelectionViewModel extends BaseViewModel {

    public void onSelection(View view, SelectionModel model) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        Tools.hideKeyboard(activity);
        Intent intent = new Intent();
        intent.putExtra(Constants.IntentExtras.SELECTION_MODEL, model);
        activity.setResult(RESULT_OK, intent);
        activity.onBackPressed();
    }
    public void onEditSelection(View view, String editedDetail) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        Tools.hideKeyboard(activity);
        Intent intent = new Intent();
        intent.putExtra(Constants.IntentExtras.EDITED_DETAILS, editedDetail);
        activity.setResult(RESULT_OK, intent);
        activity.onBackPressed();
    }
}
