package in.hvpl.affinita.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

import java.io.File;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.UserDetailModel;
import in.hvpl.affinita.model.response.MasterResponseModel;
import in.hvpl.affinita.model.response.UploadProfilePicResponseModel;
import in.hvpl.affinita.view.TutorialActivity;
import in.hvpl.affinita.view.editprofile.EditProfileActivity;
import in.hvpl.affinita.view.game.InterestSelectActivity;
import in.hvpl.affinita.view.myprofile.MyProfileFragment;
import in.hvpl.affinita.view.viewprofile.ViewProfileActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pub.devrel.easypermissions.EasyPermissions;

import static in.hvpl.affinita.common.utils.Constants.PermissionRequestCode.RC_CAMERA_AND_STORAGE;
import static in.hvpl.affinita.common.utils.Constants.PermissionRequestCode.RC_RECORD_AUDIO_AND_STORAGE;
import static in.hvpl.affinita.common.utils.Constants.PermissionRequestCode.RC_STORAGE_AUDIO;

/**
 * Created by webwerks on 22/6/17.
 */

public class MyProfileViewModel extends BaseViewModel {

    private static final String TAG = MyProfileViewModel.class.getSimpleName();

    public ObservableBoolean isPartnerPreferenceView = new ObservableBoolean();

    public ObservableBoolean isProfileInformationView = new ObservableBoolean();

    public ObservableField<UserDetailModel> userDetailModel = App.getInstance().getUserDetailModel();

    private MutableLiveData<Integer> permissionCallBack = new MutableLiveData<>();

    private MutableLiveData<Integer> onClickCallBack = new MutableLiveData<>();

    public MutableLiveData<Integer> getPermissionCallBack() {
        return permissionCallBack;
    }

    public MutableLiveData<Integer> getOnClickCallBack() {
        return onClickCallBack;
    }

    public void onProfileInformationClick(View view, MyProfileFragment myProfileFragment) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();

        if (userDetailModel.get().getOtherInformationModel().getHobbies().equals("")) {
            if (SharedPreferencesHelper.getIsGameUpdate(activity)) {
                startInterestSelectActivity(myProfileFragment);
            } else {
                startTutorialActivity(myProfileFragment, Constants.ActivityResultRequestCode.TUTORIAL_GAME, activity.getString(R.string.msg_tutorial_game));
            }
        } else {
            startEditPersonalInfoActivity(myProfileFragment);
        }
    }

    public void startEditPersonalInfoActivity(MyProfileFragment myProfileFragment) {
        if (SharedPreferencesHelper.getIsPersonalInfoUpdate(myProfileFragment.getActivity())) {

            getMasterData(myProfileFragment.getActivity());
        } else {
            startTutorialActivity(myProfileFragment, Constants.ActivityResultRequestCode.TUTORIAL_PERSONAL_INFO, myProfileFragment.getActivity().getString(R.string.msg_tutorial_personal_info));
        }
    }

    public void startInterestSelectActivity(MyProfileFragment myProfileFragment) {
        Intent intent = new Intent(myProfileFragment.getActivity(), InterestSelectActivity.class);
        intent.putExtra(Constants.IntentExtras.IMAGE_PATH, userDetailModel.get().getBasicInformationModel().getPhotoUrl());
        myProfileFragment.startActivityForResultWithAnimation(intent, Constants.ActivityResultRequestCode.GAME_PLAYED);
    }

    public void startTutorialActivity(MyProfileFragment myProfileFragment, int resultCode, String msg) {
        Intent intent = new Intent(myProfileFragment.getActivity(), TutorialActivity.class);
        intent.putExtra(Constants.IntentExtras.TUTORIAL_MESSAGE, msg);
        myProfileFragment.startActivityForResultWithBtoTAnimation(intent, resultCode);
    }

    public void takeProfilePic(View view) {
        Context context = view.getContext();
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(context, perms)) {
            // Already have permission, do the thing
            onClickCallBack.setValue(view.getId());
        } else {
            // Do not have permissions, request them now
            permissionCallBack.setValue(RC_CAMERA_AND_STORAGE);
        }
    }

    public void editAudio(View view) {
        Context context = view.getContext();
        String[] perms = {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(context, perms)) {
            // Already have permission, do the thing
            onClickCallBack.setValue(view.getId());
        } else {
            // Do not have permissions, request them now
            permissionCallBack.setValue(RC_RECORD_AUDIO_AND_STORAGE);
        }
    }

    public void onViewProfileClick(View view) {
        Context context = view.getContext();
        String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(context, perms)) {
            // Already have permission, do the thing
            onClickCallBack.setValue(view.getId());
        } else {
            // Do not have permissions, request them now
            permissionCallBack.setValue(RC_STORAGE_AUDIO);

        }
    }

    public void onPartnerPrefernceClick(MyProfileFragment myProfileFragment) {
        BaseActivity activity = myProfileFragment.getBaseActivity();
        if (SharedPreferencesHelper.getIsPartnerPreferenceUpdate(activity)) {
            Intent intent = new Intent(activity, ViewProfileActivity.class);
            intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_PARTNER_PREFERENCE);
            activity.startActivityWithAnimation(intent);
            SharedPreferencesHelper.setIsPartnerPreferenceView(activity, true);
        } else {
            startTutorialActivity(myProfileFragment, Constants.ActivityResultRequestCode.TUTORIAL_PARTNER_PREFERENCE, activity.getString(R.string.msg_tutorial_partner_preference));
        }

    }

    public MutableLiveData<UploadProfilePicResponseModel> uploadProfilePic(Activity activity, String filePath, String client_id) {

        MutableLiveData<UploadProfilePicResponseModel> responseModelMutableLiveData = new MutableLiveData<>();
        File file = new File(filePath);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("photo", file.getName(), reqFile);
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), client_id);

        NetworkHelper.create(activity)
                .callWebService(ApiClient.getClient().uploadProfilePic(body, id),
                        new NetworkHelper.NetworkCallBack<UploadProfilePicResponseModel>() {
                            @Override
                            public void onSyncData(UploadProfilePicResponseModel data) {
                                responseModelMutableLiveData.setValue(data);
                            }

                            @Override
                            public void onFailed(UploadProfilePicResponseModel error) {
                                responseModelMutableLiveData.setValue(error);
                            }

                            @Override
                            public void noInternetConnection() {

                            }
                        });

        return responseModelMutableLiveData;
    }

    public void getMasterData(Activity activity) {

        NetworkHelper.create(activity).callWebService(ApiClient.getClient().getMasterData(), new NetworkHelper.NetworkCallBack<MasterResponseModel>() {
            @Override
            public void onSyncData(MasterResponseModel data) {
                if (data.isSuccess()) {
                    Intent intent = new Intent(activity, EditProfileActivity.class);
                    intent.putExtra(Constants.IntentExtras.MASTER_DATA, data.getMasterData());
                    ((BaseLifecycleActivity) activity).startActivityWithAnimation(intent);
                } else {
                    Tools.showToast(activity, data.getMessage());
                }

            }

            @Override
            public void onFailed(MasterResponseModel error) {

            }

            @Override
            public void noInternetConnection() {

            }
        });

    }

    public MutableLiveData<UploadProfilePicResponseModel> uploadAudio(Activity activity, String filePath, String client_id) {

        MutableLiveData<UploadProfilePicResponseModel> responseModelMutableLiveData = new MutableLiveData<>();
        File file = new File(filePath);
        RequestBody reqFile = RequestBody.create(MediaType.parse("audio/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file_upload", file.getName(), reqFile);
        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), client_id);

        NetworkHelper.create(activity).callWebService(ApiClient.getClient().uploadAudio(body, id),
                new NetworkHelper.NetworkCallBack<UploadProfilePicResponseModel>() {
                    @Override
                    public void onSyncData(UploadProfilePicResponseModel data) {
                        responseModelMutableLiveData.setValue(data);
                    }

                    @Override
                    public void onFailed(UploadProfilePicResponseModel error) {
                        responseModelMutableLiveData.setValue(error);
                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });

        return responseModelMutableLiveData;
    }

    public MutableLiveData<Boolean> downloadAudio(Activity activity, File audioFile) {
        MutableLiveData<Boolean> liveData = new MutableLiveData<Boolean>();

        NetworkHelper.create(activity)
                .setMessage("Download Audio File")
                .downloadFile(userDetailModel.get().getAudioFile(), audioFile, new NetworkHelper.NetworkCallBack<ResponseBody>() {
                    @Override
                    public void onSyncData(ResponseBody data) {
                        liveData.setValue(true);
                    }

                    @Override
                    public void onFailed(ResponseBody error) {
                        liveData.setValue(false);
                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });

        return liveData;

    }

    public MutableLiveData<Boolean> downloadImage(Activity activity, File file) {
        MutableLiveData<Boolean> liveData = new MutableLiveData<Boolean>();
        String url = userDetailModel.get().getBasicInformationModel().getPhotoUrl();
        if (!url.equals("")) {
            NetworkHelper.create(activity)
                    .setMessage("Download Image File")
                    .downloadFile(url, file, new NetworkHelper.NetworkCallBack<ResponseBody>() {
                        @Override
                        public void onSyncData(ResponseBody data) {
                            liveData.setValue(true);
                        }

                        @Override
                        public void onFailed(ResponseBody error) {
                            liveData.setValue(false);
                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });
        }

        return liveData;

    }


}
