package in.hvpl.affinita.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.view.View;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseFragment;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.common.widget.CustomDatePicker;
import in.hvpl.affinita.common.widget.CustomNumberPicker;
import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.UserDetailModel;
import in.hvpl.affinita.model.request.AstroInfoRequestModel;
import in.hvpl.affinita.model.request.BackgroundRequestModel;
import in.hvpl.affinita.model.request.BasicInfoRequestModel;
import in.hvpl.affinita.model.request.CareerRequestModel;
import in.hvpl.affinita.model.request.EducationRequestModel;
import in.hvpl.affinita.model.request.FamilyInfoRequestModel;
import in.hvpl.affinita.model.request.PartnerPrefrenceRequestModel;
import in.hvpl.affinita.model.response.AboutMeResponseModel;
import in.hvpl.affinita.model.response.CastesResponseModel;
import in.hvpl.affinita.model.response.CityResponseModel;
import in.hvpl.affinita.model.response.MasterResponseModel;
import in.hvpl.affinita.model.response.StateResponseModel;
import in.hvpl.affinita.model.response.SubCasteResponseModel;
import in.hvpl.affinita.model.response.UserDetailsResponseModel;
import in.hvpl.affinita.view.editprofile.EditProfileEnterDetailActivity;
import in.hvpl.affinita.view.editprofile.EditProfileSelectionActivity;
import in.hvpl.affinita.view.editprofile.fragment.EditProfileAboutMeFragment;
import in.hvpl.affinita.view.editprofile.fragment.EditProfileAstroDetailFragment;
import in.hvpl.affinita.view.editprofile.fragment.EditProfileBackgroundFragment;
import in.hvpl.affinita.view.editprofile.fragment.EditProfileBasicInfoFragment;
import in.hvpl.affinita.view.editprofile.fragment.EditProfileCareerFragment;
import in.hvpl.affinita.view.editprofile.fragment.EditProfileEducationFragment;
import in.hvpl.affinita.view.editprofile.fragment.EditProfileFamilyDetailFragment;
import in.hvpl.affinita.view.editprofile.fragment.EditProfilePartnerPrefrenceFragment;
import neo.architecture.lifecycle.BaseLifecycleActivity;

import static neo.architecture.lifecycle.FragmentsManagerHelper.REPLACE_FRAGMENT;

/**
 * Created by webwerks1 on 24/7/17.
 */

public class EditProfileViewModel extends BaseViewModel {

    private static final String TAG = EditProfileViewModel.class.getSimpleName();
    private MasterResponseModel.MasterData masterData;
    public ObservableField<UserDetailModel> userDetailModel = App.getInstance().getUserDetailModel();
    public ObservableField<String> pendingTopic=new ObservableField<>();
    public ObservableBoolean isPendingTopicShow=new ObservableBoolean();


    public void setMasterData(MasterResponseModel.MasterData masterData) {
        this.masterData = masterData;
    }

    public MasterResponseModel.MasterData getMasterData() {
        return masterData;
    }

    public void onSelection(View view, BaseFragment baseFragment, int requestCode) {

        Context context = view.getContext();
        Intent intent = new Intent(context, EditProfileSelectionActivity.class);
        ArrayList<SelectionModel> selectionModels = new ArrayList<>();
        switch (requestCode) {
            case Constants.ActivityResultRequestCode.SELECT_DIET:
                selectionModels = masterData.getDietTypesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_diet));
                break;

            case Constants.ActivityResultRequestCode.SELECT_SMOKING:
                selectionModels = masterData.getSmokingTypesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_smoking));
                break;

            case Constants.ActivityResultRequestCode.SELECT_DRINKING:
                selectionModels = masterData.getDrinkingTypesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_drinking));
                break;

            case Constants.ActivityResultRequestCode.SELECT_MARITAL_STATUS:
                selectionModels = masterData.getMaritalStatusModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_marital_status));
                break;

            case Constants.ActivityResultRequestCode.SELECT_DISABILITY:
                selectionModels = masterData.getDisabilityTypesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_disability_type));
                break;

            case Constants.ActivityResultRequestCode.SELECT_COUNTRY:
                selectionModels = masterData.getCountriesModels();
                if (baseFragment instanceof EditProfileBackgroundFragment) {
                    addDontKnow(selectionModels, view.getContext());
                }
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_country));
                break;


            case Constants.ActivityResultRequestCode.SELECT_SALARY_RANGE:
                selectionModels = masterData.getSalaryRangesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_salary_range));
                break;

            case Constants.ActivityResultRequestCode.SELECT_CURRENCY:
                selectionModels = masterData.getCurrenciesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_currency));
                break;

            case Constants.ActivityResultRequestCode.SELECT_DISIGNATION:
                selectionModels = masterData.getDesignationsModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_designation));
                break;

            case Constants.ActivityResultRequestCode.SELECT_FIELD:
                selectionModels = masterData.getOccupationFieldsModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_occupation_field));
                break;
            case Constants.ActivityResultRequestCode.SELECT_RELIGION:
                selectionModels = masterData.getReligionsModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_religion));
                break;

            case Constants.ActivityResultRequestCode.SELECT_CASTE:
                selectionModels = masterData.getCastesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_cast));
                break;

            case Constants.ActivityResultRequestCode.SELECT_MOTHER_TONGUE:
                selectionModels = masterData.getMotherTonguesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_select_mother_tongue));
                break;

            case Constants.ActivityResultRequestCode.SELECT_SUB_CASTE:
                selectionModels = masterData.getSubcastesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_sub_cast));
                break;

            case Constants.ActivityResultRequestCode.SELECT_RESIDENT:
                selectionModels = masterData.getResidenceStatusTypesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_residency_type));
                break;

            case Constants.ActivityResultRequestCode.SELECT_OUTLOOK:
                selectionModels = masterData.getOutlooksModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_outlook));
                break;

            case Constants.ActivityResultRequestCode.SELECT_HIGHEST_EDUCATION:
                selectionModels = masterData.getEducationsModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_highest_education));
                break;
            case Constants.ActivityResultRequestCode.SELECT_MANGLIK:
                selectionModels = masterData.getManglikTypesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_maglik));
                break;
            case Constants.ActivityResultRequestCode.SELECT_NAKSHATRA:
                selectionModels = masterData.getNakshatrasModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_nakshtra));
                break;
            case Constants.ActivityResultRequestCode.SELECT_RASHI:
                selectionModels = masterData.getZodiacSignsModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_rashi));
                break;
            case Constants.ActivityResultRequestCode.SELECT_OCCUPATION_TYPE_FATHER:
                selectionModels = masterData.getOccupationTypesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_occupation_type));
                break;
            case Constants.ActivityResultRequestCode.SELECT_OCCUPATION_TYPE_MOTHER:
                selectionModels = masterData.getOccupationTypesModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_occupation_type));
                break;

            case Constants.ActivityResultRequestCode.SELECT_GOTHRA:
                selectionModels = masterData.getGothraModels();
                intent.putExtra(Constants.IntentExtras.TITLE, context.getString(R.string.activity_title_select_gothra));

                break;
        }

        if (baseFragment instanceof EditProfilePartnerPrefrenceFragment) {
            addDoesntMatter(selectionModels, view.getContext());
        }

        if (baseFragment instanceof EditProfileAstroDetailFragment) {
            addDontKnow(selectionModels, view.getContext());
        }


        intent.putParcelableArrayListExtra(Constants.IntentExtras.MASTER_LIST, selectionModels);
        baseFragment.startActivityForResultWithAnimation(intent, requestCode);

    }

    public void onEditDetail(View view, BaseFragment baseFragment, int requestCode, String string, String title) {
        Intent intent = new Intent(view.getContext(), EditProfileEnterDetailActivity.class);
        intent.putExtra(Constants.IntentExtras.EDIT_DETAIL, string);
        intent.putExtra(Constants.IntentExtras.TITLE, title);
        baseFragment.startActivityForResultWithAnimation(intent, requestCode);

    }


    public void onCitySelection(View view, BaseFragment baseFragment, int requestCode, String stateId) {

        if (stateId.equals("0")) {
            Tools.showToast(baseFragment.getContext(), baseFragment.getContext().getString(R.string.error_msg_select_state));
            return;
        }

        NetworkHelper.create(view.getContext())
                .setMessage(view.getContext().getString(R.string.dialog_message))
                .callWebService(ApiClient.getClient().getCitiesByStateId(stateId), new NetworkHelper.NetworkCallBack<CityResponseModel>() {
                    @Override
                    public void onSyncData(CityResponseModel data) {
                        if (data.isSuccess()) {
                            Intent intent = new Intent(view.getContext(), EditProfileSelectionActivity.class);
                            ArrayList<SelectionModel> selectionModels = data.getStateData().getCityeModels();
                            if (baseFragment instanceof EditProfilePartnerPrefrenceFragment) {
                                addDoesntMatter(selectionModels, view.getContext());
                            }

                            if (baseFragment instanceof EditProfileBackgroundFragment) {
                                addDontKnow(selectionModels, view.getContext());
                            }
                            intent.putParcelableArrayListExtra(Constants.IntentExtras.MASTER_LIST, selectionModels);
                            intent.putExtra(Constants.IntentExtras.TITLE, view.getContext().getString(R.string.activity_title_select_city));
                            baseFragment.startActivityForResultWithAnimation(intent, requestCode);
                        } else {
                            Tools.showToast(view.getContext(), data.getMessage());
                        }
                    }

                    @Override
                    public void onFailed(CityResponseModel error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });
    }

    public void onStateSelection(View view, BaseFragment baseFragment, int requestCode, String countryId) {

        if (countryId.equals("0")) {
            Tools.showToast(baseFragment.getContext(), baseFragment.getContext().getString(R.string.error_msg_select_country));
            return;
        }

        NetworkHelper.create(view.getContext())
                .setMessage(view.getContext().getString(R.string.dialog_message))
                .callWebService(ApiClient.getClient().getStatesByCountryId(countryId), new NetworkHelper.NetworkCallBack<StateResponseModel>() {
                    @Override
                    public void onSyncData(StateResponseModel data) {
                        if (data.isSuccess()) {
                            Intent intent = new Intent(view.getContext(), EditProfileSelectionActivity.class);
                            ArrayList<SelectionModel> selectionModels = data.getStateData().getStateModels();
                            if (baseFragment instanceof EditProfilePartnerPrefrenceFragment) {
                                addDoesntMatter(selectionModels, view.getContext());
                            }
                            if (baseFragment instanceof EditProfileBackgroundFragment) {
                                addDontKnow(selectionModels, view.getContext());
                            }
                            intent.putParcelableArrayListExtra(Constants.IntentExtras.MASTER_LIST, selectionModels);
                            intent.putExtra(Constants.IntentExtras.TITLE, view.getContext().getString(R.string.activity_title_select_state));
                            baseFragment.startActivityForResultWithAnimation(intent, requestCode);
                        } else {
                            Tools.showToast(view.getContext(), data.getMessage());
                        }
                    }

                    @Override
                    public void onFailed(StateResponseModel error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });


    }

    private void addDoesntMatter(ArrayList<SelectionModel> selectionModels, Context context) {
        if (selectionModels.get(0).getId() != 0) {
            selectionModels.add(0, new SelectionModel(0, context.getString(R.string.doesnt_matter)));
        }

    }

    private void addDontKnow(ArrayList<SelectionModel> selectionModels, Context context) {
        if (selectionModels.get(0).getId() != 0) {
            selectionModels.add(0, new SelectionModel(0, context.getString(R.string.dont_know)));
        }

    }

    public void onBasicInfoSubmitClick(View view, BasicInfoRequestModel model) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        if (model.isValid(activity)) {

            NetworkHelper.create(view.getContext())
                    .setMessage("Update basic info")
                    .callWebService(ApiClient.getClient().updateBasiciInfo(model), new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                        @Override
                        public void onSyncData(UserDetailsResponseModel data) {

                            Tools.showToast(activity, data.getMessage());
                            if (data.isSuccess()) {
                                userDetailModel.set(data.getUserDetailModel());
                                App.getInstance().setUserDetailModel(userDetailModel.get());
                                SharedPreferencesHelper.setIsBasicInfoUpdated(activity, true);
                                activity.setResult(activity.RESULT_OK);
                                activity.onBackPressed();

                            }

                        }

                        @Override
                        public void onFailed(UserDetailsResponseModel error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });
        }
    }

    public void onSubmitAboutMe(View view, String aboutMe) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        Tools.hideKeyboard(activity);
        String clientId = SharedPreferencesHelper.getClientId(activity);
        int aboutMeLength = aboutMe.trim().length();
        if (!aboutMe.trim().equalsIgnoreCase("")) {
            if (aboutMeLength>=30) {
                NetworkHelper.create(activity)
                        .callWebService(ApiClient.getClient().updateAboutMe(clientId, aboutMe),
                                new NetworkHelper.NetworkCallBack<AboutMeResponseModel>() {
                                    @Override
                                    public void onSyncData(AboutMeResponseModel data) {
                                        if (data.isSuccess()) {
                                            userDetailModel.get().setAboutMe(data.getAboutMeData().getAboutMe());
                                            App.getInstance().setUserDetailModel(userDetailModel.get());
                                            SharedPreferencesHelper.setIsAboutMeUpdated(activity, true);
                                            activity.setResult(activity.RESULT_OK);
                                            activity.onBackPressed();
                                        }
                                        Tools.showToast(activity, data.getMessage());
                                    }

                                    @Override
                                    public void onFailed(AboutMeResponseModel error) {

                                    }

                                    @Override
                                    public void noInternetConnection() {

                                    }
                                });
            } else {
                Tools.showToast(activity, activity.getString(R.string.error_msg_aboutme_length));
            }
        } else {
            Tools.showToast(activity, activity.getString(R.string.error_msg_aboutme_empty));
        }

    }

    public void onDobSelect(View view, String dob, CustomDatePicker.OnDateSelectionListener onDateSelectionListener) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(df.parse(dob));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        CustomDatePicker customDatePicker = new CustomDatePicker(onDateSelectionListener, view.getContext());
        customDatePicker.showDate(Constants.ActivityResultRequestCode.SELECT_DOB, cal);

    }

    public void onCasteSelection(View view, BaseFragment baseFragment, int requestCode, String religionId) {

        NetworkHelper.create(view.getContext())
                .setMessage(view.getContext().getString(R.string.dialog_message))
                .callWebService(ApiClient.getClient().getCasteByReligionId(religionId), new NetworkHelper.NetworkCallBack<CastesResponseModel>() {
                    @Override
                    public void onSyncData(CastesResponseModel data) {
                        if (data.isSuccess()) {
                            Intent intent = new Intent(view.getContext(), EditProfileSelectionActivity.class);
                            intent.putParcelableArrayListExtra(Constants.IntentExtras.MASTER_LIST, data.getCastesData().getCastesModels());
                            intent.putExtra(Constants.IntentExtras.TITLE, view.getContext().getString(R.string.activity_title_select_cast));
                            baseFragment.startActivityForResultWithAnimation(intent, requestCode);
                        } else {
                            Tools.showToast(view.getContext(), data.getMessage());
                        }
                    }

                    @Override
                    public void onFailed(CastesResponseModel error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });
    }

    public void onSubCasteSelection(View view, BaseFragment baseFragment, int requestCode, String casteId) {

        NetworkHelper.create(view.getContext())
                .setMessage(view.getContext().getString(R.string.dialog_message))
                .callWebService(ApiClient.getClient().getSubCasteByCasteId(casteId), new NetworkHelper.NetworkCallBack<SubCasteResponseModel>() {
                    @Override
                    public void onSyncData(SubCasteResponseModel data) {
                        if (data.isSuccess()) {

                            ArrayList<SelectionModel> selectionModels = data.getSubCasteData().getSubCastesModels();

                            if (baseFragment instanceof EditProfileBackgroundFragment) {
                                addDontKnow(selectionModels, view.getContext());
                            }

                            Intent intent = new Intent(view.getContext(), EditProfileSelectionActivity.class);
                            intent.putParcelableArrayListExtra(Constants.IntentExtras.MASTER_LIST, selectionModels);
                            intent.putExtra(Constants.IntentExtras.TITLE, view.getContext().getString(R.string.activity_title_select_sub_cast));
                            baseFragment.startActivityForResultWithAnimation(intent, requestCode);
                        } else {
                            Tools.showToast(view.getContext(), data.getMessage());
                        }
                    }

                    @Override
                    public void onFailed(SubCasteResponseModel error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });
    }

    public void onEducationUpdate(View view, EducationRequestModel model) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        if (model.isValid(activity)) {
            NetworkHelper.create(view.getContext())
                    .setMessage(activity.getString(R.string.dialog_msg_update_education))
                    .callWebService(ApiClient.getClient().updateEducation(model), new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                        @Override
                        public void onSyncData(UserDetailsResponseModel data) {

                            Tools.showToast(activity, data.getMessage());
                            if (data.isSuccess()) {
                                userDetailModel.set(data.getUserDetailModel());
                                App.getInstance().setUserDetailModel(userDetailModel.get());
                                SharedPreferencesHelper.setIsEducationUpdated(activity, true);
                                activity.setResult(activity.RESULT_OK);
                                activity.onBackPressed();
                            }

                        }

                        @Override
                        public void onFailed(UserDetailsResponseModel error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });
        }
    }

    public void onCareerUpdate(View view, CareerRequestModel model) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        if (model.isValid(activity)) {
            NetworkHelper.create(view.getContext())
                    .setMessage(activity.getString(R.string.dialog_msg_update_career))
                    .callWebService(ApiClient.getClient().updateCareer(model), new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                        @Override
                        public void onSyncData(UserDetailsResponseModel data) {

                            Tools.showToast(activity, data.getMessage());
                            if (data.isSuccess()) {
                                userDetailModel.set(data.getUserDetailModel());
                                App.getInstance().setUserDetailModel(userDetailModel.get());
                                SharedPreferencesHelper.setIsCareerUpdated(activity, true);
                                activity.setResult(activity.RESULT_OK);
                                activity.onBackPressed();
                            }

                        }

                        @Override
                        public void onFailed(UserDetailsResponseModel error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });
        }
    }

    public void onBackgroundUpdate(View view, BackgroundRequestModel model) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        if (model.isValid(activity)) {
            NetworkHelper.create(view.getContext())
                    .setMessage(activity.getString(R.string.dialog_msg_update_background))
                    .callWebService(ApiClient.getClient().updateBackground(model), new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                        @Override
                        public void onSyncData(UserDetailsResponseModel data) {

                            Tools.showToast(activity, data.getMessage());
                            if (data.isSuccess()) {
                                userDetailModel.set(data.getUserDetailModel());
                                App.getInstance().setUserDetailModel(userDetailModel.get());
                                SharedPreferencesHelper.setIsBackgroundUpdated(activity, true);
                                activity.setResult(activity.RESULT_OK);
                                activity.onBackPressed();
                            }

                        }

                        @Override
                        public void onFailed(UserDetailsResponseModel error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });
        }
    }

    public void onAstroDetailsUpdate(View view, AstroInfoRequestModel model) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        if (model.isValid(activity)) {
            NetworkHelper.create(view.getContext())
                    .setMessage(activity.getString(R.string.dialog_msg_update_astro_details))
                    .callWebService(ApiClient.getClient().updateAstroDetails(model), new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                        @Override
                        public void onSyncData(UserDetailsResponseModel data) {

                            Tools.showToast(activity, data.getMessage());
                            if (data.isSuccess()) {
                                userDetailModel.set(data.getUserDetailModel());
                                App.getInstance().setUserDetailModel(userDetailModel.get());
                                activity.setResult(activity.RESULT_OK);
                                activity.onBackPressed();
                            }

                        }

                        @Override
                        public void onFailed(UserDetailsResponseModel error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });
        }
    }


    public void onFamilyDetailsUpdate(View view, FamilyInfoRequestModel model) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        if (model.isValid(activity)) {
            NetworkHelper.create(view.getContext())
                    .setMessage(activity.getString(R.string.dialog_msg_update_family_details))
                    .callWebService(ApiClient.getClient().updateFamilyDetails(model), new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                        @Override
                        public void onSyncData(UserDetailsResponseModel data) {

                            Tools.showToast(activity, data.getMessage());
                            if (data.isSuccess()) {
                                userDetailModel.set(data.getUserDetailModel());
                                App.getInstance().setUserDetailModel(userDetailModel.get());
                                SharedPreferencesHelper.setIsFamilyDetailsUpdated(activity, true);
                                activity.setResult(activity.RESULT_OK);
                                activity.onBackPressed();
                            }

                        }

                        @Override
                        public void onFailed(UserDetailsResponseModel error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });
        }
    }

    public void onPartnerPreferenceDetailsUpdate(View view, PartnerPrefrenceRequestModel model) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        if (model.isValid(activity)) {
            NetworkHelper.create(view.getContext())
                    .setMessage(activity.getString(R.string.dialog_msg_update_partner_preference_details))
                    .callWebService(ApiClient.getClient().updatePartnerPreferenceDetails(model), new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                        @Override
                        public void onSyncData(UserDetailsResponseModel data) {

                            Tools.showToast(activity, data.getMessage());
                            if (data.isSuccess()) {
                                userDetailModel.set(data.getUserDetailModel());
                                App.getInstance().setUserDetailModel(userDetailModel.get());
                                SharedPreferencesHelper.setIsPartnerPreferenceUpdated(activity, true);
                                activity.setResult(activity.RESULT_OK);
                                activity.onBackPressed();
                            }

                        }

                        @Override
                        public void onFailed(UserDetailsResponseModel error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });
        }
    }

    public void openNumberPickerDialog(View view, String value1, String value2, int requestCode,
                                       CustomNumberPicker.OnNumberSelectionListener listener) {
        openNumberPickerDialog(view, value1, value2, requestCode, listener, 0);
    }

    public void openNumberPickerDialog(View view, String value1, String value2, int requestCode,
                                       CustomNumberPicker.OnNumberSelectionListener listener, int minHeight) {

        Context context = view.getContext();
        CustomNumberPicker customNumberPicker = CustomNumberPicker.create(context);
        customNumberPicker.setMinValue(Constants.MIN_HEIGHT_FEET, Constants.MIN_HEIGHT_INCHES);
        customNumberPicker.setMaxValue(Constants.MAX_HEIGHT_FEET, Constants.MAX_HEIGHT_INCHES);
        customNumberPicker.setNumberPickerTitle(context.getString(R.string.feet_title), context.getString(R.string.inches_title));

        switch (requestCode) {
            case Constants.ActivityResultRequestCode.SELECT_HEIGHT:
                customNumberPicker.setDialogTitle(context.getString(R.string.select_height));
                break;

            case Constants.ActivityResultRequestCode.SELECT_MIN_HEIGHT:
                customNumberPicker.setDialogTitle(context.getString(R.string.select_min_height));
                break;

            case Constants.ActivityResultRequestCode.SELECT_MAX_HEIGHT:
                customNumberPicker.setDialogTitle(context.getString(R.string.select_max_height));
                customNumberPicker.setMinHeight(minHeight);
                break;

            case Constants.ActivityResultRequestCode.SELECT_AGE:
                customNumberPicker.setDialogTitle(context.getString(R.string.select_age));

                customNumberPicker.setMinValue(Constants.MIN_AGE, Integer.parseInt(value1) + 1);
                customNumberPicker.setMaxValue(Constants.MAX_AGE - 1, Constants.MAX_AGE);

                customNumberPicker.setNumberPickerTitle(context.getString(R.string.age_min), context.getString(R.string.age_max));
                break;
        }

        customNumberPicker.setCurrentValue(Integer.parseInt(value1), Integer.parseInt(value2));
        customNumberPicker.show(listener, requestCode);


    }

    public void onCategoryClick(BaseLifecycleActivity activity, String name, boolean isFromCategory) {
        if (name.equalsIgnoreCase(activity.getString(R.string.basic_information))) {
            activity.fragmentTransaction(REPLACE_FRAGMENT, new EditProfileBasicInfoFragment(), R.id.container_edit_profile, isFromCategory);
        } else if (name.equalsIgnoreCase(activity.getString(R.string.about_me))) {
            activity.fragmentTransaction(REPLACE_FRAGMENT, new EditProfileAboutMeFragment(), R.id.container_edit_profile, isFromCategory);
        } else if (name.equalsIgnoreCase(activity.getString(R.string.education))) {
            activity.fragmentTransaction(REPLACE_FRAGMENT, new EditProfileEducationFragment(), R.id.container_edit_profile, isFromCategory);
        } else if (name.equalsIgnoreCase(activity.getString(R.string.career))) {
            activity.fragmentTransaction(REPLACE_FRAGMENT, new EditProfileCareerFragment(), R.id.container_edit_profile, isFromCategory);
        } else if (name.equalsIgnoreCase(activity.getString(R.string.background))) {
            activity.fragmentTransaction(REPLACE_FRAGMENT, new EditProfileBackgroundFragment(), R.id.container_edit_profile, isFromCategory);
        } else if (name.equalsIgnoreCase(activity.getString(R.string.astro_details))) {
            activity.fragmentTransaction(REPLACE_FRAGMENT, new EditProfileAstroDetailFragment(), R.id.container_edit_profile, isFromCategory);
        } else if (name.equalsIgnoreCase(activity.getString(R.string.family_detail))) {
            activity.fragmentTransaction(REPLACE_FRAGMENT, new EditProfileFamilyDetailFragment(), R.id.container_edit_profile, isFromCategory);
        } else if (name.equalsIgnoreCase(activity.getString(R.string.partner_preferences))) {
            activity.fragmentTransaction(REPLACE_FRAGMENT, new EditProfilePartnerPrefrenceFragment(), R.id.container_edit_profile, isFromCategory);
        }
    }


}
