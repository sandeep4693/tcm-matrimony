package in.hvpl.affinita.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;

import java.io.File;
import java.util.ArrayList;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.base.BaseActivity;
import in.hvpl.affinita.common.base.BaseViewModel;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.interfaces.DialogClickListener;
import in.hvpl.affinita.model.ConnectionModel;
import in.hvpl.affinita.model.MessageModel;
import in.hvpl.affinita.model.base.ResponseModel;
import in.hvpl.affinita.model.request.GetClientDetailsModel;
import in.hvpl.affinita.model.response.ChatMessageResponseModel;
import in.hvpl.affinita.model.response.SendResponseModel;
import in.hvpl.affinita.model.response.UserDetailsResponseModel;
import in.hvpl.affinita.view.viewprofile.ViewProfileActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;
import okhttp3.ResponseBody;

import static in.hvpl.affinita.model.MessageModel.HEADER;

/**
 * Created by Ganesh.K on 7/8/17.
 */

public class ChatViewModel extends BaseViewModel {

    public ArrayList<MessageModel> messageModels = new ArrayList<>();
    public MutableLiveData<Boolean> isChatRefresh = new MutableLiveData<>();
    public MutableLiveData<Boolean> isMessageSend = new MutableLiveData<>();
    public ConnectionModel connectionModel;


    public String getParentId() {
        int pos = 0;
        if (!connectionModel.getCommonHobbies().isEmpty()) {
            pos = 1;
        }
        if (messageModels.size() > pos) {
            return messageModels.get(pos).getParentId();
        } else {
            return connectionModel.getMessageModel().getParentId();
        }


    }

    public void getChatMessageList(BaseActivity activity, String messageId) {
        NetworkHelper.create(activity)
                .setMessage(activity.getString(R.string.dialog_msg_loading_message))
                .callWebService(ApiClient.getClient().getChatMessage(getParentId()
                        , String.valueOf(SharedPreferencesHelper.getClientId(activity)), messageId),
                        new NetworkHelper.NetworkCallBack<ChatMessageResponseModel>() {
                            @Override
                            public void onSyncData(ChatMessageResponseModel data) {
                                if (data.isSuccess()) {
                                    if (!connectionModel.getCommonHobbies().isEmpty()) {
                                        messageModels.remove(messageModels.size() - 1);
                                    }
                                    messageModels.addAll(data.getMessageModels());
                                    addChatHeader();
                                    isChatRefresh.setValue(true);

                                } else {
                                    Tools.showToast(activity, data.getMessage());
                                }
                            }

                            @Override
                            public void onFailed(ChatMessageResponseModel error) {

                            }

                            @Override
                            public void noInternetConnection() {

                            }
                        });
    }

    public void sendMessage(View view, String msg) {

        BaseActivity activity = (BaseActivity) view.getContext();
        if (msg.isEmpty()) {
            Tools.showToast(activity, activity.getString(R.string.error_msg_enter_message));
        } else {

            NetworkHelper.create(activity)
                    .setMessage(activity.getString(R.string.dialog_send_message))
                    .callWebService(ApiClient.getClient().sendMessage("" + SharedPreferencesHelper.getClientId(activity)
                            , "" + connectionModel.getClientInfo().getReceiverId(), msg
                            , "" + connectionModel.getClientMatchId(), getParentId()),
                            new NetworkHelper.NetworkCallBack<SendResponseModel>() {
                                @Override
                                public void onSyncData(SendResponseModel data) {
                                    if (data.isSuccess()) {
                                        messageModels.add(0, data.getMessageModel());
                                        isMessageSend.setValue(true);
                                    } else {
                                        Tools.showToast(activity, data.getMessage());
                                    }
                                }

                                @Override
                                public void onFailed(SendResponseModel error) {

                                }

                                @Override
                                public void noInternetConnection() {

                                }
                            });
        }
    }


    public void readMessage(BaseActivity activity, String messageId) {

        NetworkHelper.create(activity)
                .setShowProgressLoading(false)
                .setShowOnFailureDialog(false)
                .callWebService(ApiClient.getClient().readMessage(messageId), new NetworkHelper.NetworkCallBack<ResponseModel>() {
                    @Override
                    public void onSyncData(ResponseModel data) {

                    }

                    @Override
                    public void onFailed(ResponseModel error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });


    }

    public void addChatHeader() {
        if (!connectionModel.getCommonHobbies().isEmpty()) {
            MessageModel messageModel = new MessageModel();
            messageModel.setmType(HEADER);
            messageModel.setBody(connectionModel.getCommonHobbies());
            messageModels.add(messageModel);

        }
    }

    public void onMoreOption(View view) {
        Context context = view.getContext();
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        popup.inflate(R.menu.chat_more_option_menu);
        MenuItem menuItem = popup.getMenu().findItem(R.id.clear_chat);
        if (messageModels.size() == 0) {
            menuItem.setVisible(false);
        }

        popup.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.clear_chat:
                    Tools.showAlertDialog(context,
                            "",
                            context.getString(R.string.alert_msg_clear_chat),
                            "Yes", "No", 0, new DialogClickListener() {
                                @Override
                                public void onPositiveClick(int requestCode) {
                                    clearChat(context);
                                }

                                @Override
                                public void onNegativeClick(int requestCode) {

                                }
                            });

                    break;

                case R.id.report_user:
                    Tools.showAlertDialog(context,
                            "",
                            context.getString(R.string.alert_msg_report_user),
                            "Yes", "No", 0, new DialogClickListener() {
                                @Override
                                public void onPositiveClick(int requestCode) {
                                    reportUser(context);
                                }

                                @Override
                                public void onNegativeClick(int requestCode) {

                                }
                            });
                    break;
            }
            return false;
        });
        //displaying the popup
        popup.show();

    }

    private void reportUser(Context context) {
        NetworkHelper.create(context)
                .setMessage(context.getString(R.string.dialog_report_user))
                .callWebService(ApiClient.getClient().reportUser(String.valueOf(connectionModel.getClientMatchId())
                        , String.valueOf(SharedPreferencesHelper.getClientId(context)))
                        , new NetworkHelper.NetworkCallBack<ResponseModel>() {
                            @Override
                            public void onSyncData(ResponseModel data) {
                                Tools.showToast(context, data.getMessage());
                                if (data.isSuccess()) {
                                    ((BaseActivity) context).onBackPressed();
                                }

                            }

                            @Override
                            public void onFailed(ResponseModel error) {

                            }

                            @Override
                            public void noInternetConnection() {

                            }
                        });
    }

    private void clearChat(Context context) {
        NetworkHelper.create(context)
                .setMessage(context.getString(R.string.dialog_clear_chat))
                .callWebService(ApiClient.getClient().clearChat(getParentId()
                        , String.valueOf(SharedPreferencesHelper.getClientId(context)))
                        , new NetworkHelper.NetworkCallBack<ResponseModel>() {
                            @Override
                            public void onSyncData(ResponseModel data) {

                                if (data.isSuccess()) {
                                    messageModels.clear();
                                    isChatRefresh.setValue(true);
                                }
                                Tools.showToast(context, data.getMessage());
                            }

                            @Override
                            public void onFailed(ResponseModel error) {

                            }

                            @Override
                            public void noInternetConnection() {

                            }
                        });
    }

    public void onProfileViewClick(View view) {
        BaseLifecycleActivity activity = (BaseLifecycleActivity) view.getContext();
        NetworkHelper.create(activity)
                .callWebService(ApiClient.getClient().getClientDetails(new GetClientDetailsModel(String.valueOf(connectionModel.getClientInfo().getReceiverId()), String.valueOf(connectionModel.getClientMatchId()))),
                        new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                            @Override
                            public void onSyncData(UserDetailsResponseModel data) {
                                if (data.isSuccess()) {
                                    App.getInstance().setUserProfileDetailModel(data.getUserDetailModel());
                                    Intent intent = new Intent(activity, ViewProfileActivity.class);
                                    intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_PROPOSAL);
                                    intent.putExtra(Constants.IntentExtras.IMAGE_PATH, Tools.getProposalPhoto());
                                    intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE,"");
                       /* intent.putExtra(Constants.IntentExtras.PROPOSAL_MATCH_ID, matchId);
                        intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE, type);*/
                                    activity.startActivityWithAnimation(intent);

                                    //downloadAudio(activity);
                                } else {
                                    Tools.showToast(activity, data.getMessage());
                                }

                            }

                            @Override
                            public void onFailed(UserDetailsResponseModel error) {

                            }

                            @Override
                            public void noInternetConnection() {

                            }

                        });


    }

    public void downloadAudio(BaseLifecycleActivity activity) {
        File audioFile = new File(Tools.getProfileAudioFile());
        File imageFile = new File(Tools.getProposalPhoto());
        if (imageFile.exists()) {
            imageFile.delete();
        }
        if (audioFile.exists()) {
            audioFile.delete();
        }
        if (App.getInstance().getUserProfileDetailModel().getAudioFile().equals("")) {
            Intent intent = new Intent(activity, ViewProfileActivity.class);
            intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_PROPOSAL);
            intent.putExtra(Constants.IntentExtras.IMAGE_PATH, Tools.getProposalPhoto());
            intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE,"");
            activity.startActivityWithAnimation(intent);
        } else {

            NetworkHelper.create(activity)
                    .setMessage("Download Audio File")
                    .downloadFile(App.getInstance().getUserProfileDetailModel().getAudioFile(), audioFile, new NetworkHelper.NetworkCallBack<ResponseBody>() {
                        @Override
                        public void onSyncData(ResponseBody data) {
                            playAudioActivity(activity, imageFile);
                        }

                        @Override
                        public void onFailed(ResponseBody error) {

                        }

                        @Override
                        public void noInternetConnection() {

                        }
                    });

        }
    }

    private void playAudioActivity(BaseLifecycleActivity activity, File imageFile) {

        String image = App.getInstance().getUserProfileDetailModel().getBasicInformationModel().getPhotoUrl();

        NetworkHelper.create(activity)
                .setMessage("Download Image File")
                .downloadFile(image, imageFile, new NetworkHelper.NetworkCallBack<ResponseBody>() {
                    @Override
                    public void onSyncData(ResponseBody data) {
                        Intent intent = new Intent(activity, ViewProfileActivity.class);
                        intent.putExtra(Constants.IntentExtras.COME_FROM, Constants.ActivityComeFrom.COME_FROM_PROPOSAL);
                        intent.putExtra(Constants.IntentExtras.IMAGE_PATH, Tools.getProposalPhoto());
                        intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE,"");
                       /* intent.putExtra(Constants.IntentExtras.PROPOSAL_MATCH_ID, matchId);
                        intent.putExtra(Constants.IntentExtras.PROPOSAL_TYPE, type);*/
                        activity.startActivityWithAnimation(intent);
                    }

                    @Override
                    public void onFailed(ResponseBody error) {

                    }

                    @Override
                    public void noInternetConnection() {

                    }
                });
    }
}
