package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 20/6/17.
 */

    public class ParentInformationModel {

    @SerializedName("id")
    @Expose
    private int parentId;

    @SerializedName("name")
    @Expose
    private String parentName;

    @SerializedName("relation")
    @Expose
    private String relation;

    @SerializedName("occupation_type")
    @Expose
    private SelectionModel occupationTypeModel;

    @SerializedName("occupation_field")
    @Expose
    private SelectionModel occupationFieldModel;

    @SerializedName("company_name")
    @Expose
    private String companyName;

    @SerializedName("designations")
    @Expose
    private SelectionModel designationModel;

    @SerializedName("educations")
    @Expose
    private SelectionModel educationModel;

    @SerializedName("stream")
    @Expose
    private String stream;


    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public SelectionModel getOccupationTypeModel() {
        return occupationTypeModel;
    }

    public void setOccupationTypeModel(SelectionModel occupationTypeModel) {
        this.occupationTypeModel = occupationTypeModel;
    }

    public SelectionModel getOccupationFieldModel() {
        return occupationFieldModel;
    }

    public void setOccupationFieldModel(SelectionModel occupationFieldModel) {
        this.occupationFieldModel = occupationFieldModel;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public SelectionModel getDesignationModel() {
        return designationModel;
    }

    public void setDesignationModel(SelectionModel designationModel) {
        this.designationModel = designationModel;
    }

    public SelectionModel getEducationModel() {
        return educationModel;
    }

    public void setEducationModel(SelectionModel educationModel) {
        this.educationModel = educationModel;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    @Override
    public String toString() {
        return "ParentInformationModel{" +
                "parentId=" + parentId +
                ", parentName='" + parentName + '\'' +
                ", occupationTypeModel=" + occupationTypeModel +
                ", occupationFieldModel=" + occupationFieldModel +
                ", companyName='" + companyName + '\'' +
                ", designationModel=" + designationModel +
                ", educationModel=" + educationModel +
                ", stream='" + stream + '\'' +
                '}';
    }
}
