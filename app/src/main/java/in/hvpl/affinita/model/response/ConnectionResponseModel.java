package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import in.hvpl.affinita.model.ConnectionModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by Ganesh.K on 4/8/17.
 */

public class ConnectionResponseModel extends ResponseModel {

    @SerializedName("data")
    @Expose
    private ConnectionData connectionData;

    public ConnectionData getConnectionData() {
        return connectionData;
    }

    public void setConnectionData(ConnectionData connectionData) {
        this.connectionData = connectionData;
    }

    public class ConnectionData{

        @SerializedName("connectionList")
        @Expose
        private ArrayList<ConnectionModel> connectionModels;


        @SerializedName("unread_message_count")
        @Expose
        private int unreadMsgCount;

        public ArrayList<ConnectionModel> getConnectionModels() {
            return connectionModels;
        }

        public void setConnectionModels(ArrayList<ConnectionModel> connectionModels) {
            this.connectionModels = connectionModels;
        }

        public int getUnreadMsgCount() {
            return unreadMsgCount;
        }

        public void setUnreadMsgCount(int unreadMsgCount) {
            this.unreadMsgCount = unreadMsgCount;
        }
    }
}
