package in.hvpl.affinita.model.response;

import android.databinding.ObservableArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by Ganesh.K on 17/7/17.
 */

public class AlbumUploadResponseModel extends ResponseModel {

    @SerializedName("data")
    @Expose
    ObservableArrayList<AlbumInformationModel> albumInformationModels;

    public ObservableArrayList<AlbumInformationModel> getAlbumInformationModels() {
        return albumInformationModels;
    }

    public void setAlbumInformationModels(ObservableArrayList<AlbumInformationModel> albumInformationModels) {
        this.albumInformationModels = albumInformationModels;
    }
}
