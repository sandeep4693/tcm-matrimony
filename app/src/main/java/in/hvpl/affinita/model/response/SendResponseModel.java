package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.MessageModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by Ganesh.K on 7/8/17.
 */

public class SendResponseModel extends ResponseModel {

    @SerializedName("data")
    @Expose
    private MessageModel messageModel;

    public MessageModel getMessageModel() {
        return messageModel;
    }

    public void setMessageModel(MessageModel messageModel) {
        this.messageModel = messageModel;
    }
}
