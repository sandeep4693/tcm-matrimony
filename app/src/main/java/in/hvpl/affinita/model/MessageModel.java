package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import in.hvpl.affinita.App;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;

/**
 * Created by Ganesh.K on 4/8/17.
 */

public class MessageModel  implements Serializable {

    public static final int HEADER = 0;
    public static final int DATA = 1;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("client_match_id")
    @Expose
    private int clientMatchId;

    @SerializedName("sender_id")
    @Expose
    private int senderId;

    @SerializedName("receiver_id")
    @Expose
    private int receiverId;

    @SerializedName("parent_id")
    @Expose
    private String parentId;

    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("is_read")
    @Expose
    private String isRead;

    private String dateTime;

    @SerializedName("created_at")
    @Expose
    private String createdDate;


    private boolean isSend;

    private int mType;

    public MessageModel() {
        mType=DATA;
    }

    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public boolean isSend() {
        setSend(senderId== Integer.parseInt(SharedPreferencesHelper.getClientId(App.getInstance())));
        return isSend;
    }

    public void setSend(boolean send) {

        isSend = send;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClientMatchId() {
        return clientMatchId;
    }

    public void setClientMatchId(int clientMatchId) {
        this.clientMatchId = clientMatchId;
    }


    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;



    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public String getParentId() {
        if(parentId==null)
            parentId="0";
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }


    public String getDateTime() {
        setDateTime(Tools.getMessageDateORTime(createdDate));
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;

    }


    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;


    }
}
