package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by Ganesh.K on 19/7/17.
 */

public class HobbiesResponseModel extends ResponseModel {

    @SerializedName("data")
    @Expose
    private HobbiesResponseData hobbiesResponseData;

    public HobbiesResponseData getHobbiesResponseData() {
        return hobbiesResponseData;
    }

    public void setHobbiesResponseData(HobbiesResponseData hobbiesResponseData) {
        this.hobbiesResponseData = hobbiesResponseData;
    }

    public class HobbiesResponseData {

        private String id;

        @SerializedName("hobbies")
        @Expose
        private String hobbies;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHobbies() {
            return hobbies;
        }

        public void setHobbies(String hobbies) {
            this.hobbies = hobbies;
        }
    }
}
