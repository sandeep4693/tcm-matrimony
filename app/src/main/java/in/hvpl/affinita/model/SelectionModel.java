package in.hvpl.affinita.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 21/6/17.
 */


public class SelectionModel implements Parcelable {


    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("title")
    @Expose
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        if (title == null)
            title = "";
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "SelectionModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    public SelectionModel() {

    }

    public SelectionModel(int id, String title) {
        this.id = id;
        this.title = title;
    }

    protected SelectionModel(Parcel in) {
        id = in.readInt();
        title = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SelectionModel> CREATOR = new Parcelable.Creator<SelectionModel>() {
        @Override
        public SelectionModel createFromParcel(Parcel in) {
            return new SelectionModel(in);
        }

        @Override
        public SelectionModel[] newArray(int size) {
            return new SelectionModel[size];
        }
    };
}
