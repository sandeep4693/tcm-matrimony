package in.hvpl.affinita.model.response;

import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks on 19/6/17.
 */


public class RegistrationResponseModel extends ResponseModel {

   /* @SerializedName("data")
    @Expose*/
    public RegistrationData registrationData;

    public RegistrationData getRegistrationData() {
        return registrationData;
    }

    public void setRegistrationData(RegistrationData registrationData) {
        this.registrationData = registrationData;
    }

    public class RegistrationData {

        private String id;
        private String otp;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        @Override
        public String toString() {
            return "RegistrationData{" +
                    "id='" + id + '\'' +
                    ", otp='" + otp + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "RegistrationResponseModel{" +
                "registrationData=" + registrationData +
                '}';
    }
}
