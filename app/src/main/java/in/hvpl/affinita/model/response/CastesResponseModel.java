package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks1 on 27/7/17.
 */

public class CastesResponseModel extends ResponseModel{

    @SerializedName("data")
    @Expose
    private CastesData castesData;



    public CastesData getCastesData() {
        return castesData;
    }

    public void setCastesData(CastesData castesData) {
        this.castesData = castesData;
    }

    public class CastesData {
        @SerializedName("castes")
        @Expose
        private ArrayList<SelectionModel> castesModels;

        public ArrayList<SelectionModel> getCastesModels() {
            return castesModels;
        }

        public void setCastesModels(ArrayList<SelectionModel> castesModels) {
            this.castesModels = castesModels;
        }
    }

}
