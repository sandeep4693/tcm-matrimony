package in.hvpl.affinita.model.request;

import android.content.Context;
import android.databinding.Bindable;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.App;
import in.hvpl.affinita.BR;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.PartnerPreferenceDetailsModel;
import in.hvpl.affinita.model.base.RequestModel;

import static in.hvpl.affinita.common.utils.Tools.isValidId;

/**
 * Created by webwerks1 on 27/7/17.
 */

public class PartnerPrefrenceRequestModel extends RequestModel {

    @SerializedName("age_min")
    @Expose
    private String fromAge;

    @SerializedName("age_max")
    @Expose
    private String toAge;

    private String fromFeet;

    private String toFeet;

    private String fromInches;

    private String toInches;

    @SerializedName("height_min")
    @Expose
    private String heightMin;

    @SerializedName("height_max")
    @Expose
    private String heightMax;

    private String country;

    @SerializedName("country_id")
    @Expose
    private String countryId;

    private String state;

    @SerializedName("state_ids")
    @Expose
    private String stateId;

    private String city;

    @SerializedName("city_ids")
    @Expose
    private String cityId;

    private String martialStatus;

    @SerializedName("marital_status_ids")
    @Expose
    private String martialStatusId;

    private String dietTitle;

    @SerializedName("diet_type_ids")
    @Expose
    private String dietId;

    private String smoking;

    @SerializedName("smoke_ids")
    @Expose
    private String smokingId;

    private String drinking;

    @SerializedName("drink_ids")
    @Expose
    private String drinkingId;

    private String education;

    @SerializedName("education_ids")
    @Expose
    private String educationId;

    private String religion;

    @SerializedName("religion_ids")
    @Expose
    private String religionId;

    private String caste;

    @SerializedName("caste_ids")
    @Expose
    private String casteId;

    private String motherTongue;

    @SerializedName("mother_tongue_ids")
    @Expose
    private String motherTongueId;

    private String manglik;

    @SerializedName("manglik_check_ids")
    @Expose
    private String manglikId;

    @Bindable
    public String getReligion() {
        if(religion.equals("")){
            this.religion = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
        notifyPropertyChanged(BR.religion);
    }

    @Bindable
    public String getReligionId() {
        return religionId;
    }

    public void setReligionId(String religionId) {
        this.religionId = religionId;
        notifyPropertyChanged(BR.religionId);
    }

    @Bindable
    public String getCaste() {
        if(caste.equals("")){
            this.caste = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
        notifyPropertyChanged(BR.caste);
    }

    @Bindable
    public String getCasteId() {
        return casteId;
    }

    public void setCasteId(String casteId) {
        this.casteId = casteId;
        notifyPropertyChanged(BR.casteId);
    }

    @Bindable
    public String getMotherTongue() {
        if(motherTongue.equals("")){
            this.motherTongue = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return motherTongue;
    }

    public void setMotherTongue(String motherTongue) {
        this.motherTongue = motherTongue;
        notifyPropertyChanged(BR.motherTongue);
    }

    @Bindable
    public String getMotherTongueId() {
        return motherTongueId;
    }

    public void setMotherTongueId(String motherTongueId) {
        this.motherTongueId = motherTongueId;
        notifyPropertyChanged(BR.motherTongueId);
    }

    @Bindable
    public String getManglik() {
        if(manglik.equals("")){
            this.manglik = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return manglik;
    }

    public void setManglik(String manglik) {
        this.manglik = manglik;
        notifyPropertyChanged(BR.manglik);
    }

    @Bindable
    public String getManglikId() {
        return manglikId;
    }

    public void setManglikId(String manglikId) {
        this.manglikId = manglikId;
        notifyPropertyChanged(BR.manglikId);
    }

    public PartnerPrefrenceRequestModel(String clientId, PartnerPreferenceDetailsModel partnerPreferenceDetailsModel) {
        super(clientId);
        this.fromAge = partnerPreferenceDetailsModel.getAgeMin();
        this.toAge = partnerPreferenceDetailsModel.getAgeMax();
        this.fromFeet = partnerPreferenceDetailsModel.getHeightMinModel().getFeet();
        this.toFeet = partnerPreferenceDetailsModel.getHeightMaxModel().getFeet();
        this.fromInches = partnerPreferenceDetailsModel.getHeightMinModel().getInches();
        this.toInches = partnerPreferenceDetailsModel.getHeightMaxModel().getInches();
        this.country = partnerPreferenceDetailsModel.getCountryModel().getTitle();
        this.countryId = "" + partnerPreferenceDetailsModel.getCountryModel().getId();
        this.state = partnerPreferenceDetailsModel.getStateModel().getTitle();
        this.stateId = "" + partnerPreferenceDetailsModel.getStateModel().getId();
        this.city = partnerPreferenceDetailsModel.getCityModel().getTitle();
        this.cityId = "" + partnerPreferenceDetailsModel.getCityModel().getId();
        this.martialStatus = partnerPreferenceDetailsModel.getMaritalStatusModel().getTitle();
        this.martialStatusId = "" + partnerPreferenceDetailsModel.getMaritalStatusModel().getId();
        this.dietTitle = partnerPreferenceDetailsModel.getDietTypeModel().getTitle();
        this.dietId = "" + partnerPreferenceDetailsModel.getDietTypeModel().getId();
        this.smoking = partnerPreferenceDetailsModel.getSmokeModel().getTitle();
        this.smokingId = "" + partnerPreferenceDetailsModel.getSmokeModel().getId();
        this.drinking = partnerPreferenceDetailsModel.getDrinkModel().getTitle();
        this.drinkingId = "" + partnerPreferenceDetailsModel.getDrinkModel().getId();
        this.education = partnerPreferenceDetailsModel.getEducationModel().getTitle();
        this.educationId = "" + partnerPreferenceDetailsModel.getEducationModel().getId();
        this.religion = partnerPreferenceDetailsModel.getReligionModel().getTitle();
        this.religionId = "" + partnerPreferenceDetailsModel.getReligionModel().getId();
        this.caste = partnerPreferenceDetailsModel.getCasteModel().getTitle();
        this.casteId = "" + partnerPreferenceDetailsModel.getCasteModel().getId();
        this.motherTongue = partnerPreferenceDetailsModel.getMotherTongueModel().getTitle();
        this.motherTongueId = "" + partnerPreferenceDetailsModel.getMotherTongueModel().getId();
        this.manglik = partnerPreferenceDetailsModel.getManglikCheckModel().getTitle();
        this.manglikId = "" + partnerPreferenceDetailsModel.getManglikCheckModel().getId();
    }

    public boolean isValid(Context context) {

        if (!isValidId(smokingId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_smoking), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(drinkingId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_drinking), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(dietId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_diet), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(smokingId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_smoking), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(drinkingId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_drinking), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(martialStatusId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_marital_status), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(educationId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_education), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(religionId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_religion), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(casteId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_caste), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(countryId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_country), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(stateId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_state), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(cityId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_city), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(motherTongueId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_mother_tongue), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidId(manglikId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_manglik), Toast.LENGTH_LONG);
            return false;
        }


        return true;
    }

    @Bindable
    public String getFromAge() {
        return fromAge;
    }

    public void setFromAge(String fromAge) {
        this.fromAge = fromAge;
        notifyPropertyChanged(BR.fromAge);
    }

    @Bindable
    public String getToAge() {
        return toAge;
    }

    public void setToAge(String toAge) {
        this.toAge = toAge;
        notifyPropertyChanged(BR.toAge);
    }

    @Bindable
    public String getFromFeet() {
        return fromFeet;
    }

    public void setFromFeet(String fromFeet) {
        this.fromFeet = fromFeet;
        notifyPropertyChanged(BR.fromFeet);
        setHeightMin(Tools.getConvertedHight(this.fromFeet, this.fromInches));
    }

    @Bindable
    public String getToFeet() {
        return toFeet;
    }

    public void setToFeet(String toFeet) {
        this.toFeet = toFeet;
        notifyPropertyChanged(BR.toFeet);
        setHeightMax(Tools.getConvertedHight(this.toFeet, this.toInches));
    }

    @Bindable
    public String getFromInches() {
        return fromInches;
    }

    public void setFromInches(String fromInches) {
        this.fromInches = fromInches;
        notifyPropertyChanged(BR.fromInches);
        setHeightMin(Tools.getConvertedHight(this.fromFeet, this.fromInches));
    }

    @Bindable
    public String getToInches() {
        return toInches;
    }

    public void setToInches(String toInches) {
        this.toInches = toInches;
        notifyPropertyChanged(BR.toInches);
        setHeightMax(Tools.getConvertedHight(this.toFeet, this.toInches));
    }

    @Bindable
    public String getCountry() {
        if(country.equals("")){
            this.country = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
        notifyPropertyChanged(BR.country);
    }

    @Bindable
    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
        notifyPropertyChanged(BR.countryId);
    }

    @Bindable
    public String getState() {
        if(state.equals("")){
            this.state = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyPropertyChanged(BR.state);
    }

    @Bindable
    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
        notifyPropertyChanged(BR.stateId);
    }

    @Bindable
    public String getCity() {
        if(city.equals("")){
            this.city = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        notifyPropertyChanged(BR.city);
    }

    @Bindable
    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
        notifyPropertyChanged(BR.cityId);
    }

    @Bindable
    public String getMartialStatus() {
        if(martialStatus.equals("")){
            this.martialStatus = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return martialStatus;
    }

    public void setMartialStatus(String martialStatus) {
        this.martialStatus = martialStatus;
        notifyPropertyChanged(BR.martialStatus);
    }

    @Bindable
    public String getMartialStatusId() {
        return martialStatusId;
    }

    public void setMartialStatusId(String martialStatusId) {
        this.martialStatusId = martialStatusId;
        notifyPropertyChanged(BR.martialStatusId);
    }

    @Bindable
    public String getDietTitle() {
        if(dietTitle.equals("")){
            this.dietTitle = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return dietTitle;
    }

    public void setDietTitle(String dietTitle) {
        this.dietTitle = dietTitle;

        notifyPropertyChanged(BR.dietTitle);
    }

    @Bindable
    public String getDietId() {
        return dietId;
    }

    public void setDietId(String dietId) {
        this.dietId = dietId;
        notifyPropertyChanged(BR.dietId);
    }

    @Bindable
    public String getSmoking() {
        if(smoking.equals("")){
            this.smoking = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
        notifyPropertyChanged(BR.smoking);
    }

    @Bindable
    public String getSmokingId() {
        return smokingId;
    }

    public void setSmokingId(String smokingId) {
        this.smokingId = smokingId;
        notifyPropertyChanged(BR.smokingId);
    }

    @Bindable
    public String getDrinking() {
        if(drinking.equals("")){
            this.drinking = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return drinking;
    }

    public void setDrinking(String drinking) {
        this.drinking = drinking;
        notifyPropertyChanged(BR.drinking);
    }

    @Bindable
    public String getDrinkingId() {
        return drinkingId;
    }

    public void setDrinkingId(String drinkingId) {
        this.drinkingId = drinkingId;
        notifyPropertyChanged(BR.drinkingId);
    }

    @Bindable
    public String getEducation() {
        if(education.equals("")){
            this.education = App.getInstance().getApplicationContext().getString(R.string.doesnt_matter);
        }
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
        notifyPropertyChanged(BR.education);
    }

    @Bindable
    public String getEducationId() {
        return educationId;
    }

    public void setEducationId(String educationId) {
        this.educationId = educationId;
        notifyPropertyChanged(BR.educationId);
    }

    @Bindable
    public String getHeightMin() {
        return heightMin;
    }

    public void setHeightMin(String heightMin) {
        this.heightMin = heightMin;
        notifyPropertyChanged(BR.heightMin);
    }

    @Bindable
    public String getHeightMax() {
        return heightMax;
    }

    public void setHeightMax(String heightMax) {
        this.heightMax = heightMax;
        notifyPropertyChanged(BR.heightMax);
    }
}
