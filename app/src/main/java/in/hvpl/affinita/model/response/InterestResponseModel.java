
package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.InterestQuestionaryData;
import in.hvpl.affinita.model.base.ResponseModel;

public class InterestResponseModel extends ResponseModel{

    @SerializedName("data")
    @Expose
    private InterestQuestionaryData data;


    public InterestQuestionaryData getData() {
        return data;
    }

    public void setData(InterestQuestionaryData data) {
        this.data = data;
    }


}
