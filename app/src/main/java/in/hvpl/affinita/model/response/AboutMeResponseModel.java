package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks1 on 26/7/17.
 */

public class AboutMeResponseModel extends ResponseModel {


    @SerializedName("data")
    @Expose
    private AboutMeData aboutMeData;

    public AboutMeData getAboutMeData() {
        return aboutMeData;
    }

    public void setAboutMeData(AboutMeData aboutMeData) {
        this.aboutMeData = aboutMeData;
    }

    public class AboutMeData{

        String id;
        @SerializedName("about_me")
        @Expose
        String aboutMe;
        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getAboutMe() {
            return aboutMe;
        }

        public void setAboutMe(String aboutMe) {
            this.aboutMe = aboutMe;
        }

    }

}
