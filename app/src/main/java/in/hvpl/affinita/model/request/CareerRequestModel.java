package in.hvpl.affinita.model.request;

import android.content.Context;
import android.databinding.Bindable;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.BR;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.EducationInformationModel;
import in.hvpl.affinita.model.base.RequestModel;

import static in.hvpl.affinita.common.utils.Tools.isValidValue;

/**
 * Created by webwerks1 on 26/7/17.
 */

public class CareerRequestModel extends RequestModel {

    @SerializedName("company_name")
    @Expose
    private String companyName;

    private String fieldName;

    @SerializedName("occupation_field_ids")
    @Expose
    private String fieldId;

    private String designationName;

    @SerializedName("designation_ids")
    @Expose
    private String designationId;

    private String salaryRange;

    @SerializedName("salary_range_ids")
    @Expose
    private String salaryRangeId;

    private String currencyTitle;

    @SerializedName("currency_ids")
    @Expose
    private String currencyId;


    public CareerRequestModel(String clientId,EducationInformationModel educationInformationModel) {
        super(clientId);
        this.companyName = educationInformationModel.getCompanyName();
        this.designationId = "" + educationInformationModel.getDesignationModel().getId();
        this.designationName = educationInformationModel.getDesignationModel().getTitle();
        this.fieldId = "" + educationInformationModel.getOccupationFieldModel().getId();
        this.fieldName = educationInformationModel.getOccupationFieldModel().getTitle();
        this.salaryRange = educationInformationModel.getSalaryRangeModel().getTitle();
        this.salaryRangeId = "" + educationInformationModel.getSalaryRangeModel().getId();
        this.currencyTitle = educationInformationModel.getCurrencyModel().getTitle();
        this.currencyId = "" + educationInformationModel.getCurrencyModel().getId();
    }

    public boolean isValid(Context context) {

      /*  if (!isValidValue(companyName)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_enter_company_name), Toast.LENGTH_LONG);
            return false;
        } else*/ if (!isValidValue(designationId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_position), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(fieldId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_industry), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(salaryRangeId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_salary_range), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(designationName)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_position), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(fieldName)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_industry), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(salaryRange)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_salary_range), Toast.LENGTH_LONG);
            return false;
        }

        return true;
    }

    @Bindable
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
        notifyPropertyChanged(BR.companyName);
    }

    @Bindable
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
        notifyPropertyChanged(BR.fieldName);
    }

    @Bindable
    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
        notifyPropertyChanged(BR.fieldId);
    }

    @Bindable
    public String getDesignationName() {
        return designationName;
    }

    public void setDesignationName(String designationName) {
        this.designationName = designationName;
        notifyPropertyChanged(BR.designationName);
    }

    @Bindable
    public String getDesignationId() {
        return designationId;
    }

    public void setDesignationId(String designationId) {
        this.designationId = designationId;
        notifyPropertyChanged(BR.designationId);
    }

    @Bindable
    public String getSalaryRange() {
        return salaryRange;
    }

    public void setSalaryRange(String salaryRange) {
        this.salaryRange = salaryRange;
        notifyPropertyChanged(BR.salaryRange);
    }

    @Bindable
    public String getSalaryRangeId() {
        return salaryRangeId;
    }

    public void setSalaryRangeId(String salaryRangeId) {
        this.salaryRangeId = salaryRangeId;
        notifyPropertyChanged(BR.salaryRangeId);
    }

    @Bindable
    public String getCurrencyTitle() {
        return currencyTitle;
    }

    public void setCurrencyTitle(String currencyTitle) {
        this.currencyTitle = currencyTitle;
        notifyPropertyChanged(BR.currencyTitle);
    }

    @Bindable
    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
        notifyPropertyChanged(BR.currencyId);
    }
}
