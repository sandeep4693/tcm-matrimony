package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by Ganesh.K on 26/7/17.
 */

public class CityResponseModel extends ResponseModel{

    @SerializedName("data")
    @Expose
    private CityData cityData;

    public CityData getStateData() {
        return cityData;
    }

    public void setStateData(CityData cityData) {
        this.cityData = cityData;
    }

   public class CityData {
        @SerializedName("cities")
        @Expose
       private ArrayList<SelectionModel> cityeModels;


       public ArrayList<SelectionModel> getCityeModels() {
           return cityeModels;
       }

       public void setCityeModels(ArrayList<SelectionModel> cityeModels) {
           this.cityeModels = cityeModels;
       }
   }
}
