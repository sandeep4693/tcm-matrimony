package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 22/6/17.
 */

public class HeightModel {

    @SerializedName("feet")
    @Expose
    private String feet;

    @SerializedName("inches")
    @Expose
    private String inches;

    public HeightModel() {
        this.feet = "0";
        this.inches = "0";
    }

    public String getFeet() {
        return feet;
    }

    public void setFeet(String feet) {
        this.feet = feet;
    }

    public String getInches() {
        return inches;
    }

    public void setInches(String inches) {
        this.inches = inches;
    }

    @Override
    public String toString() {
        return "HeightModel{" +
                "feet='" + feet + '\'' +
                ", inches='" + inches + '\'' +
                '}';
    }
}
