package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 21/6/17.
 */

public class NativeModel {

    @SerializedName("native_address")
    @Expose
    private String nativeAddress;

    @SerializedName("native_country")
    @Expose
    private SelectionModel nativeCountry;

    @SerializedName("native_state")
    @Expose
    private SelectionModel nativeState;

    @SerializedName("native_city")
    @Expose
    private SelectionModel nativeCity;

    @SerializedName("native_pincode")
    @Expose
    private String nativePincode;

    public String getNativeAddress() {
        return nativeAddress;
    }

    public void setNativeAddress(String nativeAddress) {
        this.nativeAddress = nativeAddress;
    }

    public SelectionModel getNativeCountry() {
        if(nativeCountry==null)
            nativeCountry=new SelectionModel();
        return nativeCountry;
    }

    public void setNativeCountry(SelectionModel nativeCountry) {
        this.nativeCountry = nativeCountry;
    }

    public SelectionModel getNativeState() {
        if(nativeState==null)
            nativeState=new SelectionModel();
        return nativeState;
    }

    public void setNativeState(SelectionModel nativeState) {
        this.nativeState = nativeState;
    }

    public SelectionModel getNativeCity() {
        if(nativeCity==null)
            nativeCity=new SelectionModel();
        return nativeCity;
    }

    public void setNativeCity(SelectionModel nativeCity) {
        this.nativeCity = nativeCity;
    }

    public String getNativePincode() {

        return nativePincode;
    }

    public void setNativePincode(String nativePincode) {
        this.nativePincode = nativePincode;
    }

    @Override
    public String toString() {
        return "NativeModel{" +
                "nativeAddress='" + nativeAddress + '\'' +
                ", nativeCountry=" + nativeCountry +
                ", nativeState=" + nativeState +
                ", nativeCity=" + nativeCity +
                ", nativePincode='" + nativePincode + '\'' +
                '}';
    }
}
