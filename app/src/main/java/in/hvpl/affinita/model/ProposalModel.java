package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks1 on 18/7/17.
 */

public class ProposalModel  {


    @SerializedName("id")
    @Expose
    private String matchId;


    @SerializedName("client_id")
    @Expose
    private String profileId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("is_viewed_profile")
    @Expose
    private int isView;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("photo_path")
    @Expose
    private String imageUrl;


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public int isView() {
        return isView;
    }

    public void setView(int view) {
        isView = view;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }

    public int getIsView() {
        return isView;
    }

    public void setIsView(int isView) {
        this.isView = isView;
    }
}
