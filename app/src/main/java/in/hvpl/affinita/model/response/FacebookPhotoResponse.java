package in.hvpl.affinita.model.response;

import android.databinding.ObservableArrayList;

import in.hvpl.affinita.model.AlbumInformationModel;

/**
 * Created by webwerks on 24/5/17.
 */

public class FacebookPhotoResponse {

    private ObservableArrayList<AlbumInformationModel> facebookPhotos;
    private String beforePaging;
    private String afterPaging;

    public ObservableArrayList<AlbumInformationModel> getFacebookPhotos() {
        return facebookPhotos;
    }

    public void setFacebookPhotos(ObservableArrayList<AlbumInformationModel> facebookPhotos) {
        this.facebookPhotos = facebookPhotos;
    }

    public String getBeforePaging() {
        return beforePaging;
    }

    public void setBeforePaging(String beforePaging) {
        this.beforePaging = beforePaging;
    }

    public String getAfterPaging() {
        return afterPaging;
    }

    public void setAfterPaging(String afterPaging) {
        this.afterPaging = afterPaging;
    }

    @Override
    public String toString() {
        return "FacebookPhotoResponse{" +
                "facebookPhotos=" + facebookPhotos +
                ", beforePaging='" + beforePaging + '\'' +
                ", afterPaging='" + afterPaging + '\'' +
                '}';
    }


}
