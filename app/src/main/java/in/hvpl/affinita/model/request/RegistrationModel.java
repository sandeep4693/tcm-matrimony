package in.hvpl.affinita.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.BuildConfig;
import in.hvpl.affinita.model.base.RequestModel;

/**
 * Created by webwerks on 19/6/17.
 */


public class RegistrationModel extends RequestModel {

    @SerializedName("mobile_cc")
    @Expose
    private String mobileCC;

    @SerializedName("mobile_number")
    @Expose
    private String mobileNo;

    @SerializedName("otp")
    @Expose
    private String otp;

    @SerializedName("fcmtoken")
    @Expose
    private String fcmToken;

    @SerializedName("device_type")
    @Expose
    private String deviceType;

    @SerializedName("version_no")
    @Expose
    private String versionNo;

    public RegistrationModel() {
        mobileCC = "91";
        deviceType = "0";
        versionNo = BuildConfig.VERSION_NAME;
    }


    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getMobileCC() {
        return mobileCC;
    }

    public void setMobileCC(String mobileCC) {
        this.mobileCC = mobileCC;
    }

    public String getMobileNo() {
        if (mobileNo == null)
            mobileNo = "";
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Override
    public String toString() {
        return "RegistrationModel{" +
                "mobileCC='" + mobileCC + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", otp='" + otp + '\'' +
                '}';
    }
}
