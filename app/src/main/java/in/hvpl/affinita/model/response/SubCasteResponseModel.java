package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks1 on 27/7/17.
 */

public class SubCasteResponseModel extends ResponseModel {

    @SerializedName("data")
    @Expose
    private SubCasteData subCasteData;

    public SubCasteData getSubCasteData() {
        return subCasteData;
    }

    public void setSubCasteData(SubCasteData subCasteData) {
        this.subCasteData = subCasteData;
    }

    public class SubCasteData{
        @SerializedName("subcastes")
        @Expose
        private ArrayList<SelectionModel> subCastesModels;

        public ArrayList<SelectionModel> getSubCastesModels() {
            return subCastesModels;
        }

        public void setSubCastesModels(ArrayList<SelectionModel> subCastesModels) {
            this.subCastesModels = subCastesModels;
        }
    }

}
