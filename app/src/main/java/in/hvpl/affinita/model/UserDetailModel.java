package in.hvpl.affinita.model;

import android.databinding.BaseObservable;
import android.databinding.ObservableArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by webwerks on 3/7/17.
 */

public class UserDetailModel extends BaseObservable {

    @SerializedName("basicinfo")
    @Expose
    private BasicInformationModel basicInformationModel;

    @SerializedName("contactInfo")
    @Expose
    private ContactInformationModel contactInformationModel;

    @SerializedName("educationInfo")
    @Expose
    private EducationInformationModel educationInformationModel;

    @SerializedName("parentInfo")
    @Expose
    private ArrayList<ParentInformationModel> parentInformationModels;

    @SerializedName("otherInfo")
    @Expose
    private OtherInformationModel otherInformationModel;

    @SerializedName("preferenceDetails")
    @Expose
    private PartnerPreferenceDetailsModel partnerPreferenceDetailsModel;

    @SerializedName("audioInfo")
    @Expose
    private String audioFile = "";

    @SerializedName("albumlist")
    @Expose
    private ObservableArrayList<AlbumInformationModel> albumModels = new ObservableArrayList<>();

    @SerializedName("about_me")
    @Expose
    private String aboutMe;
    @SerializedName("advisory_url")
    @Expose
    private String advisoryUrl;
    @SerializedName("app_status")
    @Expose
    private int appStatus;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("show_family_details")
    @Expose
    private String showFamilyDetails;
    @SerializedName("unread_message_count")
    @Expose
    private int connectionUnreadCount;
    @SerializedName("proposal_count")
    @Expose
    private int proposalUnreadCount;

    public UserDetailModel() {
        this.basicInformationModel = new BasicInformationModel();
        this.contactInformationModel = new ContactInformationModel();
        this.educationInformationModel = new EducationInformationModel();
        this.parentInformationModels = new ObservableArrayList<>();
        this.otherInformationModel = new OtherInformationModel();
        this.partnerPreferenceDetailsModel = new PartnerPreferenceDetailsModel();
        this.albumModels = new ObservableArrayList<>();
    }

    public String getAdvisoryUrl() {
        return advisoryUrl;
    }

    public void setAdvisoryUrl(String advisoryUrl) {
        this.advisoryUrl = advisoryUrl;
    }

    public String getShowFamilyDetails() {

        return showFamilyDetails;
    }

    public void setShowFamilyDetails(String showFamilyDetails) {
        this.showFamilyDetails = showFamilyDetails;
    }

    public int getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(int appStatus) {
        this.appStatus = appStatus;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BasicInformationModel getBasicInformationModel() {
        if (basicInformationModel == null)
            basicInformationModel = new BasicInformationModel();
        return basicInformationModel;
    }

    public void setBasicInformationModel(BasicInformationModel basicInformationModel) {
        this.basicInformationModel = basicInformationModel;
    }

    public ContactInformationModel getContactInformationModel() {
        return contactInformationModel;
    }

    public void setContactInformationModel(ContactInformationModel contactInformationModel) {
        this.contactInformationModel = contactInformationModel;
    }

    public EducationInformationModel getEducationInformationModel() {
        return educationInformationModel;
    }

    public void setEducationInformationModel(EducationInformationModel educationInformationModel) {
        this.educationInformationModel = educationInformationModel;
    }

    public ArrayList<ParentInformationModel> getParentInformationModels() {
        return parentInformationModels;
    }

    public void setParentInformationModels(ArrayList<ParentInformationModel> parentInformationModels) {
        this.parentInformationModels = parentInformationModels;
    }

    public OtherInformationModel getOtherInformationModel() {
        return otherInformationModel;
    }

    public void setOtherInformationModel(OtherInformationModel otherInformationModel) {
        this.otherInformationModel = otherInformationModel;
    }

    public PartnerPreferenceDetailsModel getPartnerPreferenceDetailsModel() {
        return partnerPreferenceDetailsModel;
    }

    public void setPartnerPreferenceDetailsModel(PartnerPreferenceDetailsModel partnerPreferenceDetailsModel) {
        this.partnerPreferenceDetailsModel = partnerPreferenceDetailsModel;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }


    public ObservableArrayList<AlbumInformationModel> getAlbumModels() {
        if (albumModels == null)
            albumModels = new ObservableArrayList<>();
        return albumModels;
    }

    public void setAlbumModels(ObservableArrayList<AlbumInformationModel> albumModels) {
        this.albumModels = albumModels;

    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public int getConnectionUnreadCount() {
        return connectionUnreadCount;
    }

    public void setConnectionUnreadCount(int connectionUnreadCount) {
        this.connectionUnreadCount = connectionUnreadCount;
    }

    public int getProposalUnreadCount() {
        return proposalUnreadCount;
    }

    public void setProposalUnreadCount(int proposalUnreadCount) {
        this.proposalUnreadCount = proposalUnreadCount;
    }


    @Override
    public String toString() {
        return "UserDetailModel{" +
                "basicInformationModel=" + basicInformationModel +
                ", contactInformationModel=" + contactInformationModel +
                ", educationInformationModel=" + educationInformationModel +
                ", parentInformationModels=" + parentInformationModels +
                ", otherInformationModel=" + otherInformationModel +
                ", partnerPreferenceDetailsModel=" + partnerPreferenceDetailsModel +
                ", audioFile=" + audioFile +
                ", albumModels=" + albumModels +
                '}';
    }
}
