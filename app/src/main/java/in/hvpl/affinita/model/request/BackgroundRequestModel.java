package in.hvpl.affinita.model.request;

import android.content.Context;
import android.databinding.Bindable;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.App;
import in.hvpl.affinita.BR;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.BasicInformationModel;
import in.hvpl.affinita.model.ContactInformationModel;
import in.hvpl.affinita.model.OtherInformationModel;
import in.hvpl.affinita.model.base.RequestModel;

import static in.hvpl.affinita.common.utils.Tools.isValidValue;

/**
 * Created by webwerks1 on 24/7/17.
 */

public class BackgroundRequestModel extends RequestModel {

    private String religion;

    @SerializedName("religion_ids")
    @Expose
    private String religionId;


    private String motherTongue;

    @SerializedName("mother_tongue_ids")
    @Expose
    private String motherTongueId;

    private String caste;

    @SerializedName("caste_ids")
    @Expose
    private String casteId;

    private String subCaste;

    @SerializedName("subcaste_ids")
    @Expose
    private String subCasteId;

    private String country;

    @SerializedName("native_country_id")
    @Expose
    private String countryId;

    private String state;

    @SerializedName("native_state_ids")
    @Expose
    private String stateId;

    private String city;

    @SerializedName("native_city_ids")
    @Expose
    private String cityId;

    private String resident;

    @SerializedName("residence_status_ids")
    @Expose
    private String residentId;

    private String outlook;

    @SerializedName("outlook_ids")
    @Expose
    private String outlookId;

    private String feet;

    private String inches;


    public BackgroundRequestModel(String clientId, BasicInformationModel basicInformationModel, ContactInformationModel contactInformationModel, OtherInformationModel otherInformationModel) {
        super(clientId);
        this.religion = otherInformationModel.getReligionModel().getTitle();
        this.religionId = "" + otherInformationModel.getReligionModel().getId();
        this.feet = basicInformationModel.getHeightModel().getFeet();
        this.inches = basicInformationModel.getHeightModel().getInches();
        this.motherTongue = otherInformationModel.getMotherTongueModel().getTitle();
        this.motherTongueId = "" + otherInformationModel.getMotherTongueModel().getId();
        this.caste = otherInformationModel.getCasteModel().getTitle();
        this.casteId = "" + otherInformationModel.getCasteModel().getId();
        this.subCaste = otherInformationModel.getSubcasteModel().getTitle();
        this.subCasteId = "" + otherInformationModel.getSubcasteModel().getId();
        this.country = contactInformationModel.getNativeModel().getNativeCountry().getTitle();
        this.countryId = "" + contactInformationModel.getNativeModel().getNativeCountry().getId();
        this.state = contactInformationModel.getNativeModel().getNativeState().getTitle();
        this.stateId = "" + contactInformationModel.getNativeModel().getNativeState().getId();
        this.city = contactInformationModel.getNativeModel().getNativeCity().getTitle();
        this.cityId = "" + contactInformationModel.getNativeModel().getNativeCity().getId();
        this.resident = contactInformationModel.getResidenceStatusModel().getTitle();
        this.residentId = "" + contactInformationModel.getResidenceStatusModel().getId();
        this.outlook = otherInformationModel.getOutlookModel().getTitle();
        this.outlookId = "" + otherInformationModel.getOutlookModel().getId();
    }

    public boolean isValid(Context context) {

        if (!isValidValue(religion)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_religion), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(motherTongue)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_mother_tongue), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(religionId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_religion), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(motherTongueId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_mother_tongue), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(caste)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_caste), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(casteId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_caste), Toast.LENGTH_LONG);
            return false;
        } /*else if (!isValidValue(subCaste)) {
            Tools.showToast(context,context.getString(R.string.error_msg_select_subcaste));
            return false;
        } else if (!isValidValue(subCasteId)) {
            Tools.showToast(context,context.getString(R.string.error_msg_select_subcaste));
            return false;
        }*/ /*else if (!isValidValue(countryId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_country), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(stateId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_state), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(cityId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_city), Toast.LENGTH_LONG);
            return false;
        }*/ else if (!isValidValue(resident)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_resident), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(residentId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_resident), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(outlook)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_outlook), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(outlookId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_outlook), Toast.LENGTH_LONG);
            return false;
        }


        return true;
    }


    @Bindable
    public String getResidentId() {
        return residentId;
    }

    public void setResidentId(String residentId) {
        this.residentId = residentId;
        notifyPropertyChanged(BR.residentId);
    }


    @Bindable
    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
        notifyPropertyChanged(BR.religion);
    }

    @Bindable
    public String getReligionId() {
        return religionId;
    }

    public void setReligionId(String religionId) {
        this.religionId = religionId;
        notifyPropertyChanged(BR.religionId);
    }


    @Bindable
    public String getMotherTongue() {
        return motherTongue;
    }

    public void setMotherTongue(String motherTongue) {
        this.motherTongue = motherTongue;
        notifyPropertyChanged(BR.motherTongue);
    }

    @Bindable
    public String getMotherTongueId() {
        return motherTongueId;
    }

    public void setMotherTongueId(String motherTongueId) {
        this.motherTongueId = motherTongueId;
        notifyPropertyChanged(BR.motherTongueId);
    }

    @Bindable
    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
        notifyPropertyChanged(BR.caste);
    }

    @Bindable
    public String getCasteId() {
        return casteId;
    }

    public void setCasteId(String casteId) {
        this.casteId = casteId;
        notifyPropertyChanged(BR.casteId);
    }

    @Bindable
    public String getSubCaste() {
        if (subCaste.equals("")) {
            this.subCaste = App.getInstance().getApplicationContext().getString(R.string.dont_know);
        }
        return subCaste;
    }

    public void setSubCaste(String subCaste) {
        this.subCaste = subCaste;
        notifyPropertyChanged(BR.subCaste);
    }

    @Bindable
    public String getSubCasteId() {
        return subCasteId;
    }

    public void setSubCasteId(String subCasteId) {
        this.subCasteId = subCasteId;
        notifyPropertyChanged(BR.subCasteId);
    }

    @Bindable
    public String getCountry() {
        if (country.equals("")) {
            this.country = App.getInstance().getApplicationContext().getString(R.string.dont_know);
        }
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
        notifyPropertyChanged(BR.country);
    }

    @Bindable
    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
        notifyPropertyChanged(BR.countryId);
    }

    @Bindable
    public String getState() {
        if (state.equals("")) {
            this.state = App.getInstance().getApplicationContext().getString(R.string.dont_know);
        }
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyPropertyChanged(BR.state);
    }

    @Bindable
    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
        notifyPropertyChanged(BR.stateId);
    }

    @Bindable
    public String getCity() {
        if (city.equals("")) {
            this.city = App.getInstance().getApplicationContext().getString(R.string.dont_know);
        }
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        notifyPropertyChanged(BR.city);
    }

    @Bindable
    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
        notifyPropertyChanged(BR.cityId);
    }

    @Bindable
    public String getResident() {
        return resident;
    }

    public void setResident(String resident) {
        this.resident = resident;
        notifyPropertyChanged(BR.resident);
    }

    @Bindable
    public String getOutlook() {
        return outlook;
    }

    public void setOutlook(String outlook) {
        this.outlook = outlook;
        notifyPropertyChanged(BR.outlook);
    }

    @Bindable
    public String getOutlookId() {
        return outlookId;
    }

    public void setOutlookId(String outlookId) {
        this.outlookId = outlookId;
        notifyPropertyChanged(BR.outlookId);
    }

    @Bindable
    public String getFeet() {
        return feet;
    }

    public void setFeet(String feet) {
        this.feet = feet;
        notifyPropertyChanged(BR.feet);
    }

    @Bindable
    public String getInches() {
        return inches;
    }

    public void setInches(String inches) {
        this.inches = inches;
        notifyPropertyChanged(BR.inches);
    }


}
