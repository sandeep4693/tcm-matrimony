package in.hvpl.affinita.model.response;

import android.databinding.ObservableArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.ArticleModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks1 on 12/7/17.
 */

public class ArticleListResponseModel extends ResponseModel {

    @SerializedName("data")
    @Expose
    private ArticleData articleData;

    public ArticleData getArticleData() {
        return articleData;
    }

    public void setArticleData(ArticleData articleData) {
        this.articleData = articleData;
    }

    public class ArticleData{

        @SerializedName("data")
                @Expose
       private ObservableArrayList<ArticleModel> articleModels;


        @SerializedName("last_page")
        @Expose
        private int lastPage;


        public int getLastPage() {
            return lastPage;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public ObservableArrayList<ArticleModel> getArticleModels() {
            return articleModels;
        }

        public void setArticldataeModels(ObservableArrayList<ArticleModel> articleModels) {
            this.articleModels = articleModels;
        }

        @Override
        public String toString() {
            return "ArticleData{" +
                    "articleModels=" + articleModels +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ArticleListResponseModel{" +
                "articleData=" + articleData +
                '}';
    }
}
