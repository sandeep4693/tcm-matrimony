package in.hvpl.affinita.model.base;

import android.databinding.BaseObservable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.common.utils.Constants;

/**
 * Created by webwerks on 19/6/17.
 */

public abstract class RequestModel  extends BaseObservable{

    @SerializedName("bureau_id")
    @Expose
    private String bureauId;

    @SerializedName("client_id")
    @Expose
    private String clientId;


    public RequestModel() {
        bureauId = Constants.BUREAU_ID;
    }

    public RequestModel(String clientId) {
        bureauId = Constants.BUREAU_ID;
        this.clientId=clientId;
    }

    public String getBureauId() {
        return bureauId;
    }

    public String getClientId() {
        return clientId;
    }

    @Override
    public String toString() {
        return "RequestModel{" +
                "bureauId='" + bureauId + '\'' +
                '}';
    }
}
