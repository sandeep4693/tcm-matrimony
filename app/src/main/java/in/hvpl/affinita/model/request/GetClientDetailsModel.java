package in.hvpl.affinita.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.base.RequestModel;

/**
 * Created by webwerks on 4/7/17.
 */

public class GetClientDetailsModel extends RequestModel {

    @SerializedName("client_match_id")
    @Expose
    private String matchId;


    public GetClientDetailsModel(String clientID) {
        super(clientID);
    }

    public GetClientDetailsModel(String clientID,String matchId) {
        super(clientID);
        this.matchId=matchId;
    }


    public String getMatchId() {
        return matchId;
    }

    public void setMatchId(String matchId) {
        this.matchId = matchId;
    }
}
