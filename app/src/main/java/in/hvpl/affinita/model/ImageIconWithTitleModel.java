package in.hvpl.affinita.model;

/**
 * Created by webwerks1 on 19/7/17.

 */

public class ImageIconWithTitleModel {
    private String imageId;
    private String title;


    public ImageIconWithTitleModel(String imageId, String title) {
        this.imageId = imageId;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

}
