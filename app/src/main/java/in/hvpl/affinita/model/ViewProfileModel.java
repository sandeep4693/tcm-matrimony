package in.hvpl.affinita.model;

/**
 * Created by Ganesh.K on 21/7/17.
 */

public class ViewProfileModel {

    private String imageId;
    private String title;
    private String desc;
    private boolean isTitleShow;
    private boolean isPreferenceShow;
    private boolean isEditable;



    public ViewProfileModel(String imageId, String title, String desc, boolean isTitleShow, boolean isPreferenceShow,boolean isEditable) {
        this.imageId = imageId;
        this.title = title;
        this.desc = desc;
        this.isTitleShow = isTitleShow;
        this.isPreferenceShow = isPreferenceShow;
        this.isEditable = isEditable;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public boolean isTitleShow() {
        return isTitleShow;
    }

    public void setTitleShow(boolean titleShow) {
        isTitleShow = titleShow;
    }

    public boolean isPreferenceShow() {
        return isPreferenceShow;
    }

    public void setPreferenceShow(boolean preferenceShow) {
        isPreferenceShow = preferenceShow;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

}
