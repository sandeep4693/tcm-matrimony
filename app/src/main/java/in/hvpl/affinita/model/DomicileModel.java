package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 21/6/17.
 */

public class DomicileModel {

    @SerializedName("domicile_address")
    @Expose
    private String domicileAddress;

    @SerializedName("domicile_country")
    @Expose
    private SelectionModel domicileCountry;

    @SerializedName("domicile_state")
    @Expose
    private SelectionModel domicileState;

    @SerializedName("domicile_city")
    @Expose
    private SelectionModel domicileCity;

    @SerializedName("domicile_pincode")
    @Expose
    private String domicilePincode;

    public String getDomicileAddress() {
        return domicileAddress;
    }

    public void setDomicileAddress(String domicileAddress) {
        this.domicileAddress = domicileAddress;
    }

    public SelectionModel getDomicileCountry() {
        if(domicileCountry==null)
            domicileCountry=new SelectionModel();
        return domicileCountry;
    }

    public void setDomicileCountry(SelectionModel domicileCountry) {
        this.domicileCountry = domicileCountry;
    }

    public SelectionModel getDomicileState() {
        if(domicileState==null)
            domicileState=new SelectionModel();
        return domicileState;
    }

    public void setDomicileState(SelectionModel domicileState) {
        this.domicileState = domicileState;
    }

    public SelectionModel getDomicileCity() {
        if(domicileCity==null)
            domicileCity=new SelectionModel();
        return domicileCity;
    }

    public void setDomicileCity(SelectionModel domicileCity) {
        this.domicileCity = domicileCity;
    }

    public String getDomicilePincode() {
        return domicilePincode;
    }

    public void setDomicilePincode(String domicilePincode) {
        this.domicilePincode = domicilePincode;
    }

    @Override
    public String toString() {
        return "DomicileModel{" +
                "domicileAddress='" + domicileAddress + '\'' +
                ", domicileCountry=" + domicileCountry +
                ", domicileState=" + domicileState +
                ", domicileCity=" + domicileCity +
                ", domicilePincode='" + domicilePincode + '\'' +
                '}';
    }
}
