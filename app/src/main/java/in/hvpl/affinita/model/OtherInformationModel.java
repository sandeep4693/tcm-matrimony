package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import javax.annotation.CheckForNull;
import javax.annotation.Nullable;

/**
 * Created by webwerks on 21/6/17.
 */

public class OtherInformationModel {

    @SerializedName("horoscope")
    @Expose
    private String horoscope;

    @SerializedName("manglik")
    @Expose
    private SelectionModel manglikModel;

    @SerializedName("gothra")
    @Expose
    private SelectionModel gothraModel;

    @SerializedName("dont_specify_gothra")
    @Expose
    private String dontSpecifyGothra;

    @SerializedName("rashi")
    @Expose
    private SelectionModel rashiModel;

    @SerializedName("dont_specify_rashi")
    @Expose
    private String dontSpecifyRashi;

    @SerializedName("nakshatra")
    @Expose
    private SelectionModel nakshatraModel;

    @SerializedName("birth_state")
    @Expose
    private SelectionModel birthStateModel;

    @SerializedName("birth_city")
    @Expose
    private SelectionModel birthCityModel;

    @SerializedName("birth_time")
    @Expose
    private String birthTime;

    @SerializedName("mother_tongue")
    @Expose
    private SelectionModel motherTongueModel;

    @SerializedName("religion")
    @Expose
    private SelectionModel religionModel;

    @SerializedName("caste")
    @Expose
    private SelectionModel casteModel;

    @SerializedName("subcaste")
    @Expose
    private SelectionModel subcasteModel;

    @SerializedName("dont_specify_subcaste")
    @Expose
    private String dontSpecifySubcaste;

    @SerializedName("final_comment")
    @Expose
    private String finalComment;

    @SerializedName("smoke")
    @Expose
    @CheckForNull
    private SelectionModel smokeModel;

    @SerializedName("drink")
    @Expose
    private SelectionModel drinkModel;

    @SerializedName("diet_type")
    @Expose
    private SelectionModel dietTypeModel;

    @SerializedName("hobbies")
    @Expose
    private String hobbies;

    @SerializedName("family_disability")
    @Expose
    private SelectionModel familyDisabilityModel;

    @SerializedName("disability_in_family")
    @Expose
    private String disabilityInFamily;

    @SerializedName("family_income")
    @Expose
    private SelectionModel familyIncomeModel;

    @SerializedName("family_currency")
    @Expose
    private SelectionModel familyCurrencyModel;

    @SerializedName("assets")
    @Expose
    private String assets;

    @SerializedName("life_style")
    @Expose
    private SelectionModel lifeStyleModel;

    @SerializedName("outlook")
    @Expose
    private SelectionModel outlookModel;


    public String getHoroscope() {
        return horoscope;
    }

    public void setHoroscope(String horoscope) {
        this.horoscope = horoscope;
    }

    public SelectionModel getManglikModel() {
        return manglikModel;
    }

    public void setManglikModel(SelectionModel manglik) {
        this.manglikModel = manglik;
    }

    public SelectionModel getGothraModel() {
        return gothraModel;
    }

    public void setGothraModel(SelectionModel gothraModel) {
        this.gothraModel = gothraModel;
    }

    public String getDontSpecifyGothra() {
        return dontSpecifyGothra;
    }

    public void setDontSpecifyGothra(String dontSpecifyGothra) {
        this.dontSpecifyGothra = dontSpecifyGothra;
    }


    public String getDontSpecifyRashi() {
        return dontSpecifyRashi;
    }

    public void setDontSpecifyRashi(String dontSpecifyRashi) {
        this.dontSpecifyRashi = dontSpecifyRashi;
    }

    public SelectionModel getRashiModel() {
        return rashiModel;
    }

    public void setRashiModel(SelectionModel rashiModel) {
        this.rashiModel = rashiModel;
    }

    public SelectionModel getNakshatraModel() {
        return nakshatraModel;
    }

    public void setNakshatraModel(SelectionModel nakshatraModel) {
        this.nakshatraModel = nakshatraModel;
    }

    public SelectionModel getBirthStateModel() {
        return birthStateModel;
    }

    public void setBirthStateModel(SelectionModel birthStateModel) {
        this.birthStateModel = birthStateModel;
    }

    public SelectionModel getBirthCityModel() {
        return birthCityModel;
    }

    public void setBirthCityModel(SelectionModel birthCityModel) {
        this.birthCityModel = birthCityModel;
    }

    public String getBirthTime() {
        return birthTime;
    }

    public void setBirthTime(String birthTime) {
        this.birthTime = birthTime;
    }

    public SelectionModel getMotherTongueModel() {
        return motherTongueModel;
    }

    public void setMotherTongueModel(SelectionModel motherTongueModel) {
        this.motherTongueModel = motherTongueModel;
    }

    public SelectionModel getReligionModel() {
        return religionModel;
    }

    public void setReligionModel(SelectionModel religionModel) {
        this.religionModel = religionModel;
    }

    public SelectionModel getCasteModel() {
        return casteModel;
    }

    public void setCasteModel(SelectionModel casteModel) {
        this.casteModel = casteModel;
    }

    public SelectionModel getSubcasteModel() {
        return subcasteModel;
    }

    public void setSubcasteModel(SelectionModel subcasteModel) {
        this.subcasteModel = subcasteModel;
    }

    public String getDontSpecifySubcaste() {
        return dontSpecifySubcaste;
    }

    public void setDontSpecifySubcaste(String dontSpecifySubcaste) {
        this.dontSpecifySubcaste = dontSpecifySubcaste;
    }

    public String getFinalComment() {
        return finalComment;
    }

    public void setFinalComment(String finalComment) {
        this.finalComment = finalComment;
    }


    public SelectionModel getSmokeModel() {
        if (smokeModel == null)
            smokeModel = new SelectionModel();
        return smokeModel;
    }

    public void setSmokeModel(SelectionModel smoke) {
        this.smokeModel = smoke;
    }

    public SelectionModel getDrinkModel() {
        if (drinkModel == null)
            drinkModel = new SelectionModel();
        return drinkModel;
    }

    public void setDrinkModel(SelectionModel drink) {
        this.drinkModel = drink;
    }

    public SelectionModel getDietTypeModel() {
        return dietTypeModel;
    }

    public void setDietTypeModel(SelectionModel dietTypeModel) {
        this.dietTypeModel = dietTypeModel;
    }

    public String getHobbies() {
        return hobbies;
    }

    public void setHobbies(String hobbies) {
        this.hobbies = hobbies;
    }

    public SelectionModel getFamilyDisabilityModel() {
        return familyDisabilityModel;
    }

    public void setFamilyDisabilityModel(SelectionModel familyDisability) {
        this.familyDisabilityModel = familyDisability;
    }

    public String getDisabilityInFamily() {
        return disabilityInFamily;
    }

    public void setDisabilityInFamily(String disabilityInFamily) {
        this.disabilityInFamily = disabilityInFamily;
    }

    public SelectionModel getFamilyIncomeModel() {
        return familyIncomeModel;
    }

    public void setFamilyIncomeModel(SelectionModel familyIncomeModel) {
        this.familyIncomeModel = familyIncomeModel;
    }

    public SelectionModel getFamilyCurrencyModel() {
        return familyCurrencyModel;
    }

    public void setFamilyCurrencyModel(SelectionModel familyCurrencyModel) {
        this.familyCurrencyModel = familyCurrencyModel;
    }

    public String getAssets() {
        return assets;
    }

    public void setAssets(String assets) {
        this.assets = assets;
    }

    public SelectionModel getLifeStyleModel() {
        return lifeStyleModel;
    }

    public void setLifeStyleModel(SelectionModel lifeStyleModel) {
        this.lifeStyleModel = lifeStyleModel;
    }

    public SelectionModel getOutlookModel() {
        return outlookModel;
    }

    public void setOutlookModel(SelectionModel outlookModel) {
        this.outlookModel = outlookModel;
    }


}
