package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by Ganesh.K on 26/7/17.
 */

public class StateResponseModel extends ResponseModel{

    @SerializedName("data")
    @Expose
    private StateData stateData;

    public StateData getStateData() {
        return stateData;
    }

    public void setStateData(StateData stateData) {
        this.stateData = stateData;
    }

   public class StateData {
        @SerializedName("states")
        @Expose
       private ArrayList<SelectionModel> stateModels;


        public ArrayList<SelectionModel> getStateModels() {
            return stateModels;
        }

        public void setStateModels(ArrayList<SelectionModel> stateModels) {
            this.stateModels = stateModels;
        }
    }
}
