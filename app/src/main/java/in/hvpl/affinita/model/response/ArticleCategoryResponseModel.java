package in.hvpl.affinita.model.response;

import android.databinding.ObservableArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.ArticleCategoryModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks1 on 12/7/17.
 */

public class ArticleCategoryResponseModel extends ResponseModel{

    @SerializedName("data")
    @Expose
    private ObservableArrayList<ArticleCategoryModel> articleCategoryModels;

    public ObservableArrayList<ArticleCategoryModel> getArticleCategoryModels() {
        return articleCategoryModels;
    }

    public void setArticleCategoryModels(ObservableArrayList<ArticleCategoryModel> articleCategoryModels) {
        this.articleCategoryModels = articleCategoryModels;
    }


}
