package in.hvpl.affinita.model.request;

import android.content.Context;
import android.databinding.Bindable;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.BR;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.EducationInformationModel;
import in.hvpl.affinita.model.base.RequestModel;

import static in.hvpl.affinita.common.utils.Tools.isValidValue;

/**
 * Created by webwerks1 on 26/7/17.
 */

public class EducationRequestModel extends RequestModel{
    private String highestEducation;

    @SerializedName("education_ids")
    @Expose
    private String highestEducationId;

    @SerializedName("graduation_stream")
    @Expose
    private String stream;

    @SerializedName("university")
    @Expose
    private String university;

    public EducationRequestModel(String clientId,EducationInformationModel educationInformationModel) {
        super(clientId);
        this.highestEducation = educationInformationModel.getEducationModel().getTitle();
        this.highestEducationId = ""+educationInformationModel.getEducationModel().getId();
        this.stream = educationInformationModel.getGraduationStream();
        this.university = educationInformationModel.getUniversity();

    }
    public boolean isValid(Context context) {

       if (!isValidValue(highestEducationId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_education_higher_education), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(stream)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_education_stream), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(university)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_education_university), Toast.LENGTH_LONG);
            return false;
        }
        return true;
    }
    @Bindable
    public String getHighestEducation() {
        return highestEducation;
    }

    public void setHighestEducation(String highestEducation) {
        this.highestEducation = highestEducation;
        notifyPropertyChanged(BR.highestEducation);
    }

    @Bindable
    public String getHighestEducationId() {
        return highestEducationId;
    }

    public void setHighestEducationId(String highestEducationId) {
        this.highestEducationId = highestEducationId;
        notifyPropertyChanged(BR.highestEducation);
    }

    @Bindable
    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
        notifyPropertyChanged(BR.stream);
    }

    @Bindable
    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
        notifyPropertyChanged(BR.university);
    }

}
