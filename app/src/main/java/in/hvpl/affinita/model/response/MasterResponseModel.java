package in.hvpl.affinita.model.response;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import in.hvpl.affinita.model.SelectionModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks1 on 24/7/17.
 */

public class MasterResponseModel extends ResponseModel implements Parcelable {
    @SerializedName("data")
    @Expose
    private MasterData masterData;

    public MasterData getMasterData() {
        return masterData;
    }

    public void setMasterData(MasterData masterData) {
        this.masterData = masterData;
    }

    public static class MasterData implements Parcelable {

        @SerializedName("marital_statuses")
        @Expose
        private ArrayList<SelectionModel> maritalStatusModels;

        @SerializedName("countries")
        @Expose
        private ArrayList<SelectionModel> countriesModels;

        @SerializedName("educations")
        @Expose
        private ArrayList<SelectionModel> educationsModels;

        @SerializedName("occupation_types")
        @Expose
        private ArrayList<SelectionModel> occupationTypesModels;

        @SerializedName("occupation_fields")
        @Expose
        private ArrayList<SelectionModel> occupationFieldsModels;

        @SerializedName("designations")
        @Expose
        private ArrayList<SelectionModel> designationsModels;

        @SerializedName("salary_ranges")
        @Expose
        private ArrayList<SelectionModel> salaryRangesModels;

        @SerializedName("currencies")
        @Expose
        private ArrayList<SelectionModel> currenciesModels;

        @SerializedName("mother_tongues")
        @Expose
        private ArrayList<SelectionModel> motherTonguesModels;

        @SerializedName("religions")
        @Expose
        private ArrayList<SelectionModel> religionsModels;

        @SerializedName("castes")
        @Expose
        private ArrayList<SelectionModel> castesModels;

        @SerializedName("subcastes")
        @Expose
        private ArrayList<SelectionModel> subcastesModels;

        @SerializedName("diet_types")
        @Expose
        private ArrayList<SelectionModel> dietTypesModels;

        @SerializedName("life_styles")
        @Expose
        private ArrayList<SelectionModel> lifeStylesModels;

        @SerializedName("outlooks")
        @Expose
        private ArrayList<SelectionModel> outlooksModels;

        @SerializedName("industries")
        @Expose
        private ArrayList<SelectionModel> industriesModels;

        @SerializedName("nakshatras")
        @Expose
        private ArrayList<SelectionModel> nakshatrasModels;

        @SerializedName("zodiac_signs")
        @Expose
        private ArrayList<SelectionModel> zodiacSignsModels;

        @SerializedName("Packages")
        @Expose
        private ArrayList<SelectionModel> PackagesModels;

        @SerializedName("KycDocumentType")
        @Expose
        private ArrayList<SelectionModel> KycDocumentTypeModels;

        @SerializedName("smoking_types")
        @Expose
        private ArrayList<SelectionModel> smokingTypesModels;

        @SerializedName("drinking_types")
        @Expose
        private ArrayList<SelectionModel> drinkingTypesModels;

        @SerializedName("manglik_types")
        @Expose
        private ArrayList<SelectionModel> manglikTypesModels;

        @SerializedName("body_types")
        @Expose
        private ArrayList<SelectionModel> bodyTypesModels;

        @SerializedName("skin_tones_types")
        @Expose
        private ArrayList<SelectionModel> skinTonesTypesModels;

        @SerializedName("disability_types")
        @Expose
        private ArrayList<SelectionModel> disabilityTypesModels;

        @SerializedName("residence_status_types")
        @Expose
        private ArrayList<SelectionModel> residenceStatusTypesModels;

        @SerializedName("gothra")
        @Expose
        private ArrayList<SelectionModel> gothraModels;

        public ArrayList<SelectionModel> getGothraModels() {
            return gothraModels;
        }

        public void setGothraModels(ArrayList<SelectionModel> gothraModels) {
            this.gothraModels = gothraModels;
        }

        public ArrayList<SelectionModel> getMaritalStatusModels() {
            return maritalStatusModels;
        }

        public void setMaritalStatusModels(ArrayList<SelectionModel> maritalStatusModels) {
            this.maritalStatusModels = maritalStatusModels;
        }

        public ArrayList<SelectionModel> getCountriesModels() {
            return countriesModels;
        }

        public void setCountriesModels(ArrayList<SelectionModel> countriesModels) {
            this.countriesModels = countriesModels;
        }

        public ArrayList<SelectionModel> getEducationsModels() {
            return educationsModels;
        }

        public void setEducationsModels(ArrayList<SelectionModel> educationsModels) {
            this.educationsModels = educationsModels;
        }

        public ArrayList<SelectionModel> getOccupationTypesModels() {
            return occupationTypesModels;
        }

        public void setOccupationTypesModels(ArrayList<SelectionModel> occupationTypesModels) {
            this.occupationTypesModels = occupationTypesModels;
        }

        public ArrayList<SelectionModel> getOccupationFieldsModels() {
            return occupationFieldsModels;
        }

        public void setOccupationFieldsModels(ArrayList<SelectionModel> occupationFieldsModels) {
            this.occupationFieldsModels = occupationFieldsModels;
        }

        public ArrayList<SelectionModel> getDesignationsModels() {
            return designationsModels;
        }

        public void setDesignationsModels(ArrayList<SelectionModel> designationsModels) {
            this.designationsModels = designationsModels;
        }

        public ArrayList<SelectionModel> getSalaryRangesModels() {
            return salaryRangesModels;
        }

        public void setSalaryRangesModels(ArrayList<SelectionModel> salaryRangesModels) {
            this.salaryRangesModels = salaryRangesModels;
        }

        public ArrayList<SelectionModel> getCurrenciesModels() {
            return currenciesModels;
        }

        public void setCurrenciesModels(ArrayList<SelectionModel> currenciesModels) {
            this.currenciesModels = currenciesModels;
        }

        public ArrayList<SelectionModel> getMotherTonguesModels() {
            return motherTonguesModels;
        }

        public void setMotherTonguesModels(ArrayList<SelectionModel> motherTonguesModels) {
            this.motherTonguesModels = motherTonguesModels;
        }

        public ArrayList<SelectionModel> getReligionsModels() {
            return religionsModels;
        }

        public void setReligionsModels(ArrayList<SelectionModel> religionsModels) {
            this.religionsModels = religionsModels;
        }

        public ArrayList<SelectionModel> getCastesModels() {
            return castesModels;
        }

        public void setCastesModels(ArrayList<SelectionModel> castesModels) {
            this.castesModels = castesModels;
        }

        public ArrayList<SelectionModel> getSubcastesModels() {
            return subcastesModels;
        }

        public void setSubcastesModels(ArrayList<SelectionModel> subcastesModels) {
            this.subcastesModels = subcastesModels;
        }

        public ArrayList<SelectionModel> getDietTypesModels() {
            return dietTypesModels;
        }

        public void setDietTypesModels(ArrayList<SelectionModel> dietTypesModels) {
            this.dietTypesModels = dietTypesModels;
        }

        public ArrayList<SelectionModel> getLifeStylesModels() {
            return lifeStylesModels;
        }

        public void setLifeStylesModels(ArrayList<SelectionModel> lifeStylesModels) {
            this.lifeStylesModels = lifeStylesModels;
        }

        public ArrayList<SelectionModel> getOutlooksModels() {
            return outlooksModels;
        }

        public void setOutlooksModels(ArrayList<SelectionModel> outlooksModels) {
            this.outlooksModels = outlooksModels;
        }

        public ArrayList<SelectionModel> getIndustriesModels() {
            return industriesModels;
        }

        public void setIndustriesModels(ArrayList<SelectionModel> industriesModels) {
            this.industriesModels = industriesModels;
        }

        public ArrayList<SelectionModel> getNakshatrasModels() {
            return nakshatrasModels;
        }

        public void setNakshatrasModels(ArrayList<SelectionModel> nakshatrasModels) {
            this.nakshatrasModels = nakshatrasModels;
        }

        public ArrayList<SelectionModel> getZodiacSignsModels() {
            return zodiacSignsModels;
        }

        public void setZodiacSignsModels(ArrayList<SelectionModel> zodiacSignsModels) {
            this.zodiacSignsModels = zodiacSignsModels;
        }

        public ArrayList<SelectionModel> getPackagesModels() {
            return PackagesModels;
        }

        public void setPackagesModels(ArrayList<SelectionModel> packagesModels) {
            PackagesModels = packagesModels;
        }

        public ArrayList<SelectionModel> getKycDocumentTypeModels() {
            return KycDocumentTypeModels;
        }

        public void setKycDocumentTypeModels(ArrayList<SelectionModel> kycDocumentTypeModels) {
            KycDocumentTypeModels = kycDocumentTypeModels;
        }


        public ArrayList<SelectionModel> getSmokingTypesModels() {
            return smokingTypesModels;
        }

        public void setSmokingTypesModels(ArrayList<SelectionModel> smokingTypesModels) {
            this.smokingTypesModels = smokingTypesModels;
        }

        public ArrayList<SelectionModel> getDrinkingTypesModels() {
            return drinkingTypesModels;
        }

        public void setDrinkingTypesModels(ArrayList<SelectionModel> drinkingTypesModels) {
            this.drinkingTypesModels = drinkingTypesModels;
        }

        public ArrayList<SelectionModel> getManglikTypesModels() {
            return manglikTypesModels;
        }

        public void setManglikTypesModels(ArrayList<SelectionModel> manglikTypesModels) {
            this.manglikTypesModels = manglikTypesModels;
        }

        public ArrayList<SelectionModel> getBodyTypesModels() {
            return bodyTypesModels;
        }

        public void setBodyTypesModels(ArrayList<SelectionModel> bodyTypesModels) {
            this.bodyTypesModels = bodyTypesModels;
        }

        public ArrayList<SelectionModel> getSkinTonesTypesModels() {
            return skinTonesTypesModels;
        }

        public void setSkinTonesTypesModels(ArrayList<SelectionModel> skinTonesTypesModels) {
            this.skinTonesTypesModels = skinTonesTypesModels;
        }

        public ArrayList<SelectionModel> getDisabilityTypesModels() {
            return disabilityTypesModels;
        }

        public void setDisabilityTypesModels(ArrayList<SelectionModel> disabilityTypesModels) {
            this.disabilityTypesModels = disabilityTypesModels;
        }

        public ArrayList<SelectionModel> getResidenceStatusTypesModels() {
            return residenceStatusTypesModels;
        }

        public void setResidenceStatusTypesModels(ArrayList<SelectionModel> residenceStatusTypesModels) {
            this.residenceStatusTypesModels = residenceStatusTypesModels;
        }

        protected MasterData(Parcel in) {
            if (in.readByte() == 0x01) {
                maritalStatusModels = new ArrayList<SelectionModel>();
                in.readList(maritalStatusModels, SelectionModel.class.getClassLoader());
            } else {
                maritalStatusModels = null;
            }
            if (in.readByte() == 0x01) {
                countriesModels = new ArrayList<SelectionModel>();
                in.readList(countriesModels, SelectionModel.class.getClassLoader());
            } else {
                countriesModels = null;
            }
            if (in.readByte() == 0x01) {
                educationsModels = new ArrayList<SelectionModel>();
                in.readList(educationsModels, SelectionModel.class.getClassLoader());
            } else {
                educationsModels = null;
            }
            if (in.readByte() == 0x01) {
                occupationTypesModels = new ArrayList<SelectionModel>();
                in.readList(occupationTypesModels, SelectionModel.class.getClassLoader());
            } else {
                occupationTypesModels = null;
            }
            if (in.readByte() == 0x01) {
                occupationFieldsModels = new ArrayList<SelectionModel>();
                in.readList(occupationFieldsModels, SelectionModel.class.getClassLoader());
            } else {
                occupationFieldsModels = null;
            }
            if (in.readByte() == 0x01) {
                designationsModels = new ArrayList<SelectionModel>();
                in.readList(designationsModels, SelectionModel.class.getClassLoader());
            } else {
                designationsModels = null;
            }
            if (in.readByte() == 0x01) {
                salaryRangesModels = new ArrayList<SelectionModel>();
                in.readList(salaryRangesModels, SelectionModel.class.getClassLoader());
            } else {
                salaryRangesModels = null;
            }
            if (in.readByte() == 0x01) {
                currenciesModels = new ArrayList<SelectionModel>();
                in.readList(currenciesModels, SelectionModel.class.getClassLoader());
            } else {
                currenciesModels = null;
            }
            if (in.readByte() == 0x01) {
                motherTonguesModels = new ArrayList<SelectionModel>();
                in.readList(motherTonguesModels, SelectionModel.class.getClassLoader());
            } else {
                motherTonguesModels = null;
            }
            if (in.readByte() == 0x01) {
                religionsModels = new ArrayList<SelectionModel>();
                in.readList(religionsModels, SelectionModel.class.getClassLoader());
            } else {
                religionsModels = null;
            }
            if (in.readByte() == 0x01) {
                castesModels = new ArrayList<SelectionModel>();
                in.readList(castesModels, SelectionModel.class.getClassLoader());
            } else {
                castesModels = null;
            }
            if (in.readByte() == 0x01) {
                subcastesModels = new ArrayList<SelectionModel>();
                in.readList(subcastesModels, SelectionModel.class.getClassLoader());
            } else {
                subcastesModels = null;
            }
            if (in.readByte() == 0x01) {
                dietTypesModels = new ArrayList<SelectionModel>();
                in.readList(dietTypesModels, SelectionModel.class.getClassLoader());
            } else {
                dietTypesModels = null;
            }
            if (in.readByte() == 0x01) {
                lifeStylesModels = new ArrayList<SelectionModel>();
                in.readList(lifeStylesModels, SelectionModel.class.getClassLoader());
            } else {
                lifeStylesModels = null;
            }
            if (in.readByte() == 0x01) {
                outlooksModels = new ArrayList<SelectionModel>();
                in.readList(outlooksModels, SelectionModel.class.getClassLoader());
            } else {
                outlooksModels = null;
            }
            if (in.readByte() == 0x01) {
                industriesModels = new ArrayList<SelectionModel>();
                in.readList(industriesModels, SelectionModel.class.getClassLoader());
            } else {
                industriesModels = null;
            }
            if (in.readByte() == 0x01) {
                nakshatrasModels = new ArrayList<SelectionModel>();
                in.readList(nakshatrasModels, SelectionModel.class.getClassLoader());
            } else {
                nakshatrasModels = null;
            }
            if (in.readByte() == 0x01) {
                zodiacSignsModels = new ArrayList<SelectionModel>();
                in.readList(zodiacSignsModels, SelectionModel.class.getClassLoader());
            } else {
                zodiacSignsModels = null;
            }
            if (in.readByte() == 0x01) {
                PackagesModels = new ArrayList<SelectionModel>();
                in.readList(PackagesModels, SelectionModel.class.getClassLoader());
            } else {
                PackagesModels = null;
            }
            if (in.readByte() == 0x01) {
                KycDocumentTypeModels = new ArrayList<SelectionModel>();
                in.readList(KycDocumentTypeModels, SelectionModel.class.getClassLoader());
            } else {
                KycDocumentTypeModels = null;
            }
            if (in.readByte() == 0x01) {
                smokingTypesModels = new ArrayList<SelectionModel>();
                in.readList(smokingTypesModels, SelectionModel.class.getClassLoader());
            } else {
                smokingTypesModels = null;
            }
            if (in.readByte() == 0x01) {
                drinkingTypesModels = new ArrayList<SelectionModel>();
                in.readList(drinkingTypesModels, SelectionModel.class.getClassLoader());
            } else {
                drinkingTypesModels = null;
            }
            if (in.readByte() == 0x01) {
                manglikTypesModels = new ArrayList<SelectionModel>();
                in.readList(manglikTypesModels, SelectionModel.class.getClassLoader());
            } else {
                manglikTypesModels = null;
            }
            if (in.readByte() == 0x01) {
                bodyTypesModels = new ArrayList<SelectionModel>();
                in.readList(bodyTypesModels, SelectionModel.class.getClassLoader());
            } else {
                bodyTypesModels = null;
            }
            if (in.readByte() == 0x01) {
                skinTonesTypesModels = new ArrayList<SelectionModel>();
                in.readList(skinTonesTypesModels, SelectionModel.class.getClassLoader());
            } else {
                skinTonesTypesModels = null;
            }
            if (in.readByte() == 0x01) {
                disabilityTypesModels = new ArrayList<SelectionModel>();
                in.readList(disabilityTypesModels, SelectionModel.class.getClassLoader());
            } else {
                disabilityTypesModels = null;
            }
            if (in.readByte() == 0x01) {
                residenceStatusTypesModels = new ArrayList<SelectionModel>();
                in.readList(residenceStatusTypesModels, SelectionModel.class.getClassLoader());
            } else {
                residenceStatusTypesModels = null;
            }

            if (in.readByte() == 0x01) {
                gothraModels = new ArrayList<SelectionModel>();
                in.readList(gothraModels, SelectionModel.class.getClassLoader());
            } else {
                gothraModels = null;
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (maritalStatusModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(maritalStatusModels);
            }
            if (countriesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(countriesModels);
            }
            if (educationsModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(educationsModels);
            }
            if (occupationTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(occupationTypesModels);
            }
            if (occupationFieldsModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(occupationFieldsModels);
            }
            if (designationsModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(designationsModels);
            }
            if (salaryRangesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(salaryRangesModels);
            }
            if (currenciesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(currenciesModels);
            }
            if (motherTonguesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(motherTonguesModels);
            }
            if (religionsModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(religionsModels);
            }
            if (castesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(castesModels);
            }
            if (subcastesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(subcastesModels);
            }
            if (dietTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(dietTypesModels);
            }
            if (lifeStylesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(lifeStylesModels);
            }
            if (outlooksModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(outlooksModels);
            }
            if (industriesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(industriesModels);
            }
            if (nakshatrasModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(nakshatrasModels);
            }
            if (zodiacSignsModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(zodiacSignsModels);
            }
            if (PackagesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(PackagesModels);
            }
            if (KycDocumentTypeModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(KycDocumentTypeModels);
            }
            if (smokingTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(smokingTypesModels);
            }
            if (drinkingTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(drinkingTypesModels);
            }
            if (manglikTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(manglikTypesModels);
            }
            if (bodyTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(bodyTypesModels);
            }
            if (skinTonesTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(skinTonesTypesModels);
            }
            if (disabilityTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(disabilityTypesModels);
            }
            if (residenceStatusTypesModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(residenceStatusTypesModels);
            }

            if (gothraModels == null) {
                dest.writeByte((byte) (0x00));
            } else {
                dest.writeByte((byte) (0x01));
                dest.writeList(gothraModels);
            }
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<MasterData> CREATOR = new Parcelable.Creator<MasterData>() {
            @Override
            public MasterData createFromParcel(Parcel in) {
                return new MasterData(in);
            }

            @Override
            public MasterData[] newArray(int size) {
                return new MasterData[size];
            }
        };
    }


    protected MasterResponseModel(Parcel in) {
        masterData = (MasterData) in.readValue(MasterData.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(masterData);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MasterResponseModel> CREATOR = new Parcelable.Creator<MasterResponseModel>() {
        @Override
        public MasterResponseModel createFromParcel(Parcel in) {
            return new MasterResponseModel(in);
        }

        @Override
        public MasterResponseModel[] newArray(int size) {
            return new MasterResponseModel[size];
        }
    };
}