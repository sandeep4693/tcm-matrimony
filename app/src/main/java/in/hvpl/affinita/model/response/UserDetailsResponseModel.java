package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.UserDetailModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks on 22/6/17.
 */

public class UserDetailsResponseModel extends ResponseModel {

    @SerializedName("data")
    @Expose
    private UserDetailModel userDetailModel;

    public UserDetailModel getUserDetailModel() {
        return userDetailModel;
    }



    @Override
    public String toString() {
        return "UserDetailsResponseModel{" +
                "userDetailModel=" + userDetailModel +
                '}';
    }
}
