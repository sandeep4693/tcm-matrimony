package in.hvpl.affinita.model.response;

import android.databinding.ObservableArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.ProposalModel;
import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks1 on 18/7/17.
 */

public class ProposalResponseModel extends ResponseModel{

    @SerializedName("data")
    @Expose
    private ProposalData proposalData;

    public ProposalData getProposalData() {
        return proposalData;
    }

    public void setProposalData(ProposalData proposalData) {
        this.proposalData = proposalData;
    }


    public class ProposalData{

        @SerializedName("people_who_like_your_profile")
        @Expose
        private ObservableArrayList<ProposalModel> whoLikeYouModels;

        @SerializedName("you_like_profile")
        @Expose
        private ObservableArrayList<ProposalModel> youLikeModels;

        @SerializedName("client_match")
        @Expose
        private ObservableArrayList<ProposalModel> matcheModels;

        @SerializedName("recently_viewed_profile")
        @Expose
        private ObservableArrayList<ProposalModel> recentViewModels;


        @SerializedName("proposal_count")
        @Expose
        private int proposalCount;

        public ObservableArrayList<ProposalModel> getRecentViewModels() {
            return recentViewModels;
        }

        public void setRecentViewModels(ObservableArrayList<ProposalModel> recentViewModels) {
            this.recentViewModels = recentViewModels;
        }
        public ObservableArrayList<ProposalModel> getWhoLikeYouModels() {
            return whoLikeYouModels;
        }

        public void setWhoLikeYouModels(ObservableArrayList<ProposalModel> whoLikeYouModels) {
            this.whoLikeYouModels = whoLikeYouModels;
        }

        public ObservableArrayList<ProposalModel> getYouLikeModels() {
            return youLikeModels;
        }

        public void setYouLikeModels(ObservableArrayList<ProposalModel> youLikeModels) {
            this.youLikeModels = youLikeModels;
        }

        public ObservableArrayList<ProposalModel> getMatcheModels() {
            return matcheModels;
        }

        public void setMatcheModels(ObservableArrayList<ProposalModel> matcheModels) {
            this.matcheModels = matcheModels;
        }

        public int getProposalCount() {
            return proposalCount;
        }

        public void setProposalCount(int proposalCount) {
            this.proposalCount = proposalCount;
        }
    }


}
