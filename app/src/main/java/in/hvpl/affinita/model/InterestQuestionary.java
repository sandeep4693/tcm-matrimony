package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class InterestQuestionary implements Serializable {

	@SerializedName( "question" )
	@Expose
	private String question;

	@SerializedName( "options" )
	@Expose
	private ArrayList< Options > options;

	public String getQuestion() {
		return question;
	}

	public void setQuestion( String question ) {
		this.question = question;
	}

	public ArrayList< Options > getOptions() {
		return options;
	}

	public void setOptions( ArrayList< Options > options ) {
		this.options = options;
	}
}
