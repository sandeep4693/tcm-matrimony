package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandeep on 7/7/17.
 */

public class Options {

	public String getOption() {
		return option;
	}

	public void setOption( String option ) {
		this.option = option;
	}

	@SerializedName( "option" )
	@Expose
	private String option;

}
