package in.hvpl.affinita.model.request;

import android.content.Context;
import android.databinding.Bindable;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.BR;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.BasicInformationModel;
import in.hvpl.affinita.model.ContactInformationModel;
import in.hvpl.affinita.model.OtherInformationModel;
import in.hvpl.affinita.model.base.RequestModel;

import static in.hvpl.affinita.common.utils.Tools.isValidValue;

/**
 * Created by webwerks1 on 24/7/17.
 */

public class BasicInfoRequestModel extends RequestModel {


    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("height")
    @Expose
    private String height;


    private String dietTitle;

    @SerializedName("diet_type_ids")
    @Expose
    private String dietId;

    private String smoking;

    @SerializedName("smoke_ids")
    @Expose
    private String smokingId;


    private String drinking;

    @SerializedName("drink_ids")
    @Expose
    private String drinkingId;

    private String martialStatus;

    @SerializedName("marital_status_ids")
    @Expose
    private String martialStatusId;


    private String disability;

    @SerializedName("disability_ids")
    @Expose
    private String disabilityId;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("pincode")
    @Expose
    private String pinCode;


    private String country;

    @SerializedName("country_id")
    @Expose
    private String countryId;


    private String state;

    @SerializedName("state_ids")
    @Expose
    private String stateId;

    private String city;

    @SerializedName("city_ids")
    @Expose
    private String cityId;


    private String feet;

    private String inches;

    public BasicInfoRequestModel(String clientID,BasicInformationModel basicInformationModel, ContactInformationModel contactInformationModel, OtherInformationModel otherInformationModel) {
        super(clientID);
        this.dob = basicInformationModel.getDob();
        this.feet = basicInformationModel.getHeightModel().getFeet();
        this.inches = basicInformationModel.getHeightModel().getInches();
        this.dietTitle = otherInformationModel.getDietTypeModel().getTitle();
        this.dietId = "" + otherInformationModel.getDietTypeModel().getId();
        this.smoking = otherInformationModel.getSmokeModel().getTitle();
        this.smokingId = "" + otherInformationModel.getSmokeModel().getId();
        this.drinking = otherInformationModel.getDrinkModel().getTitle();
        this.drinkingId = "" + otherInformationModel.getDrinkModel().getId();
        this.martialStatus = basicInformationModel.getMaritalStatusModel().getTitle();
        this.martialStatusId = "" + basicInformationModel.getMaritalStatusModel().getId();
        this.disability = basicInformationModel.getDisabilityModel().getTitle();
        this.disabilityId = "" + basicInformationModel.getDisabilityModel().getId();
        this.address = contactInformationModel.getAddress();
        this.pinCode = contactInformationModel.getPincode();
        this.country = contactInformationModel.getCountryModel().getTitle();
        this.countryId = "" + contactInformationModel.getCountryModel().getId();
        this.state = contactInformationModel.getStateModel().getTitle();
        this.stateId = "" + contactInformationModel.getStateModel().getId();
        this.city = contactInformationModel.getCityModel().getTitle();
        this.cityId = "" + contactInformationModel.getCityModel().getId();
    }

    public boolean isValid(Context context) {

        if (!isValidValue(dob)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_dob), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(height)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_height), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(dietId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_diet), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(smokingId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_smoking), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(drinkingId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_drinking), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(martialStatusId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_marital_status), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(disability)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_disability), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(address)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_enter_address), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(pinCode)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_enter_pincode), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(countryId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_country), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(stateId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_state), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(cityId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_city), Toast.LENGTH_LONG);
            return false;
        }


        return true;
    }



    @Bindable
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
        notifyPropertyChanged(BR.dob);


    }

    @Bindable
    public String getDietTitle() {
        return dietTitle;
    }

    public void setDietTitle(String dietTitle) {
        this.dietTitle = dietTitle;
        notifyPropertyChanged( BR.dietTitle);
    }

    @Bindable
    public String getDietId() {
        return dietId;
    }

    public void setDietId(String dietId) {
        this.dietId = dietId;
        notifyPropertyChanged( BR.dietId);
    }

    @Bindable
    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
        notifyPropertyChanged( BR.height);
    }

    @Bindable
    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
        notifyPropertyChanged( BR.smoking);
    }

    @Bindable
    public String getSmokingId() {
        return smokingId;
    }

    public void setSmokingId(String smokingId) {
        this.smokingId = smokingId;
        notifyPropertyChanged( BR.smokingId);
    }

    @Bindable
    public String getDrinking() {
        return drinking;
    }

    public void setDrinking(String drinking) {
        this.drinking = drinking;
        notifyPropertyChanged(BR.drinking);
    }

    @Bindable
    public String getDrinkingId() {
        return drinkingId;
    }

    public void setDrinkingId(String drinkingId) {
        this.drinkingId = drinkingId;
        notifyPropertyChanged(BR.drinkingId);
    }

    @Bindable
    public String getMartialStatus() {
        return martialStatus;
    }

    public void setMartialStatus(String martialStatus) {
        this.martialStatus = martialStatus;
        notifyPropertyChanged( BR.martialStatus);
    }

    @Bindable
    public String getMartialStatusId() {
        return martialStatusId;
    }

    public void setMartialStatusId(String martialStatusId) {
        this.martialStatusId = martialStatusId;
        notifyPropertyChanged( BR.martialStatusId);
    }

    @Bindable
    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
        notifyPropertyChanged( BR.disability);
    }

    @Bindable
    public String getDisabilityId() {
        return disabilityId;
    }

    public void setDisabilityId(String disabilityId) {
        this.disabilityId = disabilityId;
        notifyPropertyChanged(BR.disability);
    }

    @Bindable
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        notifyPropertyChanged(BR.address);
    }

    @Bindable
    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
        notifyPropertyChanged( BR.pinCode);
    }

    @Bindable
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
        notifyPropertyChanged(BR.country);
    }

    @Bindable
    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
        notifyPropertyChanged( BR.countryId);
    }

    @Bindable
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyPropertyChanged( BR.state);
    }

    @Bindable
    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
        notifyPropertyChanged(BR.stateId);
    }

    @Bindable
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        notifyPropertyChanged(BR.city);
    }

    @Bindable
    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
        notifyPropertyChanged( BR.cityId);
    }

    @Bindable
    public String getFeet() {
        return feet;
    }

    public void setFeet(String feet) {
        this.feet = feet;
        notifyPropertyChanged(BR.feet);
        setHeight(Tools.getConvertedHight(this.feet, inches));
    }

    @Bindable
    public String getInches() {
        return inches;
    }

    public void setInches(String inches) {
        this.inches = inches;
        notifyPropertyChanged(BR.inches);
        setHeight(Tools.getConvertedHight(feet, this.inches));
    }


}
