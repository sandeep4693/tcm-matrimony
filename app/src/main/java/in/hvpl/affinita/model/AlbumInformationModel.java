package in.hvpl.affinita.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.common.utils.Constants;

/**
 * Created by webwerks on 22/6/17.
 */

public class AlbumInformationModel implements Parcelable {


    @SerializedName("id")
    @Expose
    private String albumPhotoId;

    @SerializedName("photo_path")
    @Expose
    private String albumPhotoPath;


    private int fromPhoto;

    public AlbumInformationModel() {
        fromPhoto= Constants.FROM_IMAGE.FROM_SERVER;
    }

    public AlbumInformationModel(String albumPhotoId, String albumPhotoPath) {
        this.albumPhotoId = albumPhotoId;
        this.albumPhotoPath = albumPhotoPath;
    }

    public AlbumInformationModel(String albumPhotoId, String albumPhotoPath, int fromPhoto) {
        this.albumPhotoId = albumPhotoId;
        this.albumPhotoPath = albumPhotoPath;
        this.fromPhoto = fromPhoto;
    }

    public int getFromPhoto() {
        return fromPhoto;
    }

    public void setFromPhoto(int fromPhoto) {
        this.fromPhoto = fromPhoto;
    }

    public String getAlbumPhotoId() {
        return albumPhotoId;
    }

    public void setAlbumPhotoId(String albumPhotoId) {
        this.albumPhotoId = albumPhotoId;
    }

    public String getAlbumPhotoPath() {
        return albumPhotoPath;
    }

    public void setAlbumPhotoPath(String albumPhotoPath) {
        this.albumPhotoPath = albumPhotoPath;
    }

    @Override
    public String toString() {
        return "AlbumInformationModel{" +
                "albumPhotoId=" + albumPhotoId +
                ", albumPhotoPath='" + albumPhotoPath + '\'' +
                '}';
    }

    protected AlbumInformationModel(Parcel in) {
        albumPhotoId = in.readString();
        albumPhotoPath = in.readString();
        fromPhoto = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(albumPhotoId);
        dest.writeString(albumPhotoPath);
        dest.writeInt(fromPhoto);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AlbumInformationModel> CREATOR = new Parcelable.Creator<AlbumInformationModel>() {
        @Override
        public AlbumInformationModel createFromParcel(Parcel in) {
            return new AlbumInformationModel(in);
        }

        @Override
        public AlbumInformationModel[] newArray(int size) {
            return new AlbumInformationModel[size];
        }
    };
}