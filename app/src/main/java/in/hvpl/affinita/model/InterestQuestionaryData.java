package in.hvpl.affinita.model;

import android.databinding.ObservableArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Sandeep on 7/7/17.
 */

public class InterestQuestionaryData {

	@SerializedName( "questionList" )
	@Expose
	private ObservableArrayList< InterestQuestionary > questionaryList;

	public ObservableArrayList< InterestQuestionary > getQuestionaryList() {
		return questionaryList;
	}

	public void setQuestionaryList( ObservableArrayList< InterestQuestionary > questionaryList ) {
		this.questionaryList = questionaryList;
	}
}
