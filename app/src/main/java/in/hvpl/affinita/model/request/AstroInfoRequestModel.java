package in.hvpl.affinita.model.request;

import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.BR;
import in.hvpl.affinita.model.OtherInformationModel;
import in.hvpl.affinita.model.base.RequestModel;
import neo.architecture.lifecycle.BaseLifecycleActivity;

/**
 * Created by webwerks1 on 26/7/17.
 */

public class AstroInfoRequestModel extends RequestModel {

    @SerializedName("gothra")
    @Expose
    private String gothra;

    @SerializedName("gothra_ids")
    @Expose
    private String gothraId;

    private String rashi;

    @SerializedName("rashi_ids")
    @Expose
    private String rashiId;

    private String nakshatra;

    @SerializedName("nakshatra_ids")
    @Expose
    private String nakshatraId;

    private String manglik;

    @SerializedName("manglik_ids")
    @Expose
    private String manglikId;


    public AstroInfoRequestModel(String clientId,OtherInformationModel otherInformationModel) {
        super(clientId);
        this.gothra = otherInformationModel.getGothraModel().getTitle();
        this.gothraId= String.valueOf(otherInformationModel.getGothraModel().getId());
        this.rashi = otherInformationModel.getRashiModel().getTitle();
        this.rashiId = "" + otherInformationModel.getRashiModel().getId();
        this.nakshatra = otherInformationModel.getNakshatraModel().getTitle();
        this.nakshatraId = "" + otherInformationModel.getNakshatraModel().getId();
        this.manglik = otherInformationModel.getManglikModel().getTitle();
        this.manglikId = "" + otherInformationModel.getManglikModel().getId();
    }

    @Bindable
    public String getGothra() {
        return gothra;
    }

    public void setGothra(String gothra) {
        this.gothra = gothra;
        notifyPropertyChanged(BR.gothra);
    }

    @Bindable
    public String getRashi() {
        return rashi;
    }

    public void setRashi(String rashi) {
        this.rashi = rashi;
        notifyPropertyChanged(BR.rashi);
    }

    @Bindable
    public String getNakshatra() {
        return nakshatra;
    }

    public void setNakshatra(String nakshatra) {
        this.nakshatra = nakshatra;
        notifyPropertyChanged(BR.nakshatra);
    }

    @Bindable
    public String getManglik() {
        return manglik;
    }

    public void setManglik(String manglik) {
        this.manglik = manglik;
        notifyPropertyChanged(BR.manglik);
    }

    @Bindable
    public String getRashiId() {
        return rashiId;
    }

    public void setRashiId(String rashiId) {
        this.rashiId = rashiId;
        notifyPropertyChanged(BR.rashiId);
    }

    @Bindable
    public String getNakshatraId() {
        return nakshatraId;
    }

    public void setNakshatraId(String nakshatraId) {
        this.nakshatraId = nakshatraId;
        notifyPropertyChanged(BR.nakshatraId);
    }

    @Bindable
    public String getManglikId() {
        return manglikId;
    }

    public void setManglikId(String manglikId) {
        this.manglikId = manglikId;
        notifyPropertyChanged(BR.manglikId);
    }

    public boolean isValid(BaseLifecycleActivity activity) {

        return true;
    }

    public String getGothraId() {
        return gothraId;
    }

    public void setGothraId(String gothraId) {
        this.gothraId = gothraId;
    }
}
