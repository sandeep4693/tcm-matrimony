package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks1 on 11/7/17.
 */

public class ArticleModel  implements Serializable{

    @SerializedName("id")
    @Expose
    private String articleId;

    @SerializedName("title")
    @Expose
    private String articleTitle;

    @SerializedName("image")
    @Expose
    private String articleImgUrl;

    @SerializedName("description")
    @Expose
    private String articleDesc;

    @SerializedName("article_categories")
    @Expose
    private ArticleCategoryModel categoryModel;

    public ArticleCategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(ArticleCategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public ArticleModel(String articleId, String articleTitle, String articleImgUrl, String articleDesc) {
        this.articleId = articleId;
        this.articleTitle = articleTitle;
        this.articleImgUrl = articleImgUrl;
        this.articleDesc = articleDesc;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleImgUrl() {
        return articleImgUrl;
    }

    public void setArticleImgUrl(String articleImgUrl) {
        this.articleImgUrl = articleImgUrl;
    }

    public String getArticleDesc() {
        return articleDesc;
    }

    public void setArticleDesc(String articleDesc) {
        this.articleDesc = articleDesc;
    }

    @Override
    public String toString() {
        return "ArticleModel{" +
                "articleId='" + articleId + '\'' +
                ", articleTitle='" + articleTitle + '\'' +
                ", articleImgUrl='" + articleImgUrl + '\'' +
                ", articleDesc='" + articleDesc + '\'' +
                ", categoryModel=" + categoryModel +
                '}';
    }
}
