package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ganesh.K on 4/8/17.
 */

public class ConnectionModel implements Serializable {

    @SerializedName("id")
    @Expose
    private int clientMatchId;

    @SerializedName("is_connected")
    @Expose
    private int isConnected;

    @SerializedName("client_info")
    @Expose
    private ClientInfo clientInfo;

    @SerializedName("unread_message_list_count")
    @Expose
    private int unreadMsgCount;


    private ArrayList<MessageModel> messageModels;

    @SerializedName("common_hobbies")
    @Expose
    private String commonHobbies;

    @SerializedName("messages")
    @Expose
    private MessageModel messageModel;


    public int getClientMatchId() {
        return clientMatchId;
    }

    public void setClientMatchId(int clientMatchId) {
        this.clientMatchId = clientMatchId;

    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;

    }

    public int getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(int isConnected) {
        this.isConnected = isConnected;
    }

    public ArrayList<MessageModel> getMessageModels() {
        return messageModels;
    }

    public void setMessageModels(ArrayList<MessageModel> messageModels) {
        this.messageModels = messageModels;


    }


    public MessageModel getMessageModel() {

       /* if (messageModels == null)
            messageModels = new ArrayList<>();

        if (messageModels.size() > 0) {
            setMessageModel(messageModels.get(0));
        }*/
        if (messageModel == null) {
            messageModel = new MessageModel();
        }
        return messageModel;
    }

    public void setMessageModel(MessageModel messageModel) {
        this.messageModel = messageModel;

    }

    public String getCommonHobbies() {
        return commonHobbies;
    }

    public void setCommonHobbies(String commonHobbies) {
        this.commonHobbies = commonHobbies;
    }


    public int getUnreadMsgCount() {
        return unreadMsgCount;
    }

    public void setUnreadMsgCount(int unreadMsgCount) {
        this.unreadMsgCount = unreadMsgCount;
    }

    public class ClientInfo implements Serializable {

        @SerializedName("connected_id")
        @Expose
        private int receiverId;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("photo")
        @Expose
        private String photo;


        public int getReceiverId() {
            return receiverId;
        }

        public void setReceiverId(int receiverId) {
            this.receiverId = receiverId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }
    }


}
