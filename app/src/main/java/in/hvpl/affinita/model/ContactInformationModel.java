package in.hvpl.affinita.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 20/6/17.
 */

public class ContactInformationModel {

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("city")
    @Expose
    private SelectionModel cityModel;

    @SerializedName("state")
    @Expose
    private SelectionModel stateModel;

    @SerializedName("pincode")
    @Expose
    private String pincode;

    @SerializedName("landline_no")
    @Expose
    private String landlineNo;

    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;

    @SerializedName("email_id")
    @Expose
    private String emailId;

    @SerializedName("countries")
    @Expose
    private SelectionModel countryModel;

    @SerializedName("mobile_cc")
    @Expose
    private String mobileCC;

    @SerializedName("residence_status")
    @Expose
    private SelectionModel residenceStatusModel;

    @SerializedName("domicile")
    @Expose
    private DomicileModel domicileModel;

    @SerializedName("native")
    @Expose
    private NativeModel nativeModel;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public SelectionModel getCityModel() {
        return cityModel;
    }

    public void setCityModel(SelectionModel cityModel) {
        this.cityModel = cityModel;
    }

    public SelectionModel getStateModel() {
        return stateModel;
    }

    public void setStateModel(SelectionModel stateModel) {
        this.stateModel = stateModel;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getLandlineNo() {
        return landlineNo;
    }

    public void setLandlineNo(String landlineNo) {
        this.landlineNo = landlineNo;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public SelectionModel getCountryModel() {
        return countryModel;
    }

    public void setCountryModel(SelectionModel countryModel) {
        this.countryModel = countryModel;
    }

    public String getMobileCC() {
        return mobileCC;
    }

    public void setMobileCC(String mobileCC) {
        this.mobileCC = mobileCC;
    }

    public SelectionModel getResidenceStatusModel() {
        return residenceStatusModel;
    }

    public void setResidenceStatusModel(SelectionModel residenceStatus) {
        this.residenceStatusModel = residenceStatus;
    }

    public DomicileModel getDomicileModel() {
        return domicileModel;
    }

    public void setDomicileModel(DomicileModel domicileModel) {
        this.domicileModel = domicileModel;
    }

    public NativeModel getNativeModel() {
        return nativeModel;
    }

    public void setNativeModel(NativeModel nativeModel) {
        this.nativeModel = nativeModel;
    }

    @Override
    public String toString() {
        return "ContactInformationModel{" +
                "address='" + address + '\'' +
                ", cityModel=" + cityModel +
                ", stateModel=" + stateModel +
                ", pincode='" + pincode + '\'' +
                ", landlineNo='" + landlineNo + '\'' +
                ", mobileNo='" + mobileNo + '\'' +
                ", emailId='" + emailId + '\'' +
                ", countryModel=" + countryModel +
                ", mobileCC='" + mobileCC + '\'' +
                ", residenceStatusModel='" + residenceStatusModel + '\'' +
                ", domicileModel=" + domicileModel +
                ", nativeModel=" + nativeModel +
                '}';
    }
}
