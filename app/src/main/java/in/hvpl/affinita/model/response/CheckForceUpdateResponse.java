package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by Ganesh.K on 22/9/17.
 */

public class CheckForceUpdateResponse extends ResponseModel {

    @SerializedName("data")
    @Expose
    private CheckForceUpdateData checkForceUpdateData;

    public CheckForceUpdateData getCheckForceUpdateData() {
        return checkForceUpdateData;
    }

    public void setCheckForceUpdateData(CheckForceUpdateData checkForceUpdateData) {
        this.checkForceUpdateData = checkForceUpdateData;
    }

    public class CheckForceUpdateData {

        @SerializedName("force_update")
        @Expose
        private int forceUpdate;

        public int getForceUpdate() {
            return forceUpdate;
        }

        public void setForceUpdate(int forceUpdate) {
            this.forceUpdate = forceUpdate;
        }
    }
}
