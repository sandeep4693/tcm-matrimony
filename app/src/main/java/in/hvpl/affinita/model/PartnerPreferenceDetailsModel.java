package in.hvpl.affinita.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.BR;

/**
 * Created by webwerks on 22/6/17.
 */

public class PartnerPreferenceDetailsModel extends BaseObservable{

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("state")
    @Expose
    private SelectionModel stateModel;

    @SerializedName("city")
    @Expose
    private SelectionModel cityModel;

    @SerializedName("caste")
    @Expose
    private SelectionModel casteModel;

    @SerializedName("mother_tongue")
    @Expose
    private SelectionModel motherTongueModel;

    @SerializedName("subcaste")
    @Expose
    private SelectionModel subcasteModel;

    @SerializedName("education")
    @Expose
    private SelectionModel educationModel;

    @SerializedName("industries")
    @Expose
    private SelectionModel industriesModel;

    @SerializedName("religion")
    @Expose
    private SelectionModel religionModel;

    @SerializedName("residenceCity")
    @Expose
    private SelectionModel residenceCityModel;

    @SerializedName("residenceState")
    @Expose
    private SelectionModel residenceStateModel;

    @SerializedName("dietType")
    @Expose
    private SelectionModel dietTypeModel;

    @SerializedName("maritalStatus")
    @Expose
    private SelectionModel maritalStatusModel;

    @SerializedName("countries")
    @Expose
    private SelectionModel countryModel;

    @SerializedName("nativeState")
    @Expose
    private SelectionModel nativeStateModel;

    @SerializedName("nativeCity")
    @Expose
    private SelectionModel nativeCityModel;

    @SerializedName("working")
    @Expose
    private String working;

    @SerializedName("working_as")
    @Expose
    private SelectionModel workingDesignationModel;

    @SerializedName("age_min")
    @Expose
    private String ageMin;

    @SerializedName("age_max")
    @Expose
    private String ageMax;

    @SerializedName("height_min")
    @Expose
    private HeightModel heightMinModel;

    @SerializedName("height_max")
    @Expose
    private HeightModel heightMaxModel;

    @SerializedName("body_type")
    @Expose
    private SelectionModel bodyTypeModel;

    @SerializedName("skin_tone")
    @Expose
    private SelectionModel skinToneModel;

    @SerializedName("smoke")
    @Expose
    private SelectionModel smokeModel;

    @SerializedName("drink")
    @Expose
    private SelectionModel drinkModel;

    @SerializedName("manglik_check")
    @Expose
    private SelectionModel manglikCheckModel;

    @SerializedName("residence_status")
    @Expose
    private SelectionModel residenceStatusModel;

    public PartnerPreferenceDetailsModel() {
        this.description = "";
        this.stateModel = new SelectionModel();
        this.cityModel =  new SelectionModel();
        this.casteModel =  new SelectionModel();
        this.motherTongueModel =  new SelectionModel();
        this.subcasteModel =  new SelectionModel();
        this.educationModel =  new SelectionModel();
        this.industriesModel =  new SelectionModel();
        this.religionModel =  new SelectionModel();
        this.residenceCityModel =  new SelectionModel();
        this.residenceStateModel =  new SelectionModel();
        this.dietTypeModel =  new SelectionModel();
        this.maritalStatusModel =  new SelectionModel();
        this.countryModel =  new SelectionModel();
        this.nativeStateModel =  new SelectionModel();
        this.nativeCityModel =  new SelectionModel();

        this.workingDesignationModel =  new SelectionModel();

        this.heightMinModel =  new HeightModel();
        this.heightMaxModel = new HeightModel();
        this.bodyTypeModel = new SelectionModel();
        this.skinToneModel = new SelectionModel();
        this.smokeModel = new SelectionModel();
        this.drinkModel = new SelectionModel();
        this.manglikCheckModel = new SelectionModel();
        this.residenceStatusModel = new SelectionModel();

        this.ageMin = "";
        this.ageMax = "";
        /* this.working =  new SelectionModel();

       * */
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;

    }

    @Bindable
    public SelectionModel getStateModel() {
        return stateModel;
    }

    public void setStateModel(SelectionModel stateModel) {
        this.stateModel = stateModel;
    }

    public SelectionModel getCityModel() {
        return cityModel;
    }

    public void setCityModel(SelectionModel cityModel) {
        this.cityModel = cityModel;
    }

    public SelectionModel getCasteModel() {
        return casteModel;
    }

    public void setCasteModel(SelectionModel casteModel) {
        this.casteModel = casteModel;
    }

    public SelectionModel getMotherTongueModel() {
        return motherTongueModel;
    }

    public void setMotherTongueModel(SelectionModel motherTongueModel) {
        this.motherTongueModel = motherTongueModel;
    }

    public SelectionModel getSubcasteModel() {
        return subcasteModel;
    }

    public void setSubcasteModel(SelectionModel subcasteModel) {
        this.subcasteModel = subcasteModel;
    }

    public SelectionModel getEducationModel() {
        return educationModel;
    }

    public void setEducationModel(SelectionModel educationModel) {
        this.educationModel = educationModel;
    }

    public SelectionModel getIndustriesModel() {
        return industriesModel;
    }

    public void setIndustriesModel(SelectionModel industriesModel) {
        this.industriesModel = industriesModel;
    }

    public SelectionModel getReligionModel() {
        return religionModel;
    }

    public void setReligionModel(SelectionModel religionModel) {
        this.religionModel = religionModel;
    }

    public SelectionModel getResidenceCityModel() {
        return residenceCityModel;
    }

    public void setResidenceCityModel(SelectionModel residenceCityModel) {
        this.residenceCityModel = residenceCityModel;
    }

    public SelectionModel getResidenceStateModel() {
        return residenceStateModel;
    }

    public void setResidenceStateModel(SelectionModel residenceStateModel) {
        this.residenceStateModel = residenceStateModel;
    }

    public SelectionModel getDietTypeModel() {
        return dietTypeModel;
    }

    public void setDietTypeModel(SelectionModel dietTypeModel) {
        this.dietTypeModel = dietTypeModel;
    }

    public SelectionModel getMaritalStatusModel() {
        return maritalStatusModel;
    }

    public void setMaritalStatusModel(SelectionModel maritalStatusModel) {
        this.maritalStatusModel = maritalStatusModel;
    }

    public SelectionModel getCountryModel() {
        return countryModel;
    }

    public void setCountryModel(SelectionModel countryModel) {
        this.countryModel = countryModel;
    }

    public SelectionModel getNativeStateModel() {
        return nativeStateModel;
    }

    public void setNativeStateModel(SelectionModel nativeStateModel) {
        this.nativeStateModel = nativeStateModel;
    }

    public SelectionModel getNativeCityModel() {
        return nativeCityModel;
    }

    public void setNativeCityModel(SelectionModel nativeCityModel) {
        this.nativeCityModel = nativeCityModel;
    }

    public String getWorking() {
        return working;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    public SelectionModel getWorkingDesignationModel() {
        return workingDesignationModel;
    }

    public void setWorkingDesignationModel(SelectionModel workingDesignationModel) {
        this.workingDesignationModel = workingDesignationModel;
    }

    public String getAgeMin() {
        return ageMin;
    }

    public void setAgeMin(String ageMin) {
        this.ageMin = ageMin;
    }

    public String getAgeMax() {
        return ageMax;
    }

    public void setAgeMax(String ageMax) {
        this.ageMax = ageMax;
    }

    public HeightModel getHeightMinModel() {
        return heightMinModel;
    }

    public void setHeightMinModel(HeightModel heightMinModel) {
        this.heightMinModel = heightMinModel;
    }

    public HeightModel getHeightMaxModel() {
        return heightMaxModel;
    }

    public void setHeightMaxModel(HeightModel heightMaxModel) {
        this.heightMaxModel = heightMaxModel;
    }

    public SelectionModel getBodyTypeModel() {
        return bodyTypeModel;
    }

    public void setBodyTypeModel(SelectionModel bodyTypeModel) {
        this.bodyTypeModel = bodyTypeModel;
    }

    public SelectionModel getSkinToneModel() {
        return skinToneModel;
    }

    public void setSkinToneModel(SelectionModel skinTone) {
        this.skinToneModel = skinTone;
    }

    public SelectionModel getSmokeModel() {
        return smokeModel;
    }

    public void setSmokeModel(SelectionModel smoke) {
        this.smokeModel = smoke;
    }

    public SelectionModel getDrinkModel() {
        return drinkModel;
    }

    public void setDrinkModel(SelectionModel drink) {
        this.drinkModel = drink;
    }

    public SelectionModel getManglikCheckModel() {
        return manglikCheckModel;
    }

    public void setManglikCheckModel(SelectionModel manglikCheck) {
        this.manglikCheckModel = manglikCheck;
    }

    public SelectionModel getResidenceStatusModel() {
        return residenceStatusModel;
    }

    public void setResidenceStatusModel(SelectionModel residenceStatus) {
        this.residenceStatusModel = residenceStatus;
    }


}
