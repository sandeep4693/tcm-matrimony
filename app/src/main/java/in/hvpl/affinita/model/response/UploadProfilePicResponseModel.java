package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by webwerks on 3/7/17.
 */

public class UploadProfilePicResponseModel extends ResponseModel {

    @SerializedName("data")
    @Expose
    String filePath;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "UploadProfilePicResponseModel{" +
                "filePath='" + filePath + '\'' +
                '}';
    }
}
