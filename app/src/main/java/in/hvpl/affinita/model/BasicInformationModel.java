package in.hvpl.affinita.model;

import android.databinding.Bindable;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.BR;

/**
 * Created by webwerks on 20/6/17.
 */

public class BasicInformationModel implements Observable {

    private PropertyChangeRegistry registry = new PropertyChangeRegistry();

    @SerializedName("id")
    @Expose
    private String clientId;

    @SerializedName("looking_for")
    @Expose
    private String lookingFor;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("fname")
    @Expose
    private String firstName;

    @SerializedName("mname")
    @Expose
    private String middleName;

    @SerializedName("lname")
    @Expose
    private String lastName;

    @SerializedName("marital_status")
    @Expose
    private SelectionModel maritalStatusModel;

    @SerializedName("dob")
    @Expose
    private String dob;

    @SerializedName("height")
    @Expose
    private HeightModel heightModel;

    @SerializedName("weight")
    @Expose
    private String weight;

    @SerializedName("disability")
    @Expose
    private SelectionModel disabilityModel;

    @SerializedName("disability_description")
    @Expose
    private String disabilityDescription;

    @SerializedName("custom_code")
    @Expose
    private String customCode;

    @SerializedName("body_type")
    @Expose
    private SelectionModel bodyTypeModel;

    @SerializedName("skin_tone")
    @Expose
    private SelectionModel skinToneModel;

    @SerializedName("photo")
    @Expose
    private String photoUrl = "";

    @SerializedName("profile_completed_stage")
    @Expose
    private int profileCompletedStage;

    @SerializedName("app_status")
    @Expose
    private int appStatus;


    @Bindable
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
        registry.notifyChange(this, BR.clientId);
    }

    public String getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(String lookingFor) {
        this.lookingFor = lookingFor;
    }

    public String getFullname() {
        return firstName + " " + lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public SelectionModel getMaritalStatusModel() {
        return maritalStatusModel;
    }

    public void setMaritalStatusModel(SelectionModel maritalStatusModel) {
        this.maritalStatusModel = maritalStatusModel;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public HeightModel getHeightModel() {
        return heightModel;
    }

    public void setHeightModel(HeightModel heightModel) {
        this.heightModel = heightModel;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public SelectionModel getDisabilityModel() {
        return disabilityModel;
    }

    public void setDisabilityModel(SelectionModel disability) {
        this.disabilityModel = disability;
    }

    public String getDisabilityDescription() {
        return disabilityDescription;
    }

    public void setDisabilityDescription(String disabilityDescription) {
        this.disabilityDescription = disabilityDescription;
    }


    public String getCustomCode() {
        return customCode;
    }

    public void setCustomCode(String customCode) {
        this.customCode = customCode;
    }

    public SelectionModel getBodyTypeModel() {
        return bodyTypeModel;
    }

    public void setBodyTypeModel(SelectionModel bodyType) {
        this.bodyTypeModel = bodyType;
    }

    public SelectionModel getSkinToneModel() {
        return skinToneModel;
    }

    public void setSkinToneModel(SelectionModel skinTone) {
        this.skinToneModel = skinTone;
    }

    @Bindable
    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
        registry.notifyChange(this, BR.photoUrl);
    }

    public int getProfileCompletedStage() {
        return profileCompletedStage;
    }

    public void setProfileCompletedStage(int profileCompletedStage) {
        this.profileCompletedStage = profileCompletedStage;
    }

    public int getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(int appStatus) {
        this.appStatus = appStatus;
    }


    @Override
    public String toString() {
        return "BasicInformationModel{" +
                "lookingFor='" + lookingFor + '\'' +
                ", gender='" + gender + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", maritalStatusModel=" + maritalStatusModel +
                ", dob='" + dob + '\'' +
                ", heightModel=" + heightModel +
                ", weight='" + weight + '\'' +
                ", disabilityModel='" + disabilityModel + '\'' +
                ", disabilityDescription='" + disabilityDescription + '\'' +
                ", customCode='" + customCode + '\'' +
                ", bodyTypeModel='" + bodyTypeModel + '\'' +
                ", skinToneModel='" + skinToneModel + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", profileCompletedStage=" + profileCompletedStage +
                ", appStatus=" + appStatus +
                '}';
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }
}
