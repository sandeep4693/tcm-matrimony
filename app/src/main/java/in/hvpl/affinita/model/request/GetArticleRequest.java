package in.hvpl.affinita.model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.base.RequestModel;

/**
 * Created by webwerks1 on 12/7/17.
 */

public class GetArticleRequest extends RequestModel {

    @SerializedName("page")
    @Expose
    private String page;

    @SerializedName("category_id")
    @Expose
    private String articleCategoryId;

    public GetArticleRequest(String page, String articleCategoryId) {
        this.page = page;
        this.articleCategoryId = articleCategoryId;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getArticleCategoryId() {
        return articleCategoryId;
    }

    public void setArticleCategoryId(String articleCategoryId) {
        this.articleCategoryId = articleCategoryId;
    }
}
