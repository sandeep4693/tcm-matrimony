package in.hvpl.affinita.model.request;

import android.content.Context;
import android.databinding.Bindable;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import in.hvpl.affinita.BR;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.ParentInformationModel;
import in.hvpl.affinita.model.base.RequestModel;

import static in.hvpl.affinita.common.utils.Tools.isValidValue;

/**
 * Created by webwerks1 on 27/7/17.
 */

public class FamilyInfoRequestModel extends RequestModel {

    private String occupationTypeFather;

    @SerializedName("father_name")
    @Expose
    private String fatherName;

    @SerializedName("father_occupation_type_ids")
    @Expose
    private String occupationTypeFatherId;

    private String field;

    @SerializedName("father_occupation_field_ids")
    @Expose
    private String fieldId;

    private String designation;

    @SerializedName("father_designation_ids")
    @Expose
    private String designationId;

    private String education;

    @SerializedName("father_heighest_degree_ids")
    @Expose
    private String educationId;

    @SerializedName("father_graduation_stream")
    @Expose
    private String stream;

    private String occupationTypeMother;


    private String occupationTypeMotherId = "";


    public FamilyInfoRequestModel(String clientID, ArrayList<ParentInformationModel> parentInformationModels) {
        super(clientID);
        if (parentInformationModels.size() > 0) {
            ParentInformationModel fatherInformationModel = parentInformationModels.get(0);
            this.fatherName = fatherInformationModel.getParentName();
            this.occupationTypeFather = fatherInformationModel.getOccupationTypeModel().getTitle();
            this.occupationTypeFatherId = "" + fatherInformationModel.getOccupationTypeModel().getId();
            this.field = fatherInformationModel.getOccupationFieldModel().getTitle();
            this.fieldId = "" + fatherInformationModel.getOccupationFieldModel().getId();
            this.designation = fatherInformationModel.getDesignationModel().getTitle();
            this.designationId = "" + fatherInformationModel.getDesignationModel().getId();
            this.education = fatherInformationModel.getEducationModel().getTitle();
            this.educationId = "" + fatherInformationModel.getEducationModel().getId();
            this.stream = fatherInformationModel.getStream();
        }
        if (parentInformationModels.size() > 1) {
            ParentInformationModel motherInformationModel = parentInformationModels.get(1);
            this.occupationTypeMother = motherInformationModel.getOccupationTypeModel().getTitle();
            this.occupationTypeMotherId = "" + motherInformationModel.getOccupationTypeModel().getId();
        }

    }

    public boolean isValid(Context context) {
        if (!isValidValue(fatherName)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.father_name), Toast.LENGTH_LONG);
            return false;
        }else if (!isValidValue(occupationTypeFatherId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_father_occupation), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(fieldId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.activity_title_select_occupation_field), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(designationId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_designation), Toast.LENGTH_LONG);
            return false;
        } else if (!isValidValue(educationId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_education), Toast.LENGTH_LONG);
            return false;
        }/* else if (!isValidValue(occupationTypeMotherId)) {
            Tools.showToast(context, Tools.getFormatedMsgString(context, R.string.error_msg_select_mother_occupation), Toast.LENGTH_LONG);
            return false;
        }*/

        return true;
    }

    @Bindable
    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
        notifyPropertyChanged(BR.fatherName);
    }

    @Bindable
    public String getOccupationTypeFather() {
        return occupationTypeFather;
    }

    public void setOccupationTypeFather(String occupationTypeFather) {
        this.occupationTypeFather = occupationTypeFather;
        notifyPropertyChanged(BR.occupationTypeFather);
    }

    @Bindable
    public String getOccupationTypeFatherId() {
        return occupationTypeFatherId;
    }

    public void setOccupationTypeFatherId(String occupationTypeFatherId) {
        this.occupationTypeFatherId = occupationTypeFatherId;
        notifyPropertyChanged(BR.occupationTypeFatherId);
    }

    @Bindable
    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
        notifyPropertyChanged(BR.field);
    }

    @Bindable
    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
        notifyPropertyChanged(BR.fieldId);
    }

    @Bindable
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
        notifyPropertyChanged(BR.designation);
    }

    @Bindable
    public String getDesignationId() {
        return designationId;
    }

    public void setDesignationId(String designationId) {
        this.designationId = designationId;
        notifyPropertyChanged(BR.designationId);
    }

    @Bindable
    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
        notifyPropertyChanged(BR.education);
    }

    @Bindable
    public String getEducationId() {
        return educationId;
    }

    public void setEducationId(String educationId) {
        this.educationId = educationId;
        notifyPropertyChanged(BR.educationId);
    }

    @Bindable
    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
        notifyPropertyChanged(BR.stream);
    }

    @Bindable
    public String getOccupationTypeMother() {
        return occupationTypeMother;
    }

    public void setOccupationTypeMother(String occupationTypeMother) {
        this.occupationTypeMother = occupationTypeMother;
        notifyPropertyChanged(BR.occupationTypeMother);
    }

    @Bindable
    public String getOccupationTypeMotherId() {
        return occupationTypeMotherId;
    }

    public void setOccupationTypeMotherId(String occupationTypeMotherId) {
        this.occupationTypeMotherId = occupationTypeMotherId;
        notifyPropertyChanged(BR.occupationTypeMotherId);
    }

}
