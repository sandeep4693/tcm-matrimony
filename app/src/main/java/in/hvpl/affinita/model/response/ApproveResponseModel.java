package in.hvpl.affinita.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import in.hvpl.affinita.model.base.ResponseModel;

/**
 * Created by Ganesh.K on 1/8/17.
 */

public class ApproveResponseModel extends ResponseModel{


    @SerializedName("data")
    @Expose
    private ApproveResponseData data;

    public ApproveResponseData getData() {
        return data;
    }

    public void setData(ApproveResponseData data) {
        this.data = data;
    }

    public class ApproveResponseData{

        @SerializedName("status")
        @Expose
        private int status;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }

}
