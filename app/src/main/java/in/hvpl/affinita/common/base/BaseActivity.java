package in.hvpl.affinita.common.base;

import android.arch.lifecycle.MutableLiveData;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.common.network.ApiClient;
import in.hvpl.affinita.common.network.NetworkHelper;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.ToolbarBinding;
import in.hvpl.affinita.model.request.GetClientDetailsModel;
import in.hvpl.affinita.model.response.UserDetailsResponseModel;
import in.hvpl.affinita.view.TutorialActivity;
import in.hvpl.affinita.view.block.BlockActivity;
import neo.architecture.lifecycle.BaseLifecycleActivity;

/**
 * Created by webwerks on 4/7/17.
 */

public abstract class BaseActivity<T extends ViewDataBinding> extends BaseLifecycleActivity<T> {

    protected ToolbarBinding toolbarBinding;
    private MutableLiveData<Boolean> isDataLoad = new MutableLiveData<Boolean>();
    private MutableLiveData<Boolean> isUnreadCountUpdate = new MutableLiveData<>();
    private FirebaseAnalytics mFirebaseAnalytics;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setConnectionUnreadCount(intent.getIntExtra(Constants.IntentExtras.CONNECTION_UNREAD_MSG_COUNT, 0));
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCurrentScreenName();
    }

    public void setCurrentScreenName() {
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if (getScreenName() != null)
            mFirebaseAnalytics.setCurrentScreen(this, getScreenName(), getClassName());
    }

    public FirebaseAnalytics getmFirebaseAnalytics() {
        return mFirebaseAnalytics;
    }

    public MutableLiveData<Boolean> isDataLoaded() {
        return isDataLoad;
    }

    public MutableLiveData<Boolean> isUnreadCountRefresh() {
        return isUnreadCountUpdate;
    }

    public void setToolBar(ToolbarBinding toolBar) {
        toolbarBinding = toolBar;
        setSupportActionBar(toolBar.toolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(App.getInstance().isBoy() ? R.drawable.ic_boy_back : R.drawable.ic_girl_back);
    }

    public void setToolbarTitle(String string) {
        if (toolbarBinding != null) {
            toolbarBinding.title.setText(string);
        } else {
            Tools.printError("" + getScreenName(), "Toolbar not set");
        }
    }

    public void setProposalUnreadCount(int count) {
        SharedPreferencesHelper.setProposalUnreadCount(this, count);
        isUnreadCountUpdate.setValue(true);
    }

    public void setConnectionUnreadCount(int count) {
        SharedPreferencesHelper.setConnectedUnreadCount(this, count);
        isUnreadCountUpdate.setValue(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!SharedPreferencesHelper.getClientId(this).equals("0")) {
            if (App.getInstance().getUserDetailModel().get() == null) {
                NetworkHelper.create(this)
                        .callWebService(ApiClient.getClient().getClientDetails(
                                new GetClientDetailsModel(SharedPreferencesHelper.getClientId(this))),
                                new NetworkHelper.NetworkCallBack<UserDetailsResponseModel>() {
                                    @Override
                                    public void onSyncData(UserDetailsResponseModel data) {
                                        App.getInstance().setUserDetailModel(data.getUserDetailModel());
                                        SharedPreferencesHelper.setBureauUrl(BaseActivity.this, SharedPreferencesHelper.ADVISORY_URL, data.getUserDetailModel().getAdvisoryUrl());
                                        if (data.getUserDetailModel().getStatus() == Constants.AppStatus.BLOCK) {
                                            SharedPreferencesHelper.blockUser(BaseActivity.this);
                                            Intent intent = new Intent(BaseActivity.this, BlockActivity.class);
                                            startActivity(intent);
                                            finish();
                                        }


                                        isDataLoad.setValue(true);
                                        //  UserDetailModel userDetailModel = SharedPreferencesHelper.getUserDetails(BaseActivity.this);
                                    }

                                    @Override
                                    public void onFailed(UserDetailsResponseModel error) {
                                        // UserDetailModel userDetailModel = SharedPreferencesHelper.getUserDetails(BaseActivity.this);
                                        setUserData(isDataLoad);
                                    }

                                    @Override
                                    public void noInternetConnection() {
                                        setUserData(isDataLoad);
                                    }

                                });
            } else {
                isDataLoad.setValue(true);
            }
        }
    }

    private void setUserData(MutableLiveData<Boolean> isDataLoad) {
        App.getInstance().setUserDetailModel(SharedPreferencesHelper.getUserDetailModel(BaseActivity.this));

        if (App.getInstance().getUserDetailModel().get() == null) {
            isDataLoad.setValue(false);
        } else {
            isDataLoad.setValue(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);


    }

    public String getStyleString(String desc) {
        String styleString = "<html><head><style type=\"text/css\">"
                + "a {color:#93eeff; text-decoration:underline}"
                + " table{border:2px solid #ffffff; width:100%%; margin:0px 0;}"
                + " td{border-right:2px solid #ffffff; padding:5px 8px; border-bottom:2px solid #ffffff;}"
                + " table tr td:last-child{border-right:none;}"
                + " table tr:last-child td{border-bottom:none;}"
                + " table{width:100%% !important;}"
                + "@font-face {"
                + "font-family: RegularFont;" + "src: url(\"file:///android_asset/" + Constants.FontPath.REGULAR + "\");}"
                + "@font-face {"
                + "font-family: BoldFont;"
                + "src: url(\"file:///android_asset/fonts/CenturyGothic_Bold.ttf\");}"
                + "body {font-family:RegularFont;font-weight:inherit; color: #ffffff;font-size:14px; align-content:left;}"
                + "h3 {font-family:BoldFont;font-size:18px;color:#ffffff;}"
                + "</style>"
                /*+"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"*/
                + "<meta name='viewport' content='target-densityDpi=device-dpi'/>"
                + "</head>"
                + "<body>"
                + desc
                + "</body>"
                + "</html>";
        return styleString;
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver),
                new IntentFilter(Constants.ActionIntent.NEW_MESSAGE)
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    @Override
    public void onBackPressed() {
        if (this instanceof TutorialActivity) {
            isTutorialScreen = true;
        }
        super.onBackPressed();
    }

    public void setToolBarVisibility(boolean flag) {
        if (toolbarBinding != null) {
            if (flag)
                toolbarBinding.toolBar.setVisibility(View.VISIBLE);
            else
                toolbarBinding.toolBar.setVisibility(View.GONE);
        } else {
            Tools.printError("" + getScreenName(), "Toolbar not set");
        }
    }
}


/*
{

        "notification_type": "message",
        "icon": "myicon",
        "notificationId": 42,
        "message_id": 42,
        "body": "helo",
        "title": "New Message",
        "message": {
        "is_read": "0",
        "updated_at": "2017-09-01 17:18:28",
        "receiver_id": "4813",
        "parent_id": "3",
        "created_at": "2017-09-01 17:18:28",
        "deleted_by": 0,
        "client_match_id": "39",
        "id": 42,
        "body": "helo",
        "sender_id": "4841"
        },
        "unread_message_count": 1,
        "sender_id": "4841"

        }*/
