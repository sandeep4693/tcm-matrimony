package in.hvpl.affinita.common.utils;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.databinding.ObservableArrayList;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import in.hvpl.affinita.App;
import in.hvpl.affinita.R;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.model.response.FacebookPhotoResponse;
import neo.architecture.lifecycle.BaseLifecycleActivity;

/**
 * Created by webwerks on 23/5/17.
 */

public class FacebookHelper {
    private CallbackManager callbackManager;

    private static final String TAG = FacebookHelper.class.getSimpleName();

    private static FacebookHelper mHelper;

    private FacebookHelper() {

    }

    public AccessToken getAccessToken() {
        return AccessToken.getCurrentAccessToken();
    }

    public CallbackManager getCallbackManager() {
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
        }
        return callbackManager;
    }

    public static FacebookHelper getInstance() {
        if (mHelper == null) {
            FacebookSdk.sdkInitialize(App.getInstance(), () -> {
                if (AccessToken.getCurrentAccessToken() == null) {
                    Tools.printError(TAG, "Not logged ");
                } else {
                    Tools.printError(TAG, " logged ");
                }
            });


            mHelper = new FacebookHelper();


        }
        return mHelper;
    }

    public void doLogin(Activity activity, FbCallBack fbCallBack) {

        // set Permission
        LoginManager.getInstance().logInWithReadPermissions(
                activity,
                Arrays.asList("user_photos"));


        LoginManager.getInstance().registerCallback(getCallbackManager(), new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Tools.printError(TAG, ":::" + loginResult.getAccessToken().getToken());
               // AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                fbCallBack.onSuccess();
            }

            @Override
            public void onCancel() {
                Tools.printError(TAG, "::: CANCEL");
            }

            @Override
            public void onError(FacebookException e) {
                Tools.printError(TAG, ":::" + e.getMessage());
                fbCallBack.onError();
            }
        });
    }


    public void getFacebookUserPhotos(BaseLifecycleActivity activity, MutableLiveData<FacebookPhotoResponse> facebookPhotoResponseMutableLiveData) {

        final FacebookPhotoResponse[] facebookPhotoResponse = {new FacebookPhotoResponse()};
        Bundle bundle = new Bundle();
        bundle.putString("type", "uploaded");
        bundle.putString("fields", "images");
        bundle.putInt("limit", 1000);
        activity.showProgressLoading(activity.getString(R.string.dialog_msg_get_facebook_images));
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/photos",
                bundle,
                HttpMethod.GET,
                (response) -> {
                    if (response.getError() != null) {
                        facebookPhotoResponseMutableLiveData.setValue(null);
                    } else {
                        JSONObject jsonObject = response.getJSONObject();
                        facebookPhotoResponse[0] = parseFacebookResponse(jsonObject);
                        facebookPhotoResponseMutableLiveData.setValue(facebookPhotoResponse[0]);
                    }

                    activity.stopLoading();
                }
        ).executeAsync();

    }

    /**
     * Parse the response of  facebook photo API
     *
     * @param jsonObject
     * @return {@link FacebookPhotoResponse}
     */
    private FacebookPhotoResponse parseFacebookResponse(JSONObject jsonObject) {

        Tools.printError(TAG, "jsonObject" + jsonObject.toString());
        FacebookPhotoResponse facebookPhotoResponse = new FacebookPhotoResponse();
        try {
            ObservableArrayList<AlbumInformationModel> facebookPhotoArrayList = new ObservableArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject photos = jsonArray.getJSONObject(i);
                AlbumInformationModel facebookPhotos = new AlbumInformationModel();
                facebookPhotos.setAlbumPhotoId(photos.getString("id"));
                facebookPhotos.setAlbumPhotoPath(photos.getJSONArray("images").getJSONObject(0).getString("source"));
                facebookPhotos.setFromPhoto(Constants.FROM_IMAGE.FROM_FB);
                facebookPhotoArrayList.add(facebookPhotos);
            }
            facebookPhotoResponse.setFacebookPhotos(facebookPhotoArrayList);
            JSONObject cursorsJsonObj = jsonObject.getJSONObject("paging").getJSONObject("cursors");
            facebookPhotoResponse.setAfterPaging(cursorsJsonObj.getString("after"));
            facebookPhotoResponse.setBeforePaging(cursorsJsonObj.getString("before"));
        } catch (JSONException e) {
            facebookPhotoResponse = new FacebookPhotoResponse();
            e.printStackTrace();
        }

        return facebookPhotoResponse;
    }

    public interface FbCallBack {
        void onSuccess();

        void onError();
    }
}

