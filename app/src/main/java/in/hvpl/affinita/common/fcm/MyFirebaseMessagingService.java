/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package in.hvpl.affinita.common.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.model.MessageModel;
import in.hvpl.affinita.view.SplashScreenActivity;
import in.hvpl.affinita.view.block.BlockActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    private LocalBroadcastManager broadcaster;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Tools.printError(TAG, "From: " + remoteMessage.getFrom());

        //   {notification_type=action, body=We have added Final to your proposals, icon=myicon, title=Notification}

        //{notificationData={"notification_type":"action","notificationId":8,"body":"Message body 8","title":"mes"}}
        // Check if message contains a data payload.
        Tools.printError(TAG, "Message data payload: " + remoteMessage.getData());
        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("notificationData"));
            sendNotification(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Tools.printError(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }


    // [END receive_message]


    private void sendNotification(JSONObject jsonObject) {
        try {
            String notification_type = jsonObject.getString("notification_type");
            int notificationId = jsonObject.getInt("notificationId");
            String messageBody = jsonObject.getString("body");
            String title = jsonObject.getString("title");
            int message_id = 0;
            boolean canSendNotification = true;
            Intent intent;
            switch (notification_type.toLowerCase()) {

                case Constants.NotificationAction.MESSAGE:
                    int sender_id = jsonObject.getInt("sender_id");
                    if (sender_id == SharedPreferencesHelper.getReceiverId(this)) {
                        canSendNotification = false;
                    }

                    MessageModel messageModel = new Gson().fromJson(jsonObject.getJSONObject("message").toString(), MessageModel.class);
                    intent = new Intent(Constants.ActionIntent.NEW_MESSAGE);
                    intent.putExtra(Constants.IntentExtras.MESSAGE, messageModel);
                    intent.putExtra(Constants.IntentExtras.CONNECTION_UNREAD_MSG_COUNT, jsonObject.getInt("unread_message_count"));
                    broadcaster.sendBroadcast(intent);
                    break;

                case Constants.NotificationAction.BLOCK:
                    SharedPreferencesHelper.blockUser(this);
                    canSendNotification = false;
                    intent = new Intent(this, BlockActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    break;

                case Constants.NotificationAction.PROPOSAL:
                case Constants.NotificationAction.CONNECT:
                    intent = new Intent(Constants.ActionIntent.PROPOSAL);
                    intent.putExtra(Constants.IntentExtras.PROPOSAL_UNREAD_COUNT, jsonObject.getInt("proposal_count"));
                    broadcaster.sendBroadcast(intent);
                    break;

                case Constants.NotificationAction.APPROVAL:
                    intent = new Intent(Constants.ActionIntent.ACTIVATION_STATUS);
                    intent.putExtra(Constants.IntentExtras.STATUS, Constants.AppStatus.ACTIVE);
                    broadcaster.sendBroadcast(intent);
                    break;
                case Constants.NotificationAction.PROFILE_HOLD:
                    intent = new Intent(Constants.ActionIntent.ACTIVATION_STATUS);
                    intent.putExtra(Constants.IntentExtras.STATUS, Constants.AppStatus.HOLD);
                    broadcaster.sendBroadcast(intent);
                    break;
                case Constants.NotificationAction.NEW_ARTICLE:

                    break;

                case Constants.NotificationAction.CANT_APPROVE:
                    intent = new Intent(Constants.ActionIntent.ACTIVATION_STATUS);
                    intent.putExtra(Constants.IntentExtras.STATUS, Constants.AppStatus.CANT_APPROVE);
                    broadcaster.sendBroadcast(intent);
                    break;
                case Constants.NotificationAction.ACCEPT_REQUEST:
                    intent = new Intent(Constants.ActionIntent.ACCEPT_REQUEST);
                    broadcaster.sendBroadcast(intent);
                    break;


            }


            if (canSendNotification) {
                Intent intentNotification = new Intent(this, SplashScreenActivity.class);
                intentNotification.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,
                        0 /* Request code */, intentNotification,
                        PendingIntent.FLAG_ONE_SHOT);
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(Notification.PRIORITY_MAX);

                NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
                bigTextStyle.setBigContentTitle(getString(R.string.app_name));
                notificationBuilder.setContentText(title);
                bigTextStyle.bigText(messageBody);
                //bigTextStyle.setSummaryText(count + " new event");
                notificationBuilder.setStyle(bigTextStyle);
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
            }
        } catch (JSONException ex) {

        }


    }
}
