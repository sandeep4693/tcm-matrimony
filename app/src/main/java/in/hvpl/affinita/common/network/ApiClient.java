package in.hvpl.affinita.common.network;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import in.hvpl.affinita.App;
import in.hvpl.affinita.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by webwerks on 19/6/17.
 */

public class ApiClient {

    public static final String BASE_URL = BuildConfig.API_BASE_URL;
    public static final String PATH_URL =BuildConfig.API_PATH_URL;


    private static Retrofit retrofit = null;

    public static ApiInterface getClient() {

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new HeaderInterceptor())
                .addNetworkInterceptor(new StethoInterceptor())
                .build();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
                    .build();
        }
        return retrofit.create(ApiInterface.class);
    }


    /**
     * Web service method name
     */

    public abstract class ApiMethod {

        public static final String GET_OTP = "getOtp";
        public static final String VERIFY_OTP = "verifyOtp";
        public static final String UPLOAD_PROFILE_PIC = "updateClientProfile";
        public static final String UPLOAD_AUDIO = "insertOrUpdateAudio";
        public static final String GET_CLIENT_DETAILS = "getClientDetails";
        public static final String GET_INTEREST_QUESTIONS = "getInterestQuestions";
        public static final String GET_ARTICLE_CATEGORIES = "getArticleCategories";
        public static final String GET_ARTICLE_LISTING = "articleListing";
        public static final String REMOVE_IMAGE = "removeImage";
        public static final String ADD_IMAGES = "addImage";
        public static final String GET_CLIENT_MATCHES_DETAIL = "getClientMatchsDetails";
        public static final String UPDATE_HOBBIES = "updateClientHobbies";
        public static final String GET_MASTER = "getMasters";
        public static final String GET_STATES = "getStates";
        public static final String GET_CITIES = "getCities";
        public static final String UPDATE_BASIC_INFO = "updateClientDetails";
        public static final String UPDATE_ABOUT_ME = "updateAboutMe";
        public static final String GET_CASTES = "getCastes";
        public static final String GET_SUB_CASTES = "getSubCastes";
        public static final String UPDATE_FAMILY = "updateClientParent";
        public static final String UPDATE_PARTNER = "updateClientPreferencesDetails";
        public static final String SEND_FOR_APPROVAL = "sendForApproval";
        public static final String CONNECT_REQUEST = "connectRequest";
        public static final String GET_CONNECTION_LIST = "getConnectedClients";
        public static final String GET_CHAT_MESSAGE = "getThreadMessage";
        public static final String SEND_MESSAGE = "sendMessage";
        public static final String CLEAR_CHAT = "clearchat";
        public static final String REPORT_USER = "blockChat";
        public static final String READ_MESSAGE = "readMessage";
        public static final String CHECK_FORCE_UPDATE = "checkForceUpdate";


    }


    /**
     * Created by webwerks on 19/6/17.
     */

    public static class HeaderInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain)
                throws IOException {
            Request request = chain.request();
            request = request.newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("api-version", "v1")
                    .addHeader("client_accesor_id", App.getInstance().getClientId())
                    .build();
            Response response = chain.proceed(request);
            return response;
        }
    }
}
