package in.hvpl.affinita.common.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.GsonBuilder;

import in.hvpl.affinita.model.UserDetailModel;

/**
 * Created by webwerks on 14/6/17.
 */

public class SharedPreferencesHelper {

    public static final String ADVISORY_URL = "advisoryUrl";
    private static final String PREF_NAME = "affinita";
    private static final String IS_LOGIN = "isLogin";
    private static final String IS_BOY = "isBoy";
    private static final String IS_VIDEO_PLAY = "isVideoPlay";
    private static final String IS_AUDIO_PLAY = "isAudioPlay";
    private static final String IS_ALBUM_UPDATE = "isAlbumUpdate";
    private static final String IS_GAME_UPDATE = "isGameUpdate";
    private static final String IS_PERSONAL_INFO_UPDATE = "isPersonalInfoUpdate";
    private static final String IS_PARTNER_PREFERNCE_UPDATE = "isPartnerPreferenceUpdate";
    private static final String IS_VIEW_PROFILE_UPDATE = "isViewProfileUpdate";
    private static final String CLIENT_ID = "clientID";
    private static final String IS_PARTNER_PREFERENCE_VIEW = "isPartnerPreferenceView";
    private static final String IS_BASIC_INFO_UPDATED = "isBasicInfoUpdated";
    private static final String IS_ABOUT_ME_UPDATED = "isAboutMeUpdated";
    private static final String IS_EDUCATION_UPDATED = "isEducationUpdated";
    private static final String IS_CAREER_UPDATED = "isCareerUpdated";
    private static final String IS_BACKGROUND_UPDATE = "isBackgroundUpdated";
    private static final String IS_FAMILY_DETAILS_UPDATED = "isFamilyDetailsUpdated";
    private static final String IS_PARTNER_PREFERENCE_UPDATED = "isPartnerPreferenceUpdated";
    private static final String IS_DATA_UPDATED = "isDataUpdated";
    private static final String FCM_TOKEN = "fcm_token";
    private static final String RECEIVER_ID = "receiverID";
    private static final String CONNECTION_UNREAD_COUNT = "connectionUnreadCount";
    private static final String PROPOSAL_UNREAD_COUNT = "proposalUnreadCount";
    private static final String USER_DETAILS = "UserDetails";

    public static SharedPreferences init(Context context) {

        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME,
                Context.MODE_MULTI_PROCESS);

        return preferences;

    }

    private static void setBoolean(Context context, String key, boolean value) {
        SharedPreferences preferences = init(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private static void setString(Context context, String key, String value) {
        SharedPreferences preferences = init(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private static void setInteger(Context context, String key, int value) {
        SharedPreferences preferences = init(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    private static boolean getBoolean(Context context, String key, boolean defaultValue) {
        SharedPreferences preferences = init(context);
        return preferences.getBoolean(key, defaultValue);
    }

    private static String getString(Context context, String key, String defaultValue) {
        SharedPreferences preferences = init(context);
        return preferences.getString(key, defaultValue);
    }

    private static int getInteger(Context context, String key, int defaultValue) {
        SharedPreferences preferences = init(context);
        return preferences.getInt(key, defaultValue);
    }

    public static void setBureauUrl(Context context, String key, String value) {
        setString(context, key, value);
    }

    public static String getBureauUrl(Context context, String key) {
        return getString(context, key, "");
    }

    public static void setIsLogin(Context context) {
        setBoolean(context, IS_LOGIN, true);
    }

    public static void setIsLogin(Context context, boolean login) {
        setBoolean(context, IS_LOGIN, login);
    }

    public static boolean isLogin(Context context) {
        return getBoolean(context, IS_LOGIN, false);
    }


    public static boolean isBoy(Context context) {
        return getBoolean(context, IS_BOY, false);
    }

    public static void setIsBoy(Context context, boolean isBoy) {
        setBoolean(context, IS_BOY, isBoy);
    }

    public static void setIsVideoPlay(Context context, boolean isVideoPlay) {
        setBoolean(context, IS_VIDEO_PLAY, isVideoPlay);
    }

    public static boolean isVideoPlay(Context context) {
        return getBoolean(context, IS_VIDEO_PLAY, false);
    }

    public static void setIsAudioPlay(Context context, boolean isAudioPlay) {
        setBoolean(context, IS_AUDIO_PLAY, isAudioPlay);
    }

    public static boolean isAudioPlay(Context context) {
        return getBoolean(context, IS_AUDIO_PLAY, false);
    }


    public static void setClientId(Context context, String clientID) {
        setString(context, CLIENT_ID, clientID);
    }

    public static String getClientId(Context context) {
        return getString(context, CLIENT_ID, "0");
    }

    public static boolean isPartnerPreferenceView(Context context) {
        return getBoolean(context, IS_PARTNER_PREFERENCE_VIEW, false);
    }

    public static void setIsPartnerPreferenceView(Context context, boolean isView) {
        setBoolean(context, IS_PARTNER_PREFERENCE_VIEW, isView);
    }

    /**
     *
     */
    public static boolean isBasicInfoUpdated(Context context) {
        return getBoolean(context, IS_BASIC_INFO_UPDATED, false);
    }

    public static void setIsBasicInfoUpdated(Context context, boolean isUpdated) {
        setBoolean(context, IS_BASIC_INFO_UPDATED, isUpdated);
        dataUpdated(context);
    }

    public static boolean isAboutMeUpdated(Context context) {
        return getBoolean(context, IS_ABOUT_ME_UPDATED, false);
    }

    public static void setIsAboutMeUpdated(Context context, boolean isUpdated) {
        setBoolean(context, IS_ABOUT_ME_UPDATED, isUpdated);
        dataUpdated(context);
    }

    public static boolean isEducationUpdated(Context context) {
        return getBoolean(context, IS_EDUCATION_UPDATED, false);
    }

    public static void setIsEducationUpdated(Context context, boolean isUpdated) {
        setBoolean(context, IS_EDUCATION_UPDATED, isUpdated);
        dataUpdated(context);
    }

    public static boolean isCareerUpdated(Context context) {
        return getBoolean(context, IS_CAREER_UPDATED, false);
    }

    public static void setIsCareerUpdated(Context context, boolean isUpdated) {
        setBoolean(context, IS_CAREER_UPDATED, isUpdated);
        dataUpdated(context);
    }

    public static boolean isBackgroundUpdated(Context context) {
        return getBoolean(context, IS_BACKGROUND_UPDATE, false);
    }

    public static void setIsBackgroundUpdated(Context context, boolean isUpdated) {
        setBoolean(context, IS_BACKGROUND_UPDATE, isUpdated);
        dataUpdated(context);
    }

    public static boolean isFamilyDetailsUpdated(Context context) {
        return getBoolean(context, IS_FAMILY_DETAILS_UPDATED, false);
    }

    public static void setIsFamilyDetailsUpdated(Context context, boolean isUpdated) {
        setBoolean(context, IS_FAMILY_DETAILS_UPDATED, isUpdated);
        dataUpdated(context);
    }

    public static boolean isPartnerPreferenceUpdated(Context context) {
        return getBoolean(context, IS_PARTNER_PREFERENCE_UPDATED, false);
    }

    public static void setIsPartnerPreferenceUpdated(Context context, boolean isUpdated) {
        setBoolean(context, IS_PARTNER_PREFERENCE_UPDATED, isUpdated);
        dataUpdated(context);
    }

    public static boolean isDataUpdated(Context context) {
        return getBoolean(context, IS_DATA_UPDATED, false);
    }

    public static void setIsDataUpdated(Context context, boolean isUpdated) {
        setBoolean(context, IS_DATA_UPDATED, isUpdated);
        setIsBasicInfoUpdated(context, true);
        setIsAboutMeUpdated(context, true);
        setIsEducationUpdated(context, true);
        setIsCareerUpdated(context, true);
        setIsBackgroundUpdated(context, true);
        setIsFamilyDetailsUpdated(context, true);
        setIsPartnerPreferenceUpdated(context, true);
    }


    public static void dataUpdated(Context context) {
        if (!isDataUpdated(context)) {
            if (isBasicInfoUpdated(context) && isAboutMeUpdated(context) && isEducationUpdated(context) &&
                    isCareerUpdated(context) && isBackgroundUpdated(context) && isFamilyDetailsUpdated(context) && isPartnerPreferenceUpdated(context)) {
                setIsDataUpdated(context, true);
            }
        }
    }

    public static String getFcmToken(Context context) {
        if (getString(context, FCM_TOKEN, "").isEmpty()) {
            setString(context, FCM_TOKEN, FirebaseInstanceId.getInstance().getToken());
        }
        return getString(context, FCM_TOKEN, "");
    }

    public static void setFcmToken(Context context, String fcmToken) {
        setString(context, FCM_TOKEN, fcmToken);
    }

    public static void setReceiverId(Context context, int receiverId) {
        setInteger(context, RECEIVER_ID, receiverId);
    }

    public static int getReceiverId(Context context) {
        return getInteger(context, RECEIVER_ID, 0);
    }


    public static void blockUser(Context context) {
        setIsLogin(context, false);
        setIsVideoPlay(context, false);
        setIsAudioPlay(context, false);
        setClientId(context, "0");
        setIsBasicInfoUpdated(context, false);
        setIsAboutMeUpdated(context, false);
        setIsEducationUpdated(context, false);
        setIsCareerUpdated(context, false);
        setIsBackgroundUpdated(context, false);
        setIsFamilyDetailsUpdated(context, false);
        setIsPartnerPreferenceUpdated(context, false);
    }


    public static boolean getIsAlbumUpdate(Context context) {
        return getBoolean(context, IS_ALBUM_UPDATE, false);
    }

    public static void setIsAlbumUpdate(Context context) {
        setBoolean(context, IS_ALBUM_UPDATE, true);
    }

    public static boolean getIsGameUpdate(Context context) {
        return getBoolean(context, IS_GAME_UPDATE, false);
    }

    public static void setIsGameUpdate(Context context) {
        setBoolean(context, IS_GAME_UPDATE, true);
    }


    public static boolean getIsPersonalInfoUpdate(Context context) {
        return getBoolean(context, IS_PERSONAL_INFO_UPDATE, false);
    }

    public static void setIsPersonalInfoUpdate(Context context) {
        setBoolean(context, IS_PERSONAL_INFO_UPDATE, true);
    }

    public static boolean getIsPartnerPreferenceUpdate(Context context) {
        return getBoolean(context, IS_PARTNER_PREFERNCE_UPDATE, false);
    }

    public static void setIsPartnerPreferenceUpdate(Context context) {
        setBoolean(context, IS_PARTNER_PREFERNCE_UPDATE, true);
    }


    public static boolean getIsViewProfileUpdate(Context context) {
        return getBoolean(context, IS_VIEW_PROFILE_UPDATE, false);
    }

    public static void setIsViewProfileUpdate(Context context) {
        setBoolean(context, IS_VIEW_PROFILE_UPDATE, true);
    }

    public static int getConnectedUnreadCount(Context context) {
        return getInteger(context, CONNECTION_UNREAD_COUNT, 0);
    }

    public static void setConnectedUnreadCount(Context context, int count) {
        setInteger(context, CONNECTION_UNREAD_COUNT, count);
    }

    public static int getProposalUnreadCount(Context context) {
        return getInteger(context, PROPOSAL_UNREAD_COUNT, 0);
    }

    public static void setProposalUnreadCount(Context context, int count) {
        setInteger(context, PROPOSAL_UNREAD_COUNT, count);
    }

    private static String getUserDetailsString(Context context) {
        return getString(context, USER_DETAILS, "");
    }

    public static UserDetailModel getUserDetailModel(Context context) {
        UserDetailModel userDetailModel = null;
        String string = getUserDetailsString(context);

        if (!string.equals("")) {
            userDetailModel = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(string, UserDetailModel.class);
            // userDetailModel=new Gson().fromJson(string,UserDetailModel.class);
        }

        /*try {
            JSONObject jsonObject = new JSONObject(preferences.getString(key,""));
            //String s =  jsonObject.getJSONObject("UserDetailsResponseModel").getString("userDetailModel");
            userDetailModel = new Gson().fromJson(String.valueOf(jsonObject), UserDetailModel.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        return userDetailModel;
    }

    public static void setUserDetails(Context context, UserDetailModel userDetailModel) {
        String userDetailsJsonString = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().toJson(userDetailModel);
        setString(context, USER_DETAILS, userDetailsJsonString);
    }

}
