package in.hvpl.affinita.common.base;


import android.arch.lifecycle.ViewModel;

import in.hvpl.affinita.App;

/**
 * Created by webwerks on 19/6/17.
 */


public abstract class BaseViewModel extends ViewModel {
    private final boolean isBoy = App.getInstance().isBoy();

    public boolean isBoy() {
        return isBoy;
    }
}
