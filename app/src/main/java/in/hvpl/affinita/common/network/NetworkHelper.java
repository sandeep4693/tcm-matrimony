package in.hvpl.affinita.common.network;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import in.hvpl.affinita.common.utils.Tools;
import neo.architecture.lifecycle.BaseLifecycleActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by webwerks on 19/6/17.
 */

public class NetworkHelper {

    private static final String TAG = NetworkHelper.class.getSimpleName();

    private Context mContext;
    private String message = null;

    /**
     * this flag use for when you call api from service.
     */
    private boolean showProgressLoading = true;
    private boolean showOnFailureDialog = true;


    private NetworkHelper(Context mContext) {
        this.mContext = mContext;
    }

    public static NetworkHelper create(Context context) {
        return new NetworkHelper(context);
    }

    public NetworkHelper setMessage(String message) {
        this.message = message;
        return this;

    }

    public <T> void callWebService(Call<T> call, NetworkCallBack networkCallBack) {

        if (Tools.isConnected(mContext)) {
            showLoader();
            call.enqueue(new Callback<T>() {
                @Override
                public void onResponse(Call<T> call, Response<T> response) {
                    stopLoader();
                    if (response.isSuccessful()) {
                        try {
                            Tools.printError(TAG, "enter in onResponse:" + "" + response.body().toString());
                            networkCallBack.onSyncData(response.body());

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Tools.printError(TAG, "Enter in Exception:");
                            if (showOnFailureDialog) {
                                Tools.showDialogWhenException(mContext);
                            }

                        }
                    } else {
                        Tools.printError(TAG, "server contact failed");
                        networkCallBack.onFailed(null);
                    }

                }

                @Override
                public void onFailure(Call<T> call, Throwable t) {
                    t.printStackTrace();
                    Tools.printError(TAG, "enter in onFailure:" + t.toString());
                    stopLoader();
                    if (showOnFailureDialog) {
                        showOnFailureDialogResponse();
                    }
                    networkCallBack.onFailed(null);
                }
            });

        } else {
            networkCallBack.noInternetConnection();
            if (showProgressLoading) {
                Tools.showNoInternetMsg(mContext);
            }
        }
    }

    public void downloadFile(String url, File destFile, NetworkCallBack networkCallBack) {

        if (Tools.isConnected(mContext)) {
            showLoader();

            Call<ResponseBody> call = ApiClient.getClient().downloadFile(url);

            call.enqueue(new Callback<ResponseBody>() {
                @SuppressLint("StaticFieldLeak")
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                    if (response.isSuccessful()) {
                        try {
                            Log.d(TAG, "server contacted and has file");

                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    boolean writtenToDisk = writeResponseBodyToDisk(response.body(), destFile);

                                    Log.d(TAG, "file download was a success? " + writtenToDisk);

                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    super.onPostExecute(aVoid);
                                    stopLoader();
                                    networkCallBack.onSyncData(response.body());

                                }
                            }.execute();


                        } catch (Exception ex) {
                            ex.printStackTrace();
                            Tools.printError(TAG, "Enter in Exception:");
                            stopLoader();
                            if (showOnFailureDialog) {
                                Tools.showDialogWhenException(mContext);
                            }
                            networkCallBack.onFailed(null);

                        }
                    } else {
                        Tools.printError(TAG, "server contact failed");
                        networkCallBack.noInternetConnection();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();
                    Tools.printError(TAG, "enter in onFailure:" + t.toString());
                    stopLoader();
                    if (showOnFailureDialog) {
                        showOnFailureDialogResponse();
                    }
                    networkCallBack.onFailed(null);

                }
            });

        } else {
            networkCallBack.noInternetConnection();
            if (showProgressLoading) {
                Tools.showNoInternetMsg(mContext);
            }
        }
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, File audioFile) {
        try {

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(audioFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    private void showLoader() {
        if (showProgressLoading) {
            if (message == null) {
                ((BaseLifecycleActivity) mContext).showProgressLoading();
            } else {
                ((BaseLifecycleActivity) mContext).showProgressLoading(message);
            }
        }
    }


    private void stopLoader() {
        if (showProgressLoading) {
            ((BaseLifecycleActivity) mContext).stopLoading();
        }
    }

    private void showOnFailureDialogResponse() {
        if (showOnFailureDialog) {
            Tools.showFailureMsg(mContext);

        }

    }
    
    public NetworkHelper setShowOnFailureDialog(boolean showOnFailureDialog) {
        this.showOnFailureDialog = showOnFailureDialog;
        return this;
    }

    public NetworkHelper setShowProgressLoading(boolean showProgressLoading) {
        this.showProgressLoading = showProgressLoading;
        return this;
    }


    public interface NetworkCallBack<T> {
        void onSyncData(T data);


        void onFailed(T error);

        void noInternetConnection();

    }
}
