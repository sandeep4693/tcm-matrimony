package in.hvpl.affinita.common.databinding;

import android.app.Activity;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.view.widget.BlurTransformation;
import in.hvpl.affinita.view.widget.RoundedCornersTransformation;


/**
 * Created by GANESH on 02-03-2017.
 */

public class BindingAdapters {

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView view, String fontPath) {
        Log.e("BindingAdapter", "fontPath=" + fontPath);
        Typeface typeface = Typeface.createFromAsset(view.getContext().getAssets(), fontPath);
        view.setTypeface(typeface);

    }

    @BindingAdapter({"bind:font"})
    public static void setFont(EditText view, String fontPath) {
        Typeface typeface = Typeface.createFromAsset(view.getContext().getAssets(), fontPath);
        view.setTypeface(typeface);
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(CheckBox view, String fontPath) {
        Typeface typeface = Typeface.createFromAsset(view.getContext().getAssets(), fontPath);
        view.setTypeface(typeface);
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(RadioButton view, String fontPath) {
        Typeface typeface = Typeface.createFromAsset(view.getContext().getAssets(), fontPath);
        view.setTypeface(typeface);
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(Button view, String fontPath) {
        Typeface typeface = Typeface.createFromAsset(view.getContext().getAssets(), fontPath);
        view.setTypeface(typeface);
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(AutoCompleteTextView view, String fontPath) {
        Typeface typeface = Typeface.createFromAsset(view.getContext().getAssets(), fontPath);
        view.setTypeface(typeface);
    }

    @BindingAdapter({"bind:imageUrl", "bind:placeHolder"})
    public static void setImage(ImageView image, String url, Drawable placeholder) {
        Glide.with(image.getContext()).load(url)
                .asBitmap()
                .thumbnail(0.5f)
                .placeholder(placeholder)
                .centerCrop().into(new BitmapImageViewTarget(image) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(image.getContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                image.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

   /* @BindingAdapter({"bind:imageProfileUrl", "bind:placeHolder"})
    public static void setCircularImage(CircleImageView image, String url, Drawable placeholder) {
        Glide.with(image.getContext()).load(url)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(placeholder)
                .into(image);
    }*/

    @BindingAdapter({"bind:cornerImageUrl", "bind:placeHolder"})
    public static void setCornerImage(ImageView image, String url, Drawable placeholder) {

        int sCorner = 15;
        int sMargin = 10;

        Glide.with(image.getContext())
                .load(url)
                .centerCrop()
                .bitmapTransform(new RoundedCornersTransformation(image.getContext(), sCorner, sMargin))
                .placeholder(placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image);
    }

    @BindingAdapter({"bind:circularBlurImageUrl", "bind:placeHolder", "bind:isView"})
    public static void setCircularBlurImage(ImageView image, String url, Drawable placeholder, int isView) {
        Tools.printError("isView ::::::", isView + "");
        if (isView == 0) {
            Glide.with(image.getContext())
                    .load(url)
                    .asBitmap()
                    .centerCrop()
                    .transform(new BlurTransformation(image.getContext()))
                    .placeholder(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new BitmapImageViewTarget(image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(image.getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            image.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        } else {
            Glide.with(image.getContext())
                    .load(url)
                    .asBitmap()
                    .centerCrop()
                    .placeholder(placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new BitmapImageViewTarget(image) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(image.getContext().getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            image.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        }
    }

    @BindingAdapter({"bind:circularBlurImageUrl", "bind:placeHolder"})
    public static void setCircularBlurImage(ImageView image, String url, Drawable placeholder) {
        setCircularBlurImage(image, url, placeholder, 1);
    }

    @BindingAdapter({"bind:nonCacheImageUrl", "bind:placeHolder"})
    public static void setNonCacheImg(ImageView image, String url, Drawable placeholder) {
        int sCorner = 15;
        int sMargin = 0;

        Glide.with(image.getContext())
                .load(url)
                .bitmapTransform(new RoundedCornersTransformation(image.getContext(), sCorner, sMargin))
                .placeholder(placeholder)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(image);
    }


    @BindingAdapter({"bind:imageUrl"})
    public static void setImage(ImageView image, String url) {
        setImage(image, url, null);
    }

    @BindingAdapter({"bind:nextEditText"})
    public static void moveToNextEditText(AppCompatEditText et1, AppCompatEditText et2) {

        et1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (et2 == null) {
                    if (et1.getText().toString().length() != 0)
                        Tools.hideKeyboard((Activity) et1.getContext());
                } else {
                    if (et1.getText().toString().length() == 1) {
                        et2.requestFocus();
                    }
                }

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });
    }

    @BindingAdapter({"bind:previousEditText"})
    public static void moveToBackEditText(AppCompatEditText editText1, AppCompatEditText editText2) {

        editText1.setOnKeyListener((v, keyCode, event) -> {
            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
            //this is for backspace
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (editText2 == null) {
                        Tools.hideKeyboard((Activity) editText1.getContext());
                    } else {
                        editText2.requestFocus();
                    }

                }
            }
            return false;
        });
    }


    @BindingAdapter({"bind:setIconID"})
    public static void setIcon(TextView textView, String iconID) {
        Log.e("BindingAdapter", "iconID=" + iconID);
        textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/icomoon.ttf"));
        textView.setText(iconID);
    }

    @BindingAdapter({"bind:viewGroup", "bind:applyBlur"})
    public static void showBlurImg(ImageView imageView, View viewGroup, boolean applyBlur) {
        if (applyBlur) {
            viewGroup.addOnLayoutChangeListener((view, i, i1, i2, i3, i4, i5, i6, i7) -> Tools.showBlurView(viewGroup, imageView));
            viewGroup.post(() -> Tools.showBlurView(viewGroup, imageView));
        }
    }

    @BindingAdapter({"bind:isSelected", "bind:isBoy", "bind:isSelectable"})
    public static void bottomMenuSelector(TextView textView, boolean isSelected, boolean isBoy, boolean isSelectable) {
        if (isSelectable) {
            if (isSelected) {
                textView.setTextColor(isBoy ? ContextCompat.getColor(textView.getContext(), R.color.boy_bottom_menu_select) : ContextCompat.getColor(textView.getContext(), R.color.girl_bottom_menu_select));
            } else {
                textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.bottom_menu_non_select));
            }
        } else {
            textView.setTextColor(ContextCompat.getColor(textView.getContext(), R.color.divider_color));
        }
    }

    @BindingAdapter({"bind:isShow"})
    public static void setVisibility(View view, boolean isShow) {
        if (isShow) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }


    @BindingAdapter({"bind:setGravity"})
    public static void setGravity(LinearLayout linearLayout, boolean isRight) {

        if (isRight) {
            linearLayout.setGravity(Gravity.RIGHT);
        } else {
            linearLayout.setGravity(Gravity.LEFT);
        }

    }

    @BindingAdapter({"bind:setChatBackgroundIsSend", "bind:setChatBackgroundISBoy"})
    public static void setChatBackground(View view, boolean isSend, boolean isBoy) {

        if (isSend) {
            if (isBoy) {
                view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_boy_msg_reply));
            } else {
                view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_girl_msg_reply));
            }
        } else {
            if (isBoy) {
                view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_girl_msg));
            } else {
                view.setBackground(ContextCompat.getDrawable(view.getContext(), R.drawable.bg_boy_msg));
            }
        }

    }

}
