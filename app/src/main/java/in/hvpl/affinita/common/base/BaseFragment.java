package in.hvpl.affinita.common.base;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.google.firebase.analytics.FirebaseAnalytics;

import neo.architecture.lifecycle.BaseLifecycleFragment;

/**
 * Created by webwerks on 7/7/17.
 */

public abstract class BaseFragment<T extends ViewDataBinding> extends BaseLifecycleFragment<T> {


    public FirebaseAnalytics getmFirebaseAnalytics() {
        return getBaseActivity().getmFirebaseAnalytics();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getScreenName()!=null)
        getmFirebaseAnalytics().setCurrentScreen(getActivity(), getScreenName(), getClassName());
    }

    public void setToolbarTitle(String string) {
        getBaseActivity().setToolbarTitle(string);
    }

    public void setToolBarVisibility(boolean flag){
        getBaseActivity().setToolBarVisibility(flag);
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

}
