package in.hvpl.affinita.common.widget;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by webwerks on 9/3/17.
 */

public class CustomDatePicker {

    private OnDateSelectionListener customOnDateSetListener;
    private Context context;
    private int requestCode;


    public CustomDatePicker(OnDateSelectionListener customOnDateSetListener, Context context) {
        this.customOnDateSetListener = customOnDateSetListener;
        this.context = context;


    }


    private DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            monthOfYear = monthOfYear + 1;
            String date = addZero(dayOfMonth) + "-" + addZero(monthOfYear) + "-" + year;
            customOnDateSetListener.onDateSelect(date, requestCode);

        }
    };


    private String addZero(int value) {
        if (value < 10) {
            return "0" + value;
        }
        return "" + value;
    }

    public void showDate(int requestCode, Calendar cal) {
        this.requestCode = requestCode;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, onDateSetListener, year, month, day);
        datePickerDialog.show();
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            String time = getFormatedTime(hourOfDay, minutes);
            customOnDateSetListener.onDateSelect(time, requestCode);


        }

    };

    // Used to convert 24hr format to 12hr format with AM/PM values
    private String getFormatedTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        return aTime;
    }

    public void showTime(int requestCode) {
        this.requestCode = requestCode;
        final Calendar c = Calendar.getInstance();
        // Current Hour
        int hour = c.get(Calendar.HOUR_OF_DAY);
        // Current Minute
        int minute = c.get(Calendar.MINUTE);

        // set time picker as current time
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, timePickerListener, hour, minute, false);
        timePickerDialog.show();

    }

    public interface OnDateSelectionListener {
        void onDateSelect(String date, int requestCode);
    }

}
