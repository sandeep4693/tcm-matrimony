package in.hvpl.affinita.common.utils;

import java.io.File;

import in.hvpl.affinita.App;
import in.hvpl.affinita.BuildConfig;

/**
 * Created by webwerks on 22/05/17.
 */
public class Constants {

    //Proposal, Article, Approval

    public static final int FETCH_STARTED = 2001;
    public static final int FETCH_COMPLETED = 2002;
    public static final int ERROR = 2003;

    /**
     * bureau id
     */
    public static final String BUREAU_ID = BuildConfig.BUREAU_ID;
    public static final String BUREAU_URL = BuildConfig.BUREAU_DETAILS_URL;// ApiClient.BASE_URL + ApiClient.PATH_URL + "getBureauDetails/" + Constants.BUREAU_ID;
    public static final String BUREAU_TERMS_AND_CONDITION_URL = BuildConfig.BUREAU_TERMS_AND_CONDITION_URL;


    public static final int MIN_HEIGHT_FEET = 4;
    public static final int MAX_HEIGHT_FEET = 8;

    public static final int MIN_HEIGHT_INCHES = 0;
    public static final int MAX_HEIGHT_INCHES = 11;

    public static final int MIN_AGE = 18;
    public static final int MAX_AGE = 60;

    /**
     * File Name
     */
    public static final File FOLDER_PATH = /*Environment.getExternalStorageDirectory();//*/App.getInstance().getFilesDir();
    public static final File AUDIO_FILE = new File(FOLDER_PATH, "audio");
    public static final File ALBUM_PHOTO = new File(FOLDER_PATH, "albumSent");
    public static final File PROFILE_IMAGE = new File(FOLDER_PATH, "profileImage");
    public static final String AUDIO_FILE_EXTENTION = ".wav";
    public static final String PHOTO_FILE_EXTENTION = ".jpg";
    public static final String SCREEN_NAME = "_SCREEN";

    public interface FROM_IMAGE {
        int FROM_GALLERY = 0;
        int FROM_FB = 1;
        int FROM_INSTA = 2;
        int FROM_SERVER = 3;
        int FROM_EMPTY = 4;
    }

    /**
     * Font path
     */
    public interface FontPath {
        String BLACK = "fonts/black.ttf";
        String BOLD = "fonts/bold.ttf";
        String HEAVY = "fonts/heavy.ttf";
        String LIGHT = "fonts/light.ttf";
        String MEDIUM = "fonts/medium.ttf";
        String REGULAR = "fonts/regular.ttf";
        String SEMI_BOLD = "fonts/semibold.ttf";
        String THIN = "fonts/thin.ttf";

        String SANS_REGULAR = "fonts/pt_sans_caption_regular.ttf";
        String SANS_BOLD = "fonts/pt_sans_caption_bold.ttf";

    }

    public interface ProposalType {
        String RECENT_VIEW = "recently_viewed_profile";
        String YOU_LIKE = "you_like_profile";
        String WHO_YOU_LIKE = "people_who_like_your_profile";
        String LATEST_MATCH = "client_match";
    }

    public interface ConnectOperation {
        int CONNECT = 1;
        int ACCEPT_REQUEST = 2;
        int LATER = 5;
        int NOT_INTERESTED = 6;
    }

    public interface ActivityResultRequestCode {
        int CROP_IMAGE_REQUEST = 1;
        int PLAY_VIDEO_TUTORIAL = 2;
        int PLAY_AUDIO_TUTORIAL = 3;
        int REQUEST_RECORD_AUDIO = 4;
        int PARTNER_PREFE_VIEW = 5;
        int SELECT_DIET = 7;
        int SELECT_SMOKING = 8;
        int SELECT_DRINKING = 9;
        int SELECT_MARITAL_STATUS = 10;
        int SELECT_DISABILITY = 11;
        int SELECT_HIGHEST_EDUCATION = 12;
        int SELECT_SALARY_RANGE = 13;
        int SELECT_MOTHER_TONGUE = 14;
        int SELECT_CASTE = 15;
        int SELECT_SUB_CASTE = 16;
        int SELECT_CITY = 17;
        int SELECT_STATE = 18;
        int SELECT_COUNTRY = 19;
        int SELECT_RESIDENT = 20;
        int SELECT_OUTLOOK = 21;
        int SELECT_GOTHRA = 22;
        int SELECT_RASHI = 23;
        int SELECT_NAKSHATRA = 24;
        int SELECT_MANGLIK = 25;
        int SELECT_OCCUPATION_TYPE = 26;
        int SELECT_FIELD = 27;
        int SELECT_DISIGNATION = 28;
        int SELECT_RELIGION = 29;
        int SELECT_DOB = 30;
        int SELECT_CURRENCY = 31;
        int SELECT_OCCUPATION_TYPE_FATHER = 32;
        int SELECT_OCCUPATION_TYPE_MOTHER = 33;

        int SELECT_ADDRESS = 34;
        int SELECT_PINCODE = 35;
        int SELECT_STREAM = 36;
        int SELECT_UNIVERSITY = 37;
        int SELECT_COMPANY = 38;
        int SELECT_HEIGHT = 39;
        int SELECT_MIN_HEIGHT = 40;
        int SELECT_MAX_HEIGHT = 41;
        int SELECT_AGE = 42;
        int VIEW_PROFILE_SLIDER = 42;
        int VIEW_PROFILR_LIST = 43;
        int SEND_FOR_APPROVAL = 44;
        int TUTORIAL_PROFILE_PIC = 45;
        int TUTORIAL_AUDIO_PLAY = 46;
        int TUTORIAL_ALBUM = 47;
        int TUTORIAL_GAME = 48;
        int TUTORIAL_PERSONAL_INFO = 49;
        int TUTORIAL_PARTNER_PREFERENCE = 50;
        int TUTORIAL_VIEW_PROFILE = 51;
        int GAME_PLAYED = 52;
        int FATHER_NAME = 53;


    }

    public interface IntentExtras {
        String ACTION_CAMERA = "action-camera";
        String ACTION_GALLERY = "action-gallery";
        String IMAGE_PATH = "image_path";
        String ARTICLE_MODEL = "article_model";
        String IS_FROM_PARTNER_PREFE = "isFromPartner";
        String MASTER_DATA = "master";
        String MASTER_LIST = "master_list";
        String TITLE = "title";
        String SELECTION_MODEL = "selection_model";
        String COME_FROM = "come_from";
        String EDIT_DETAIL = "edit_details";
        String EDITED_DETAILS = "edited_detail";
        String PROPOSAL_TYPE = "proposal_type";
        String PROPOSAL_MATCH_ID = "proposal_id";
        String AUDIO_FILE_PATH = "audio_file";
        String CONNECTION_DATA = "connection_data";
        String MESSAGE = "message";
        String PHOTO_LIST = "photoList";
        String TUTORIAL_MESSAGE = "tutorial_message";
        String CONNECTION_UNREAD_MSG_COUNT = "unread_connection_msg_count";
        String PROPOSAL_UNREAD_COUNT = "unread_proposal_count";
        String FORCE_UPDATE = "isForceUpdate";
        String FORCE_UPDATE_MSG = "isForceUpdateMsg";
        String STATUS = "status";
    }

    public interface PermissionRequestCode {
        int RC_CAMERA = 101;
        int RC_STORAGE = 102;
        int RC_CAMERA_AND_STORAGE = 103;
        int RC_RECORD_AUDIO_AND_STORAGE = 104;
        int RC_STORAGE_AUDIO = 105;
        int RC_SMS = 106;

    }

    public interface ActivityComeFrom {
        int COME_FROM_PARTNER_PREFERENCE = 1;
        int COME_FROM_VIEW_PROFILE = 2;
        int COME_FROM_AUDIO_PLAY = 3;
        int COME_FROM_PROPOSAL = 4;


    }

    public interface ActionIntent {
        String NEW_MESSAGE = "in.hvpl.affinita.message";
        String PROPOSAL = "in.hvpl.affinita.proposal";
        String ACTIVATION_STATUS = "in.hvpl.affinita.status";
        String ACCEPT_REQUEST = "in.hvpl.affinita.accept.request";
    }

    public interface Icon {
        String IC_DIET = String.valueOf((char) 0xe901);
        String IC_LOCATION = String.valueOf((char) 0xe902);
        String IC_SMOKING = String.valueOf((char) 0xe903);
        String IC_RULER = String.valueOf((char) 0xe904);
        String IC_BACK_ARROW = String.valueOf((char) 0xe905);
        String IC_REMOVE_IMAGE = String.valueOf((char) 0xe906);
        String IC_RELIGION_COMUNITY = String.valueOf((char) 0xe908);
        String IC_FEMALE = String.valueOf((char) 0xe909);
        String IC_PROFILE_INFO = String.valueOf((char) 0xe90a);
        String IC_PLAY_VIDEO = String.valueOf((char) 0xe90b);
        String IC_EDIT = String.valueOf((char) 0xe90c);
        String IC_EDIT_WITH_CIRCLE = String.valueOf((char) 0xe90d);
        String IC_DRINK = String.valueOf((char) 0xe90f);
        String IC_NEXT_ARROW = String.valueOf((char) 0xe910);
        String IC_MALE = String.valueOf((char) 0xe911);
        String IC_MICROPHONE = String.valueOf((char) 0xe912);
        String IC_MARITAL_STATUS = String.valueOf((char) 0xe913);
        String IC_AFFINITA = String.valueOf((char) 0xe914);
        String IC_INSIGHTS = String.valueOf((char) 0xe915);
        String IC_PATNER_PREFERENCE = String.valueOf((char) 0xe916);
        String IC_EDUCATION = String.valueOf((char) 0xe917);
        String IC_DONE_WITH_CIRCLE = String.valueOf((char) 0xe918);
        String IC_CAREER = String.valueOf((char) 0xe919);
        String IC_BIRTHDAY = String.valueOf((char) 0xe91a);
        String IC_BUREAU = String.valueOf((char) 0xe91b);
        String IC_BACKGROUND = String.valueOf((char) 0xe91c);
        String IC_ASTRO_DETAILS = String.valueOf((char) 0xe91d);
        String IC_ANNUAL_INCOME = String.valueOf((char) 0xe91e);
        String IC_ABOUT_ME = String.valueOf((char) 0xe91f);
        String IC_HOBBIES = String.valueOf((char) 0xe92c);
        String IC_FAMILY = String.valueOf((char) 0xe92d);
        String IC_DISABILITY = String.valueOf((char) 0xe92e);

    }

    public interface AppStatus {
        int INACTIVE = 0;
        int PENDING_FOR_APPROVAL = 1;
        int UPDATE_REQUEST = 2;
        int ACTIVE = 3;
        int CANT_APPROVE = 4;
        int BLOCK = 5;
        int HOLD = 6;
        int ACTIVATION_LINK = 7;
    }

    public interface NotificationAction {

        String BLOCK = "block";
        String MESSAGE = "message";
        String PROPOSAL = "proposal";
        String APPROVAL = "approval";
        String PROFILE_HOLD = "profilehold";
        String NEW_ARTICLE = "article";
        String CONNECT = "connection";
        String UNBLOCK = "unblock";
        String CANT_APPROVE = "cantapprove";
        String ACCEPT_REQUEST = "acceptrequest";
    }

    public interface AnalyticsEventKey {
        String SEND_FOR_APPROVAL = "PROFILE_SEND_FOR_APPROVAL";
        String YOU_LIKE = "PROPOSAL_YOU_LIKE";
        String MUTUAL_CONNECTION = "MUTUAL_CONNECTION";
        String USER_NAME = "Username";
    }

    public interface AnalyticsScreenName {

        String SPLASH_SCREEN = "SPLASH_SCREEN";
        String REGISTRATION_SCREEN = "REGISTRATION_SCREEN";
        String TAKE_SELFIE_SCREEN = "TAKE_SELFIE_SCREEN";
        String CREATE_AUDIO_SCREEN = "CREATE_AUDIO_SCREEN";
        String PLAY_GAME_SCREEN = "PLAY_GAME_SCREEN";
        String BUREAU_DETAILS = "BUREAU_DETAILS";
        String DEMO_VIDEO_SCREEN = "DEMO_VIDEO_SCREEN";
        String DEMO_AUDIO_SCREEN = "DEMO_AUDIO_SCREEN";
        String CREATE_ALBUM = "CREATE_ALBUM_SCREEN";
        String EDIT_PROFILE = "EDIT_PROFILE_SCREEN";
        String PARTNER_PREFERNCE_SCREEN = "PARTNER_PREFERENCE_SCREEN";
        String CONNECTION_SCREEN = "CONNECTION_SCREEN";
        String PROPOSALS_SCREEN = "PROPOSALS_SCREEN";
        String INSIGHTS_SCREEN = "INSIGHTS_SCREEN";
        String TUTORIAL_SCREEN = "TUTORIAL_SCREEN";

        String PERSONAL_PROFILE_INFO = "PERSONAL_PROFILE_INFO_SCREEN";
        String EDIT_BASIC_INFORMATION_SCREEN = "EDIT_BASIC_INFORMATION_SCREEN";
        String EDIT_ABOUT_ME_SCREEN = "EDIT_ABOUT_ME_SCREEN";
        String EDIT_EDUCATION_SCREEN = "EDIT_EDUCATION_SCREEN";
        String EDIT_CAREER_SCREEN = "EDIT_CAREER_SCREEN";
        String EDIT_BACKGROUND_SCREEN = "EDIT_BACKGROUND_SCREEN";
        String EDIT_ASTRO_DETAILS_SCREEN = "EDIT_ASTRO_DETAILS_SCREEN";
        String EDIT_FAMILY_DETAILS_SCREEN = "EDIT_FAMILY_DETAILS_SCREEN";
        String EDIT_PARTNER_PREFERENCE_SCREEN = "EDIT_PARTNER_PREFERENCE_SCREEN";
    }
}
