package in.hvpl.affinita.common.network;

import java.util.List;

import in.hvpl.affinita.model.base.ResponseModel;
import in.hvpl.affinita.model.request.AstroInfoRequestModel;
import in.hvpl.affinita.model.request.BackgroundRequestModel;
import in.hvpl.affinita.model.request.BasicInfoRequestModel;
import in.hvpl.affinita.model.request.CareerRequestModel;
import in.hvpl.affinita.model.request.EducationRequestModel;
import in.hvpl.affinita.model.request.FamilyInfoRequestModel;
import in.hvpl.affinita.model.request.GetArticleRequest;
import in.hvpl.affinita.model.request.GetClientDetailsModel;
import in.hvpl.affinita.model.request.PartnerPrefrenceRequestModel;
import in.hvpl.affinita.model.request.RegistrationModel;
import in.hvpl.affinita.model.response.AboutMeResponseModel;
import in.hvpl.affinita.model.response.AlbumUploadResponseModel;
import in.hvpl.affinita.model.response.ApproveResponseModel;
import in.hvpl.affinita.model.response.ArticleCategoryResponseModel;
import in.hvpl.affinita.model.response.ArticleListResponseModel;
import in.hvpl.affinita.model.response.CastesResponseModel;
import in.hvpl.affinita.model.response.ChatMessageResponseModel;
import in.hvpl.affinita.model.response.CheckForceUpdateResponse;
import in.hvpl.affinita.model.response.CityResponseModel;
import in.hvpl.affinita.model.response.ConnectionResponseModel;
import in.hvpl.affinita.model.response.HobbiesResponseModel;
import in.hvpl.affinita.model.response.InterestResponseModel;
import in.hvpl.affinita.model.response.MasterResponseModel;
import in.hvpl.affinita.model.response.ProposalResponseModel;
import in.hvpl.affinita.model.response.RegistrationResponseModel;
import in.hvpl.affinita.model.response.SendResponseModel;
import in.hvpl.affinita.model.response.StateResponseModel;
import in.hvpl.affinita.model.response.SubCasteResponseModel;
import in.hvpl.affinita.model.response.UploadProfilePicResponseModel;
import in.hvpl.affinita.model.response.UserDetailsResponseModel;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by webwerks on 19/6/17.
 */

public interface ApiInterface {

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_OTP)
    Call<RegistrationResponseModel> getOtp(@Body RegistrationModel registrationModel);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.VERIFY_OTP)
    Call<UserDetailsResponseModel> verifyOtp(@Body RegistrationModel registrationModel);

    @Multipart
    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPLOAD_PROFILE_PIC)
    Call<UploadProfilePicResponseModel> uploadProfilePic(@Part MultipartBody.Part photo, @Part("client_id") RequestBody client_id);

    @Multipart
    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPLOAD_AUDIO)
    Call<UploadProfilePicResponseModel> uploadAudio(@Part MultipartBody.Part photo, @Part("client_id") RequestBody client_id);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_CLIENT_DETAILS)
    Call<UserDetailsResponseModel> getClientDetails(@Body GetClientDetailsModel getClientDetailsModel);

    @Streaming
    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);

    @GET(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_INTEREST_QUESTIONS)
    Call<InterestResponseModel> getInterestQuestions();

    @GET(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_ARTICLE_CATEGORIES)
    Call<ArticleCategoryResponseModel> getArticleCategories();

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_ARTICLE_LISTING)
    Call<ArticleListResponseModel> getArticleList(@Body GetArticleRequest articleRequest);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.REMOVE_IMAGE)
    @FormUrlEncoded
    Call<ResponseModel> removeImage(@Field("client_id") String clientId,
                                    @Field("image_id") String imageId);

    @Multipart
    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.ADD_IMAGES)
    Call<AlbumUploadResponseModel> addImages(@Part List<MultipartBody.Part> images, @Part("client_id") RequestBody client_id);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_CLIENT_MATCHES_DETAIL)
    @FormUrlEncoded
    Call<ProposalResponseModel> getProposalData(@Field("client_id") String clientId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_HOBBIES)
    @FormUrlEncoded
    Call<HobbiesResponseModel> updateHobbies(@Field("client_id") String clientId, @Field("hobbies") String hobbies);

    @GET(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_MASTER)
    Call<MasterResponseModel> getMasterData();


    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_STATES)
    @FormUrlEncoded
    Call<StateResponseModel> getStatesByCountryId(@Field("country_id") String clientId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_CITIES)
    @FormUrlEncoded
    Call<CityResponseModel> getCitiesByStateId(@Field("state_id") String stateId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_BASIC_INFO)
    Call<UserDetailsResponseModel> updateBasiciInfo(@Body BasicInfoRequestModel basicInfoRequestModel);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_ABOUT_ME)
    @FormUrlEncoded
    Call<AboutMeResponseModel> updateAboutMe(@Field("client_id") String clientId, @Field("about_me") String aboutMe);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_CASTES)
    @FormUrlEncoded
    Call<CastesResponseModel> getCasteByReligionId(@Field("religion_id") String religionId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_SUB_CASTES)
    @FormUrlEncoded
    Call<SubCasteResponseModel> getSubCasteByCasteId(@Field("caste_id") String casteId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_BASIC_INFO)
    Call<UserDetailsResponseModel> updateEducation(@Body EducationRequestModel educationRequestModel);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_BASIC_INFO)
    Call<UserDetailsResponseModel> updateCareer(@Body CareerRequestModel careerRequestModel);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_BASIC_INFO)
    Call<UserDetailsResponseModel> updateBackground(@Body BackgroundRequestModel backgroundRequestModel);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_BASIC_INFO)
    Call<UserDetailsResponseModel> updateAstroDetails(@Body AstroInfoRequestModel astroInfoRequestModel);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_FAMILY)
    Call<UserDetailsResponseModel> updateFamilyDetails(@Body FamilyInfoRequestModel familyInfoRequestModel);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.UPDATE_PARTNER)
    Call<UserDetailsResponseModel> updatePartnerPreferenceDetails(@Body PartnerPrefrenceRequestModel partnerPrefrenceRequestModel);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.SEND_FOR_APPROVAL)
    @FormUrlEncoded
    Call<ApproveResponseModel> sendForApproval(@Field("client_id") String clientId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.CONNECT_REQUEST)
    @FormUrlEncoded
    Call<ResponseModel> connectRequest(@Field("client_id") String clientId, @Field("id") String id, @Field("operation") String operation);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_CONNECTION_LIST)
    @FormUrlEncoded
    Call<ConnectionResponseModel> getConnectionList(@Field("client_id") String clientId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.GET_CHAT_MESSAGE)
    @FormUrlEncoded
    Call<ChatMessageResponseModel> getChatMessage(@Field("parent_id") String parentId, @Field("sender_id") String senderId, @Field("message_id") String messageId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.SEND_MESSAGE)
    @FormUrlEncoded
    Call<SendResponseModel> sendMessage(@Field("sender_id") String senderId, @Field("receiver_id") String receiverId, @Field("body") String body, @Field("client_match_id") String clientMatchId, @Field("parent_id") String parentId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.CLEAR_CHAT)
    @FormUrlEncoded
    Call<ResponseModel> clearChat(@Field("parent_id") String parentId, @Field("deleter_id") String clientId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.REPORT_USER)
    @FormUrlEncoded
    Call<ResponseModel> reportUser(@Field("client_match_id") String clientMatchId, @Field("blocked_by") String clientId);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.READ_MESSAGE)
    @FormUrlEncoded
    Call<ResponseModel> readMessage(@Field("message_id") String message_id);

    @POST(ApiClient.PATH_URL + ApiClient.ApiMethod.CHECK_FORCE_UPDATE)
    @FormUrlEncoded
    Call<CheckForceUpdateResponse> checkForceUpdate(@Field("app_type") String appType,
                                                    @Field("current_version") String currentVersion,
                                                    @Field("bureau_id") String bureauId);
}
