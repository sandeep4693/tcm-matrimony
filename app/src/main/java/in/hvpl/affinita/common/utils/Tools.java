package in.hvpl.affinita.common.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Handler;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import in.hvpl.affinita.R;
import in.hvpl.affinita.databinding.CustomToastBinding;
import in.hvpl.affinita.interfaces.DialogClickListener;

/**
 * Created by webwerks on 23/5/17.
 */

public class Tools {
    /**
     * print error log
     */
    public static void printError(String tag, String msg) {
        Log.e(tag, msg);
    }

    /**
     * print error Debug msg
     */
    public static void printDebug(String tag, String msg) {
        Log.e(tag, msg);
    }

    /**
     * show test show
     */
    public static void showTestToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * @param context
     * @param msg
     */
    public static void showToast(Context context, String msg) {

        showToast(context, msg, Toast.LENGTH_SHORT);
    }

    /**
     * @param context
     * @param msg
     */
    public static void showToast(Context context, String msg, int duration) {

        CustomToastBinding binding = CustomToastBinding.inflate(LayoutInflater.from(context), null);
        binding.setIsBoy(SharedPreferencesHelper.isBoy(context));
        binding.text.setText(msg);
        Toast toast = new Toast(context);
        toast.setDuration(duration);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setView(binding.getRoot());
        toast.show();
        //Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Checks if is connected.
     *
     * @param context the context
     * @return true, if is connected
     */
    public static boolean isConnected(Context context) {
//        ConnectivityManager connectivityManager = (ConnectivityManager) context
//                .getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager
//                .getActiveNetworkInfo();
//
//
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        return NetworkUtil.isConnected();
    }
//    public static boolean isConnected(Context context) {
//        String TAG = context.getPackageName();
//        if (NetworkUtil.isConnected()) {
//            try {
//                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
//                urlc.setRequestProperty("User-Agent", "Test");
//                urlc.setRequestProperty("Connection", "close");
//                urlc.setConnectTimeout(1500);
//                urlc.connect();
//                return (urlc.getResponseCode() == 200);
//            } catch (IOException e) {
//                Log.e(TAG, "Error checking internet connection", e);
//            }
//        } else {
//            Log.d(TAG, "No network available!");
//        }
//        return false;
//    }

    /**
     * @param context
     */
    public static void showDialogWhenException(Context context) {
        showToast(context, context.getString(R.string.server_error));
    }


    public static void showNoInternetMsg(Context context) {
        showToast(context, context.getString(R.string.no_internet));
    }


    public static void showFailureMsg(Context context) {
        showToast(context, context.getString(R.string.server_error));
    }

    /**
     * Set up viewpager indicator
     *
     * @param viewPager
     * @param indicatorContainer
     * @param positionObs
     */
    public static void setUpViewPagerIndicator(ViewPager viewPager, LinearLayout indicatorContainer, ObservableInt positionObs) {
        setUpViewPagerIndicator(viewPager, indicatorContainer, positionObs, null, false);
    }


    public static void setUpViewPagerIndicator(ViewPager viewPager, LinearLayout indicatorContainer, ObservableInt positionObs, ObservableField<String> msg, boolean isAutoScroll) {
        Context context = viewPager.getContext();
        /**
         * Indicator added in layout
         */
        int dotsCount = viewPager.getAdapter().getCount();
        ImageView[] dots = addIndicators(context, dotsCount, indicatorContainer);
        dots[0].setImageDrawable(ContextCompat.getDrawable(context, (R.drawable.selecteditem_dot)));

        /**
         *
         * Set view pager msg
         */
        String[] arrMsg = viewPager.getContext().getResources().getStringArray(R.array.arr_tutorial_msg);
        if (msg != null)
            msg.set(arrMsg[0]);

        /**
         * logic for auto scroll
         */
        final int[] currentPage = {1};

        Handler handler = new Handler();
        final Runnable runnable = () -> {
            viewPager.setCurrentItem(currentPage[0], true);
        };
        if (isAutoScroll) {
            handler.postDelayed(runnable, 3000);
        }


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(context, (R.drawable.nonselecteditem_dot)));
                }
                dots[position].setImageDrawable(ContextCompat.getDrawable(context, (R.drawable.selecteditem_dot)));

                if (currentPage[0] == dotsCount - 1) {
                    currentPage[0] = 0;
                    if (handler != null) {
                        handler.removeCallbacks(runnable);
                    }
                } else {
                    currentPage[0] = position + 1;
                    if (isAutoScroll) {
                        if (handler != null) {
                            handler.removeCallbacks(runnable);
                        }

                        handler.postDelayed(runnable, 3000);
                    }
                }
                if (positionObs != null)
                    positionObs.set(position);

                if (msg != null)
                    msg.set(arrMsg[position]);


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private static ImageView[] addIndicators(Context context, int dotsCount, LinearLayout indicatorContainer) {
        ImageView[] dots = new ImageView[dotsCount];
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(context);
            dots[i].setImageDrawable(ContextCompat.getDrawable(context, (R.drawable.nonselecteditem_dot)));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(8, 0, 8, 0);
            indicatorContainer.addView(dots[i], params);
        }
        return dots;
    }


    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showBlurView(View view, ImageView imageView) {
        Bitmap bitmap = getBlurBitMap(view);
        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
    }

    private static Bitmap getScreenshot(View v) {
        Bitmap b = Bitmap.createBitmap(v.getWidth(), v.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);
        return b;
    }

    private static Bitmap getBlurBitMap(View view) {
        Bitmap image = getScreenshot(view);
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(view.getContext());
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);
        ScriptIntrinsicBlur theIntrinsic = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
            theIntrinsic.setRadius(25f);

            theIntrinsic.setInput(tmpIn);

            theIntrinsic.forEach(tmpOut);

            tmpOut.copyTo(outputBitmap);
        }


        return outputBitmap;

    }

    /**
     * Copy a file from one location to another.
     *
     * @param sourceFile File to copy from.
     * @param destFile   File to copy to.
     * @return True if successful, false otherwise.
     * @throws IOException
     */
    public static Boolean copyFile(File sourceFile, File destFile) throws IOException {
        printError("Copy File", "sourceFile=" + sourceFile.getAbsolutePath());
        printError("Copy File", "destFile=" + destFile.getAbsolutePath());
        if (destFile.exists()) {
            destFile.delete();
        }
        destFile.createNewFile();

        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        } finally {
            if (source != null)
                source.close();
            if (destination != null)
                destination.close();
        }

    }


    public static String getAudioFile(Activity activity) {
        if (!Constants.AUDIO_FILE.exists()) {
            Constants.AUDIO_FILE.mkdir();
        }
        return Constants.AUDIO_FILE + File.separator + SharedPreferencesHelper.getClientId(activity) + Constants.AUDIO_FILE_EXTENTION;
    }

    public static String getProfilePhoto(Activity activity) {
        if (!Constants.PROFILE_IMAGE.exists()) {
            Constants.PROFILE_IMAGE.mkdir();
        }
        return Constants.PROFILE_IMAGE + File.separator + SharedPreferencesHelper.getClientId(activity) + Constants.PHOTO_FILE_EXTENTION;
    }

    public static String getProposalPhoto() {
        if (!Constants.PROFILE_IMAGE.exists()) {
            Constants.PROFILE_IMAGE.mkdir();
        }
        return Constants.PROFILE_IMAGE + File.separator + "temp" + Constants.PHOTO_FILE_EXTENTION;
    }

    public static String getProfileAudioFile() {
        if (!Constants.AUDIO_FILE.exists()) {
            Constants.AUDIO_FILE.mkdir();
        }
        return Constants.AUDIO_FILE + File.separator + "temp" + Constants.AUDIO_FILE_EXTENTION;
    }


    public static int getAge(String dobString) {
        int age = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Calendar dob = Calendar.getInstance();
            dob.setTime(sdf.parse(dobString));
            Calendar today = Calendar.getInstance();

            int curYear = today.get(Calendar.YEAR);
            int dobYear = dob.get(Calendar.YEAR);

            age = curYear - dobYear;

            // if dob is month or day is behind today's month or day
            // reduce age by 1
            int curMonth = today.get(Calendar.MONTH);
            int dobMonth = dob.get(Calendar.MONTH);
            if (dobMonth > curMonth) { // this year can't be counted!
                age--;
            } else if (dobMonth == curMonth) { // same month? check for day
                int curDay = today.get(Calendar.DAY_OF_MONTH);
                int dobDay = dob.get(Calendar.DAY_OF_MONTH);
                if (dobDay > curDay) { // this year can't be counted!
                    age--;
                }
            }
        } catch (Exception ex) {

        }
        return age;
    }

    public static String getConvertedHight(String feet, String inches) {
        String result = "0";
        try {
            int f = Integer.parseInt(feet);
            int i = Integer.parseInt(inches);
            int r = (f * 12) + i;
            return String.valueOf(r);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            result = "0";
        }

        return result;
    }

    public static boolean isValidValue(String string) {
        if (string == null) {
            return false;
        }
        if (string.equals("") || string.equals("0")) {
            return false;
        }

        return true;
    }

    public static boolean isValidId(String string) {
        if (string == null) {
            return false;
        }
        if (string.equals("")) {
            return false;
        }

        return true;
    }

    public static String getFormatedMsgString(Context context, Integer sFieldName) {
        return String.format(context.getString(R.string.error_msg_mandatory_fields), context.getString(sFieldName));
    }


    public static String getMessageDateORTime(String date) {

        if (date != null) {
            SimpleDateFormat serverDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            serverDateTimeFormat.setTimeZone(getTimeZone());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setTimeZone(getTimeZone());
            SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
            timeFormat.setTimeZone(getTimeZone());
            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
            dateTimeFormat.setTimeZone(getTimeZone());


            try {
             /*   Date serverDateTime = dateTimeFormat.parse(getDateTime(serverDateTimeFormat.parse(date).getTime()));
                Date serverDate = dateFormat.parse(getDate(serverDateTime.getTime()));
                Date currentDate = dateFormat.parse(getDate(new Date().getTime()));*/

                Date serverDateTime = serverDateTimeFormat.parse(date);
                Date serverDate = dateFormat.parse(getDate(serverDateTime.getTime()));
                Date currentDate = dateFormat.parse(getDate(new Date().getTime()));

                if (serverDate.before(currentDate)) {
                    return dateTimeFormat.format(serverDate);
                } else {
                    return timeFormat.format(serverDateTime);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return "";
    }

    private static TimeZone getTimeZone() {
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        return tz;
    }


    public static String getDateTime(long timeStamp) {

//        Timestamp timestamp = new Timestamp(timeStamp*1000L);
        Date date = new Date(timeStamp);
        //Date date = new Date(timestamp.getTime());
        // S is the millisecond
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        dateTimeFormat.setTimeZone(tz);

        return dateTimeFormat.format(date);
    }

    public static String getDate(long timeStamp) {
        // Timestamp timestamp = new Timestamp(timeStamp*1000L);
        Date date = new Date(timeStamp);
        // S is the millisecond
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        simpleDateFormat.setTimeZone(tz);
        return simpleDateFormat.format(date);
    }

    public static void showAlertDialog(Context context, String title, String msg, String positiveButton, String negativeButton, int requestCode, DialogClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveButton,
                (dialog, which) -> {
                    dialog.dismiss();
                    listener.onPositiveClick(requestCode);
                });


        if (!negativeButton.equals("")) {
            builder.setNegativeButton(negativeButton, (dialogInterface, i) -> {
                dialogInterface.dismiss();
                listener.onNegativeClick(requestCode);
            });
        }

        builder.show();
    }


    public static long getDurationOfSound(Context context, Object soundFile) {
        int millis = 0;
        MediaPlayer mp = new MediaPlayer();
        try {
            Class<? extends Object> currentArgClass = soundFile.getClass();
            if (currentArgClass == Integer.class) {
                AssetFileDescriptor afd = context.getResources().openRawResourceFd((Integer) soundFile);
                mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
            } else if (currentArgClass == String.class) {
                mp.setDataSource((String) soundFile);
            } else if (currentArgClass == File.class) {
                mp.setDataSource(((File) soundFile).getAbsolutePath());
            }
            mp.prepare();
            millis = mp.getDuration();
        } catch (Exception e) {
            //  Logger.e(e.toString());
        } finally {
            mp.release();
            mp = null;
        }
        return millis;
    }
}
