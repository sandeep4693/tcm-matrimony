package in.hvpl.affinita.common.widget;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import in.hvpl.affinita.R;
import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.Tools;
import in.hvpl.affinita.databinding.CustomNumberPickerDialogBinding;

/**
 * Created by webwerks1 on 28/7/17.
 */

public class CustomNumberPicker implements View.OnClickListener {

    private Dialog dialog;
    private CustomNumberPickerDialogBinding binding;
    private OnNumberSelectionListener listener;
    private int requestCode;
    private int minHeight = 0;

    private CustomNumberPicker(Context context) {

        this.dialog = new Dialog(context);
        this.binding = CustomNumberPickerDialogBinding.inflate(LayoutInflater.from(context), null, false);
        dialog.setContentView(binding.getRoot());

        binding.numberPicker1.setOnValueChangedListener((numberPicker, i, i1) -> {
            switch (requestCode) {
                case Constants.ActivityResultRequestCode.SELECT_AGE:
                    binding.numberPicker2.setMinValue(numberPicker.getValue() + 1);
                    break;
            }
        });
    }

    public static CustomNumberPicker create(Context context) {
        return new CustomNumberPicker(context);
    }

    public CustomNumberPicker setDialogTitle(String title) {
        binding.txtTitle.setText(title);
        return this;
    }

    public CustomNumberPicker setNumberPickerTitle(String title1, String title2) {
        binding.txtTitle1.setText(title1);
        binding.txtTitle2.setText(title2);
        return this;
    }

    public CustomNumberPicker setCurrentValue(int value1, int value2) {
        binding.numberPicker1.setValue(value1);
        binding.numberPicker2.setValue(value2);
        return this;
    }

    public CustomNumberPicker setMinValue(int min1, int min2) {
        binding.numberPicker1.setMinValue(min1);
        binding.numberPicker2.setMinValue(min2);
        return this;
    }

    public CustomNumberPicker setMaxValue(int max1, int max2) {
        binding.numberPicker1.setMaxValue(max1);
        binding.numberPicker2.setMaxValue(max2);
        return this;
    }

    public CustomNumberPicker setMinHeight(int minHeight) {
        this.minHeight = minHeight;
        return this;
    }

    public void show(OnNumberSelectionListener listener, int requestCode) {
        this.requestCode = requestCode;
        this.listener = listener;
        binding.btnUpdate.setOnClickListener(this);
        dialog.show();
    }


    @Override
    public void onClick(View v) {
        String value1 = String.valueOf(binding.numberPicker1.getValue());
        String value2 = String.valueOf(binding.numberPicker2.getValue());
        listener.onNumberSelection(value1, value2, requestCode);

        if (requestCode == Constants.ActivityResultRequestCode.SELECT_MAX_HEIGHT) {
            int maxHeight = Integer.parseInt(Tools.getConvertedHight(value1, value2));
            if (maxHeight > minHeight) {
                dialog.dismiss();
            } else {
                Tools.showToast(v.getContext(),v.getContext().getString(R.string.error_msg_max_height_validation));
            }
        } else {
            dialog.dismiss();
        }


    }


    public interface OnNumberSelectionListener {
        void onNumberSelection(String value1, String value2, int requestCode);
    }
}
