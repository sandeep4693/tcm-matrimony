package in.hvpl.affinita.interfaces;

/**
 * Created by Ganesh.K on 10/8/17.
 */

public interface DialogClickListener {
    void onPositiveClick(int requestCode);
    void onNegativeClick(int requestCode);
}
