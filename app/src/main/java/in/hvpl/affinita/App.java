package in.hvpl.affinita;

import android.app.Application;
import android.databinding.ObservableField;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.facebook.stetho.Stetho;

import in.hvpl.affinita.common.utils.Constants;
import in.hvpl.affinita.common.utils.SharedPreferencesHelper;
import in.hvpl.affinita.model.AlbumInformationModel;
import in.hvpl.affinita.model.UserDetailModel;

/**
 * Created by webwerks on 23/5/17.
 * w3bne0321
 */

public class App extends Application {

    private static App instance;
    private ObservableField<UserDetailModel> userDetailModel = new ObservableField<>();
    private UserDetailModel userProfileDetailModel;

    public static synchronized App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        MultiDex.install(this);
        Stetho.initializeWithDefaults(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
        StrictMode.setVmPolicy(builder.build());
    }

    public boolean isBoy() {
        return SharedPreferencesHelper.isBoy(getApplicationContext());
    }

    public String getClientId() {
        return SharedPreferencesHelper.getClientId(getApplicationContext());
    }

    public UserDetailModel getUserProfileDetailModel() {
        return userProfileDetailModel;
    }

    public void setUserProfileDetailModel(UserDetailModel userProfileDetailModel) {
        this.userProfileDetailModel = userProfileDetailModel;
    }

    public ObservableField<UserDetailModel> getUserDetailModel() {
        return userDetailModel;
    }

    public void setUserDetailModel(UserDetailModel userDetailModel) {
        SharedPreferencesHelper.setClientId(this, userDetailModel.getBasicInformationModel().getClientId());
        SharedPreferencesHelper.setIsBoy(this, userDetailModel.getBasicInformationModel().getGender().toLowerCase().equals("male"));
        SharedPreferencesHelper.setIsAudioPlay(this, !userDetailModel.getAudioFile().equals(""));
        SharedPreferencesHelper.setIsVideoPlay(this, !userDetailModel.getBasicInformationModel().getPhotoUrl().equals(""));
        int pendingAlbum = 9 - userDetailModel.getAlbumModels().size();
        for (int i = 0; i < pendingAlbum; i++) {
            userDetailModel.getAlbumModels().add(new AlbumInformationModel("", "", Constants.FROM_IMAGE.FROM_EMPTY));
        }

        SharedPreferencesHelper.setUserDetails(this, userDetailModel);
        this.userDetailModel.set(userDetailModel);

        /*Gson gson = new Gson();
        String json = gson.toJson(userDetailModel);
        SharedPreferencesHelper.setUserData(getInstance(), json);*/
    }

    public void storeUserDetailsOffline(UserDetailModel userDetailModel) {
        SharedPreferencesHelper.setUserDetails(this, userDetailModel);
    }
}
