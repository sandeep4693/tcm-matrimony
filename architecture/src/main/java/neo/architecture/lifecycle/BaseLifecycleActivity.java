package neo.architecture.lifecycle;

import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import neo.architecture.R;

/**
 * Created by webwerks on 22/05/17.
 */

public abstract class BaseLifecycleActivity<T extends ViewDataBinding> extends AppCompatActivity implements LifecycleRegistryOwner, UICallbacks {


    private final LifecycleRegistry mRegistry = new LifecycleRegistry(this);
    protected  boolean isTutorialScreen=false;
    @Override
    public LifecycleRegistry getLifecycle() {
        return mRegistry;
    }

    protected T mBinding;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mBinding = DataBindingUtil.setContentView(this, getContentViewResource());
        initUI();
    }


    @Override
    public void overridePendingTransition(int enterAnim, int exitAnim) {
        getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        super.overridePendingTransition(enterAnim, exitAnim);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(!isTutorialScreen){
            setAnimation();
        }else {
            finish();
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        }

    }



    public void startActivityWithAnimation(Intent intent) {
        startActivity(intent);
        setAnimation();
    }

    public void startActivityForResultWithAnimation(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
        setAnimation();
    }

    public void setAnimation() {
        overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
    }

    public void setBottomToTopAnimation() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
    }

    public void setTopToBottomAnimation() {
        overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
    }

    public void fragmentTransaction(int transactionType, Fragment fragment,
                                    int container, boolean isAddToBackStack) {
        FragmentsManagerHelper.fragmentTransaction(this, transactionType, fragment, container, isAddToBackStack);
    }

    public void showProgressLoading() {

        showProgressLoading("Loading..");
    }

    public void showProgressLoading(String msg) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Please wait");
            progressDialog.setMessage(msg);
            progressDialog.setCancelable(true);

            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
    }

    public void stopLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

   /* public void finish() {
        onBackPressed();
    }*/

}
