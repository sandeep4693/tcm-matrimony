package neo.architecture.lifecycle;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by webwerks on 22/05/17.
 */

public abstract class BaseLifecycleFragment<T extends ViewDataBinding> extends Fragment implements UICallbacks {

    protected T mBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, getContentViewResource(), container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
    }

    public BaseLifecycleActivity getBaseLifeCycleActivity() {
        return ((BaseLifecycleActivity) getActivity());
    }

    public void setAnimation() {
        getBaseLifeCycleActivity().setAnimation();
    }

    public void startActivityWithAnimation(Intent intent) {
        getBaseLifeCycleActivity().startActivityWithAnimation(intent);
    }

    public void startActivityForResultWithAnimation(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
        getBaseLifeCycleActivity().setAnimation();

    }

    public void startActivityForResultWithBtoTAnimation(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
        getBaseLifeCycleActivity().setBottomToTopAnimation();

    }

    public void fragmentTransaction(int transactionType, Fragment fragment,
                                    int container, boolean isAddToBackStack) {
        getBaseLifeCycleActivity().fragmentTransaction(transactionType, fragment, container, isAddToBackStack);
    }

    /*public void finish() {
        getBaseLifeCycleActivity().finish();
    }*/
}
