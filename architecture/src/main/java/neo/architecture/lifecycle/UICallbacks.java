package neo.architecture.lifecycle;

/**
 * Created by webwerks on 23/05/17.
 */

public interface UICallbacks {

    int getContentViewResource();

    void initUI();

    String getScreenName();

    String getClassName();

}
