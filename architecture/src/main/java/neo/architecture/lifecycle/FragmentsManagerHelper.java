package neo.architecture.lifecycle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import neo.architecture.R;


/**
 * Created by GANESH on 01-03-2017.
 */

public class FragmentsManagerHelper {

    /**
     * The Constant ADD_FRAGMENT.
     */
    public static final int ADD_FRAGMENT = 0;

    /**
     * The Constant REPLACE_FRAGMENT.
     */
    public static final int REPLACE_FRAGMENT = 1;


    /**
     *
     * @param lifecycleActivity
     * @param transactionType
     * @param fragment
     * @param container
     * @param isAddToBackStack
     */
    public static  void fragmentTransaction(AppCompatActivity lifecycleActivity, int transactionType, Fragment fragment,
                                            int container, boolean isAddToBackStack) {

        FragmentTransaction trans = lifecycleActivity.getSupportFragmentManager()
                .beginTransaction();
/***
 * set transaction animation vertical or horizontal
 * **/
        trans.setCustomAnimations(R.anim.activity_back_in,
                R.anim.activity_back_out,R.anim.activity_back_in,R.anim.activity_back_out);
        /*****
         *
         * set transaction type add or replace
         *
         * **/
        switch (transactionType) {
            case (ADD_FRAGMENT):
                trans.add(container, fragment, fragment.getClass().getSimpleName());
                break;
            case (REPLACE_FRAGMENT):
                trans.replace(container, fragment, fragment.getClass().getSimpleName());
                if (isAddToBackStack)
                    trans.addToBackStack(null);
                break;
            default:
                break;

        }


        trans.commit();
    }

}
